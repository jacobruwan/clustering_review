# For individual .java files to work, you need to:
* Install all packages used by the .java files.
* Adjust the CLASSPATH environment variable.

# List of packages to Install. Note, some are with other .java files
You need to place all the files into a general location. I.e
```
/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/lib/
```
and ensure that the relevant .jar file is added to the CLASSPATH variable

Start by changing directory to then try compiling TwoPFourC.java
```
cd ~/Dropbox/_semester_1_2018/MXB381/private_code/src/src/com/uob/mars/algorithms/clustering/hybrid

javac TwoPFourC.java
```

The code above will produce many errors. Now begin downloading all packages.

##org.apache.log4j.Logger
```
export CLASSPATH=/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/lib/apache-log4j-2.11.0-bin/log4j-1.2-api-2.11.0.jar

export CLASSPATH=${CLASSPATH}:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/lib/apache-log4j-2.11.0-bin/log4j-1.2-api-2.11.0.jar
```

##com.uob.mars.data.DataUtils
```
export CLASSPATH=${CLASSPATH}:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/private_code/src/src/
```

## For all downloaded packages, save the .jar files to the lib folder in the 'clustering_review' repository

```
/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/lib/

export CLASSPATH=/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/log4j-1.2-api-2.11.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/log4j-api-2.11.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/log4j-core-2.11.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/commons-math3-3.6.1.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/commons-codec-1.11.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/joda-time-2.9.9.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/jackson-all-1.9.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/opencsv-2.2.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/commons-cli-1.4.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/jts-core-1.15.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/lib/h2database/h2/src/tools/:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/lib/h2database/h2/src/main/:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/executables/2p4c/src/

# Or use the jar file for h2

export CLASSPATH=/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/log4j-1.2-api-2.11.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/log4j-api-2.11.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/log4j-core-2.11.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/commons-math3-3.6.1.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/commons-codec-1.11.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/joda-time-2.9.9.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/jackson-all-1.9.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/opencsv-2.2.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/commons-cli-1.4.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/jts-core-1.15.0.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/lib/h2-1.3.175.jar:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/clustering_review/executables/2p4c/src/:/c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/lib/h2database/h2/src/tools/
```

# Another file that needs to be compiled from the h2 git repository

```
javac /c/Users/Jacob/Dropbox/_semester_1_2018/MXB381/lib/h2database/h2/src/main/org/h2/tools/Server.java
```

## To run the code, you need to be in a certain directory and have the data file in a ./data/ subdirectory:

```
cd ~/Dropbox/_semester_1_2018/MXB381/clustering_review/executables/2p4c/src

# Data should be in executables/2p4c/src/data/clustering_data.csv
# So some of the h2 stuff comes from the /tools/ sub folder, some comes from the /main/ subfolder

java com/uob/mars/algorithms/clustering/hybrid/TwoPFourC -f clustering_data.csv --p 1e-5
```
