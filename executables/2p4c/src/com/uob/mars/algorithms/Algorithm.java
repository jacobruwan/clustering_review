package com.uob.mars.algorithms;

import com.uob.mars.algorithms.patterns.Result;


/**
 * 
 * @fileName	Algorithm.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

public interface Algorithm {
	
	
	/**
	 * Main method to run the algorithm
	 * @return
	 */
	public boolean run(/*MarsInfoOldOld marsInfo, double poissonThreshold*/);
	
	
	/**
	 * Main method to get the results
	 * @return
	 */
	public Result getResult();
	
	
	/**
	 * Get the arguments used in the algorithm. This is intended for storing in the database.
	 * @return
	 */
	//public Map<String, String> getArgs();
}
