package com.uob.mars.algorithms.clustering;

import com.uob.mars.algorithms.Algorithm;
import com.uob.mars.algorithms.patterns.Result;


/*
 * Abstract class for all clustering algorithms.
 */
public interface ClusteringAlgorithm extends Algorithm{
	
	
	@Override
	public boolean run(/*MarsInfoOldOld marsInfo, double poissonThreshold*/);
	
	
	@Override
	public Result getResult();
	
	
}
