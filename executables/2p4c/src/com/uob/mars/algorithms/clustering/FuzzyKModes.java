package com.uob.mars.algorithms.clustering;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.uob.mars.data.MarsInfo;



/**
 * 
 * Huang, Z and Ng, M.K. (1999) "A fuzzy k-modes algorithm for clustering categorical data", Fuzzy Systems, IEEE Transactions on, 7(4), pp. 446-452.
 *
 */
public class FuzzyKModes {
	
	
	private static final Logger log = Logger.getLogger(FuzzyKModes.class);
	
	
	private double weightingExponent = 2.0;
	
	/**
	 * Termination tolerance, referred to as epsilon symbol
	 */
	private double terminationTolerance = 0.00001;
	
	
	private MarsInfo marsInfo = null;
	
	
	/**
	 * Number of clusters
	 */
	private int k = 1;
	
	
	/**
	 * List of clusters. Size is k.
	 */
	private List<List<String>> Z = new ArrayList<List<String>>();
	
	
	public FuzzyKModes (MarsInfo marsInfo, int numberClusters, double weightingExponent) {
		
		this.marsInfo = marsInfo;
		
		this.weightingExponent = weightingExponent;
		
		// Ensure there are not more clusters to find than number of data elements.
		this.k = Math.min(marsInfo.getEntities().size(), numberClusters);
	}
	
	
	public void setInitialPrototypes(List<List<String>> prototypes) {
		Z = prototypes;
	}
	
	
	public boolean run() {
		
		log.info("FuzzyKModes: start");
		
		
		
		
		
		log.info("FuzzyKModes: end");
		return true;
	}
	
}
