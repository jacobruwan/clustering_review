package com.uob.mars.algorithms.clustering.hybrid.Interval;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeMap;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;


/**
 * 
 * @fileName	P3CComparators.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

/**
 * Provides comparator objects with compare() for comparing some data types in
 * P3Cmodded.
 */
public class P3CComparators{


	/**
	 *  Provides functionality for comparing P3CInterval<? extends Number>
	 *  objects.
	 */
	public static Comparator<P3CInterval<? extends Number>> comparatorP3CInterval =
			new Comparator<P3CInterval<? extends Number>>() {
		
		
		/**
		 * Only compares the keys and not the values. Hence the values are of
		 * type Number, so crisp
		 * 
		 * @param a the first P3CInterval to be compared
		 * @param b the second P3CInterval to be compared
		 * @return -1 if a is less than b
		 * 1 if a is greater than b
		 * 0 if a and b are equal
		 */
		public int compare(P3CInterval<? extends Number> a, P3CInterval<? extends Number> b) {
			
			NavigableSet<String> setA = a.navigableKeySet();
			Iterator<String> iterA = setA.iterator();
			
			NavigableSet<String> setB = b.navigableKeySet();
			Iterator<String> iterB = setB.iterator();
			
			String strA = null;
			String strB = null;
			int comparison = 0;
			
			while(iterA.hasNext() && iterB.hasNext()) {
				
				strA = iterA.next();
				strB = iterB.next();
				
				comparison = strA.compareToIgnoreCase(strB);
				
				// strB > strA
				if(comparison < 0) {
					return -1;
				// strA > strB
				} else if (comparison > 0) {
					return 1;
				}
				// Otherwise 0, so the same
			}
			
			if(!iterA.hasNext() && iterB.hasNext()) {
				return -1;
			}
			
			if(!iterB.hasNext() && iterA.hasNext()) {
				return 1;
			}
			
			return 0;
		}	
	};
	
	
	/**
	 * Used with the MarsInfo dataset structure. This structure uses the Short names of the full String names.
	 */
	public static Comparator<TreeMap<Short, P3CInterval<? extends Number>>> comparatorTreeMapOfComparatorP3CIntervalWithShortNames =
			new Comparator<TreeMap<Short, P3CInterval<? extends Number>>>() {
		
		
		/**
		 * @param a the first P3CInterval to be compared
		 * @param b the second P3CInterval to be compared
		 * @return a negative integer, zero, or a positive integer if the
		 * first argument is less than, equal to, or greater than the
		 * second, respectively.
		 */
		public int compare(TreeMap<Short, P3CInterval<? extends Number>> a,
				TreeMap<Short, P3CInterval<? extends Number>> b) {
			
			Iterator<Short> iterA = a.keySet().iterator();
			Iterator<Short> iterB = b.keySet().iterator();
			Short strA = null;
			Short strB = null;
			//int comparison = 0;
			
			while(iterA.hasNext() && iterB.hasNext()) {
				
				strA = iterA.next();
				strB = iterB.next();
				
				//comparison = strA.compareToIgnoreCase(strB);
				
				// strB > strA
				//if(comparison < 0) {
				if(strB > strA) {
					return -1;
				// strB < strA
				//} else if (comparison > 0) {
				} else if (strB < strA) {
					return 1;
				//} else if (comparison == 0) {
				} else if (strA == strB) {
					//int valueSimilarity = comparatorTree.compare(a.get(strA), b.get(strB));
					int valueSimilarity = comparatorP3CInterval.compare(a.get(strA), b.get(strB));
					
					if(valueSimilarity == 0) {
						// do nothing, and check the next iteration
					} else {
						return valueSimilarity;
					}
				}
				// Otherwise, 0, so the same
			}
			
			if(!iterA.hasNext() && iterB.hasNext()) {
				return -1;
			}
			
			if(!iterB.hasNext() && iterA.hasNext()) {
				return 1;
			}
			
			return 0;
		}
	};
	
	
	/**
	 * Used when interfacing with the database. The database contains full String names.
	 */
	public static Comparator<TreeMap<String, P3CInterval<? extends Number>>> comparatorTreeMapOfComparatorP3CIntervalWithStringNames =
			new Comparator<TreeMap<String, P3CInterval<? extends Number>>>() {
		
		
		/**
		 * @param a the first P3CInterval to be compared
		 * @param b the second P3CInterval to be compared
		 * @return a negative integer, zero, or a positive integer if the
		 * first argument is less than, equal to, or greater than the
		 * second, respectively.
		 */
		public int compare(TreeMap<String, P3CInterval<? extends Number>> a,
				TreeMap<String, P3CInterval<? extends Number>> b) {
			
			Iterator<String> iterA = a.keySet().iterator();
			Iterator<String> iterB = b.keySet().iterator();
			String strA = null;
			String strB = null;
			int comparison = 0;
			
			while(iterA.hasNext() && iterB.hasNext()) {
				
				strA = iterA.next();
				strB = iterB.next();
				
				comparison = strA.compareToIgnoreCase(strB);
				
				// strB > strA
				if(comparison < 0) {
				//if(strB > strA) {
					return -1;
				// strB < strA
				} else if (comparison > 0) {
				//} else if (strB < strA) {
					return 1;
				} else if (comparison == 0) {
				//} else if (strA == strB) {
					//int valueSimilarity = comparatorTree.compare(a.get(strA), b.get(strB));
					int valueSimilarity = comparatorP3CInterval.compare(a.get(strA), b.get(strB));
					
					if(valueSimilarity == 0) {
						// do nothing, and check the next iteration
					} else {
						return valueSimilarity;
					}
				}
				// Otherwise, 0, so the same
			}
			
			if(!iterA.hasNext() && iterB.hasNext()) {
				return -1;
			}
			
			if(!iterB.hasNext() && iterA.hasNext()) {
				return 1;
			}
			
			return 0;
		}
	};
}
