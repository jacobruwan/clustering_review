package com.uob.mars.algorithms.clustering.hybrid.Interval;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CIntervalInterface;


/**
 * 
 * @fileName	P3CInterval.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */


//@JsonIgnoreType
public abstract class P3CInterval<T> implements P3CIntervalInterface<T> {
	
	
	/**
	 * Required for serialisation
	 */
	private static final long serialVersionUID = 1L;
	
	
	protected TreeMap<String, T> interval;
	
	
	@Override
	public TreeMap<String, T> getInterval() {
		return interval;
	}
	
	
	@Override
	public void setInterval (TreeMap<String, T> interval) {
		this.interval = interval;
	}
	
	
	public T get(String str) {
		
		return interval.get(str);
	}
	
	
	/**
	 * @return
	 */
	//@JsonIgnore
	public boolean isEmpty() {
		
		return interval.isEmpty();
	}
	
	
	public int size() {
		
		return interval.size();
	}
	
	
	/**
	 * 
	 */
	public NavigableSet<String> navigableKeySet() {
		
		return interval.navigableKeySet();
	}
	
	
	/**
	 * 
	 */
	@Override
	public Iterator<String> iterator() {
		return interval.keySet().iterator();
	}
	
	
	public boolean containsKey(String tag) {
		
		return interval.containsKey(tag);
	}
	
	
	public String firstKey() {
		return interval.firstKey();
	}
	
	
	public String lastKey() {
		return interval.lastKey();
	}
	
	
	public String toString() {
		return interval.toString();
	}
	
	
	public Set<String> keySet() {
		return interval.keySet();
	}
	
	
	@Override
	public int hashCode() {
		return interval.hashCode();
	}
}
