package com.uob.mars.algorithms.clustering.hybrid.Interval;

import java.util.Iterator;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CIntervalCrisp;


/**
 * 
 * @fileName	P3CIntervalCrisp.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

/**
 * An interval of categorical data that has crisp membership.
 */
public class P3CIntervalCrisp extends P3CInterval<Integer> {
	
	
	private static final Logger log = Logger.getLogger(P3CIntervalCrisp.class);
	
	
	// Increment when a change will affect serialisation
	private static final long serialVersionUID = 1L;
	
	
	public P3CIntervalCrisp() {
		interval = new TreeMap<String, Integer>();
	}
	
	
	@Override
	public Object put(String index, Object e) {
		return interval.put(index, (Integer)e);
	}
	
	
	/**
	 * Add a value/bin to the interval with default crisp membership, i.e., 1.
	 * 
	 * @param tag Value/bin
	 */
	//TODO change return type to match that of put()?
	public boolean add(String index) {
		return this.add(index, new Integer(1));
	}
	
	
	/**
	 * Add a value/bin to the interval with specified crisp membership.
	 * 
	 * @param tag Value/bin
	 * @param membership Specified membership value
	 * @return
	 */
	public boolean add(String index, Integer membership) {
		interval.put(index, new Integer(membership));
		return true;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(obj == this) {
			return true;
		}
		if(!(obj instanceof P3CIntervalCrisp)) {
			return false;
		}
		
		P3CIntervalCrisp P3Cobj = (P3CIntervalCrisp) obj;
		
		if(this.interval.keySet().equals(P3Cobj.keySet())) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.bt.mars.algorithms.clustering.hybrid.P3CIntervalInterface#deepCopy()
	 */
	@Override
	public P3CInterval<Integer> deepCopy() {
		
		P3CIntervalCrisp tempSignatureA = new P3CIntervalCrisp();
		Iterator<String> tempSignatureAIter = this.interval.keySet().iterator();
		
		while(tempSignatureAIter.hasNext()) {
			String index = tempSignatureAIter.next();
			tempSignatureA.put(index, interval.get(index));
		}
		
		return tempSignatureA;
	}
	
	
	@Override
	public void append(P3CInterval<? extends Number> interval) {
		
		Iterator<String> tempSignatureAIter = interval.iterator();
		
		while(tempSignatureAIter.hasNext()) {
			String index = tempSignatureAIter.next();
			this.interval.put(index, (Integer)interval.get(index));
		}
	}
}
