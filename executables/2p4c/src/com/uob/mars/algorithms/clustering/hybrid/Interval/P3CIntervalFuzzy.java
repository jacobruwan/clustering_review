package com.uob.mars.algorithms.clustering.hybrid.Interval;

import java.util.Iterator;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CIntervalFuzzy;


/**
 * 
 * @fileName	P3CIntervalFuzzy.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

/**
 * An interval of categorical data that has fuzzy membership.
 */
public class P3CIntervalFuzzy extends P3CInterval<Double> {
	
	
	private static final Logger log = Logger.getLogger(P3CIntervalFuzzy.class);
	
	
	// Increment when a change will affect serialisation
	private static final long serialVersionUID = 1L;
	
	
	public P3CIntervalFuzzy() {
		interval = new TreeMap<String, Double>();
	}
	
	
	@Override
	public Object put(String tag, Object e) {
		return interval.put(tag, (Double)e);
	}
	
	
	/**
	 * Add a value/bin to the interval with default fuzzy membership, i.e., 1.0.
	 * 
	 * @param tag Value/bin
	 */
	//TODO change return type to match that of put()?
	public boolean add(String tag) {
		return this.add(tag, new Double(1.0));
	}
	
	
	/**
	 * Add a value/bin to the interval with specified fuzzy membership.
	 * 
	 * @param tag Value/bin
	 * @param membership Specified membership value
	 * @return
	 */
	public boolean add(String tag, Double membership) {
		interval.put(tag, new Double(membership));
		return true;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(obj == this) {
			return true;
		}
		if(!(obj instanceof P3CIntervalFuzzy)) {
			return false;
		}
		
		P3CIntervalFuzzy P3Cobj = (P3CIntervalFuzzy) obj;
		
		if(this.interval.keySet().equals(P3Cobj.keySet())) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.bt.mars.algorithms.clustering.hybrid.P3CIntervalInterface#deepCopy()
	 */
	@Override
	public P3CInterval<Double> deepCopy() {
		
		P3CIntervalFuzzy tempSignatureA = new P3CIntervalFuzzy();
		Iterator<String> tempSignatureAIter = this.interval.keySet().iterator();
		
		while(tempSignatureAIter.hasNext()) {
			String key = tempSignatureAIter.next();
			tempSignatureA.put(key, interval.get(key));
		}
		
		return tempSignatureA;
	}
	
	
	@Override
	public void append(P3CInterval<? extends Number> interval) {
		
		Iterator<String> tempSignatureAIter = interval.iterator();
		
		while(tempSignatureAIter.hasNext()) {
			String key = tempSignatureAIter.next();
			this.interval.put(key, (Double)interval.get(key));
		}
	}
}
