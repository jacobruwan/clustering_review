package com.uob.mars.algorithms.clustering.hybrid.Interval;

import java.io.Serializable;
import java.util.TreeMap;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;


/**
 * 
 * @fileName	P3CIntervalInterface.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */


/**
 * <p>
 * An interval of categorical data that has membership of type <T>.
 * </p>
 * <p>
 * This could be an abstract class, so that some fields and methods are inherited.
 * Chose not to, just for simplicity.
 * </p>
 * TODO Remove Serialisation and serialVersionUID in implementation classes
 */
public interface P3CIntervalInterface<T> extends Iterable<String>, Serializable {
	
	
	public TreeMap<String, T> getInterval();
	
	
	public void setInterval (TreeMap<String, T> interval);
	
	
	/**
	 * 
	 * @param tag
	 * @param item
	 * @return
	 */
	public Object put(String tag, Object item);
	
	
	public boolean add(String tag);
	
	
	@Override
	public boolean equals(Object obj);
	
	
	@Override
	public int hashCode();
	
	
	public P3CInterval<? extends Number> deepCopy();
	
	
	public void append(P3CInterval<? extends Number> interval);
}
