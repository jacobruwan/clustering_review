package com.uob.mars.algorithms.clustering.hybrid;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.log4j.Logger;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CComparators;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;
import com.uob.mars.algorithms.patterns.Result;
import com.uob.mars.data.DataUtils;
import com.uob.mars.data.MarsInfo;
import com.uob.mars.db.PatternsDatabase;
import com.uob.mars.db.SubspaceClustersEntry;
import com.uob.mars.db.SubspaceClustersParamConfigsEntry;
import com.uob.mars.maths.distributions.Poisson;
import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.util.PrimitiveDataTypes;
import com.uob.mars.data.MarsInfoEntity;
import com.uob.mars.algorithms.clustering.ClusteringAlgorithm;
import com.uob.mars.algorithms.clustering.hybrid.P3C;
import com.uob.mars.algorithms.clustering.hybrid.P3CInterface;
import com.uob.mars.algorithms.graphs.Graphs;


/**
 * 
 * @fileName	P3C.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

/**
 * <p>
 * Provides the P3C (Projected Clustering via Cluster Cores) algorithm, an
 * algorithm to find projected clusters in high dimensional spaces, which also
 * has the option to find subspace clusters.
 * </p>
 *
 * <p>
 * Requires saturn.config to set CommaSeparatedTags=false. This ensures commas
 * within tags do not produce multiple tags. For example, setting to false
 * ensures that "KOREA, REPUBILC OF" is not split into two tags for country.
 * </p>
 * 
 * <p>
 * It is recommended to read the paper "Robust projected clustering" first
 * before attempting to understand the code. Knowledge of the key concepts and
 * terms used is required.
 * </p>
 * 
 * <p>
 * For viewing H2 database:
 * http://theopentutorials.com/tutorials/eclipse/dtp/eclipse-dtp-configure-h2-datasource-using-data-source-explorer/
 * </p>
 * 
 * <p>
 * Reference: <br>
 * Moise, Gabriela, J�rg Sander, and Martin Ester,
 * "Robust projected clustering"
 * Knowledge and Information Systems 14(3), 2008, pp. 273-298.
 * http://dx.doi.org/10.1109/ICDM.2006.123
 * </p>
 * 
 * <p>
 * YouTube video from GoogleTechTalks:
 * https://www.youtube.com/watch?v=GE-2FAWy3Bw
 * </p>
 * 
 * <p>
 * Note, the following reference is very good for understanding Apriori:
 * http://www-users.cs.umn.edu/~kumar/dmbook/ch6.pdf
 * </p>
 * TODO Test when critical value is zero (occurs when dof is high > ~100?)
 * TODO Implement ability to handle numeric datasets in P3C
 */
public abstract class P3C<T> implements ClusteringAlgorithm, P3CInterface<T> {
	
	
	private static final Logger log = Logger.getLogger(P3C.class);
	
	
	protected MarsInfo marsInfo;
	
	
	/**
	 * 1e-20 is a low error probability according to the paper 
	 */
	protected double poissonThreshold = 1e-20;
	
	
	/**
	 * Poisson threshold used for interval generation. Always the same as 
	 * poissonThreshold unless user specifies a different value.
	 */
	protected double poissonThresholdIntGen = poissonThreshold;
	
	
	/**
	 * Minimum limit that Poisson must not go below.
	 */
	protected double minPoissonThreshold = 1e-100;
	
	
	/**
	 * Maximum limit that Poisson must not go above.
	 */
	protected double maxPoissonThreshold = 1e-1;
	
	
	/**
	 * Confidence level (aka alpha) for Chi-square test when discretising
	 * categorical/nominal attributes.
	 */
	protected double confidenceLevel = 0.001;
	
	
	/**
	 * The number of generate-and-test candidates required for creating
	 * intervals can be large. If all candidates calculated at once then
	 * it is to run out of memory. When this parameter > 0 the candidates
	 * are generated and tested in given batch sizes.
	 */
	protected int batchCandidates = 0;
	
	
	/**
	 * Initially, this stores all bins. A bin is an attribute value, such as
	 * 192.168.0.1, UK, or 21. A bin is 'marked' when it satisfies a statistical
	 * test and is moved into {@link P3C#markedBins}
	 * 
	 * The first String, in the HashMap, refers to the attribute name, such as
	 * IP source.
	 * 
	 * The second String, in the Map, refers to the attribute value, such as
	 * 192.168.0.1
	 * 
	 * The List of Integers holds the transaction IDs
	 */
	//protected HashMap<String, Map<String, P3CSupportInterface>> unmarkedBins = null;
	
	
	/**
	 * Stores all bins that are determined to be non-uniform by the Chi-square
	 * statistical test. They are 'marked' and stored here.
	 * 
	 * A bin is an attribute value, such as 192.168.0.1, UK, or 21.
	 * 
	 * The String, in the TreeSet, refers to the attribute name and value, such
	 * as "src IP: 192.168.0.1"
	 * 
	 * The List of Integers holds the transaction IDs
	 */
	protected TreeMap<P3CInterval<? extends Number>, P3CSupportInterface> markedBins = null;
	
	
	/**
	 * Stores cardinality of bins in each attribute from markedBins.
	 * 
	 * For example, key is attribute name, such as "IP src", and value is the
	 * number of bins for that attribute.
	 */
	protected TreeMap<Short, Integer> markedBinsAttributeCardinality = null;
	
	
	/**
	 * A cluster core consists of a p-signature and its support set. Each item
	 * in this list consists of a collection of signatures of size p, i.e.,
	 * index 0 contains 1-signatures, index 1 contains 2-signatures, index 2
	 * contains 3-signatures, and so on.
	 */
	protected List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> clusterCores = null;
	
	
	/**
	 * 
	 */
	protected boolean supportAsList;
	
	
	/**
	 * Main constructor
	 */
	public P3C() {
		
		markedBins = new TreeMap<P3CInterval<? extends Number>, P3CSupportInterface> (P3CComparators.comparatorP3CInterval);
		
		markedBinsAttributeCardinality = new TreeMap<Short, Integer>();
		
		supportAsList = false;
	}
	
	
	/**
	 * @param marsInfo Contains dataset and metadata
	 * @param possionThreshold The error probability that can be accepted when 
	 * creating intervals of categorical data. See Section 3.1.2 of the
	 * referenced paper.
	 */
	//@Deprecated
	public P3C (MarsInfo marsInfo, double poissonThreshold, double poissonThresholdIntGen, int batchCandidates) {
		
		// Call default constructor to setup data structures.
		this();
		
		if(poissonThreshold > maxPoissonThreshold) {
			log.warn("High value of possionThreshold might not be useful, try a value <= " + maxPoissonThreshold);
		}
		if(poissonThresholdIntGen > maxPoissonThreshold) {
			log.warn("High value of poissonThresholdIntGen might not be useful, try a value <= " + maxPoissonThreshold);
		}
		
		if(poissonThreshold < minPoissonThreshold) {
			log.warn("possionThreshold seems very low, perhaps try a value >= " + minPoissonThreshold);
		}
		if(poissonThresholdIntGen < minPoissonThreshold) {
			log.warn("poissonThresholdIntGen seems very low, perhaps try a value >= " + minPoissonThreshold);
		}
		
		if(batchCandidates < 1) {
			log.warn("batch number cannot be less than 1");
		}
		
		/*if(batchCandidates > marsInfo.getEntities().size()) {
			batchCandidates = marsInfo.getEntities().size();
			log.info("batch-candidates exceeds number of datasets records, so " +
					"batch-candidates decreased to " + marsInfo.getEntities().size());
		}*/
		
		this.marsInfo = marsInfo;
		this.poissonThreshold = poissonThreshold;
		this.poissonThresholdIntGen = poissonThresholdIntGen;
		this.batchCandidates = batchCandidates;
		
		log.info("index\tvariable");
		for(int i = 0; i < this.marsInfo.getAttributes().size(); i++) {
			log.info(i + "\t" + this.marsInfo.getAttributes().get(i));
		}
	}
	
	
	/**
	 * Runs the algorithm.
	 * 
	 * @param marsInfo Contains dataset and metadata
	 * @param possionThreshold The error probability that can be accepted when
	 * 
	 * @seecom.uob.mars.algorithms.clusteringng.ClusteringAlgorithm#run()
	 */
	@Override
	public boolean run () {
		
		/*if(poissonThreshold > maxPoissonThreshold) {
			log.warn("High value of possionThreshold might not be useful, try a value <= " + maxPoissonThreshold);
		}
		
		if(poissonThreshold < minPoissonThreshold) {
			log.warn("possionThreshold seems very low, perhaps try a value >= " + minPoissonThreshold);
		}
		
		this.marsInfo = marsInfo;
		this.poissonThreshold = poissonThreshold;
		*/
		// Stores candidate signatures that need to be tested before they
		// become signatures and are kept
		TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidateSignatures = //null;
				new TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>(P3CComparators.comparatorTreeMapOfComparatorP3CIntervalWithShortNames);
		
		// L_k in Apriori.
		// index mapping is +1, i.e., 0->k=1, 1->k=2, and so on
		List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> frequentKSignatures =
				new ArrayList<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>>();
		
		logExecutionStart();
		
		//////////////////////////////////////////////////////////////////////
		//1. Discretise each attribute into bins (Section 3.1 of reference).
		//////////////////////////////////////////////////////////////////////
		//createBins();
		createBinsOneByOne();
		System.out.println(this.markedBins);
		TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> intervals = createIntervals();
		System.out.println("intervals = " + intervals);
		
		calculateMarkedBinCardinalities();
		
		// Generate and test 1-signatures
		test1ItemsSigs(candidateSignatures, intervals);
		
		// Add 1-signatures to index 0
		frequentKSignatures.add(candidateSignatures);
		
		//////////////////////////////////////////////////////////////////////
		//3. Aggregate the intervals into cluster cores (Section 3.2 of
		//   reference).
		//////////////////////////////////////////////////////////////////////
		int k = 2;
		
		System.out.println(frequentKSignatures.get(k - 2));
		
		// Generate k-signatures, note k is now +1
		// Note: index 0 is 1-signatures, index 1 is 2-signatures, and so on
		candidateSignatures =
				generateKSigsCandsCatInts(frequentKSignatures.get(k - 2), k);
		
		
		while(candidateSignatures.size() > 0) {
			log.info("Processing: " + k +"-signatures");
			log.info("Number of candidates = " + candidateSignatures.size());
			
			long startTimeSupportCounting = System.nanoTime();
			
			log.info("Counting support of candidates");
			countSupport(candidateSignatures, k);
			System.out.println(candidateSignatures);
			
			log.info("Testing candidates");
			// Test k-signatures
			testKItemsCands(candidateSignatures, frequentKSignatures.get(k - 2), k);
			
			// After first iteration, remove because P3C does not have 1D clusters
			if(k == 2) { 
				frequentKSignatures.set(0, new TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>(P3CComparators.comparatorTreeMapOfComparatorP3CIntervalWithShortNames));
			}
			
			double endTimeSupportCounting =
					(System.nanoTime() - startTimeSupportCounting) / 1000000000.0;
			
			log.info("Execution time of support counting (seconds) = " + endTimeSupportCounting);
			log.info("Number of candidates after support counting and pruning = " + candidateSignatures.size());
			
			frequentKSignatures.add(candidateSignatures);
			k++;
			
			System.out.println("frequentKSignatures before removing nested structures");
			System.out.println(frequentKSignatures);
			
			log.info("Removing nested signatures");
			removeNestedSignatures(frequentKSignatures.get(k - 3), frequentKSignatures.get(k - 2));
			
			log.info("Generating candidates");
			// Generate k-signatures, note k is now +1
			// Note: index 0 is 1-signatures, index 1 is 2-signatures, and so on
			candidateSignatures =
					generateKSigsCandsCatInts(frequentKSignatures.get(k - 2), k);
			//if(k == 5) {break;}
		}
		
		//System.out.println("frequentKSignatures");
		//System.out.println(frequentKSignatures);
		
		log.info("Computing maximal signatures");
		clusterCores = identifyMaximalSignatures(frequentKSignatures);
		System.out.println(clusterCores);
		
		//////////////////////////////////////////////////////////////////////
		//4. Refine cluster cores into projected clusters, compute outliers,
		//   and detect clusters' relevant attributes (Section 3.3).
		//////////////////////////////////////////////////////////////////////
		log.info("Computing projected clusters (similarity matrix)");
		// Cluster cores are projected onto data
		TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, List<Double>> similarityMatrix =
				computeProjectedClusters(clusterCores);
		
		// Data points with similarity 0 to all cluster cores are outliers
		List<MarsInfoEntity> entitiesOutliers =
				computeOutliers(this.marsInfo.getEntities(),
						similarityMatrix);
		
		log.info("Number of outliers: " + entitiesOutliers.size());
		
		logExecutionEnd();
		
		return true;
	}
	
	
	/**
	 * Create intervals from marked bins.
	 * 
	 * TODO Use indexes for intervals
	 * 
	 * @return
	 */
	private void createBins() {
		
		// For iterating over attributes
		Iterator<Entry<Short, Map<String, P3CSupportInterface>>> iterAttr = null;
		
		// For iterating over bins in attributes
		Iterator<Map.Entry<String, P3CSupportInterface>> iterAttrBin = null;
		
		//log.info("Dataset type: " + marsInfo.getDatasetType().toString());
		
		/*
		switch(marsInfo.getDatasetType()) {
		case EMPTY:
			log.warn("Unable to discretise attributes");
			break;
		case QUANTITATIVE:
			// Divided into same number off equi-sized bins.
			// Sturge's rule is used
			//
			
			break;
		case NOMINAL:
			// A bin for each value, so no need to do anything here
			break;
		default:
			log.error("Dataset type is not recognised");
		}
		 */
		
		/*
		 * Next step is to "identify attributes with uniform distribution, and,
		 * for the non-uniform attributes, to identify intervals with unusual
		 * high support".
		 */
		
		
		
		
		/*
		 * COUNT SUPPORT OF ALL UNMARKED BINS
		 */
		log.info("Counting support of all unmarked bins");
		
		/* Maximum load factor ensures automatic resizing occurs only
		 * when full capacity is reached.
		 * Key = attribute id
		 * Value = Map with key attribute bin and support count
		 */
		HashMap<Short, Map<String, P3CSupportInterface>> unmarkedBins =
				new HashMap<Short, Map<String, P3CSupportInterface>>();
		
		/* For each attribute, create a mapping from attribute name to
		 * another map (of attribute bins and supports).
		 */
		/*Iterator<Map.Entry<String, Type>> iteratorTags =
				marsInfo.getTagTypes().entrySet().iterator();
		
		while (iteratorTags.hasNext()) {
			
			Map.Entry<String, Type> pairs =
					iteratorTags.next();
			
			//Key is attribute name (e.g., IP address, country etc.) 
			unmarkedBins.put((String)pairs.getKey(), new HashMap<String, P3CSupportInterface>());
		}*/
		
		Integer numberOfAttributes = marsInfo.getAttributes().size();
		
		for(short i = 0; i < numberOfAttributes; i++) {
			
			unmarkedBins.put(new Short(i), new HashMap<String, P3CSupportInterface>());
		}
		
		//int recordNumber = 0;
		
		// Loop over every record and count support of attribute bins
		for(MarsInfoEntity entity : marsInfo.getEntities()) {
			
			//List<String> tags = entity.getTags();
			List<Short> indexes = entity.getIndexes();
			
			for(Short index : indexes) {
				
				//String[] tagSplit = tag.split(":");
				
				// Initialised inside the next conditional statement
				P3CSupportInterface support;
				
				// If support object exist already, then increment
				if(unmarkedBins.get(index).containsKey(entity.getTag(index))) {
					
					// Existing attribute bin, so support is increased
					support = unmarkedBins.get(index).get(entity.getTag(index));
				}
				// Otherwise, create new support object
				else {
					
					// New attribute bin, so support starts at 1
					support = newSupport(this.supportAsList);
				}
				
				// Add support, and update
				support.addSupport(/*new Integer(recordNumber)*/);
				unmarkedBins.get(index).put(entity.getTag(index), support);
			}
			
			//recordNumber++;
		}
		
		
		/*
		 * IDENTIFY BINS THAT DO NOT FIT NORMAL DISTRIBUTION
		 * AND LABEL THESE AS MARKED BINS
		 */
		log.info("Looking for bins that do not fit normal distribution");
		
		iterAttr = unmarkedBins.entrySet().iterator();
		
		while (iterAttr.hasNext()) { // For each attribute
			
			Map.Entry<Short, Map<String, P3CSupportInterface>> attr =
					iterAttr.next();
			
			do {
				/*
				 * Chi-Square (Goodness-of-Fit) Test
				 * Ref: http://www.itl.nist.gov/div898/handbook/eda/section3/eda35f.htm
				 * Help: http://www.aaec.ttu.edu/faculty/eelam/3401/CourseMaterials/Notes_Fall07/Notes_Chi-Square.pdf
				 * 
				 * H_0 (null hypothesis): Attribute is uniform
				 * H (hypothesis): Attribute is non uniform 
				 * 
				 * Significance level:	alpha = 0.001 (high; defined in P3C paper)
				 * Degrees of freedom:	dof
				 * Test statistic:		X^2
				 * Critical value:		X^2_(1-alpha,dof)
				 * Critical region:		Reject H_0 if X^2 > X^2_(1-alpha,dof)
				 */
				double avgBinSupp = 0.0;
				double chiSquare = 0.0;
				int dof = 0;
				ChiSquaredDistribution chiSquDist;
				
				iterAttrBin = attr.getValue().entrySet().iterator();
				
				// For each attribute bin,
				// calculate the average over all bins of an attribute
				while (iterAttrBin.hasNext()) {
					avgBinSupp += (double)iterAttrBin.next().getValue().getSupport();
				}
				avgBinSupp /= (double)attr.getValue().entrySet().size();
				
				// Reset iterator
				iterAttrBin = attr.getValue().entrySet().iterator();
				
				// Calculate squared difference between bin support and
				// average bin support
				while (iterAttrBin.hasNext()) {
					double binSupp = (double)iterAttrBin.next().getValue().getSupport();
					chiSquare +=
							(binSupp - avgBinSupp)
							* (binSupp - avgBinSupp);
				}
				chiSquare /= avgBinSupp; // Normalise
				
				// Number of levels of the independent variable
				// (when more than 2 variables it is number of classes)
				// dof = n-1
				dof = attr.getValue().entrySet().size() - 1;
				
				if (dof > 0) {
					
					chiSquDist= new ChiSquaredDistribution(dof);
					
					/*
					log.info("avgBinSup = " + avgBinSupp);
					log.info("chiSquared = " + chiSquare);
					log.info("dof = " + dof);
					log.info("critical value = " +
							chiSquDist.density(confidenceLevel));
					*/
					
					/* Reject the null hypothesis of uniform if Test statistic
					 * is greater than critical value. Hence, attribute is non
					 * uniform. It is non-uniform attributes that contribute
					 * towards subspace projections.
					 * 
					 * Reject H_0 if X^2 > X^2_(1-alpha,dof)
					 */
					double chiSquareTestValue = chiSquDist.density(confidenceLevel);
					if (chiSquare > chiSquareTestValue) {
						
						int maxBinSupp = 0;
						String maxBinKeyValue = "";
						P3CSupportInterface maxBinSuppSet = null;
						
						// Find bin with largest support and remove it
						iterAttrBin = attr.getValue().entrySet().iterator();
						while (iterAttrBin.hasNext()) {
							
							Entry<String, P3CSupportInterface> entry = iterAttrBin.next();
							
							if (entry.getValue().getSupport() > maxBinSupp) {
								maxBinSuppSet = entry.getValue();
								maxBinSupp = maxBinSuppSet.getSupport();
								maxBinKeyValue = entry.getKey();
							}
						}
						
						/* 
						 * - maxBinKey becomes a marked bin
						 * - Remove marked bin from allBins, because we continue
						 * to iterate over unmarked bins that are (hypothesis
						 * to be) normal.
						 * - Add marked bin to a new structure, so we can use in
						 * Stage 2 (compute intervals). 
						 */
						
						// Add bin
						markedBins.put(newInterval(attr.getKey() + ":" + maxBinKeyValue), maxBinSuppSet);
						
						attr.getValue().remove(maxBinKeyValue);
					}
					// Accept the null hypothesis of uniform
					else {
						/* This is our termination criterion: once the null
						 * hypothesis is accepted, we stop.
						 * Break out of infinite loop
						 */
						break;
					}
				} else {
					// dof of 0 means there is only 1 value for this attribute,
					// hence it must be normal, so we do nothing
					break;
				}
			} while (true);
		}
	}
	
	
	/**
	 * Method suitable for less RAM use
	 */
	private void createBinsOneByOne() {
		
		// Stores dataset
		List<MarsInfoEntity> entities;
		
		// For iterating over attributes
		//Iterator<Entry<String, P3CSupportInterface>> iterAttr = null;
		
		// For iterating over bins in attributes
		Iterator<Map.Entry<String, P3CSupportInterface>> iterAttrBin = null;
		
		//log.info("Dataset type: " + marsInfo.getDatasetType().toString());
		
		/*
		switch(marsInfo.getDatasetType()) {
		case EMPTY:
			log.warn("Unable to discretise attributes");
			break;
		case QUANTITATIVE:
			// Divided into same number off equi-sized bins.
			// Sturge's rule is used
			//
			
			break;
		case NOMINAL:
			// A bin for each value, so no need to do anything here
			break;
		default:
			log.error("Dataset type is not recognised");
		}
		 */
		
		/*
		 * Next step is to "identify attributes with uniform distribution, and,
		 * for the non-uniform attributes, to identify intervals with unusual
		 * high support".
		 */
		
		
		
		
		/*
		 * COUNT SUPPORT OF ALL UNMARKED BINS
		 */
		log.info("Creating bins");
		
		
		
		/* For each attribute, create a mapping from attribute name to
		 * another map (of attribute bins and supports).
		 */
		//Iterator<Map.Entry<String, Type>> iteratorTags =
		//		marsInfo.getTagTypes().entrySet().iterator();
		
		Integer numberOfAttributes = marsInfo.getAttributes().size();
		
		for(short i = 0; i < numberOfAttributes; i++) {
		
		//while (iteratorTags.hasNext()) {
			
			/* Maximum load factor ensures automatic resizing occurs only
			 * when full capacity is reached.
			 * Key = attribute name
			 * Value = Map with key attribute bin and support count
			 */
			HashMap<String, P3CSupportInterface> unmarkedBins =
					new HashMap<String, P3CSupportInterface>();
			
			//Map.Entry<String, Type> pairs =
			//		iteratorTags.next();
			
			log.info("Creating bins from " + marsInfo.getAttributes().get(i));
			
			//Key is attribute name (e.g., IP address, country etc.) 
			//unmarkedBins.put((String)pairs.getKey(), new HashMap<String, P3CSupportInterface>());
		//}
		
			// Get data records (aka entities)
			entities = marsInfo.getEntities();
			
			//int recordNumber = 0;
			
			// Loop over every record and count support of attribute bins
			for(MarsInfoEntity entity : entities) {
				
				List<Short> indexes = entity.getIndexes();
				
				for(Short index : indexes) {
				
				//List<String> tags = entity.getTags();
				
				//for(String tag : tags) {
					
					//String[] tagSplit = tag.split(":");
					
					// Initialised inside the next conditional statement
					P3CSupportInterface support;
					
					//if(tagSplit[0].equals(marsInfo.getSaturnInfo().getAttributes().get(i))) {
					if (index == i) {
						if(unmarkedBins.containsKey(entity.getTag(index))) {
							support = unmarkedBins.get(entity.getTag(index));
						} else {
							support = newSupport(this.supportAsList);
						}
						
						// If support object exist already, then increment
						/*if(unmarkedBins.get(tagSplit[0]).containsKey(tagSplit[1])) {
							
							// Existing attribute bin, so support is increased
							support = unmarkedBins.get(tagSplit[0]).get(tagSplit[1]);
						}
						// Otherwise, create new support object
						else {
							
							// New attribute bin, so support starts at 1
							support = newSupport(this.supportAsList);
						}*/
						
						// Add support, and update
						support.addSupport(/*new Integer(recordNumber)*/);
						unmarkedBins.put(entity.getTag(index), support);
						
						// exit loop when we find the attribute.
						break;
					}
				}
				//recordNumber++;
			}
			
			
			/*
			 * IDENTIFY BINS THAT DO NOT FIT NORMAL DISTRIBUTION
			 * AND LABEL THESE AS MARKED BINS
			 */
			log.info("Looking for bins from " + marsInfo.getAttributes().get(i)
					+ " that do not fit normal distribution");
			
			//iterAttr = unmarkedBins.entrySet().iterator();
			
			//while (iterAttr.hasNext()) { // For each attribute
				
				//Entry<String, P3CSupportInterface> attr =
						//iterAttr.next();
				do {
					/*
					 * Chi-Square (Goodness-of-Fit) Test
					 * Ref: http://www.itl.nist.gov/div898/handbook/eda/section3/eda35f.htm
					 * Help: http://www.aaec.ttu.edu/faculty/eelam/3401/CourseMaterials/Notes_Fall07/Notes_Chi-Square.pdf
					 * 
					 * H_0 (null hypothesis): Attribute is uniform
					 * H (hypothesis): Attribute is non uniform 
					 * 
					 * Significance level:	alpha = 0.001 (high; defined in P3C paper)
					 * Degrees of freedom:	dof
					 * Test statistic:		X^2
					 * Critical value:		X^2_(1-alpha,dof)
					 * Critical region:		Reject H_0 if X^2 > X^2_(1-alpha,dof)
					 */
					double avgBinSupp = 0.0;
					double chiSquare = 0.0;
					int dof = 0;
					ChiSquaredDistribution chiSquDist;
					
					iterAttrBin = unmarkedBins.entrySet().iterator();
					
					// For each attribute bin,
					// calculate the average over all bins of an attribute
					while (iterAttrBin.hasNext()) {
						avgBinSupp += (double)iterAttrBin.next().getValue().getSupport();
					}
					avgBinSupp /= (double)unmarkedBins.entrySet().size();
					
					// Reset iterator
					iterAttrBin = unmarkedBins.entrySet().iterator();
					
					// Calculate squared difference between bin support and
					// average bin support
					while (iterAttrBin.hasNext()) {
						double binSupp = (double)iterAttrBin.next().getValue().getSupport();
						chiSquare +=
								(binSupp - avgBinSupp)
								* (binSupp - avgBinSupp);
					}
					chiSquare /= avgBinSupp; // Normalise
					
					// Number of levels of the independent variable
					// (when more than 2 variables it is number of classes)
					dof = unmarkedBins.entrySet().size() - 1;
					
					if (dof > 0) {
						
						chiSquDist= new ChiSquaredDistribution(dof);
						
						/*
						log.info("avgBinSup = " + avgBinSupp);
						log.info("chiSquared = " + chiSquare);
						log.info("dof = " + dof);
						log.info("critical value = " +
								chiSquDist.density(confidenceLevel));
						*/
						
						/* Reject the null hypothesis of uniform if Test statistic
						 * is greater than critical value. Hence, attribute is non
						 * uniform. It is non-uniform attributes that contribute
						 * towards subspace projections.
						 * 
						 * Reject H_0 if X^2 > X^2_(1-alpha,dof)
						 */
						double chiSquareTestValue = chiSquDist.density(confidenceLevel);
						
						if (chiSquare > chiSquareTestValue) {
							
							int maxBinSupp = 0;
							String maxBinKeyValue = "";
							P3CSupportInterface maxBinSuppSet = null;
							
							// Find bin with largest support and remove it
							iterAttrBin = unmarkedBins.entrySet().iterator();
							while (iterAttrBin.hasNext()) {
								
								Entry<String, P3CSupportInterface> entry = iterAttrBin.next();
								
								if (entry.getValue().getSupport() > maxBinSupp) {
									maxBinSuppSet = entry.getValue();
									maxBinSupp = maxBinSuppSet.getSupport();
									maxBinKeyValue = entry.getKey();
								}
							}
							
							/* 
							 * - maxBinKey becomes a marked bin
							 * - Remove marked bin from allBins, because we continue
							 * to iterate over unmarked bins that are (hypothesis
							 * to be) normal.
							 * - Add marked bin to a new structure, so we can use in
							 * Stage 2 (compute intervals). 
							 */
							
							// Add bin
							markedBins.put(newInterval(marsInfo.getAttributes().get(i) + ":" + maxBinKeyValue), maxBinSuppSet);
							
							//attr.getValue().remove(maxBinKeyValue);
							unmarkedBins.remove(maxBinKeyValue);
						}
						// Accept the null hypothesis of uniform
						else {
							//log.info("Accept");
							/* This is our termination criterion: once the null
							 * hypothesis is accepted, we stop.
							 * Break out of infinite loop
							 */
							break;
						}
					} else {
						int maxBinSupp = 0;
						String maxBinKeyValue = "";
						P3CSupportInterface maxBinSuppSet = null;
						
						// Find bin with largest support and remove it
						iterAttrBin = unmarkedBins.entrySet().iterator();
						while (iterAttrBin.hasNext()) {
							
							Entry<String, P3CSupportInterface> entry = iterAttrBin.next();
							
							if (entry.getValue().getSupport() > maxBinSupp) {
								maxBinSuppSet = entry.getValue();
								maxBinSupp = maxBinSuppSet.getSupport();
								maxBinKeyValue = entry.getKey();
							}
						}
						// Add bin
						markedBins.put(newInterval(marsInfo.getAttributes().get(i) + ":" + maxBinKeyValue), maxBinSuppSet);
						
						unmarkedBins.remove(maxBinKeyValue);
						
						// dof of 0 means there is only 1 value for this attribute,
						// hence it must be normal, so we do nothing
						break;
					}
				} while (true);
			//}
		}
	}
	
	
	/**
	 * 
	 * @return
	 */
	private TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> createIntervals() {
		
		//////////////////////////////////////////////////////////////////////
		//2. Determine attributes with non-uniform distribution, and compute
		//   intervals that approximate projections of clusters onto these 
		//   attributes (Sections 3.1.1 and 3.1.2 of reference).
		//////////////////////////////////////////////////////////////////////
		/*log.info("size of markedBins is " + markedBins.size());
		
		// Generate all (mb1,mb2) candidate pairs (i.e., 2-itemsets) of each attribute for all attributes
		// e.g., (A.a,A.b), (A.a,A.c), (B.a,B.b), (B.a,B.c) ...
		// Note: Candidates are pairs of values from different attributes, and
		// not actually intervals of values from the same attribute. We use
		// P3CInterval here for convenience of storing pairs
		HashMap<P3CInterval<? extends Number>, P3CSupportInterface> candidates =
				generate2SigsCands(markedBins);
		
		log.info("size of candidates (of length 2) is " + candidates.size());*/
		
		/*
		Adjacency matrix of bins. Stores the same information as a list of
		2-itemsets. The bins represent Strings of "AttributeName:
		AttributeValue". binToIntMapping maps these strings to integer
		indexes in adjacencyMatrix.
		*/
		// Initialise with number of candidates, which is maximum theoretical
		// size, and trim() after structure is populated
		//Matrix<? extends Number> adjacencyMatrix = createAdjacencyMatrix(candidates.size());
		Matrix<? extends Number> adjMatAllBinBinPairs = createAdjacencyMatrix();
		
		TreeMap<String, Integer> binToIntMapping =
				new TreeMap<String, Integer>();
		
		/*log.info("Counting support of candidates");
		if(!countSupport(candidates)) {
			log.error("countSupport() returned false");
		}*/
		
		log.info("Testing candidates for intervals (of length 2)...");
		if(!testWithPoissonBasedCriterionForCreatingIntervalsEfficient(/*candidates,*/ markedBins,
				adjMatAllBinBinPairs, binToIntMapping)) {
			log.error("testWithPoissonBasedCriterionForCreatingIntervalsEfficient() returned false");
		}
		
		// Trim now that structure has been populated
		adjMatAllBinBinPairs.trimToSize();
		
		log.info("dimension of adjacencyMatrix is " + adjMatAllBinBinPairs.numberOfRows());
		
		/*
		// Print unordered values
		for(int i = 0; i < adjacencyMatrix.size(); i++) {
			for(int j = 0; j < adjacencyMatrix.get(i).size(); j++) {
				System.out.print(adjacencyMatrix.get(i).get(j) ? 1 : 0);
			}
			System.out.println();
		}
		System.out.println();
		*/
		
		adjMatAllBinBinPairs.printAdjacencyMatrix(binToIntMapping);
		
		/*String csv = adjacencyMatrix.toString(binToIntMapping, false);
		
		try {
			PrintWriter out = new PrintWriter("data\\adj-mat.csv");
			out.println(csv);
			out.close();
		} catch (IOException e) {
			log.error("Error writing csv file for adjacency matrix", e);
		}*/
		
		TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> categoricalIntervals =
				generateCategoricalIntervals(adjMatAllBinBinPairs, binToIntMapping);
		/*
		Iterator<Entry<P3CInterval<? extends Number>, List<Integer>>> iterMarkedBins = 
				markedBins.entrySet().iterator();
		
		while(iterMarkedBins.hasNext()) {
			
			Entry<P3CInterval<? extends Number>, List<Integer>> currentMarkedBin =
					iterMarkedBins.next();
			
			//candidates.put(, new ArrayList<Integer>());
			
			String binKey = currentMarkedBin.getKey().firstKey();
			String attribute = binKey.split(":")[0];
			String value = binKey.split(":")[1];
			
			// Create a new interval
			P3CInterval<? extends Number> interval =
					this.newInterval(value);
			
			TreeSet<P3CInterval<? extends Number>> intervalSet;
			
			if(categoricalIntervals.containsKey(attribute)) {
				
				intervalSet = categoricalIntervals.get(attribute);
				
				// Add the new interval. If interval is not present then it
				// is added by add(), otherwise it is not added.
				intervalSet.add(interval);
				
			} else {
				intervalSet = new TreeSet<P3CInterval<? extends Number>>(P3CComparators.comparatorP3CInterval);
				
				intervalSet.add(interval);
				
				categoricalIntervals.put(attribute, intervalSet);
			}
		}
		*/
		
		return categoricalIntervals;
	}
	
	
	/**
	 * Inputs to this function are the adjacency matrix produced from all
	 * bin-bin pairs. There is a matrix and a mapping that identifies the
	 * rows and columns. The matrix is Boolean for P3C and real value
	 * (support) for 2P4C.
	 * 
	 * 1. For every attribute, every pair of distinct values is produced.
	 * For example, A-B, A-C, A-D, B-C, B-D, C-D. If any of these pairs have
	 * an "adjacency" (transitive) relation then store this knowledge in an
	 * auxiliary n x n matrix where n is the number of values in the attribute.
	 * 
	 * 2. For each auxiliary attribute, find the connected components using DFS.
	 * Each connected component represents an interval. For example, if a
	 * component contains contains nodes A and B then the interval contains 
	 * A and B.
	 * 
	 * Stages 1 and 2 are performed in generateGraphs() and called from this
	 * function.
	 * 
	 * 3. Now that we know the members of the interval, assign membership values.
	 *  For intervals of cardinality 1, this is done inside the function.
	 *  Otherwise, this is done by calling generateCategoricalIntervalFromGraphComponent()
	 * 
	 * Use the adjacency matrix to create intervals of bins. See Section 3.1.2
	 * of referenced paper.
	 * 
	 * Find all pairs, and construct a graph.
	 * 
	 * Find all connected components in a disconnected graph. This is the same
	 * as finding all disconnected subgraphs in a graph. Depth-first search is
	 * performed.
	 * 
	 * @param adjMatAllBinBinPairs
	 * @param binToIntMapping
	 * @return
	 */
	public TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> generateCategoricalIntervals(
			Matrix<? extends Number> adjMatAllBinBinPairs,
			TreeMap<String, Integer> binToIntMapping) {
		
		//Key is attribute label
		TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> allIntervals =
				new TreeMap<Short, TreeSet<P3CInterval<? extends Number>>>();
		
		TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes =
				new TreeMap<Short, Matrix<? extends Number>>();
		
		TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes =
				new TreeMap<Short, TreeMap<String, Integer>>();
		
		// STAGE 1
		// Call generic method for creating graphs (of edge relationships) for each attribute
		if(!generateGraphs(adjMatAllBinBinPairs, binToIntMapping, adjacencyMatrixAttributes, binToIntMappingAttributes)) {
			log.error("generateGraphs() failed");
		}
		
		// STAGE 2
		// Create components of disconnected graph
		TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributesLabels =
				new TreeMap<Short, Matrix<? extends Number>>();
		
		Iterator<Short> iterMatrices = adjacencyMatrixAttributes.keySet().iterator();
		
		TreeMap<Short, List<List<Integer>>> components =
				new TreeMap<Short, List<List<Integer>>>();
		
		// For every attribute's matrix
		while(iterMatrices.hasNext()) {
			
			Short key = iterMatrices.next();
			Matrix<? extends Number> matrix = adjacencyMatrixAttributes.get(key);
			
			Matrix<? extends Number> matrixLabels = this.createAdjacencyMatrix(matrix.numberOfRows());
			adjacencyMatrixAttributesLabels.put(key, matrixLabels);
			
			// DFS on adjacency matrix to find connected components
			// http://math.stackexchange.com/questions/277045/easiest-way-to-determine-all-disconnected-sets-from-a-graph
			components.put(key, Graphs.findConnectedComponents(matrix));
			
			// Incorrect approach: Label connected components
			//Graphs.connectedComponentsLabelling(matrix, matrixLabels, true);
			
			/*PrintWriter out = null;
			try {
				out = new PrintWriter(key + ".csv");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			out.print(matrix.toString());
			out.close();*/
		}
		
		
		// STAGE 3
		// Create intervals and assign memberships
		// Start with intervals of cardinality one
		// Then call fucntion to handle greater than one
		
		iterMatrices = adjacencyMatrixAttributes.keySet().iterator();
		
		// For every attribute's matrix
		while(iterMatrices.hasNext()) {
			
			Short key = iterMatrices.next();
			/*Matrix<? extends Number> matrix =
					adjacencyMatrixAttributes.get(key);
			
			// Print graph to default output
			System.out.println("\n" + key);
			
			for(int i = 0; i < matrix.numberOfRows(); i++) {
				
				System.out.print("\n" + matrix.get(i, 0));
				
				for(int j = 1; j < matrix.numberOfRows(); j++) {
					
					System.out.print("," + matrix.get(i, j));
				}
			}
			
			
			// Print components to default output
			System.out.println();
			
			for(List<Integer> component : components.get(key)) {
				
				System.out.println(component);
				
				//components.get(key);
			}
			*/
			
			// Add bins that were not connected in the graph
			// (which creates intervals of length == 0, hence they do not
			// belong to an interval)
			//
			
			TreeMap<String, Integer> valueToIntMapping =
					binToIntMappingAttributes.get(key);
			
			Iterator<String> iterBinLabels =
					valueToIntMapping.keySet().iterator();
			
			TreeSet<P3CInterval<? extends Number>> intervalsForAttribute =
					new TreeSet<P3CInterval<? extends Number>>(P3CComparators.comparatorP3CInterval);
			
			while(iterBinLabels.hasNext()) {
				
				String binLabel = iterBinLabels.next();
				
				int binId = valueToIntMapping.get(binLabel).intValue();
				
				// Now that we have binId, check if this was in a component
				// of a disconnected graph
				
				boolean alreadyInAnInterval = false;
				
				for(List<Integer> component : components.get(key)) {
					
					if(component.contains(new Integer(binId))) {
						alreadyInAnInterval = true;
					}
				}
				
				// If not in a component of a disconnected graph then
				// create a new interval containing just this bin
				if(!alreadyInAnInterval) {
					
					// Create a new interval
					P3CInterval<? extends Number> interval = this.newInterval();
					
					// Add with default membership
					interval.add(binLabel);
					intervalsForAttribute.add(interval);
				}
			}
			
			//
			// Add bins that were connected in the graph (which creates
			// intervals of length > 1)
			//
			generateCategoricalIntervalFromGraphComponent(
			 		intervalsForAttribute,
					adjMatAllBinBinPairs,
					key,
					components,
					binToIntMapping,
					valueToIntMapping,
					adjacencyMatrixAttributes,
					binToIntMappingAttributes);
			
			allIntervals.put(key, intervalsForAttribute);
		}
		
		return allIntervals;
	}
	
	
	/**
	 * 
	 */
	public boolean test1ItemsSigs(
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidates,
			TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> intervals) {
		
		Iterator<Short> iterIntervals = intervals.keySet().iterator();
		
		// For every attribute's set of intervals 
		while(iterIntervals.hasNext()) {
			
			Short attribute = iterIntervals.next();
			
			TreeSet<P3CInterval<? extends Number>> attrIntervals = intervals.get(attribute);
			Iterator<P3CInterval<? extends Number>> attrIntervalsIter =
					attrIntervals.iterator();
			
			// For every interval in this attribute
			while(attrIntervalsIter.hasNext()) {
				
				P3CInterval<? extends Number> attrInterval = attrIntervalsIter.next();
				
				TreeMap<Short, P3CInterval<? extends Number>> tempTreeMap =
						new TreeMap<Short, P3CInterval<? extends Number>>();
				
				P3CInterval<? extends Number> tempP3CInterval = attrInterval.deepCopy();
				
				tempTreeMap.put(attribute, tempP3CInterval);
				
				//"If the map previously contained a mapping for the key,
				//the old value is replaced."
				candidates.put(tempTreeMap, newSupport(this.supportAsList));
			}
		}
		
		
		// Stores dataset
		List<MarsInfoEntity> entities =
				marsInfo.getEntities();
		
		/*
		 * 1. For each record in dataset
		 * 2. Generate combinations of all k-itemsets in a transaction.
		 * A Trie/PrefixTree is used in Apriori, but instead, an external
		 * library is used to compute the combinations. The reason is ease
		 * of implementation.
		 * 3. 
		 */
		//int recordNumber = 0;
		
		for(MarsInfoEntity entity : entities) {
			
			List<Short> indexes = entity.getIndexes();
			
			for(Short index: indexes) {
				
				//String[] tagAttrValue = tag.split(":");
				
				Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterCandidates =
						candidates.keySet().iterator();
				
				while(iterCandidates.hasNext()) {
					
					TreeMap<Short, P3CInterval<? extends Number>> candidate =
							iterCandidates.next();
					
					if(candidate.containsKey(index)) {
						
						if(candidate.get(index).containsKey(entity.getTag(index))) {
							
							P3CSupportInterface supportObject = candidates.get(candidate);
							incrementSupport(supportObject, getMembership(candidate, entity)/*, recordNumber*/);
							candidates.put(candidate, supportObject);
						}
					}
				}
			}
			//recordNumber++;
		}
		
		return true;
	}
	
	
	/**
	 * Generates k-signature candidates from (k-1)-signatures. These are
	 * candidates that are then tested outside of this function.
	 * 
	 * @param frequentKMinus1Signatures Must contains itemsets that ordered
	 * lexicographically. candidatesKMinus1Signatures is ordered so that iteration
	 * order is ensured when using two iterators.
	 * @param k The size of the itemset
	 * @return HashMap of candidates k-itemsets. HashMap ensures constant time
	 * retrieval when testing against dataset. Lexiographic ordering is not
	 * required.
	 * 
	 * Example return data:
	 * {{Source City=[CRAWLEY, NYON, REDWOOD CITY], 
	 *	Source IP=[195.213.58.248, 195.213.58.250]}=0,
	 *
	 * {Destination City=[KALISPELL, REDWOOD CITY, WEST HOLLYWOOD], 
	 * Source Country=[ REPUBLIC OF, CHINA, KOREA, UNITED STATES]}=0}
	 */
	public TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> generateKSigsCandsCatInts(
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> frequentKMinus1Signatures,
			int k) {
		
		if(k < 2) {
			log.warn("k is less than 2 in generateKItemsCandsCatInts()");
			return null;
		}
		
		TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidates =
				new TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>(P3CComparators.comparatorTreeMapOfComparatorP3CIntervalWithShortNames);
		
		// For each signature
		Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterSignatureA =
				frequentKMinus1Signatures.keySet().iterator();
		
		while(iterSignatureA.hasNext()) {
			
			TreeMap<Short, P3CInterval<? extends Number>> keyA = iterSignatureA.next();
			
			Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterSignatureB =
					//candidatesKMinus1Signatures.keySet().iterator();
					frequentKMinus1Signatures.tailMap(keyA, false).keySet().iterator();
			
			while(iterSignatureB.hasNext()) {
				
				TreeMap<Short, P3CInterval<? extends Number>> keyB = iterSignatureB.next();
				
				TreeMap<Short, P3CInterval<? extends Number>> tempSignature =
								new TreeMap<Short, P3CInterval<? extends Number>>();
				
				// When generating 2-signatures (i.e., k=2) keyA contains one
				// attribute and keyB contains one attribute
				if(k == 2) {
					
					//If different attributes
					//if(!keyA.firstKey().equalsIgnoreCase(keyB.firstKey())) {
					if(keyA.firstKey() != keyB.firstKey()) {
						
						// Deep copy of A
						P3CInterval<? extends Number> tempSignatureA =
								keyA.get(keyA.firstKey()).deepCopy();
						
						// Deep copy of B
						P3CInterval<? extends Number> tempSignatureB =
								keyB.get(keyB.firstKey()).deepCopy();
						
						tempSignature.put(keyA.firstKey(), tempSignatureA);
						tempSignature.put(keyB.firstKey(), tempSignatureB);
						
						// Add 2-signature
						candidates.put(tempSignature, newSupport(this.supportAsList));
					}
					
				} else { // Implies k > 2 when using "keyA.keySet().size() == 1"
					
					// Stores the number of matching elements
					// between two signatures
					//int numberOfSubsets = 0;
					boolean candidatesMatch = true;
					
					Iterator<Short> iterKeysA = keyA.keySet().iterator();
					Iterator<Short> iterKeysB = keyB.keySet().iterator();
					int i = 0;
					
					// Compare all elements except the last
					// For example, compare the first two elements in
					// {A,B,C} and {A,B,D}
					while(iterKeysA.hasNext() /* && iterKeysB.hasNext() */ && i < (k - 2)) { // Assumes sizes of keyA and keyB are equal
						
						Short keyAattributeName = iterKeysA.next();
						Short keyBattributeName = iterKeysB.next();
						
						//Note: not using comparatorTreeMapOfComparatorP3CInterval.compare(),
						//because we work on k - 2 and not k - 1 (k refers to
						// the size TO BE generated)
						
						// If same attribute
						//if(keyAattributeName.equalsIgnoreCase(keyBattributeName)) {
						if(keyAattributeName == keyBattributeName) {
							// ?
						} else {
							candidatesMatch = false;
							break;
						}
						
						i++;
					}
					
					/*if(numberOfSubsets == (k - 2)) {
						
						candidatesMatch = false;
					}*/
					
					// Check that last element is not from the same attribute
					if(iterKeysA.hasNext() /* && iterKeysB.hasNext() */) { // Assumes sizes of keyA and keyB are equal
						
						Short keyAattributeName = iterKeysA.next();
						Short keyBattributeName = iterKeysB.next();
						
						//Note: not using comparatorTreeMapOfComparatorP3CInterval.compare(),
						//because we work on k - 2 and not k - 1
						
						// If same attribute
						//if(keyAattributeName.equalsIgnoreCase(keyBattributeName)) {
						if(keyAattributeName == keyBattributeName) {
							
							// prevent generation of invalid candidate.
							candidatesMatch = false;
						}
					} else {
						log.error("There should be an element here at k-1");
					}
					
					
					/*
					If:
					- first k-2 elements are the same
					- if element k-1 is not the same attribute
					Then:
					- merge
					*/
					if(candidatesMatch) {
						
						Short attributeName;
						
						//Copy over all of signature A
						iterKeysA = keyA.keySet().iterator();
						while(iterKeysA.hasNext()) {
							
							attributeName = iterKeysA.next();
							
							// Deep copy of A
							P3CInterval<? extends Number> tempSignatureA =
									keyA.get(attributeName).deepCopy();
							
							tempSignature.put(attributeName, tempSignatureA);
						}
						
						//Copy last interval from signature B
						attributeName = keyB.lastKey();
						
						// Deep copy of A
						P3CInterval<? extends Number> tempSignatureB =
								keyB.get(attributeName).deepCopy();
						
						// Add final interval to signature
						tempSignature.put(attributeName, tempSignatureB);
						
						// PRUNING
						// Test all subSignatures of this new signature.
						// We do this to ensure the Apriori principle.
						// This is effectively pruning, but we do it before
						// rather than after adding to the set of candidates
						TreeMap<Short, P3CInterval<? extends Number>> testSubSignature =
								new TreeMap<Short, P3CInterval<? extends Number>>();
						
						iterKeysA = tempSignature.keySet().iterator();
						iterKeysB = tempSignature.keySet().iterator();
						
						boolean addSignature = true;
						
						while(iterKeysA.hasNext()) {
							
							Short keyAstr = iterKeysA.next();
							
							while(iterKeysB.hasNext()) {
								
								Short keyBstr = iterKeysB.next();
								
								//if(!keyAstr.equalsIgnoreCase(keyBstr)) {
								if(keyAstr != keyBstr) {
									
									testSubSignature.put(keyBstr, tempSignature.get(keyBstr));
								}
							}
							
							// We now have a (k-1)-signature. We test whether or not
							// this is present in the frequentKMinus1Signatures.
							// If (k-1)-signature does not exist
							if(!frequentKMinus1Signatures.containsKey(testSubSignature)) {
							//if(!containsKeyWithSubsets(frequentKMinus1Signatures, testSubSignature)) {
								
								addSignature = false;
								break;
							}
						}
						
						// If all (k-1)-signature of new k-signature exist
						// then add the candidate
						if(addSignature) {
							
							// Add k-signature
							candidates.put(tempSignature, newSupport(this.supportAsList));
						}
					}
				}
			}
		}
		
		return candidates;
	}
	
	
	/**
	 * Produces all combinations of 2-signatures from bins that are
	 * statistically not normal (hence they are 'marked').
	 * 
	 * This is where a lot of memory is consumed.
	 *  
	 * @param markedBins Collection of bins that have passed statistical test
	 * @return 2-signatures candidates
	 */
	public HashMap<P3CInterval<? extends Number>, P3CSupportInterface> generate2SigsCands
		(TreeMap<P3CInterval<? extends Number>, P3CSupportInterface> markedBins) {
		
		HashMap<P3CInterval<? extends Number>, P3CSupportInterface> candidates =
				new HashMap<P3CInterval<? extends Number>, P3CSupportInterface>();
		
		Iterator<Entry<P3CInterval<? extends Number>, P3CSupportInterface>> iterMarkedBins =
				markedBins.entrySet().iterator();
		
		while(iterMarkedBins.hasNext()) {
			
			Entry<P3CInterval<? extends Number>, P3CSupportInterface> currentMarkedBin =
					iterMarkedBins.next();
			
			// Note: tailMap() returns a SortedMap
			SortedMap<P3CInterval<? extends Number>, P3CSupportInterface> tailMarkedBins = null;
			try {
				// Returns a view of this portion of the map (same as shallow copy?)
				tailMarkedBins = markedBins.tailMap(currentMarkedBin.getKey(), false);
			} catch (Exception e) {
				log.error(e);
			}
			
			Iterator<Entry<P3CInterval<? extends Number>, P3CSupportInterface>> tailIter =
					tailMarkedBins.entrySet().iterator();
			
			while(tailIter.hasNext()) {
				
				Entry<P3CInterval<? extends Number>, P3CSupportInterface> tailMarkedBin =
						tailIter.next();
				
				// Each bin only contains one key
				
				String strTail[] = DataUtils.splitByFirstOccurrence(tailMarkedBin.getKey().iterator().next(), this.marsInfo.getSplitKeyword());
				String strCurrent[] = DataUtils.splitByFirstOccurrence(currentMarkedBin.getKey().iterator().next(), this.marsInfo.getSplitKeyword());
				
				// Prevent combinations of values from the same attribute
				if(!strTail[0].equalsIgnoreCase(strCurrent[0])) {
					
					P3CInterval<? extends Number> markedBin = currentMarkedBin.getKey().deepCopy();
					
					markedBin.append(tailMarkedBin.getKey());
					
					candidates.put(markedBin, newSupport(this.supportAsList)); //new ArrayList<Integer>());
				}
			}
		}
		
		return candidates;
	}
	
	
	/**
	 * Count support before calling testWithPoissonBasedCriterionForCreatingIntervalsEfficient()
	 * @param candidates
	 * @return
	 */
	/*public boolean countSupport(
			HashMap<P3CInterval<? extends Number>, P3CSupportInterface> candidates) {
		
		//int recordNumber = 0;
		for(MarsInfoEntity entity : marsInfo.getEntities()) {
			
			List<List<String>> combinations = new ArrayList<List<String>>();
			List<String> tags = entity.getTags();
			
			Iterator<String> tagsIterator = tags.iterator();
			
			while(tagsIterator.hasNext()) {
				
				String value = tagsIterator.next();
				
				Iterator<String> tagsIterator2 = tags.iterator();
				
				while(tagsIterator2.hasNext()) {
					
					String value2 = tagsIterator2.next();
					
					if(!value.equalsIgnoreCase(value2)) {
						List<String> newList = new ArrayList<String>();
						newList.add(value);
						newList.add(value2);
						combinations.add(newList);
					}
				}
			}
			
			Iterator<List<String>> iterComb = combinations.iterator();
			
			while(iterComb.hasNext()) {
				List<String> combination = iterComb.next();
				
				P3CInterval<? extends Number> combinationP3CInterval =
						newInterval(combination);
				
				// containsKey() has constant time, i.e., O(1).
				if(candidates.containsKey(combinationP3CInterval)) {
					
					P3CSupportInterface supportObject = candidates.get(combinationP3CInterval);
					//incrementSupport(supportObject, getMembership(combinationP3CInterval, entity));
					supportObject.addSupport();
					candidates.put(combinationP3CInterval, supportObject);
				}
			}
			//recordNumber++;
		}
		
		return true;
	}*/
	
	
	/**
	 * Count support of candidate signatures.
	 * 
	 * @param candidates
	 * @param k Number of intervals in signatures (i.e., k-signature)
	 * @return
	 */
	public boolean countSupport(
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidates,
			int k) {
		
		//int recordNumber = 0;
		//int recordDisplayNumber = 100;
		
		Set<TreeMap<Short, P3CInterval<? extends Number>>> candKeySet = candidates.keySet();
		
		Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterCandidates;
		TreeMap<Short, P3CInterval<? extends Number>> signature;
		P3CInterval<? extends Number> interval;
		
		// For each record in dataset
		for(MarsInfoEntity entity : marsInfo.getEntities()) {
			
			/*if(recordNumber % recordDisplayNumber == 0) {
				if(recordNumber > 0) {
					log.info("Records processed for support counting: " + recordNumber);
				}
			}*/
			
			iterCandidates = candKeySet.iterator();
			
			// For each k-signature
			while(iterCandidates.hasNext()) {
				
				signature = iterCandidates.next();
				
				int signatureMatch = 0;
				
				// Set to maximum, because we use minimum for the intersection operator
				//double supportValue = Double.MAX_VALUE;
				
				List<Short> indexes = entity.getIndexes();
				
				for(Short index: indexes) {
					
					interval = signature.get(index);
					
					// If interval (from transaction) exists in candidate signature
					if(interval != null) {
						
						// And, if the value is present in the attribute
						if(interval.containsKey(entity.getTag(index))) {
							
							signatureMatch++;
							
							// If each item in k-itemset combination matches 
							if(signatureMatch == k) {
								
								P3CSupportInterface supportObject = candidates.get(signature);
								incrementSupport(supportObject, getMembership(signature, entity)/*, recordNumber*/);
								candidates.put(signature, supportObject);
							}
						}
					}
				}
			}
			
			//recordNumber++;
		}
		
		return true;
	}
	
	
	/**
	 * Use the adjacency matrix to create intervals of bins. See Section 3.1.2
	 * of referenced paper.
	 * 
	 * 
	 * 
	 * @param adjacencyMatrix
	 * @param binToIntMapping
	 * @return Intervals. First String is the attribute name, and second String
	 * is the attribute value.
	 */
	/*public TreeMap<String, TreeSet<P3CInterval<? extends Number>>> generateCategoricalIntervals(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping) {
		
		//Key is attribute label
		TreeMap<String, TreeSet<P3CInterval<? extends Number>>> allIntervals =
				new TreeMap<String, TreeSet<P3CInterval<? extends Number>>>();
		
		Iterator<String> iterBinsRow = binToIntMapping.keySet().iterator();
		
		while(iterBinsRow.hasNext()) {
			
			String binRow = iterBinsRow.next();
			
			// Start from first element (lexiographical order).
			// This will iterator over cells where the indexes are the same (diagonal line),
			// but this does not matter because these diagnoal blocks contains values of 'false' 
			Iterator<String> iterBinsColumn =
					//binToIntMapping.tailMap(binRow).keySet().iterator();
					binToIntMapping.keySet().iterator();
			
			P3CInterval<? extends Number> interval = newInterval();
			
			String attributeLabelColumnPrevious = new String("");
			
			while(iterBinsColumn.hasNext()) {
				
				String binColumn = iterBinsColumn.next();
				String attributeLabelColumn = binColumn.split(":")[0];
				String attributeValueColumn = binColumn.split(":")[1];
				
				// If attribute is different to previous attribute
				if(!attributeLabelColumnPrevious.equalsIgnoreCase(attributeLabelColumn)) {
					
					// If not the first iteration
					if(!attributeLabelColumnPrevious.equalsIgnoreCase("")) {
						
						if(allIntervals.containsKey(attributeLabelColumnPrevious)) {
							
							if(!interval.isEmpty()) {
								// Add the interval (set of bins) to the previous attribute
								allIntervals.get(attributeLabelColumnPrevious).add(interval);
								//TreeSet<TreeSet<String>> temp = allIntervals.get(attributeLabelColumnPrevious);
								//temp.add(new TreeSet<String>());
							}
						} else {
							
							// Create a new set of intervals and add
							TreeSet<P3CInterval<? extends Number>> newSetOfIntervals = 
									new TreeSet<P3CInterval<? extends Number>>(P3CComparators.comparatorP3CInterval);
							if(!interval.isEmpty()) {
								newSetOfIntervals.add(interval);
							}
							allIntervals.put(attributeLabelColumnPrevious, newSetOfIntervals);
						}
					}
					
					// Start a new interval for the new attribute
					interval = newInterval();
					
					//Record current attributeLabelColumn
					attributeLabelColumnPrevious = attributeLabelColumn;
				}
				
				// Test "adjacency" relationship
				///if(adjacencyMatrix.isAdjacent(binToIntMapping.get(binRow).intValue(),
				///		binToIntMapping.get(binColumn).intValue(),
				//		new Integer(1))) {
				
				//System.out.println("binRow = " + binRow);
				//System.out.println("binColumn = " + binColumn);
				if (isConnected(adjacencyMatrix,
						binToIntMapping.get(binRow).intValue(),
						binToIntMapping.get(binColumn).intValue())) {
					//System.out.println("adjacent");
					interval.add(attributeValueColumn);
				}
			}
		}
		
		return allIntervals;
	}*/
	
	
	/**
	 * Find all bin-bin pairs for each attribute.
	 * 
	 * If there is an adjacency relation then add that relationship to an
	 * auxiliary n x n matrix where n is the number of values of that
	 * variable. The n x n matrix appears to be directed because we only
	 * set (i,j) and not the counterpart (j,i), which would be undirected.
	 * We do this to save on time and space.
	 * 
	 * @param adjacencyMatrix Graph of all attributes-value entries.
	 * @param binToIntMapping adjacencyMatrix is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @param adjacencyMatrixAttributes Maps an attribute to its adjacency matrix.
	 * @param binToIntMappingAttributes adjacencyMatrixAttributes is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @return
	 */
	public boolean generateGraphs(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping,
			TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes,
			TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes) {
		
		String attributeLabelRowPrevious = "";
		
		Object[] keysAdjacencyMatrix = binToIntMapping.keySet().toArray();
		
		Matrix<? extends Number> adjacencyMatrixAttribute =
				this.createAdjacencyMatrix(1);
		TreeMap<String, Integer> binToIntMappingAttribute =
				new TreeMap<String, Integer>();
		
		int innerRows = 0;
		int outerRows = 0;
		
		//
		// PREPARE (GRAPH) DATA STRUCTURES
		//
		// Create one graph for each attribute
		for(; outerRows < keysAdjacencyMatrix.length; outerRows++) {
			
			String[] binOuterRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[outerRows], this.marsInfo.getSplitKeyword());
			String attributeLabelRow = binOuterRow[0];
			String attributeValueRow = binOuterRow[1];
			
			// If a different attribute
			if(!attributeLabelRow.equals(attributeLabelRowPrevious)) {
				
				// If not the first iteration
				if(outerRows != 0) {
					
					Short attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowPrevious));
					
					if(null != adjacencyMatrixAttributes.put(
							attributeId,
							adjacencyMatrixAttribute)) {
						log.error("Overwritten a key");
					}
					
					binToIntMappingAttributes.put(attributeId, binToIntMappingAttribute);
				}
				
				// Create a new graph
				adjacencyMatrixAttribute = createAdjacencyMatrix(1);
				binToIntMappingAttribute =
						new TreeMap<String, Integer>();
				
				// reset inner loop counter
				innerRows = 0;
				
			} else {
			
				// Add attribute value to the graph, i.e., just increase its size.
				if(!adjacencyMatrixAttribute.growByOne()) {
					log.error("Failed to grow adjacency matrix by one");
				}
			}
			
			binToIntMappingAttribute.put(attributeValueRow, innerRows);
			
			innerRows++;
			
			attributeLabelRowPrevious = attributeLabelRow;
		}
		
		// Don't forget the last attribute!
		Short attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowPrevious));
		adjacencyMatrixAttributes.put(attributeId, adjacencyMatrixAttribute);
		binToIntMappingAttributes.put(attributeId, binToIntMappingAttribute);
		
		//
		// POPULATE GRAPHS WITH ADJACENCIES
		//
		/*
		 * For every pair of values (vertically in adjacencyMatrix) from the same attribute. 
		 * Find at least 
		 **/
		for(outerRows = 0; outerRows < keysAdjacencyMatrix.length; outerRows++) {
			
			String[] binOuterRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[outerRows], this.marsInfo.getSplitKeyword());
			String attributeLabelRowOuter = binOuterRow[0];
			String attributeValueRowOuter = binOuterRow[1];
			
			for(innerRows = outerRows + 1; innerRows < keysAdjacencyMatrix.length; innerRows++) {
				
				String[] binInnerRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[innerRows], this.marsInfo.getSplitKeyword());
				String attributeLabelRowInner = binInnerRow[0];
				String attributeValueRowInner = binInnerRow[1];
				
				// Now analysing a different attribute
				if((!attributeLabelRowInner.equals(attributeLabelRowOuter))) {
					break; // out of inner loop
				}
				
				// Analyse adjacency by looping through every bin in all other
				// attributes.
				
				for(int cols = 0; cols < keysAdjacencyMatrix.length; cols++) {
					
					String[] binColumn = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[cols], this.marsInfo.getSplitKeyword());
					String attributeLabelColumn = binColumn[0];
					
					// Ensure we look at different attributes
					if(!attributeLabelColumn.equals(attributeLabelRowOuter)) {
						
						// If an "adjacency" is present
						// (at least one relationship)
						if(isConnected(adjacencyMatrix, binToIntMapping.get(keysAdjacencyMatrix[outerRows]), binToIntMapping.get(keysAdjacencyMatrix[cols]))
								&&
								isConnected(adjacencyMatrix, binToIntMapping.get(keysAdjacencyMatrix[innerRows]), binToIntMapping.get(keysAdjacencyMatrix[cols]))) {
							
							attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowOuter));
							
							int attributeMatrixRow =
									binToIntMappingAttributes.get(attributeId).get(attributeValueRowOuter);
							int attributeMatrixColumn =
									binToIntMappingAttributes.get(attributeId).get(attributeValueRowInner);
							
							// Update attribute's matrix to record the
							// "adjacency" relationship between the two
							// values of the same attribute
							Matrix<? extends Number> matrix =
									adjacencyMatrixAttributes.get(attributeId);
							Number value = matrix.newCellValue(1);
							matrix.setEntry(attributeMatrixRow, attributeMatrixColumn, value);
							
							adjacencyMatrixAttributes.get(attributeId).setEntry(attributeMatrixColumn, attributeMatrixRow, value);
							
							// break out of for loop, because we are just
							// looking for one "adjacency" relationship
							break;
						}
					}
				}
			}
			
			attributeLabelRowPrevious = attributeLabelRowOuter;
		}
		
		return true;
	}
	
	
	/**
	 * Identify all signatures that are maximal. Maximal refers to maximal
	 * itemsets in frequent itemset mining. A k-signature is maximal when it
	 * does not contribute to a (k+1)-signature.
	 * 
	 * There's a good description of maximal itemsets in
	 * http://www-users.cs.umn.edu/~kumar/dmbook/ch6.pdf
	 * 
	 * @param frequentKSignatures All k-signatures of different lengths (k)
	 * @return All maximal k-signatures
	 */
	private List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> identifyMaximalSignatures(
			List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> frequentKSignatures) {
		
		List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> maximalKSignatures =
				new ArrayList<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>>();
		
		// For each set of signatures of length k
		for(int i = 0; i < (frequentKSignatures.size() - 1); i++) {
			
			maximalKSignatures.add(new TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>(P3CComparators.comparatorTreeMapOfComparatorP3CIntervalWithShortNames));
			
			Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterKSig =
					frequentKSignatures.get(i).keySet().iterator();
			
			// For each signature of length k
			while(iterKSig.hasNext()) {
				
				TreeMap<Short, P3CInterval<? extends Number>> signatureK = iterKSig.next();
				
				Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterKPlusOneSig =
						frequentKSignatures.get(i + 1).keySet().iterator();
				
				int numberOfIterations = 0;
				
				// For each signature of length k+1
				while(iterKPlusOneSig.hasNext()) {
					
					TreeMap<Short, P3CInterval<? extends Number>> signatureKPlusOne =
							iterKPlusOneSig.next();
					
					Iterator<Short> signatureKIter =
							signatureK.keySet().iterator();
					
					//Number of attributes (and values) in k-signature
					//that match k-signature
					int matchingAttribute = 0;
					
					// For each attribute in k-signature.
					// Count the number of attributes of k-signature that are
					// in (k+1)-signature.
					while(signatureKIter.hasNext()) {
						
						Short attributeLabel = signatureKIter.next();
						
						// Try using key from k-signature with (k+1)-signature
						P3CInterval<? extends Number> attributeIntervalKPlusOne =
								signatureKPlusOne.get(attributeLabel);
						
						// If attribute matches
						if(attributeIntervalKPlusOne != null) {
							
							P3CInterval<? extends Number> attributeInterval =
									signatureK.get(attributeLabel);
							
							// If attribute values match
							if(attributeInterval.equals(attributeIntervalKPlusOne)) {
								
								matchingAttribute++;
							}
						}
					}
					
					// If the k-signature matches a subsignature from
					// (k+1)-signatures
					if(matchingAttribute == signatureK.size()) {
						
						// Then break out of loop that searches remaining
						// (k+1)-signatures. We just need one match, and we
						// have found it.
						break;
					}
					
					numberOfIterations++;
				}
				
				// If k-signature not present in subsignatures of (k+1)-signatures
				// then it is maximal
				if(numberOfIterations == frequentKSignatures.get(i + 1).size()) {
					maximalKSignatures.get(i).put(signatureK, frequentKSignatures.get(i).get(signatureK));
				}
			}
		}
		
		// The largest k-signature/k-itemset does not have a superset, so
		// we add it.
		maximalKSignatures.add(frequentKSignatures.get(frequentKSignatures.size() - 1));
		
		return maximalKSignatures;
	}
	
	
	/**
	 * Computes projected clusters from the maximal k-signatures
	 * 
	 * @param maximalKSignatures Maximal k-signatures
	 * @return A similarity matrix defining similarity between data points and
	 * cluster cores
	 */
	private TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, List<Double>> computeProjectedClusters(
			List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> maximalKSignatures) {
		
		TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, List<Double>> similarityMatrix =
				new TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, List<Double>>(P3CComparators.comparatorTreeMapOfComparatorP3CIntervalWithShortNames);
		
		for(TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> maximalSignatures: maximalKSignatures) {
			
			Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iter =
					maximalSignatures.keySet().iterator();
			
			// While there is another cluster core
			while(iter.hasNext()) {
				
				HashMap<String, Double> relevanceScore = new HashMap<String, Double>();
				
				TreeMap<Short, P3CInterval<? extends Number>> clusterCore = iter.next();
				
				Iterator<Short> candidateAttrIter = clusterCore.keySet().iterator();
				
				while(candidateAttrIter.hasNext()) {
					
					Short attrIndex = candidateAttrIter.next();
					String attr = marsInfo.getAttributes().get(attrIndex);
					
					Iterator<String> attrValueIter = clusterCore.get(attrIndex).iterator();
					
					while(attrValueIter.hasNext()) {
						
						String attrValue = attrValueIter.next();
						
						String attrBin = attr + ":" + attrValue;
						
						P3CInterval<? extends Number> tempAttrBinSet =
								newInterval(attrBin);
						
						P3CSupportInterface suppAttrBin =
								markedBins.get(tempAttrBinSet);
						P3CSupportInterface suppClusterCore =
								maximalSignatures.get(clusterCore);
						
						double firstTerm = 0.0;
						double secondTerm = 0.0;
						
						
						
						
						
						
						////HashMap<P3CInterval<? extends Number>, P3CSupportInterface> candidates
						////countSupport(candidates);
						
						
						//Set<TreeMap<String, P3CInterval<? extends Number>>> candKeySet = candidates.keySet();
						
						//Iterator<TreeMap<String, P3CInterval<? extends Number>>> iterCandidates;
						//TreeMap<String, P3CInterval<? extends Number>> signature;
						String[] tagAttributeValue;
						P3CInterval<? extends Number> interval;
						int suppAttrBinClusterCore = 0;
						
						// For each record in dataset
						for(MarsInfoEntity entity : marsInfo.getEntities()) {
							
							List<String> tags = entity.getTags();
							
							if(tags.contains(attrBin)) {
								
								for(String tag: tags) {
								
									tagAttributeValue = DataUtils.splitByFirstOccurrence(tag, this.marsInfo.getSplitKeyword());
									
									interval = clusterCore.get(tagAttributeValue[0]);
									
									// If tagAttribute exists from transaction exists in candidate signature
									if(interval != null) {
										
										// And, if the value is present in the attribute
										if(interval.containsKey(tagAttributeValue[1])) {
											
											suppAttrBinClusterCore++;
										}
									}
								}
							}
						}
						
						
						
						
						
						
						
						/*int suppAttrBinClusterCore =
								//CollectionsUtils.intersectionCount(suppAttrBin.getSupportItems(), suppClusterCore.getSupportItems());
								CollectionsUtils.intersectionCount(suppAttrBinTIDs, suppClusterCoreTIDs);*/
						
						//Assumes suppClusterCore.size() is never 0
						firstTerm = (double)suppAttrBinClusterCore /
								(double)suppClusterCore.getSupport();
						
						if(suppAttrBin.getSupport() == 0) {
							log.warn("size of collection is 0");
						}
						
						secondTerm = 1 / (suppAttrBin.getSupport() / (double)this.marsInfo.getEntities().size());
						
						relevanceScore.put(attrBin, firstTerm * secondTerm);
					}
				}
				
				List<Double> clusterCoreSimilarity =
						new ArrayList<Double>(this.marsInfo.getEntities().size());
				
				for(MarsInfoEntity entity: this.marsInfo.getEntities()) {
					
					Double product = new Double(0.0);
					
					for(String tag: entity.getTags()) {
						
						Double score = relevanceScore.get(tag);
						
						if(score != null) {
							
							product += score;
						}
					}
					
					clusterCoreSimilarity.add(product);
				}
				
				similarityMatrix.put(clusterCore, clusterCoreSimilarity);
			}
		}
		
		return similarityMatrix;
	}
	
	
	/**
	 * Compute which data points are outliers
	 * 
	 * @param entities Data points
	 * @param similarityMatrix A similarity matrix defining similarity between
	 * data points and cluster cores
	 * @return
	 */
	private List<MarsInfoEntity> computeOutliers(List<MarsInfoEntity> entities,
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, List<Double>> similarityMatrix) {
		
		List<MarsInfoEntity> entitiesOutliers =
					new ArrayList<MarsInfoEntity>();
		
		for(int entityIndex = 0; entityIndex < entities.size(); entityIndex++){
			
			Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iter =
					similarityMatrix.keySet().iterator();
			
			boolean outlier = true;
			
			while(iter.hasNext()) {
				
				TreeMap<Short, P3CInterval<? extends Number>> key = iter.next();
				
				if(similarityMatrix.get(key).get(entityIndex) >= 0.0) {
					outlier = false;
				}
			}
			
			if(outlier) {
				entitiesOutliers.add(entities.get(entityIndex));
			}
		}
		
		return entitiesOutliers;
	}
	
	
	/**
	 * Remove any nested signatures. The definition of a nested signature is
	 * 
	 * "True p-signatures S and R are nested if for every interval S_i in S~,
	 * there is an interval S_j in R~ so that S_i is a subset of S_j" (p. 277)
	 * 
	 * @param kMinus1Sigs (k-1)-signatures
	 * @param kSigs k-signatures
	 * @return true on successful completetion and false otherwise
	 */
	public boolean removeNestedSignatures(
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> kMinus1Sigs,
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> kSigs) {
		
		if(kMinus1Sigs == null || kSigs == null) { return false; }
		
		Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterKMinus1Sigs =
				kMinus1Sigs.keySet().iterator();
		
		Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterKSigs =
						kSigs.keySet().iterator();
		
		while(iterKSigs.hasNext()) {
			
			TreeMap<Short, P3CInterval<? extends Number>> kSig = iterKSigs.next();
			
			while(iterKMinus1Sigs.hasNext()) {
				
				TreeMap<Short, P3CInterval<? extends Number>> kMinus1Sig = iterKMinus1Sigs.next();
				
				// Is kMinus1Sig an exact subset of kSig?
				// An exact subset means that all items of kMinus1Sig are in
				// kSig
				
				Iterator<Short> iterKMinus1Sig = kMinus1Sig.keySet().iterator();
				
				int numberOfExactValueMatches = 0;
				
				while(iterKMinus1Sig.hasNext()) {
					
					Short kMinus1SigKey = iterKMinus1Sig.next();
					
					// If the key (variable name, such as IPsrc) is the same
					if(kSig.containsKey(kMinus1SigKey)) {
						
						//Check the value, such as 192.168.1.1
						int similarity =
								P3CComparators.comparatorP3CInterval.compare(
										kMinus1Sig.get(kMinus1SigKey),
										kSig.get(kMinus1SigKey));
						
						// If they have exactly the same values
						if(similarity == 0) {
							numberOfExactValueMatches++;
						}
						
					} else {
						// No need to check the rest, because we need an exact match
						break;
					}
				}
				
				// If we have an exact match, kMinus1Sig is an exact subset of kSig
				if(numberOfExactValueMatches == kMinus1Sig.size()) {
					
					// So, we remove kMinus1Sig, because it is a nested signature
					iterKMinus1Sigs.remove();
				}
			}
		}
		
		return true;
	}
	
	
	/**
	 * Get the result of P3C.
	 * 
	 * @secom.uob.mars.algorithms.clusteringing.ClusteringAlgorithm#getResult()
	 * TODO Implement getResult() and Result class for P3C
	 */
	@Override
	public Result getResult() {
		return null;
	}
	
	
	/**
	 * Get the subspace clusters with TIDs.
	 * 
	 * @return
	 */
	public List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> getSubspaceClustersAndTIDs() {
		return clusterCores;
	}
	
	
	/**
	 * 
	 */
	/*public void updateSaturnInfoEntity() {
		
		for (int tid = 0; tid < this.marsInfo.getSaturnInfoEntities().size(); tid++) {
			
			Iterator<TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface>> clusterCoresIterator =
					clusterCores.iterator();
			
			int pClusterCoresI = 0;
			
			while(clusterCoresIterator.hasNext()) {
				
				TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> pClusterCores =
						clusterCoresIterator.next();
				
				Iterator<TreeMap<String, P3CInterval<? extends Number>>> pClusterCoresIterator =
						pClusterCores.navigableKeySet().iterator();
				
				while(pClusterCoresIterator.hasNext()) {
					
					TreeMap<String, P3CInterval<? extends Number>> pClusterCore =
							pClusterCoresIterator.next();
					
					P3CSupportInterface support = pClusterCores.get(pClusterCore);
					
					// For each cluster core, check if this record (tid)
					// belongs to the cluster
					if(support.getSupportItems().contains(new Integer(tid))) {
						
						this.marsInfo.getSaturnInfoEntities().get(tid)
							.addCategory("Cluster " + pClusterCoresI + "." + 1.0);
					}
					
					pClusterCoresI++;
				}
			}
		}
	}*/
	
	
	/**
	 * Write the cluster cores to the database.
	 * 
	 * Attributes have a Short name instead of a String name to reduce the
	 * memory usage. So, for each cluster core, we need to convert the
	 * attribute from a Short to an Integer before writing to the database.
	 * 
	 * @param dataset String dataset name, which does not include folder structure
	 * @param start Timestamp of first transaction
	 * @param end Timestamp of last transaction
	 * @param parameters String of parameters
	 * @return true upon success, false otherwise
	 */
	public boolean saveClusterCoresToDatabase(String algorithmId, String dataset, Timestamp start, Timestamp end) {
		
		// Create an entry in table SubspaceClustersParamConfigs
		SubspaceClustersParamConfigsEntry scpc =
				SubspaceClustersParamConfigsEntry.create(algorithmId, this.poissonThreshold, dataset, start, end);
		
		Iterator<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> clusterCoresIterator =
				clusterCores.iterator();
		
		List<String> attributeNames = this.marsInfo.getAttributes();
		
		int pClusterCoresI = 1;
		
		while(clusterCoresIterator.hasNext()) {
			
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> pClusterCores =
					clusterCoresIterator.next();
			
			Iterator<TreeMap<Short, P3CInterval<? extends Number>>> pClusterCoresIterator =
					pClusterCores.navigableKeySet().iterator();
			
			while(pClusterCoresIterator.hasNext()) {
				
				TreeMap<Short, P3CInterval<? extends Number>> pClusterCore =
						pClusterCoresIterator.next();
				
				TreeMap<String, P3CInterval<? extends Number>> pClusterCoreVerbose =
						new TreeMap<String, P3CInterval<? extends Number>>();
				
				P3CSupportInterface support = pClusterCores.get(pClusterCore);
				
				Iterator<Short> pClusterCoreIter = pClusterCore.keySet().iterator();
				
				while(pClusterCoreIter.hasNext()) {
					
					Short key = pClusterCoreIter.next();
					pClusterCoreVerbose.put(attributeNames.get(key.intValue()), pClusterCore.get(key));
				}
				
				// Foreign key is ID from SubspaceClustersParamConfigsEntry
				SubspaceClustersEntry sce =
						createSubspaceClustersEntry(pClusterCoreVerbose, pClusterCoresI, support, scpc.id);
				
				if(sce == null) {
					log.error("Error occured when writing a SubspaceClustersEntry to H2 database");
					return false;
				}
				
				pClusterCoresI++;
			}
		}
		
		// Shutdown the database, which prevents a lock remaining present.
		PatternsDatabase.getInstance().stopInstance();
		
		return true;
	}
	
	
	public boolean measurePeformance() {
		
		Iterator<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> clusterCoresIterator = null;
		
		TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, HashMap<String, Integer>> clusterSharedClasses =
				new TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, HashMap<String, Integer>>(P3CComparators.comparatorTreeMapOfComparatorP3CIntervalWithShortNames);
		
		//int k = 1;
		int pClusterCoresI = 0;
		double avgF_oneScore = 0.0;
		double avgF_oneScorePsize = 0.0;
		
		Map<String, Integer> classSupports = new HashMap<String, Integer>(); 
		//List<TreeMap<TreeMap<String, P3CInterval<? extends Number>>, Integer>> f1ScoresByPsize =
		//		new ArrayList<TreeMap<TreeMap<String, P3CInterval<? extends Number>>, Integer>>();
		
		
		//
		// Count support of each class in the dataset
		//
		
		Map<Integer, Double> f1ScoresByPsize = new HashMap<Integer, Double>();
		
		for(MarsInfoEntity entity : marsInfo.getEntities()) {
			
			List<String> classes = entity.getCategories();
			
			if(!classes.isEmpty()) {
				
				if(classes.size() > 1) {
					
					log.warn("Too many classes in labeled data. We'll use the first one.");
					
				} else {
					
					String className = classes.get(0);
					
					// increment existing support
					if(classSupports.containsKey(className)) {
						
						Integer supp = classSupports.get(className) + new Integer(1);
						classSupports.put(className, supp);
						
					// otherwise, create a new support
					} else {
						
						classSupports.put(className, new Integer(1));
					}
				}
				
			} else {
				
				log.warn("Data is unlabelled (contains at least one " +
						"unlabled record), hence F-measure not computed");
				return false;
			}
		}
		
		//
		// Count support of each cluster to a class
		//
		
		// For each record
		for(MarsInfoEntity entity : this.marsInfo.getEntities()) {
			
			int k = 1;
			clusterCoresIterator = clusterCores.iterator();
			
			// For each set of clusters of size p
			while(clusterCoresIterator.hasNext()) {
				
				//f1ScoresByPsize.add(new TreeMap<TreeMap<String, P3CInterval<? extends Number>>, Integer>());
				//f1ScoresByPsize.put(new Integer(k), new Double(0.0));
				
				TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> pClusterCores =
						clusterCoresIterator.next();
				
				Iterator<TreeMap<Short, P3CInterval<? extends Number>>> pClusterCoresIterator =
						pClusterCores.navigableKeySet().iterator();
				
				//avgF_oneScorePsize = 0.0;
				
				// Reset supports, and ensure we count the TIDs
				/*while(pClusterCoresIterator.hasNext()) {
					
					// Get the cluster
					TreeMap<Short, P3CInterval<? extends Number>> pClusterCore =
							pClusterCoresIterator.next();
					
					// Set support object so that it stores TIDs
					pClusterCores.put(pClusterCore, newSupport(true));
				}*/
				
				//countSupport(pClusterCores, k);
				
				// Reset iterator, because we used it in reset supports, see while
				// loop above
				pClusterCoresIterator =
						pClusterCores.navigableKeySet().iterator();
				
				// For each cluster
				while(pClusterCoresIterator.hasNext()) {
					
					// get the cluster
					TreeMap<Short, P3CInterval<? extends Number>> pClusterCore =
							pClusterCoresIterator.next();
					
					// Increment number of clusters
					//pClusterCoresI++;
					
					// Create new support, so it's ready to be incremented
					//trueClusterSupports.put(pClusterCore, newSupport());
					
					
					
					//P3CSupportInterface clusterSupport = pClusterCores.get(pClusterCore);
					
					//List<Integer> TIDs = clusterSupport.getSupportItems();
					
					
					
					
					
					int signatureMatch = 0;
					
					// Set to maximum, because we use minimum for the intersection operator
					//double supportValue = Double.MAX_VALUE;
					
					// For each item in record
					List<Short> indexes = entity.getIndexes();
					for(Short index: indexes) {
						
						P3CInterval<? extends Number> interval = pClusterCore.get(index);
						
						// If interval (from transaction) exists in candidate signature
						if(interval != null) {
							
							// And, if the value is present in the attribute
							if(interval.containsKey(entity.getTag(index))) {
								
								signatureMatch++;
								
								// Record supports the cluster
								// (if each item in k-itemset combination matches)
								if(signatureMatch == k) {
									
									/*P3CSupportInterface supportObject = candidates.get(signature);
									incrementSupport(supportObject, getMembership(signature, entity));
									candidates.put(signature, supportObject);*/
									
									List<String> classes = entity.getCategories();
									
									if(!classes.isEmpty()) {
										
										if(classes.size() == 1) {
											
											String className = classes.get(0);
											
											HashMap<String, Integer> classSharedPoints = clusterSharedClasses.get(pClusterCore);
											
											if(classSharedPoints == null) {
												classSharedPoints = new HashMap<String, Integer>();
											}
											
											// increment existing support
											if(classSharedPoints.containsKey(className)) {
												
												Integer supp = classSharedPoints.get(className) + new Integer(1);
												classSharedPoints.put(className, supp);
												
											// otherwise, create a new support
											} else {
												
												classSharedPoints.put(className, new Integer(1));
											}
											
											clusterSharedClasses.put(pClusterCore, classSharedPoints);
										} else {
											
											log.warn("More than one class per record. Only using the first");
										}
										
									} else {
										log.error("Data is unlabelled (contains at least one " +
												"unlabeled record), hence F-measure not computed");
										return false;
									}
								}
							}
						}
					}
					
					
					
					
					/*Iterator<String> keys = classSharedPoints.keySet().iterator();
					
					String maxSuppKey = new String();
					Integer maxSuppValue = new Integer(Integer.MIN_VALUE);
					
					while(keys.hasNext()) {
						
						String key = keys.next();
						
						Integer supp = classSharedPoints.get(key);
						
						if(supp > maxSuppValue) {
							
							maxSuppValue = supp;
							maxSuppKey = key;
						}
					}
					
					// Calculate precision and recall for this cluster
					double clusterPrecision = 0.0d;
					double clusterRecall = 0.0d;
					clusterPrecision = maxSuppValue / (double)clusterSupport.getSupport();
					clusterRecall = maxSuppValue / (double)classSupports.get(maxSuppKey);
					
					// Calculate F_1 score
					// http://en.wikipedia.org/wiki/F1_score
					double f_oneScore = 0.0d;
					
					if((clusterPrecision + clusterRecall) > 0.0d) {
						
						f_oneScore = (clusterPrecision * clusterRecall) /
								(clusterPrecision + clusterRecall);
						
						f_oneScore *= 2.0d;
					}
					
					avgF_oneScore += f_oneScore;
					avgF_oneScorePsize += f_oneScore;
					
					P3CSupportInterface support = pClusterCores.get(pClusterCore);
					
					log.info("Cluster # " + pClusterCoresI + " = " + pClusterCore +
							": precision = " + clusterPrecision +
							", recall = " + clusterRecall +
							", F_one score = " + f_oneScore +
							", " + getSupport(support));
					
					*/
				}
				
				/*if(pClusterCores.navigableKeySet().size() > 0) {
					avgF_oneScorePsize /= (double)pClusterCores.navigableKeySet().size();
				}
				f1ScoresByPsize.put(new Integer(k), new Double(avgF_oneScorePsize));*/
				
				// Increment cluster size
				k++;
			}
		}
		
		
		//
		// Calculate F scores
		//
		// reset iterator
		clusterCoresIterator = clusterCores.iterator();
		// For each set of clusters of size p
		while(clusterCoresIterator.hasNext()) {
			
			//f1ScoresByPsize.add(new TreeMap<TreeMap<String, P3CInterval<? extends Number>>, Integer>());
			//f1ScoresByPsize.put(new Integer(k), new Double(0.0));
			
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> pClusterCores =
					clusterCoresIterator.next();
			
			Iterator<TreeMap<Short, P3CInterval<? extends Number>>> pClusterCoresIterator =
					pClusterCores.navigableKeySet().iterator();
			
			avgF_oneScorePsize = 0.0;
			
			// Reset supports, and ensure we count the TIDs
			/*while(pClusterCoresIterator.hasNext()) {
				
				// Get the cluster
				TreeMap<Short, P3CInterval<? extends Number>> pClusterCore =
						pClusterCoresIterator.next();
				
				// Set support object so that it stores TIDs
				pClusterCores.put(pClusterCore, newSupport(true));
			}*/
			
			//countSupport(pClusterCores, k);
			
			// Reset iterator, because we used it in reset supports, see while
			// loop above
			pClusterCoresIterator =
					pClusterCores.navigableKeySet().iterator();
			
			// For each cluster
			while(pClusterCoresIterator.hasNext()) {
				
				// get the cluster
				TreeMap<Short, P3CInterval<? extends Number>> pClusterCore =
						pClusterCoresIterator.next();
				
				// Increment the number of clusters
				pClusterCoresI++;
				
				HashMap<String, Integer> classSharedPoints = clusterSharedClasses.get(pClusterCore);
				
				Iterator<String> keys = classSharedPoints.keySet().iterator();
				
				String maxSuppKey = new String();
				Integer maxSuppValue = new Integer(Integer.MIN_VALUE);
				
				while(keys.hasNext()) {
					
					String key = keys.next();
					
					Integer supp = classSharedPoints.get(key);
					
					if(supp > maxSuppValue) {
						
						maxSuppValue = supp;
						maxSuppKey = key;
					}
				}
				
				// Calculate precision and recall for this cluster
				double clusterPrecision = 0.0d;
				double clusterRecall = 0.0d;
				clusterPrecision = maxSuppValue / (double)pClusterCores.get(pClusterCore).getSupport();
				clusterRecall = maxSuppValue / (double)classSupports.get(maxSuppKey);
				
				// Calculate F_1 score
				// http://en.wikipedia.org/wiki/F1_score
				double f_oneScore = 0.0d;
				
				if((clusterPrecision + clusterRecall) > 0.0d) {
					
					f_oneScore = (clusterPrecision * clusterRecall) /
							(clusterPrecision + clusterRecall);
					
					f_oneScore *= 2.0d;
				}
				
				avgF_oneScore += f_oneScore;
				avgF_oneScorePsize += f_oneScore;
				
				P3CSupportInterface support = pClusterCores.get(pClusterCore);
				
				log.info("Cluster # " + pClusterCoresI + " = " + pClusterCore +
						": precision = " + clusterPrecision +
						", recall = " + clusterRecall +
						", F_one score = " + f_oneScore +
						", " + getSupport(support));
			}
		}
		
		if(pClusterCoresI != 0 ) {
			avgF_oneScore /= (double)pClusterCoresI;
		}
		
		log.info("F1 score (avg. of all clusters) = " + avgF_oneScore);
		
		Iterator<Integer> f1ScoresByPsizeIter = f1ScoresByPsize.keySet().iterator();
		
		while(f1ScoresByPsizeIter.hasNext()) {
			
			Integer key = f1ScoresByPsizeIter.next();
			
			log.info("F1 score (avg. of all " + key + "-clusters) = " + f1ScoresByPsize.get(key));
		}
		
		//System.out.println("trueClusterSupports = ");
		//System.out.println(trueClusterSupports);
		
		return true;
	}
	
	
	public boolean setConfidenceLevel (double confidenceLevel) {
		
		if(confidenceLevel > 0.0d) {
			log.warn("Attempt to set confidenceLevel > 0.0d");
			return false;
		}
		
		if(confidenceLevel < 1.0d) {
			log.warn("Attempt to set confidenceLevel > 0.0d");
			return false;
		}
		
		this.confidenceLevel = confidenceLevel;
		
		return true;
	}
	
	
	public double getConfidenceLevel () {
		return confidenceLevel;
	}
	
	
	/**
	 * Calculate the attribute cardinality of marked bins.
	 */
	private void calculateMarkedBinCardinalities() {
		
		Iterator<P3CInterval<? extends Number>> iterMarkedItemsets =
				this.markedBins.keySet().iterator();
		
		// Speed improvement: move this to pre-processing
		while(iterMarkedItemsets.hasNext()) {
			
			P3CInterval<? extends Number> markedBin = iterMarkedItemsets.next();
			
			// Note, each bin contains only one key
			String attr = DataUtils.splitByFirstOccurrence(markedBin.firstKey(), this.marsInfo.getSplitKeyword())[0];
			Short attrIndex = marsInfo.getAttributesIndex(attr).shortValue();
			
			// If this is a new attribute then create data structure for it
			if(!markedBinsAttributeCardinality.containsKey(attrIndex)) {
				markedBinsAttributeCardinality.put(attrIndex, new Integer(0));
			}
			
			// Count the cardinality
			int cardinality = markedBinsAttributeCardinality.get(attrIndex).intValue() + 1;
			markedBinsAttributeCardinality.put(attrIndex, new Integer(cardinality));
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * The Poisson significance test must match that used in testKItemsCands()
	 */
	@Override
	public boolean testWithPoissonBasedCriterionForCreatingIntervals(
			HashMap<P3CInterval<? extends Number>, P3CSupportInterface> candidates,
			TreeMap<P3CInterval<? extends Number>, P3CSupportInterface> frequentKminus1Signatures,
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping) {
		
		Iterator<P3CInterval<? extends Number>> iterCandidates = candidates.keySet().iterator();
		Poisson poissonDistribution = new Poisson();
		
		//int i = 0;
		//long startTime = System.nanoTime();
		
		while(iterCandidates.hasNext()) {
			
			// Observed support, i.e., Supp({A} U {B})
			int supp;
			
			// Expected support, i.e., ESupp({A} U {B} | {A})
			// Note, ESupp is also defined as:
			// ESupp(R = S U {S'} | S) := Supp(S)*width(S'), and
			// width(S') = number of attributes in S' (i.e., cardinality)
			double eSuppA = 0.0;
			double eSuppB = 0.0;
			
			// Support of 1-itemset, i.e., Supp({A})
			double suppA = 0.0;
			double suppB = 0.0;
			
			// width(S') where S' is a bin.
			// Numeric attributes are normalised to [0,1] and width is calculated
			// by upper - lower, e.g., width of [0.5, 0.7] = 0.7 - 0.5 = 0.2
			// So we assume (paper does not state) that width of categorical attributes 
			// is defined as the number of attribute values in interval / the total
			// number of attribute values, e.g., interval of 5 values from all 10
			// values for that attribute gives a width of 5 / 10 = 0.5. This is effectively
			// the same as normalising, but for categorical data.
			double widthA = 0.0;
			double widthB = 0.0;
			
			P3CInterval<? extends Number> candidate = iterCandidates.next();
			
			supp = candidates.get(candidate).getSupport();
			
			// This just speeds up overall execution time, because
			// poissionTestForIntervals() returns false with 0 supp
			if(supp > 0) {
				
				//Temporary collection for just one item (first)
				P3CInterval<? extends Number> item1ItemsetA = newInterval(candidate.firstKey());
				suppA = this.markedBins.get(item1ItemsetA).getSupport();
				
				//Temporary collection for just one item (last)
				// Changed firstKey to lastKey
				P3CInterval<? extends Number> item1ItemsetB = newInterval(candidate.lastKey());
				suppB = this.markedBins.get(item1ItemsetB).getSupport();
				
				Iterator<P3CInterval<? extends Number>> iterMarkedItemsets =
						this.markedBins.keySet().iterator();
				
				// Calculate width of bin
				while(iterMarkedItemsets.hasNext()) {
					
					P3CInterval<? extends Number> markedBin = iterMarkedItemsets.next();
					
					String tagAttr = DataUtils.splitByFirstOccurrence(markedBin.firstKey(), this.marsInfo.getSplitKeyword())[0];
					
					// Count the number of values in tagAttr, i.e., cardinality of tagAttr
					if(item1ItemsetA.firstKey().contains(tagAttr)) {widthA++;}
					if(item1ItemsetB.firstKey().contains(tagAttr)) {widthB++;}
				}
				
				widthA = 1.0 / widthA;
				widthB = 1.0 / widthB;
				
				//eSupp = supp / (double)frequentKminus1Itemsets.get(item1Itemset);
				eSuppA = suppA * widthB;
				eSuppB = suppB * widthA;
				
				// If all combinations of new bin/interval are evaluated
				// E.g., {A, B, C} would produce combinations
				// {{A, B}, {C}}, {{A, C}, {B}}, {{B, C}, {A}}
				// then add those that satisfy both conditions to the adjacency
				// matrix. Not adding has the same meaning as pruning.
				
				//
				// Definition 3.3 
				//
				// Condition 1, part1
				if(supp > eSuppA) {
					
					// Condition 1, part 2
					if(poissonDistribution.test(supp, eSuppA, this.poissonThresholdIntGen)) {
						
						// Condition 2, part1
						if(supp > eSuppB) {
							
							// Condition 2, part 2
							if(poissonDistribution.test(supp, eSuppB, this.poissonThresholdIntGen)) {
								
								String bin1 = candidate.firstKey(); //(String)candidate.toArray()[0];
								String bin2 = candidate.lastKey(); //(String)candidate.toArray()[1];
								
								for(String bin : candidate) {
									if(!binToIntMapping.containsKey(bin)){
										
										binToIntMapping.put(bin, adjacencyMatrix.numberOfRows());
										
										adjacencyMatrix.growByOne();
									}
								}
								
								adjacencyMatrix.setEntry(binToIntMapping.get(bin1),
										binToIntMapping.get(bin2),
										newNumber(supp));
								adjacencyMatrix.setEntry(binToIntMapping.get(bin2),
										binToIntMapping.get(bin1),
										newNumber(supp));
							}
						}
					}
				}
			}
			//i++;
			/*if((i % 1000) == 0) {
				// Nano second is one billionth of a second. Note, 10^9 = 1000000000.
				// Some rounding occurs with conversion from long to float, but we assume this is ok
				double elapsedTime = (System.nanoTime() - startTime) / 1000000000f;
				startTime = System.nanoTime();
				log.info("i = " + i + " and exec time (s) = " + elapsedTime);
			}*/
		}
		
		return true;
	}
	
	
	/**
	 * 
	 */
	@Override
	public boolean testWithPoissonBasedCriterionForCreatingIntervalsEfficient(
			TreeMap<P3CInterval<? extends Number>, P3CSupportInterface> frequentKminus1Signatures,
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping) {
		
		long numberOfCandidates = 0;
		
		Iterator<Entry<P3CInterval<? extends Number>, P3CSupportInterface>> iterMarkedBins =
				markedBins.entrySet().iterator();
		
		// 
		HashMap<P3CInterval<? extends Number>, P3CSupportInterface> candidates =
				new HashMap<P3CInterval<? extends Number>, P3CSupportInterface>();
		
		while(iterMarkedBins.hasNext()) {
			
			Entry<P3CInterval<? extends Number>, P3CSupportInterface> currentMarkedBin =
					iterMarkedBins.next();
			
			// Note: tailMap() returns a SortedMap
			SortedMap<P3CInterval<? extends Number>, P3CSupportInterface> tailMarkedBins = null;
			try {
				// Returns a view of this portion of the map (same as shallow copy?)
				tailMarkedBins = markedBins.tailMap(currentMarkedBin.getKey(), false);
			} catch (Exception e) {
				log.error(e);
			}
			
			Iterator<Entry<P3CInterval<? extends Number>, P3CSupportInterface>> tailIter =
					tailMarkedBins.entrySet().iterator();
			
			while(tailIter.hasNext()) {
				
				Entry<P3CInterval<? extends Number>, P3CSupportInterface> tailMarkedBin =
						tailIter.next();
				
				// Each bin only contains one key
				String test = tailMarkedBin.getKey().iterator().next();
				String test2 = currentMarkedBin.getKey().iterator().next();
				String strTail[] = DataUtils.splitByFirstOccurrence(test, this.marsInfo.getSplitKeyword());
				String strCurrent[] = DataUtils.splitByFirstOccurrence(test2, this.marsInfo.getSplitKeyword());
				
				/*if((strCurrent[0].equalsIgnoreCase("ASCITES") && strTail[0].equalsIgnoreCase("VARICES")) ||
						(strCurrent[0].equalsIgnoreCase("VARICES") && strTail[0].equalsIgnoreCase("ASCITES"))) {
					log.info("ASCITES and VARICES found");
				}*/
				
				// Prevent combinations of values from the same attribute
				if(!strTail[0].equalsIgnoreCase(strCurrent[0])) {
					
					//P3CInterval<? extends Number> bin1 = currentMarkedBin.getKey().deepCopy();
					P3CInterval<? extends Number> candidate = currentMarkedBin.getKey().deepCopy();
					
					//candidate looks like {dst:128.11.19.150=1, dstport:0=1}
					candidate.append(tailMarkedBin.getKey());
					//P3CInterval<? extends Number> bin2 = tailMarkedBin.getKey();
					
					candidates.put(candidate, newSupport(this.supportAsList));
					
					// we have a new candidate
					numberOfCandidates++;
					if((numberOfCandidates % batchCandidates) == 0) {
						
						log.info("numberOfCandidates = " + numberOfCandidates);
						
						batchTestCandidates(candidates, adjacencyMatrix, binToIntMapping);
						
						// Remove all candidates and start to add new candidates.
						// This prevents running out of memory.
						candidates = new HashMap<P3CInterval<? extends Number>, P3CSupportInterface>();
					}
				}
			}
		}
		
		if(numberOfCandidates < batchCandidates) {
			
			log.info("note: numberOfCandidates was less (" + numberOfCandidates + ") than batchCandidates");
			
			batchTestCandidates(candidates, adjacencyMatrix, binToIntMapping);
		}
		
		return true;
	}
	
	
	private void batchTestCandidates(
			HashMap<P3CInterval<? extends Number>, P3CSupportInterface> candidates,
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping) {
		
		Poisson poissonDistribution = new Poisson();
		
		//candidates.put(markedBin, newSupport(this.supportAsList)); //new ArrayList<Integer>());
		
		//String bin1key = bin1.firstKey(); //(String)candidate.toArray()[0];
		//String bin2key = bin2.lastKey(); //(String)candidate.toArray()[1];
		//String[] bin1tag = bin1key.split(":");
		//String[] bin2tag = bin2key.split(":");
		
		
		
		
		// Observed support, i.e., Supp({A} U {B})
		//int supp = 0;
		//int index1 = marsInfo.getSaturnInfo().getAttributesIndex(bin1tag[0]);
		//int index2 = marsInfo.getSaturnInfo().getAttributesIndex(bin2tag[0]);
		
		//int recordNumber = 0;
		for(MarsInfoEntity entity : marsInfo.getEntities()) {
			
			/*String entityValue1 = entity.getTag(PrimitiveDataTypes.safeIntegerToShort(index1));
			String entityValue2 = entity.getTag(PrimitiveDataTypes.safeIntegerToShort(index2));
			
			// If candidate matches a record/entity in dataset
			if(null != entityValue1) {
				if(entityValue1.equalsIgnoreCase(bin1tag[1])) {
					if(null != entityValue2) {
						if(entityValue2.equalsIgnoreCase(bin2tag[1])) {
							supp++;
						}
					}
				}
			}*/
			
			List<List<String>> combinations = new ArrayList<List<String>>();
			List<String> tags = entity.getTags();
			List<Short> indexes = entity.getIndexes();
			
			
			
			//Iterator<String> tagsIterator = tags.iterator();
			int index1 = 0;
			//while(tagsIterator.hasNext()) {
			while(index1 < indexes.size()) {
				
				String value1 = tags.get(index1);
				//tagsIterator.next();
				
				//Iterator<String> tagsIterator2 = tags.iterator();
				int index2 = 0;
				//while(tagsIterator2.hasNext()) {
				while(index2 < indexes.size()) {
					
					String value2 = tags.get(index2);
					//tagsIterator2.next();
					
					if(!value1.equalsIgnoreCase(value2)) {
						List<String> newList = new ArrayList<String>();
						
						newList.add(marsInfo.getAttributes().get(indexes.get(index1)) + ":" + value1);
						newList.add(marsInfo.getAttributes().get(indexes.get(index2)) + ":" + value2);
						//newList.add(value2);
						combinations.add(newList);
					}
					
					index2++;
				}
				
				index1++;
			}
			
			Iterator<List<String>> iterComb = combinations.iterator();
			
			while(iterComb.hasNext()) {
				List<String> combination = iterComb.next();
				
				P3CInterval<? extends Number> combinationP3CInterval =
						newInterval(combination);
				
				if(candidates.containsKey(combinationP3CInterval)) {
					
					P3CSupportInterface supportObject = candidates.get(combinationP3CInterval);
					//incrementSupport(supportObject, getMembership(signature, entity)/*, recordNumber*/);
					supportObject.addSupport(/*new Integer(recordNumber)*/);
					candidates.put(combinationP3CInterval, supportObject);
				}
			}
			//recordNumber++;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		Iterator<P3CInterval<? extends Number>> candidatesIter = candidates.keySet().iterator();
		
		while(candidatesIter.hasNext()) {
			
			P3CInterval<? extends Number> candidateToTest = candidatesIter.next();
			
			// Expected support, i.e., ESupp({A} U {B} | {A})
			// Note, ESupp is also defined as:
			// ESupp(R = S U {S'} | S) := Supp(S)*width(S'), and
			// width(S') = number of attributes in S' (i.e., cardinality)
			double eSuppA = 0.0;
			double eSuppB = 0.0;
			
			// Support of 1-itemset, i.e., Supp({A})
			double suppA = 0.0;
			double suppB = 0.0;
			
			// width(S') where S' is a bin.
			// Numeric attributes are normalised to [0,1] and width is calculated
			// by upper - lower, e.g., width of [0.5, 0.7] = 0.7 - 0.5 = 0.2
			// So we assume (paper does not state) that width of categorical attributes 
			// is defined as the number of attribute values in interval / the total
			// number of attribute values, e.g., interval of 5 values from all 10
			// values for that attribute gives a width of 5 / 10 = 0.5. This is effectively
			// the same as normalising, but for categorical data.
			double widthA = 0.0;
			double widthB = 0.0;
			
			//P3CInterval<? extends Number> candidate = iterCandidates.next();
			//P3CInterval<? extends Number> candidate = markedBin;
			
			// Observed support, i.e., Supp({A} U {B})
			int supp = candidates.get(candidateToTest).getSupport();
			
			// This just speeds up overall execution time, because
			// poissionTestForIntervals() returns false with 0 supp
			if(supp > 0) {
				
				//Temporary collection for just one item (first)
				P3CInterval<? extends Number> item1ItemsetA = newInterval(candidateToTest.firstKey());
				suppA = this.markedBins.get(item1ItemsetA).getSupport();
				
				//Temporary collection for just one item (last)
				// Changed firstKey to lastKey
				P3CInterval<? extends Number> item1ItemsetB = newInterval(candidateToTest.lastKey());
				suppB = this.markedBins.get(item1ItemsetB).getSupport();
				
				Iterator<P3CInterval<? extends Number>> iterMarkedItemsets =
						this.markedBins.keySet().iterator();
				
				String keyA = DataUtils.splitByFirstOccurrence(item1ItemsetA.firstKey(), this.marsInfo.getSplitKeyword())[0];
				String keyB = DataUtils.splitByFirstOccurrence(item1ItemsetB.firstKey(), this.marsInfo.getSplitKeyword())[0];
				
				// Calculate width of bin
				while(iterMarkedItemsets.hasNext()) {
					
					P3CInterval<? extends Number> markedBin = iterMarkedItemsets.next();
					
					// Each markedBin contains just one bin, hence firstKey() is used
					String tagAttr = DataUtils.splitByFirstOccurrence(markedBin.firstKey(), this.marsInfo.getSplitKeyword())[0];
					
					// Count the number of values in tagAttr, i.e., cardinality of tagAttr
					if(keyA.equalsIgnoreCase(tagAttr)) {
						widthA++;
						}
					if(keyB.equalsIgnoreCase(tagAttr)) {widthB++;}
				}
				
				widthA = 1.0 / widthA;
				widthB = 1.0 / widthB;
				
				//eSupp = supp / (double)frequentKminus1Itemsets.get(item1Itemset);
				eSuppA = suppA * widthB;
				eSuppB = suppB * widthA;
				
				// If all combinations of new bin/interval are evaluated
				// E.g., {A, B, C} would produce combinations
				// {{A, B}, {C}}, {{A, C}, {B}}, {{B, C}, {A}}
				// then add those that satisfy both conditions to the adjacency
				// matrix. Not adding has the same meaning as pruning.
				
				/*if((keyA.equalsIgnoreCase("ASCITES") && keyB.equalsIgnoreCase("VARICES")) ||
						(keyA.equalsIgnoreCase("VARICES") && keyB.equalsIgnoreCase("ASCITES"))) {
					log.info("ASCITES and VARICES found");
				}*/
				
				//
				// Definition 3.3 
				//
				// Condition 1, part1
				if(supp > eSuppA) {
					
					// Condition 1, part 2
					if(poissonDistribution.test(supp, eSuppA, this.poissonThresholdIntGen)) {
						
						// Condition 2, part1
						if(supp > eSuppB) {
							
							// Condition 2, part 2
							if(poissonDistribution.test(supp, eSuppB, this.poissonThresholdIntGen)) {
								
								
								
								for(String bin : candidateToTest) {
									//TODO Looks odd!!!!!!!!!!! Why twice?
									if(!binToIntMapping.containsKey(bin)){
										
										binToIntMapping.put(bin, adjacencyMatrix.numberOfRows());
										
										adjacencyMatrix.growByOne();
									}
									/*if(!binToIntMapping.containsKey(bin)){
										
										binToIntMapping.put(bin, adjacencyMatrix.numberOfRows());
										
										adjacencyMatrix.growByOne();
									}*/
								}
								
								String bin1 = candidateToTest.firstKey(); //(String)candidate.toArray()[0];
								String bin2 = candidateToTest.lastKey(); //(String)candidate.toArray()[1];
								
								adjacencyMatrix.setEntry(binToIntMapping.get(bin1),
										binToIntMapping.get(bin2),
										newNumber(supp));
										//new Integer(1));
								adjacencyMatrix.setEntry(binToIntMapping.get(bin2),
										binToIntMapping.get(bin1),
										newNumber(supp));
										//new Integer(1));
							}
						}
					}
				}
			}
			//i++;
			/*if((i % 1000) == 0) {
				// Nano second is one billionth of a second. Note, 10^9 = 1000000000.
				// Some rounding occurs with conversion from long to float, but we assume this is ok
				double elapsedTime = (System.nanoTime() - startTime) / 1000000000f;
				startTime = System.nanoTime();
				log.info("i = " + i + " and exec time (s) = " + elapsedTime);
			}*/
		}
	}
	
	
}
