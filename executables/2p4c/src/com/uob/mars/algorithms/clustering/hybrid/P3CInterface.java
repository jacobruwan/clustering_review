package com.uob.mars.algorithms.clustering.hybrid;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import com.uob.mars.data.MarsInfoEntity;
import com.uob.mars.db.SubspaceClustersEntry;
import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;


/**
 * 
 * @fileName	P3CInterface.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

/**
 * Interface for projected clustering algorithms based on P3C
 * 
 * @param <T> Type should be either Integer for (Boolean) P3C implementation
 * or Double for (real-valued) 2P4C implementation 
 */
public interface P3CInterface<T> {
	
	
	public P3CInterval<? extends Number> newInterval();
	
	
	public P3CInterval<? extends Number> newInterval(String key);
	
	
	public P3CInterval<? extends Number> newInterval(List<String> keys);
	
	
	public P3CSupportInterface newSupport(boolean supportAsList);
	
	
	/**
	 * Used when creating a value in an adjacency matrix.
	 * We use an interface to create different implementations that are specific to each algorithm.
	 * Crisp implementation should return 1
	 * Fuzzy implementation should return a real value (e.g., supp / number of records)
	 * 
	 * @param support
	 * @return
	 */
	public Number newNumber(Number number);
	
	
	/**
	 * Initialise with type of implementation.
	 * @return
	 */
	public Matrix<? extends Number> createAdjacencyMatrix();
	
	/**
	 * Initialise with type of implementation.
	 * @param size
	 * @return
	 */
	public Matrix<? extends Number> createAdjacencyMatrix(int size);
	
	
	/**
	 * Candidate pruning occurs by testing the candidate k-itemsets and removing
	 * those candidates that are do not meet some criteria. The criteria in Apriori
	 * is support, and in P3C it is support and a Poisson threshold.
	 * 
	 * @param candidates Generated candidate k-itemsets that are to be tested.
	 * Support values should be calculated with supportCount() before calling
	 * this function.
	 * @param frequentKminus1Signatures Frequent (k-1)-itemsets used for pruning.
	 * @param adjacencyMatrix Adjacency matrix of adjacencies between marked bins.
	 * The caller of this function is responsible for initialising the structure.
	 * This function populates the structure with adjacency relationships.
	 * @param binToIntMapping Maps a bin name to an integer index, so it can be
	 * used in adjacencyMatrix.
	 * @return
	 */
	@Deprecated
	public boolean testWithPoissonBasedCriterionForCreatingIntervals(
			HashMap<P3CInterval<? extends Number>, P3CSupportInterface> candidates,
			TreeMap<P3CInterval<? extends Number>, P3CSupportInterface> frequentKminus1Signatures,
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping);
	
	
	/**
	 * Same as testWithPoissonBasedCriterionForCreatingIntervals() but a
	 * candidate is tested after a set number (see batchCandidates variable)
	 * are generated. This avoids storing all generated candidates in one data
	 * structure before testing them.
	 * 
	 * @param frequentKminus1Signatures
	 * @param adjacencyMatrix
	 * @param binToIntMapping
	 * @return
	 */
	public boolean testWithPoissonBasedCriterionForCreatingIntervalsEfficient(
			TreeMap<P3CInterval<? extends Number>, P3CSupportInterface> frequentKminus1Signatures,
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping);
	
	
	/**
	 * 
	 * @param adjacencyMatrix
	 * @param row
	 * @param column
	 * @return
	 */
	public boolean isConnected(Matrix<? extends Number> adjacencyMatrix, int row, int column);
	
	
	/**
	 * 
	 * Test, for all combinations
		supp(candidate k-signature) > (supp(k-1-sub-signature) * width(S'))
		Requires:
		- candidate k-signatures
		- k-1-sub-signatures
	 *
	 * The Poisson significance test must match that used in testWithPoissonBasedCriterion()
	 *
	 * @param candidates Collection of p-signatures (similar to k-itemsets). 
	 * @param candidatesKMinus1Signatures Collection of p-signatures (similar 
	 * to k-l-itemsets). 
	 * @param k Size of signature.
	 */
	public boolean testKItemsCands(
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidates,
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidatesKMinus1Signatures,
			int k);
	
	
	/**
	 * Allows for different methods of creating categorical intervals.
	 * 
	 * @param adjacencyMatrix
	 * @param binToIntMapping
	 * @return
	 */
	/*public TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> generateCategoricalIntervals(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping);*/
	
	
	/**
	 * Takes connected components of a graph for one attribute. Determines the
	 * intervals from the connected components. This is where P3C and 2P4C have
	 * a large difference in algorithm.
	 * 
	 * @param intervalsForAttribute
	 * @param adjMatAllBinBinPairs of 
	 * @param key Index of attribute
	 * @param components Connected components of a graph
	 * @param binToIntMapping Mapping from an attribute-value pair (i.e., bin) to an index
	 * @param valueToIntMapping Mapping from attribute value to an index
	 * @param adjacencyMatrixAttributes
	 * @param binToIntMappingAttributes
	 */
	public void generateCategoricalIntervalFromGraphComponent(
			TreeSet<P3CInterval<? extends Number>> intervalsForAttribute,
			Matrix<? extends Number> adjMatAllBinBinPairs,
			Short key,
			TreeMap<Short, List<List<Integer>>> components,
			TreeMap<String, Integer> binToIntMapping,
			TreeMap<String, Integer> valueToIntMapping,
			TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes,
			TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes);
	
	
	/**
	 * Log a start message. Used to identify which implementation is running.
	 */
	public void logExecutionStart();
	
	
	/**
	 * Log an end message. Used to identify which implementation is running.
	 */
	public void logExecutionEnd();
	
	
	/**
	 * Count support of 1-signatures
	 * 
	 * @param candidates
	 * @param intervals
	 * @return
	 */
	public boolean test1ItemsSigs(
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidates,
			TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> intervals);
	
	
	/**
	 * 
	 * @param interval
	 * @param entity
	 * @return
	 */
	public Number getMembership(TreeMap<Short, P3CInterval<? extends Number>> signature, MarsInfoEntity entity);
	
	
	/**
	 * Increment support object based on implemented method, i.e., crisp,
	 * possibilistic, fuzzy etc.
	 * 
	 * @param support
	 * @param membership
	 * @param recordNumber
	 * @return
	 */
	public boolean incrementSupport(P3CSupportInterface support, Number membership/*, int recordNumber*/);
	
	
	/**
	 * Calculate statistics of result clusters.
	 */
	public void calculateStats();
	
	
	/**
	 * 
	 * @return
	 */
	public String getSupport(P3CSupportInterface support);
	
	
	/**
	 * Implementations may have different support measures, such as crisp and
	 * fuzzy, that can only be accessed by the implementation.
	 * 
	 * @param cluster
	 * @param clusterId
	 * @param support
	 * @param paramId
	 * @return
	 */
	public SubspaceClustersEntry createSubspaceClustersEntry(TreeMap<String, P3CInterval<? extends Number>> cluster,
			int clusterId,
			P3CSupportInterface support,
			int paramId);
	
	
}
