package com.uob.mars.algorithms.clustering.hybrid;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.uob.mars.csv.CSV;
import com.uob.mars.db.SubspaceClustersEntry;
import com.uob.mars.maths.distributions.Poisson;
import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.maths.matrices.MatrixCrisp;
import com.uob.mars.util.Parameters;
import com.uob.mars.data.MarsInfo;
import com.uob.mars.data.MarsInfoEntity;
import com.uob.mars.algorithms.clustering.hybrid.P3C;
import com.uob.mars.algorithms.clustering.hybrid.ProjectedClusteringCommandLineOptions;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CIntervalCrisp;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportCrisp;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;



/**
 * 
 * @fileName	P3CCrisp.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

/**
 * Standard, vanilla P3C implementation for categorical data.
 * 
 * Reference:
 * Gabriela Moise, J�rg Sander, Martin Ester (2008) Robust projected
 * clustering. Knowledge and Information Systems. 14 (3), pp. 273-298.
 * 
 * @param <E> refers to P3CInterval<E>, which is P3CIntervalCrisp.
 */
public class P3CVanilla extends P3C<P3CIntervalCrisp> {
	
	
	private static final Logger log = Logger.getLogger(P3CVanilla.class);
	
	
	private String className = new String("P3CVanilla");
	
	
	/**
	 * 
	 * @param marsInfo
	 * @param poissonThreshold
	 * @param poissonThresholdIntGen
	 * @param batchCandidates
	 */
	P3CVanilla (MarsInfo marsInfo, double poissonThreshold,
			double poissonThresholdIntGen, int batchCandidates) {
		
		super(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
		
		// Must call after super, otherwise arguments arguments will not have been initialised
		//arguments.put("poissonThreshold", Double.toString(poissonThreshold));
		
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public P3CIntervalCrisp newInterval() {
		
		return new P3CIntervalCrisp();
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public P3CIntervalCrisp newInterval(String str) {
		
		P3CIntervalCrisp bin = new P3CIntervalCrisp();
		bin.put(str, new Integer (1));
		
		return bin;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public P3CInterval<? extends Number> newInterval(List<String> keys) {
		
		P3CIntervalCrisp bin = new P3CIntervalCrisp();
		
		Iterator<String> iterKeys = keys.iterator();
		
		while(iterKeys.hasNext()) {
			
			bin.put(iterKeys.next(), new Integer(1));
		}
		
		return bin;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public P3CSupportInterface newSupport(boolean recordSupportAsList) {
		
		return new P3CSupportCrisp(recordSupportAsList);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public MatrixCrisp createAdjacencyMatrix() {
		
		return new MatrixCrisp(new Integer(0));
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public MatrixCrisp createAdjacencyMatrix(int size) {
		
		return new MatrixCrisp(size, new Integer(0));
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isConnected(Matrix<? extends Number> adjacencyMatrix, int row, int column) {
		
		return adjacencyMatrix.compareToEntry(row,  column, new Integer(1)) == 0 ? true : false;
	}
	
	
	/**
	 * 
	 */
	public Number getMembership(TreeMap<Short, P3CInterval<? extends Number>> signature, MarsInfoEntity entity) {
		
		return new Integer(1);
	}
	
	/**
	 * Uses crisp support
	 * 
	 * {@inheritDoc}
	 */
	public boolean incrementSupport(P3CSupportInterface support, Number membership/*, int recordNumber*/) {
		
		return support.addSupport(/*recordNumber*/);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public boolean testKItemsCands(
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidates,
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidatesKMinus1Signatures,
			int k) {
		
		Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterCandidates;
		Poisson poissonDistribution = new Poisson();
		
		//////////////////////////////////////////////////////////////////////
		//2. Check downward closure property (i.e., prune).
		//////////////////////////////////////////////////////////////////////
		iterCandidates = candidates.keySet().iterator();
		
		log.info("Number of candidates before pruning = " + candidates.size());
		
		while(iterCandidates.hasNext()) {
			
			TreeMap<Short, P3CInterval<? extends Number>> candidateSignature =
					iterCandidates.next();
			
			Iterator<Short> signatureInterval =
					candidateSignature.keySet().iterator();
			
			// Number of times the set of both conditions are satisfied.
			int conditionsSatisfied = 0;
			
			// For any q-signature (a sub-signature Q of candidateSignature S in Definition 3.5)
			while(signatureInterval.hasNext()) {
				
				// intervalLabel is equivalent to S' in Definitions 3.2 and 3.5
				Short intervalLabel = signatureInterval.next();
				
				int supp = candidates.get(candidateSignature).getSupport();
				
				// Save some time: if candidateSignature S has crisp support of
				// 0 then condition 1 of Definition 3.5 will not hold, so don't
				// both wasting computation time
				if(supp > 0) {
					
					// A sub-signature. Equivalent to Q in Definition 3.5
					TreeMap<Short, P3CInterval<? extends Number>> candidateSubSignature =
							new TreeMap<Short, P3CInterval<? extends Number>>();
					
					// A one-signature, i.e., this is full-signature - sub-signature.
					// Equivalent to S' in Definition 3.5
					/*TreeMap<String, P3CInterval<? extends Number>> candidateOneSignature =
							new TreeMap<String, P3CInterval<? extends Number>>();*/
					
					Iterator<Short> subSignatureInterval =
							candidateSignature.keySet().iterator();
					
					while(subSignatureInterval.hasNext()) {
						
						Short intervalLabelSub = subSignatureInterval.next();
						
						//if(!intervalLabel.equalsIgnoreCase(intervalLabelSub)) {
						if(intervalLabel != intervalLabelSub) {
							
							candidateSubSignature.put(intervalLabelSub,
									candidateSignature.get(intervalLabelSub));
						}
					}
					
					P3CSupportInterface supportSubSig =
							candidatesKMinus1Signatures.get(candidateSubSignature);
					
					// If candidateSubSignature matches
					if(supportSubSig != null) {
						
						double width = candidateSignature.get(intervalLabel).size();
						
						/*
						"For numerical data sets, we can assume, without restricting
						the generality, that all attributes have normalized values
						... [0,1]" in second paragraph of Section 2. Hence, width
						for a numeric attribute is [0,1]. Although it is not stated
						in the paper, we assume that the width for categorical
						attributes is also normalised.
						*/
						width /= markedBinsAttributeCardinality.get(intervalLabel).doubleValue();
						
						double eSupp = (double)(supportSubSig.getSupport()) * width;
						
						// Definition 3.5, condition 1, part 1
						if((double)supp > eSupp) {
							
							// Definition 3.5, condition 1, part 2
							if(poissonDistribution.test(supp, eSupp, this.poissonThreshold)) {
								
								conditionsSatisfied++;
							}
						}
					}
				}
			}
			
			// Signature must be statistically larger than all of its
			// sub-signatures. If not then it is removed.
			if(conditionsSatisfied != candidateSignature.size()) {
				// Remove last candidate returned by iterator
				iterCandidates.remove();
			}
		}
		
		log.info("Number of candidates after pruning = " + candidates.size());
		
		return true;
	}
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * 
	 */
	@Override
	public void generateCategoricalIntervalFromGraphComponent(
			TreeSet<P3CInterval<? extends Number>> intervalsForAttribute,
			Matrix<? extends Number> adjMatAllBinBinPairs,
			Short key,
			TreeMap<Short, List<List<Integer>>> components,
			TreeMap<String, Integer> binToIntMapping,
			TreeMap<String, Integer> valueToIntMapping,
			TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes,
			TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes) {
		
		for(List<Integer> component : components.get(key)) {
			
			P3CInterval<? extends Number> interval = this.newInterval();
			
			Iterator<String> iterBinLabels = valueToIntMapping.keySet().iterator();
			
			while(iterBinLabels.hasNext()) {
				
				String binLabel = iterBinLabels.next();
				
				if(component.contains(valueToIntMapping.get(binLabel).intValue())) {
					
					interval.add(binLabel);
				}
			}
			
			intervalsForAttribute.add(interval);
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public void logExecutionStart() {
		
		log.info(className + ": started execution of algorithm");
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public void logExecutionEnd() {
		
		log.info(className + ": finished execution of algorithm");
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public void calculateStats() {
		
		Iterator<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> clusterCoresIterator =
				clusterCores.iterator();
		
		int k = 1;
		int pClusterCoresI = 0;
		Map<Integer, Integer> numberOfClustersPerK = new TreeMap<Integer, Integer>();
		Map<Integer, Integer> averageFuzzySupportPerK = new TreeMap<Integer, Integer>();
		
		while(clusterCoresIterator.hasNext()) {
			
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> pClusterCores =
					clusterCoresIterator.next();
			
			Iterator<TreeMap<Short, P3CInterval<? extends Number>>> pClusterCoresIterator =
					pClusterCores.navigableKeySet().iterator();
			
			int numberOfKClusters = 0;
			int averageSupportForK = 0;
			
			while(pClusterCoresIterator.hasNext()) {
				
				// get the cluster
				TreeMap<Short, P3CInterval<? extends Number>> pClusterCore =
						pClusterCoresIterator.next();
				
				// Perform stats
				pClusterCoresI++;
				//kCluster++;				
				averageSupportForK += pClusterCores.get(pClusterCore).getSupport();
				numberOfKClusters++;
			}
			
			if(numberOfKClusters != 0) {
				averageSupportForK /= numberOfKClusters;
			}
			
			numberOfClustersPerK.put(new Integer(k), new Integer(numberOfKClusters));
			averageFuzzySupportPerK.put(new Integer(k), new Integer(averageSupportForK));
			
			k++;
		}
		
		log.info("Number of clusters = " + pClusterCoresI);
		
		for(Integer key : numberOfClustersPerK.keySet()) {
			
			log.info(key + "-signature cluster" +
					": qty. = " + numberOfClustersPerK.get(key) +
					", avg. (crisp) support = " + averageFuzzySupportPerK.get(key));
		}
	}
	
	
	public String getSupport(P3CSupportInterface support) {
		
		String str = new String();
		str += "crisp support = " + support.getSupport();
		return str;
	}
	
	
	/**
	 * This implementation only adds crisp support, and fills fuzzy support
	 * with 0.0.
	 * 
	 * {@inheritDoc}
	 */
	public SubspaceClustersEntry createSubspaceClustersEntry(
			TreeMap<String, P3CInterval<? extends Number>> cluster,
			int clusterId,
			P3CSupportInterface support,
			int paramId) {
		
		SubspaceClustersEntry sce = 
				SubspaceClustersEntry.create(cluster,
						clusterId,
						support.getSupport(),
						0.0, // Fill fuzzy support with 0.0
						paramId);
		
		return sce;
	}
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * number is only included in the interface for fuzzy implementations.
	 * This crisp implementation just uses 1. 
	 */
	@Override
	public Number newNumber(Number number) {
		return new Integer(1);
	}
	
	
	public static void main (String[] args) {
		
		String filename;
		double poissonThreshold = 1e-10;
		double poissonThresholdIntGen = poissonThreshold;
		int limit = 0;
		int batchCandidates = 0;
		Parameters parameters = new Parameters();
		ProjectedClusteringCommandLineOptions cmdLineOptions = new ProjectedClusteringCommandLineOptions();
		P3CVanilla p3CVanilla;
		MarsInfo marsInfo;
		CSV csv = new CSV();
		String value;
		
		
		//
		// SETUP PARAMETERS AND GET COMMAND LINE ARGUMENTS
		//
		parameters.putParameterMin("poisson-threshold", new String("1e-100"));
		parameters.putParameterMax("poisson-threshold", new String("1e-1"));
		
		if(!cmdLineOptions.parseCommandLine(parameters, args)) {
			log.error("Not all required arguments were provided on the command line");
		}
		
		// From here onwards, it is safe to assume that all required arguments
		// were supplied at the command line.
		
		value = parameters.getArgument("limit");
		if(value != null) {
			limit = new Integer (value).intValue();
		}
		
		value = parameters.getArgument("poisson-threshold");
		if(value != null) {
			poissonThreshold = new Double (value).doubleValue();
		}
		
		value = parameters.getArgument("poisson-threshold-int-gen");
		if(value != null) {
			poissonThresholdIntGen = new Double (value).doubleValue();
		} else {
			poissonThresholdIntGen = poissonThreshold;
		}
		
		value = parameters.getArgument("batch-candidates");
		if(value != null) {
			batchCandidates = new Integer (value).intValue();
		}
		
		filename = parameters.getArgument("file");
		
		
		//
		// INITIALISE OBJECTS FOR ALGORITHM
		//
		marsInfo = csv.getInfo(0, 0, filename, limit);
		//marsInfoOld = new MarsInfoOld(marsInfo);
		p3CVanilla = new P3CVanilla(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
		
		//System.exit(0);
		
		//
		// RUN ALGORITHM
		//
		if(marsInfo.getEntities().size() > 0) {
			
			long startTime = System.nanoTime();
			boolean successfulRun = p3CVanilla.run();
			// Nano second is one billionth of a second. Note, 10^9 = 1000000000.
			// Some rounding occurs with conversion from long to float, but we assume this is ok
			double elapsedTime = (System.nanoTime() - startTime) / 1000000000f;
			log.info("Execution time (seconds): " + elapsedTime);
			
			if(successfulRun == false) {
				log.warn("P3C returned false");
			}
			
			List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> maximalKSignatures =
					p3CVanilla.getSubspaceClustersAndTIDs();
			
			/*
			log.info("Writing formatted xlsx file");
			List<String[]> csvRecords = CSVUtils.csvToList("data" + File.separator + filename + ".csv");
			MSExcelFormatter formatter = new MSExcelFormatter();
			// formatSubspaceCluster() adds xlsx extension 
			formatter.formatSubspaceClusterInXlsx(csvRecords, maximalKSignatures, "data" + File.separator + filename);
			*/
			
			//log.info("Number of subspace clusters: " + clusterCores.size());
			
			
			log.info("Writing text file of subspace clusters ...");
			try {
				PrintWriter out = new PrintWriter("data" + File.separator + filename + ".txt");
				out.println(maximalKSignatures.toString());
				out.close();
			} catch (IOException e) {
				log.error("Error writing txt file for subspace clusters", e);
			}
			
			
			//log.info("Updating MarsInfoEntity");
			//p3cCrisp.updateSaturnInfoEntity();
			
			log.info("Computing stats");
			p3CVanilla.calculateStats();
			
			log.info("Computing performance measures");
			p3CVanilla.measurePeformance();
			
			/*
			new CSVExport().outputToCsv(new MarsMarsInfonfo.getSaturnInfoEntities()));
			log.warn("THE CSV HEADERS MUST BE CORRECTED");
			
			
			log.info("Serialising subspace clusters ...");
			List<TreeMap<String, TreeSet<String>>> subspaceClusters =
					Utils.formatSubspaceClustersForSaturnSerObject(p3cv.getSubspaceClustersAndTIDs());
			//		p3cv.formatSubspaceClustersForSaturn();
			
			if(subspaceClusters == null) {
				log.warn("Formatted subspaceClusters is null");
			}
			
			if(!Egress.writeSerialisedObject(subspaceClusters, "data" + File.separator + folder + datasetName + ".ser")) {
				log.error("Failed to serialise subspaceClusters");
			}
			*/
			
			
			log.info("Writing subspace clusters to H2 database ...");
			
			MarsInfoEntity firstEntity = marsInfo.getEntities().get(0);
			MarsInfoEntity lastEntity = marsInfo.getEntities().get(marsInfo.getEntities().size() - 1);
			Timestamp start = new Timestamp(firstEntity.getStartTime().getTime());
			Timestamp end = new Timestamp(lastEntity.getStartTime().getTime());
			
			if(!p3CVanilla.saveClusterCoresToDatabase(p3CVanilla.getClass().getSimpleName(),
					filename, start, end)) {
				log.error("saveClusterCoresToDatabase() returned false");
			}
			
			
		} else {
			log.error("No instances in dataset");
		}
	}
	
	
}
