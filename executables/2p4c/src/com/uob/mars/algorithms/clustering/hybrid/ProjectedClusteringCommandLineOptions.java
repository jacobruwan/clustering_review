/**
 * 
 * @fileName	P3CCommandLineOptions.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */
package com.uob.mars.algorithms.clustering.hybrid;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import com.uob.mars.util.Parameters;
import com.uob.mars.util.CommandLineOptionsInterface;


/**
 * An implementation of CommandLineOptionsInterface for parsing P3C command
 * line arguments.
 */
public class ProjectedClusteringCommandLineOptions implements CommandLineOptionsInterface {
	
	
	private static final Logger log = Logger.getLogger(ProjectedClusteringCommandLineOptions.class);
	
	
	/**
	 * For parsing command line arguments
	 */
	private CommandLineParser parser;
	
	
	/**
	 * For specifying the options available to command line
	 */
	private Options options;
	
	
	/**
	 * Main constructor
	 */
	public ProjectedClusteringCommandLineOptions() {}
	
	
	/**
	 * Create and setup the parser of command line arguments.
	 * @param parameters
	 */
	private void setupCommandLineOptions(Parameters parameters) {
		
		// create the command line parser
		//parser = new BasicParser();
		parser = new GnuParser();
		
		// create the Options
		options = new Options();
		options.addOption( "f", "file", true,
				"Dataset in CSV file format. First line annotates each column " +
				"with type of data -- \"categories\" for class label, \"tags\" " +
				"for data. Second line annotates each column with a variable name.");
		options.addOption( "l", "limit", true,
				"Limit the number of records read from file to this specified number.");
		options.addOption( "p", "poisson-threshold", true,
				"The poisson threshold. Minimum value is " +
				parameters.getParameterMin("poisson") + " and the maximum is " +
				parameters.getParameterMax("poisson") + ".");
		options.addOption( "P", "poisson-threshold-int-gen", true,
				"The poission threshold used only for generating intervals." +
				"Minimum value is " +
				parameters.getParameterMin("poisson") + " and the maximum is " +
				parameters.getParameterMax("poisson") + ".");
		options.addOption("b", "batch-candidates", true, "Produces candidates in " +
				"batches of given size. This is useful when there area lot of " +
				"2-signatures generated for creating intervals");
	}
	
	
	@Override
	public boolean parseCommandLine(Parameters parameters, String[] args) {
		
		setupCommandLineOptions(parameters);
		
		try {
			// parse the command line arguments
			CommandLine line = parser.parse(options, args);
			
			// Validate command line arguments
			if(!testKeyAndStoreValue(parameters, line, "file")){
				log.error("Command line option file is missing");
				return false;
			}
			
			testKeyAndStoreValue(parameters, line, "limit");
			testKeyAndStoreValue(parameters, line, "poisson-threshold");
			testKeyAndStoreValue(parameters, line, "poisson-threshold-int-gen");
			testKeyAndStoreValue(parameters, line, "batch-candidates");
			
		}
		catch(ParseException exp) {
			log.error("Unexpected exception when parsing command line arguments: " + exp.getMessage());
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 
	 * @param parameters
	 * @param line
	 * @param key
	 * @return
	 */
	private boolean testKeyAndStoreValue(Parameters parameters, CommandLine line, String key) {
		
		String value;
		
		if(line.hasOption(key)) {
			
			value = line.getOptionValue(key);
			parameters.putArgument(key, value);
			
			log.info("Command line option " + key + " = " + value);
			
			return true;
		}
		return false;
	}
	
	
}
