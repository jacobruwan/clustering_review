package com.uob.mars.algorithms.clustering.hybrid;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.List;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;
import com.uob.mars.csv.CSV;
import com.uob.mars.data.DataUtils;
import com.uob.mars.data.MarsInfo;
import com.uob.mars.data.MarsInfoEntity;
import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.util.Parameters;
import com.uob.mars.util.PrimitiveDataTypes;


/**
 * 
 * @fileName	ProjectedClusteringWithTrannyTest.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */


public class ProjectedClusteringWithThresholdCondition extends P3CVanilla {
	
	
	private static final Logger log = Logger.getLogger(ProjectedClusteringWithThresholdCondition.class);
	
	
	public ProjectedClusteringWithThresholdCondition(MarsInfo marsInfo, double poissonThreshold,
			double poissonThresholdIntGen, int batchCandidates) {
		super(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
	}
	
	
	/**
	 * Find all bin-bin pairs for each attribute.
	 * 
	 * If there is an adjacency relation then add that relationship to an
	 * auxiliary n x n matrix where n is the number of values of that
	 * variable. The n x n matrix appears to be directed because we only
	 * set (i,j) and not the counterpart (j,i), which would be undirected.
	 * We do this to save on time and space.
	 * 
	 * @param adjacencyMatrix Graph of all attributes-value entries.
	 * @param binToIntMapping adjacencyMatrix is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @param adjacencyMatrixAttributes Maps an attribute to its adjacency matrix.
	 * @param binToIntMappingAttributes adjacencyMatrixAttributes is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @return
	 */
	@Override
	public boolean generateGraphs(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping,
			TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes,
			TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes) {
		
		String attributeLabelRowPrevious = "";
		
		Object[] keysAdjacencyMatrix = binToIntMapping.keySet().toArray();
		
		Matrix<? extends Number> adjacencyMatrixAttribute =
				this.createAdjacencyMatrix(1);
		TreeMap<String, Integer> binToIntMappingAttribute =
				new TreeMap<String, Integer>();
		
		int innerRows = 0;
		int outerRows = 0;
		
		// Threshold for number of adjacency relationships
		int numberOfAdjRelsThreshold = 3;
		
		//
		// PREPARE (GRAPH) DATA STRUCTURES
		//
		// Create one graph for each attribute
		for(; outerRows < keysAdjacencyMatrix.length; outerRows++) {
			
			String[] binOuterRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[outerRows], this.marsInfo.getSplitKeyword());
			String attributeLabelRow = binOuterRow[0];
			String attributeValueRow = binOuterRow[1];
			
			// If a different attribute
			if(!attributeLabelRow.equals(attributeLabelRowPrevious)) {
				
				// If not the first iteration
				if(outerRows != 0) {
					
					Short attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowPrevious));
					
					if(null != adjacencyMatrixAttributes.put(
							attributeId,
							adjacencyMatrixAttribute)) {
						log.error("Overwritten a key");
					}
					
					binToIntMappingAttributes.put(attributeId, binToIntMappingAttribute);
				}
				
				// Create a new graph
				adjacencyMatrixAttribute = createAdjacencyMatrix(1);
				binToIntMappingAttribute =
						new TreeMap<String, Integer>();
				
				// reset inner loop counter
				innerRows = 0;
				
			} else {
			
				// Add attribute value to the graph, i.e., just increase its size.
				if(!adjacencyMatrixAttribute.growByOne()) {
					log.error("Failed to grow adjacency matrix by one");
				}
			}
			
			binToIntMappingAttribute.put(attributeValueRow, innerRows);
			
			innerRows++;
			
			attributeLabelRowPrevious = attributeLabelRow;
		}
		
		// Don't forget the last attribute!
		Short attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowPrevious));
		adjacencyMatrixAttributes.put(attributeId, adjacencyMatrixAttribute);
		binToIntMappingAttributes.put(attributeId, binToIntMappingAttribute);
		
		//
		// POPULATE GRAPHS WITH ADJACENCIES
		//
		/*
		 * For every pair of values (vertically in adjacencyMatrix) from the same attribute. 
		 * Find at least 
		 **/
		for(outerRows = 0; outerRows < keysAdjacencyMatrix.length; outerRows++) {
			
			String[] binOuterRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[outerRows], this.marsInfo.getSplitKeyword());
			String attributeLabelRowOuter = binOuterRow[0];
			String attributeValueRowOuter = binOuterRow[1];
			
			for(innerRows = outerRows + 1; innerRows < keysAdjacencyMatrix.length; innerRows++) {
				
				String[] binInnerRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[innerRows], this.marsInfo.getSplitKeyword());
				String attributeLabelRowInner = binInnerRow[0];
				String attributeValueRowInner = binInnerRow[1];
				
				// Now analysing a different attribute
				if((!attributeLabelRowInner.equals(attributeLabelRowOuter))) {
					break; // out of inner loop
				}
				
				// Analyse adjacency by looping through every bin in all other
				// attributes.
				
				// Number of adjacency relationships
				int numberOfAdjRels = 0;
				
				for(int cols = 0; cols < keysAdjacencyMatrix.length; cols++) {
					
					String[] binColumn = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[cols], this.marsInfo.getSplitKeyword());
					String attributeLabelColumn = binColumn[0];
					
					// Ensure we look at different attributes
					if(!attributeLabelColumn.equals(attributeLabelRowOuter)) {
						
						// If an "adjacency" is present
						// (at least one relationship)
						if(isConnected(adjacencyMatrix, binToIntMapping.get(keysAdjacencyMatrix[outerRows]), binToIntMapping.get(keysAdjacencyMatrix[cols]))
								&&
								isConnected(adjacencyMatrix, binToIntMapping.get(keysAdjacencyMatrix[innerRows]), binToIntMapping.get(keysAdjacencyMatrix[cols]))) {
							
							numberOfAdjRels++;
							
							if(numberOfAdjRels > numberOfAdjRelsThreshold) {
								
								attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowOuter));
								
								int attributeMatrixRow =
										binToIntMappingAttributes.get(attributeId).get(attributeValueRowOuter);
								int attributeMatrixColumn =
										binToIntMappingAttributes.get(attributeId).get(attributeValueRowInner);
								
								// Update attribute's matrix to record the
								// "adjacency" relationship between the two
								// values of the same attribute
								Matrix<? extends Number> matrix =
										adjacencyMatrixAttributes.get(attributeId);
								Number value = matrix.newCellValue(1);
								matrix.setEntry(attributeMatrixRow, attributeMatrixColumn, value);
								
								adjacencyMatrixAttributes.get(attributeId).setEntry(attributeMatrixColumn, attributeMatrixRow, value);
								
								// break out of for loop, because we have
								// found minimum number of "adjacency" relationships
								break;
							}
						}
					}
				}
			}
			
			attributeLabelRowPrevious = attributeLabelRowOuter;
		}
		
		return true;
	}
	
	
	public static void main (String[] args) {
		
		String filename;
		double poissonThreshold = 1e-10;
		double poissonThresholdIntGen = poissonThreshold;
		int limit = 0;
		int batchCandidates = 0;
		Parameters parameters = new Parameters();
		ProjectedClusteringCommandLineOptions cmdLineOptions = new ProjectedClusteringCommandLineOptions();
		ProjectedClusteringWithThresholdCondition projectedClusteringWithThresholdCondition;
		MarsInfo marsInfo;
		CSV csv = new CSV();
		String value;
		
		
		//
		// SETUP PARAMETERS AND GET COMMAND LINE ARGUMENTS
		//
		parameters.putParameterMin("poisson-threshold", new String("1e-100"));
		parameters.putParameterMax("poisson-threshold", new String("1e-1"));
		
		if(!cmdLineOptions.parseCommandLine(parameters, args)) {
			log.error("Not all required arguments were provided on the command line");
		}
		
		// From here onwards, it is safe to assume that all required arguments
		// were supplied at the command line.
		
		value = parameters.getArgument("limit");
		if(value != null) {
			limit = new Integer (value).intValue();
		}
		
		value = parameters.getArgument("poisson-threshold");
		if(value != null) {
			poissonThreshold = new Double (value).doubleValue();
		}
		
		value = parameters.getArgument("poisson-threshold-int-gen");
		if(value != null) {
			poissonThresholdIntGen = new Double (value).doubleValue();
		} else {
			poissonThresholdIntGen = poissonThreshold;
		}
		
		value = parameters.getArgument("batch-candidates");
		if(value != null) {
			batchCandidates = new Integer (value).intValue();
		}
		
		filename = parameters.getArgument("file");
		
		
		//
		// INITIALISE OBJECTS FOR ALGORITHM
		//
		marsInfo = csv.getInfo(0, 0, filename, limit);
		projectedClusteringWithThresholdCondition =
				new ProjectedClusteringWithThresholdCondition(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
		
		//System.exit(0);
		
		//
		// RUN ALGORITHM
		//
		if(marsInfo.getEntities().size() > 0) {
			
			long startTime = System.nanoTime();
			boolean successfulRun = projectedClusteringWithThresholdCondition.run();
			// Nano second is one billionth of a second. Note, 10^9 = 1000000000.
			// Some rounding occurs with conversion from long to float, but we assume this is ok
			double elapsedTime = (System.nanoTime() - startTime) / 1000000000f;
			log.info("Execution time (seconds): " + elapsedTime);
			
			if(successfulRun == false) {
				log.warn("P3C returned false");
			}
			
			List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> maximalKSignatures =
					projectedClusteringWithThresholdCondition.getSubspaceClustersAndTIDs();
			
			/*
			log.info("Writing formatted xlsx file");
			List<String[]> csvRecords = CSVUtils.csvToList("data\\" + filename + ".csv");
			MSExcelFormatter formatter = new MSExcelFormatter();
			// formatSubspaceCluster() adds xlsx extension 
			formatter.formatSubspaceClusterInXlsx(csvRecords, maximalKSignatures, "data\\" + filename);
			*/
			
			//log.info("Number of subspace clusters: " + clusterCores.size());
			
			
			log.info("Writing text file of subspace clusters ...");
			try {
				PrintWriter out = new PrintWriter("data" + File.separator + filename + ".txt");
				out.println(maximalKSignatures.toString());
				out.close();
			} catch (IOException e) {
				log.error("Error writing txt file for subspace clusters", e);
			}
			
			
			//log.info("Updating MarsInfoEntity;
			//p3cCrisp.updateSaturnInfoEntity();
			
			log.info("Computing stats");
			projectedClusteringWithThresholdCondition.calculateStats();
			
			/*log.info("Computing performance measures");
			xPFourC.measurePeformance();*/
			
			/*
			new CSVExport().outputToCsv(new MarsMarsInfonfo.getSaturnInfoEntities()));
			log.warn("THE CSV HEADERS MUST BE CORRECTED");
			
			
			log.info("Serialising subspace clusters ...");
			List<TreeMap<String, TreeSet<String>>> subspaceClusters =
					Utils.formatSubspaceClustersForSaturnSerObject(p3cv.getSubspaceClustersAndTIDs());
			//		p3cv.formatSubspaceClustersForSaturn();
			
			if(subspaceClusters == null) {
				log.warn("Formatted subspaceClusters is null");
			}
			
			if(!Egress.writeSerialisedObject(subspaceClusters, "data\\" + folder + datasetName + ".ser")) {
				log.error("Failed to serialise subspaceClusters");
			}
			*/
			
			
			log.info("Writing subspace clusters to H2 database ...");
			
			MarsInfoEntity firstEntity = marsInfo.getEntities().get(0);
			MarsInfoEntity lastEntity = marsInfo.getEntities().get(marsInfo.getEntities().size() - 1);
			Timestamp start = new Timestamp(firstEntity.getStartTime().getTime());
			Timestamp end = new Timestamp(lastEntity.getStartTime().getTime());
			
			if(!projectedClusteringWithThresholdCondition.saveClusterCoresToDatabase(projectedClusteringWithThresholdCondition.getClass().getSimpleName(),
					filename, start, end)) {
				log.error("saveClusterCoresToDatabase() returned false");
			}
			
			
		} else {
			log.error("No instances in dataset");
		}
	}
	
	
}
