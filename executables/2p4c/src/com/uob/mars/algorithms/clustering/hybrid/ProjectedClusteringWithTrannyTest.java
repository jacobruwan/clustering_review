package com.uob.mars.algorithms.clustering.hybrid;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.log4j.Logger;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;
import com.uob.mars.csv.CSV;
import com.uob.mars.data.DataUtils;
import com.uob.mars.data.MarsInfo;
import com.uob.mars.data.MarsInfoEntity;
import com.uob.mars.maths.distributions.Poisson;
import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.maths.matrices.MatrixCrisp;
import com.uob.mars.util.Parameters;
import com.uob.mars.util.PrimitiveDataTypes;
import com.uob.mars.algorithms.clustering.hybrid.ProjectedClusteringCommandLineOptions;


/**
 * 
 * @fileName	ProjectedClusteringWithTrannyTest.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */


public class ProjectedClusteringWithTrannyTest extends P3CVanilla{
//public class ProjectedClusteringWithTrannyTest extends TwoPFourC{
	
	
	private static final Logger log = Logger.getLogger(ProjectedClusteringWithTrannyTest.class);
	
	
	/**
	 * Poisson threshold for the transitivity test
	 */
	protected double poissonThresholdTransTest = this.poissonThreshold;
	
	
	public ProjectedClusteringWithTrannyTest(MarsInfo marsInfo, double poissonThreshold,
			double poissonThresholdIntGen, int batchCandidates) {
		super(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
	}
	
	
	/**
	 * Used to allow easy commenting out/in of different implementations.
	 * 
	 * @param adjacencyMatrix Graph of all attributes-value entries.
	 * @param binToIntMapping adjacencyMatrix is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @param adjacencyMatrixAttributes Maps an attribute to its adjacency matrix.
	 * @param binToIntMappingAttributes adjacencyMatrixAttributes is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @return
	 */
	@Override
	public boolean generateGraphs(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping,
			TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes,
			TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes) {
		
		return generateGraphsPoissonTest(
		//return generateGraphsChiSquareTest(
				adjacencyMatrix,
				binToIntMapping,
				adjacencyMatrixAttributes,
				binToIntMappingAttributes);
	}
	
	
	/**
	 * Same os overwritten method, however, a chi-square test is performed on
	 * the number of transitive relations.
	 * 
	 * @param adjacencyMatrix Graph of all attributes-value entries.
	 * @param binToIntMapping adjacencyMatrix is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @param adjacencyMatrixAttributes Maps an attribute to its adjacency matrix.
	 * @param binToIntMappingAttributes adjacencyMatrixAttributes is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @return
	 */
	//@Override
	public boolean generateGraphsPoissonTest(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping,
			TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes,
			TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes) {
		
		String attributeLabelRowPrevious = "";
		
		Object[] keysAdjacencyMatrix = binToIntMapping.keySet().toArray();
		
		MatrixCrisp adjacencyMatrixAttribute =
				new MatrixCrisp(1, new Integer(0));
		MatrixCrisp adjacencyMatrixAttributeAdjCount =
				new MatrixCrisp(1, new Integer(0));
				//this.createAdjacencyMatrix(1);
		TreeMap<String, Integer> binToIntMappingAttribute =
				new TreeMap<String, Integer>();
		
		TreeMap<Short, MatrixCrisp> adjacencyMatrixAttributesAdjCounts = 
				new TreeMap<Short, MatrixCrisp>();
		
		Poisson poissonDistribution = new Poisson();
		
		int innerRows = 0;
		int outerRows = 0;
		
		//
		// PREPARE (GRAPH) DATA STRUCTURES
		//
		// Create one graph for each attribute
		for(; outerRows < keysAdjacencyMatrix.length; outerRows++) {
			
			String[] binOuterRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[outerRows], this.marsInfo.getSplitKeyword());
			String attributeLabelRow = binOuterRow[0];
			String attributeValueRow = binOuterRow[1];
			
			// If a different attribute
			if(!attributeLabelRow.equals(attributeLabelRowPrevious)) {
				
				// If not the first iteration
				if(outerRows != 0) {
					
					Short attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowPrevious));
					
					if(null != adjacencyMatrixAttributes.put(
							attributeId,
							adjacencyMatrixAttribute)) {
						log.error("Overwritten a key");
					}
					
					if(null != adjacencyMatrixAttributesAdjCounts.put(
							attributeId,
							adjacencyMatrixAttributeAdjCount)) {
						log.error("Overwritten a key");
					}
					
					binToIntMappingAttributes.put(attributeId, binToIntMappingAttribute);
				}
				
				// Create a new graph
				adjacencyMatrixAttribute = new MatrixCrisp(1, new Integer(0));
				adjacencyMatrixAttributeAdjCount = new MatrixCrisp(1, new Integer(0));
						//createAdjacencyMatrix(1);
				binToIntMappingAttribute =
						new TreeMap<String, Integer>();
				
				// reset inner loop counter
				innerRows = 0;
				
			} else {
			
				// Add attribute value to the graph, i.e., just increase its size.
				if(!adjacencyMatrixAttribute.growByOne()) {
					log.error("Failed to grow adjacency matrix by one");
				}
				if(!adjacencyMatrixAttributeAdjCount.growByOne()) {
					log.error("Failed to grow adjacency matrix (of adjacency counts) by one");
				}
			}
			
			binToIntMappingAttribute.put(attributeValueRow, innerRows);
			
			innerRows++;
			
			attributeLabelRowPrevious = attributeLabelRow;
		}
		
		// Don't forget the last attribute!
		Short attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowPrevious));
		adjacencyMatrixAttributes.put(attributeId, adjacencyMatrixAttribute);
		adjacencyMatrixAttributesAdjCounts.put(attributeId, adjacencyMatrixAttribute);
		binToIntMappingAttributes.put(attributeId, binToIntMappingAttribute);
		
		//
		// POPULATE GRAPHS WITH COUNTS OF ADJACENCIES
		//
		/*
		 * For every pair of values (vertically in adjacencyMatrix) from the same attribute. 
		 * Find at least 
		 **/
		for(outerRows = 0; outerRows < keysAdjacencyMatrix.length; outerRows++) {
			
			String[] binOuterRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[outerRows], this.marsInfo.getSplitKeyword());
			String attributeLabelRowOuter = binOuterRow[0];
			String attributeValueRowOuter = binOuterRow[1];
			
			for(innerRows = outerRows + 1; innerRows < keysAdjacencyMatrix.length; innerRows++) {
				
				String[] binInnerRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[innerRows], this.marsInfo.getSplitKeyword());
				String attributeLabelRowInner = binInnerRow[0];
				String attributeValueRowInner = binInnerRow[1];
				
				// Now analysing a different attribute, so record the average
				// number of adjacency relationships found for an attribute
				if((!attributeLabelRowInner.equals(attributeLabelRowOuter))) {
					break; // out of inner loop
				}
				
				// Analyse adjacency by looping through every bin in all other
				// attributes.
				
				for(int cols = 0; cols < keysAdjacencyMatrix.length; cols++) {
					
					String[] binColumn = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[cols], this.marsInfo.getSplitKeyword());
					String attributeLabelColumn = binColumn[0];
					
					// Ensure we look at different attributes
					if(!attributeLabelColumn.equals(attributeLabelRowOuter)) {
						
						// If an "adjacency" is present
						// (at least one relationship)
						if(isConnected(adjacencyMatrix, binToIntMapping.get(keysAdjacencyMatrix[outerRows]), binToIntMapping.get(keysAdjacencyMatrix[cols]))
								&&
								isConnected(adjacencyMatrix, binToIntMapping.get(keysAdjacencyMatrix[innerRows]), binToIntMapping.get(keysAdjacencyMatrix[cols]))) {
							
							attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowOuter));
							
							int attributeMatrixRow =
									binToIntMappingAttributes.get(attributeId).get(attributeValueRowOuter);
							int attributeMatrixColumn =
									binToIntMappingAttributes.get(attributeId).get(attributeValueRowInner);
							
							MatrixCrisp matrix =
									adjacencyMatrixAttributesAdjCounts.get(attributeId);
							Integer value = matrix.get(attributeMatrixRow, attributeMatrixColumn);
							matrix.setEntry(attributeMatrixRow, attributeMatrixColumn, new Integer(value.intValue() + 1));
							
							//adjacencyMatrixAttributesAdjCounts.get(attributeId).setEntry(attributeMatrixColumn, attributeMatrixRow, value);
							
							// DO NOT break out of for loop, because we are COUNTING "adjacency" relationship
							//break;
						}
					}
				}
			}
			
			attributeLabelRowPrevious = attributeLabelRowOuter;
		}
		
		// Calculate chi square
		
		Iterator<Short> attributeIter = 
				adjacencyMatrixAttributesAdjCounts.keySet().iterator();
		
		while(attributeIter.hasNext()) {
			
			Short attributeKey = attributeIter.next();
			MatrixCrisp matrix =
					adjacencyMatrixAttributesAdjCounts.get(attributeKey);
			
			double avgRel = 0.0;
			int avgCardinality = 0;
			
			for(int row = 0; row < matrix.numberOfRows(); row++) {
				for(int col = 0; col < matrix.numberOfColumns(); col++) {
					int adjRelValue = matrix.get(row, col);
					if(adjRelValue > 0) {
						avgCardinality++;
						avgRel += adjRelValue;
					}
				}
			}
			System.out.println(this.marsInfo.getAttributes().get(attributeKey));
			System.out.println("\tn = " + avgCardinality);
			System.out.println("\tsum = " + avgRel);
			avgRel /= avgCardinality;
			System.out.println("\tmean = " + avgRel);
			
			for(int row = 0; row < matrix.numberOfRows(); row++) {
				
				for(int col = 0; col < matrix.numberOfColumns(); col++) {
					
					int adjRelValue = matrix.get(row, col);
					
					if(adjRelValue > 0) {
						
						// adjRelValue = observed
						// avgRel = expected
						//System.out.println("\tobserved = " + adjRelValue + "\texpected = " + avgRel);
						if(poissonDistribution.test(adjRelValue, avgRel, this.poissonThresholdTransTest)) {
							
							// Add significantly different number of "adjacency"
							// relationships to data structure that is returned.
							adjacencyMatrixAttributes.get(attributeKey).setEntry(row, col, adjRelValue);
							System.out.println("\t" + adjRelValue + " is statistically different");
						}
					}
				}
			}
		}
		
		return true;
	}
	
	
	/**
	 * Chi-square is incorrect choice because it performs a
	 * goodness-of-fit test.
	 * 
	 * Same os overwritten method, however, a chi-square test is performed on
	 * the number of transitive relations.
	 * 
	 * @param adjacencyMatrix Graph of all attributes-value entries.
	 * @param binToIntMapping adjacencyMatrix is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @param adjacencyMatrixAttributes Maps an attribute to its adjacency matrix.
	 * @param binToIntMappingAttributes adjacencyMatrixAttributes is accessed using Integer indexes, so this structure maps the Strings to Integers.
	 * @return
	 */
	//@Override
	public boolean generateGraphsChiSquareTest(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping,
			TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes,
			TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes) {
		
		String attributeLabelRowPrevious = "";
		
		Object[] keysAdjacencyMatrix = binToIntMapping.keySet().toArray();
		
		MatrixCrisp adjacencyMatrixAttribute =
				new MatrixCrisp(1, new Integer(0));
		MatrixCrisp adjacencyMatrixAttributeAdjCount =
				new MatrixCrisp(1, new Integer(0));
				//this.createAdjacencyMatrix(1);
		TreeMap<String, Integer> binToIntMappingAttribute =
				new TreeMap<String, Integer>();
		
		TreeMap<Short, MatrixCrisp> adjacencyMatrixAttributesAdjCounts = 
				new TreeMap<Short, MatrixCrisp>();
		
		int innerRows = 0;
		int outerRows = 0;
		
		//
		// PREPARE (GRAPH) DATA STRUCTURES
		//
		// Create one graph for each attribute
		for(; outerRows < keysAdjacencyMatrix.length; outerRows++) {
			
			String[] binOuterRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[outerRows], this.marsInfo.getSplitKeyword());
			String attributeLabelRow = binOuterRow[0];
			String attributeValueRow = binOuterRow[1];
			
			// If a different attribute
			if(!attributeLabelRow.equals(attributeLabelRowPrevious)) {
				
				// If not the first iteration
				if(outerRows != 0) {
					
					Short attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowPrevious));
					
					if(null != adjacencyMatrixAttributes.put(
							attributeId,
							adjacencyMatrixAttribute)) {
						log.error("Overwritten a key");
					}
					
					if(null != adjacencyMatrixAttributesAdjCounts.put(
							attributeId,
							adjacencyMatrixAttributeAdjCount)) {
						log.error("Overwritten a key");
					}
					
					binToIntMappingAttributes.put(attributeId, binToIntMappingAttribute);
				}
				
				// Create a new graph
				adjacencyMatrixAttribute = new MatrixCrisp(1, new Integer(0));
				adjacencyMatrixAttributeAdjCount = new MatrixCrisp(1, new Integer(0));
						//createAdjacencyMatrix(1);
				binToIntMappingAttribute =
						new TreeMap<String, Integer>();
				
				// reset inner loop counter
				innerRows = 0;
				
			} else {
			
				// Add attribute value to the graph, i.e., just increase its size.
				if(!adjacencyMatrixAttribute.growByOne()) {
					log.error("Failed to grow adjacency matrix by one");
				}
				if(!adjacencyMatrixAttributeAdjCount.growByOne()) {
					log.error("Failed to grow adjacency matrix (of adjacency counts) by one");
				}
			}
			
			binToIntMappingAttribute.put(attributeValueRow, innerRows);
			
			innerRows++;
			
			attributeLabelRowPrevious = attributeLabelRow;
		}
		
		// Don't forget the last attribute!
		Short attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowPrevious));
		adjacencyMatrixAttributes.put(attributeId, adjacencyMatrixAttribute);
		adjacencyMatrixAttributesAdjCounts.put(attributeId, adjacencyMatrixAttributeAdjCount);
		binToIntMappingAttributes.put(attributeId, binToIntMappingAttribute);
		
		//
		// POPULATE GRAPHS WITH COUNTS OF ADJACENCIES
		//
		/*
		 * For every pair of values (vertically in adjacencyMatrix) from the same attribute. 
		 * Find at least 
		 **/
		for(outerRows = 0; outerRows < keysAdjacencyMatrix.length; outerRows++) {
			
			String[] binOuterRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[outerRows], this.marsInfo.getSplitKeyword());
			String attributeLabelRowOuter = binOuterRow[0];
			String attributeValueRowOuter = binOuterRow[1];
			
			for(innerRows = outerRows + 1; innerRows < keysAdjacencyMatrix.length; innerRows++) {
				
				String[] binInnerRow = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[innerRows], this.marsInfo.getSplitKeyword());
				String attributeLabelRowInner = binInnerRow[0];
				String attributeValueRowInner = binInnerRow[1];
				
				// Now analysing a different attribute, so record the average
				// number of adjacency relationships found for an attribute
				if((!attributeLabelRowInner.equals(attributeLabelRowOuter))) {
					break; // out of inner loop
				}
				
				// Analyse adjacency by looping through every bin in all other
				// attributes.
				
				for(int cols = 0; cols < keysAdjacencyMatrix.length; cols++) {
					
					String[] binColumn = DataUtils.splitByFirstOccurrence((String)keysAdjacencyMatrix[cols], this.marsInfo.getSplitKeyword());
					String attributeLabelColumn = binColumn[0];
					
					// Ensure we look at different attributes
					if(!attributeLabelColumn.equals(attributeLabelRowOuter)) {
						
						// If an "adjacency" is present
						// (at least one relationship)
						if(isConnected(adjacencyMatrix, binToIntMapping.get(keysAdjacencyMatrix[outerRows]), binToIntMapping.get(keysAdjacencyMatrix[cols]))
								&&
								isConnected(adjacencyMatrix, binToIntMapping.get(keysAdjacencyMatrix[innerRows]), binToIntMapping.get(keysAdjacencyMatrix[cols]))) {
							
							attributeId = PrimitiveDataTypes.safeIntegerToShort(marsInfo.getAttributesIndex(attributeLabelRowOuter));
							
							int attributeMatrixRow =
									binToIntMappingAttributes.get(attributeId).get(attributeValueRowOuter);
							int attributeMatrixColumn =
									binToIntMappingAttributes.get(attributeId).get(attributeValueRowInner);
							
							MatrixCrisp matrix =
									adjacencyMatrixAttributesAdjCounts.get(attributeId);
							Integer value = matrix.get(attributeMatrixRow, attributeMatrixColumn);
							matrix.setEntry(attributeMatrixRow, attributeMatrixColumn, new Integer(value.intValue() + 1));
							
							//adjacencyMatrixAttributesAdjCounts.get(attributeId).setEntry(attributeMatrixColumn, attributeMatrixRow, value);
							
							// DO NOT break out of for loop, because we are COUNTING "adjacency" relationship
							//break;
						}
					}
				}
			}
			
			attributeLabelRowPrevious = attributeLabelRowOuter;
		}
		
		// Calculate chi square
		
		Iterator<Short> attributeIter = 
				adjacencyMatrixAttributesAdjCounts.keySet().iterator();
		
		while(attributeIter.hasNext()) {
			
			Short attributeKey = attributeIter.next();
			MatrixCrisp matrix =
					adjacencyMatrixAttributesAdjCounts.get(attributeKey);
			
			boolean first = true;
			
			do {
				
				double chiSquare = 0.0;
				double avgRel = 0.0;
				int avgCardinality = 0;
				int dof = 0;
				
				for(int row = 0; row < matrix.numberOfRows(); row++) {
					for(int col = 0; col < matrix.numberOfColumns(); col++) {
						int adjRelValue = matrix.get(row, col);
						if(adjRelValue > 0) {
							avgCardinality++;
							avgRel += adjRelValue;
						}
					}
				}
				//avgRel /= avgCardinality;
				
				if(first) {
					System.out.println(this.marsInfo.getAttributes().get(attributeKey));
					System.out.println("\tn = " + avgCardinality);
					System.out.println("\tsum = " + avgRel);
					avgRel /= avgCardinality;
					System.out.println("\tmean = " + avgRel);
					
					first = false;
				} else {
					avgRel /= avgCardinality;
				}
				
				for(int row = 0; row < matrix.numberOfRows(); row++) {
					for(int col = 0; col < matrix.numberOfColumns(); col++) {
						int adjRelValue = matrix.get(row, col);
						if(adjRelValue > 0) {
							chiSquare += (adjRelValue - avgRel) *
									(adjRelValue - avgRel);
						}
					}
				}
				chiSquare /= avgRel;
				
				ChiSquaredDistribution chiSquDist;
				// dof = n-1
				dof = avgCardinality - 1;
				if (dof > 0) {
					
					chiSquDist= new ChiSquaredDistribution(dof);
					
					/* Reject the null hypothesis of uniform if Test statistic
					 * is greater than critical value. Hence, attribute is non
					 * uniform.
					 */
					double chiSquareTestValue = chiSquDist.density(confidenceLevel);
					if (chiSquare > chiSquareTestValue) {
						
						// find "adjacency" relationship with largest value
						// mark it, i.e., add to a data structure
						// remove relationship from current set
						
						int maxRow = -1; // -1 indicates unset
						int maxCol = -1;
						int maxValue = -1;
						
						for(int row = 0; row < matrix.numberOfRows(); row++) {
							for(int col = 0; col < matrix.numberOfColumns(); col++) {
								int adjRelValue = matrix.get(row, col);
								if(adjRelValue > maxValue) {
									maxValue = adjRelValue;
									maxRow = row;
									maxCol = col;
								}
							}
						}
						
						// Add non-uniform "adjacency" relationship to data
						// structure that is returned.
						adjacencyMatrixAttributes.get(attributeKey).setEntry(maxRow, maxCol, maxValue);
						
						// remove relationship from auxiliary data structure
						adjacencyMatrixAttributesAdjCounts.get(attributeKey).setEntry(maxRow, maxCol, 0);
					}
					// Accept the null hypothesis of uniform
					else {
						/* This is our termination criterion: once the null
						 * hypothesis is accepted, we stop.
						 * Break out of infinite loop
						 */
						break;
					}
					
				} else {
					// dof of 0 means there is only 1 value for this attribute,
					// hence it must be normal, so we do nothing
					break;
				}
			} while (true);
		}
		
		return true;
	}
	
	
	public static void main (String[] args) {
		
		String filename;
		double poissonThreshold = 1e-10;
		double poissonThresholdIntGen = poissonThreshold;
		int limit = 0;
		int batchCandidates = 0;
		Parameters parameters = new Parameters();
		ProjectedClusteringCommandLineOptions cmdLineOptions = new ProjectedClusteringCommandLineOptions();
		ProjectedClusteringWithTrannyTest projectedClusteringWithTrannyTest;
		MarsInfo marsInfo;
		CSV csv = new CSV();
		String value;
		
		
		//
		// SETUP PARAMETERS AND GET COMMAND LINE ARGUMENTS
		//
		parameters.putParameterMin("poisson-threshold", new String("1e-100"));
		parameters.putParameterMax("poisson-threshold", new String("1e-1"));
		
		if(!cmdLineOptions.parseCommandLine(parameters, args)) {
			log.error("Not all required arguments were provided on the command line");
		}
		
		// From here onwards, it is safe to assume that all required arguments
		// were supplied at the command line.
		
		value = parameters.getArgument("limit");
		if(value != null) {
			limit = new Integer (value).intValue();
		}
		
		value = parameters.getArgument("poisson-threshold");
		if(value != null) {
			poissonThreshold = new Double (value).doubleValue();
		}
		
		value = parameters.getArgument("poisson-threshold-int-gen");
		if(value != null) {
			poissonThresholdIntGen = new Double (value).doubleValue();
		} else {
			poissonThresholdIntGen = poissonThreshold;
		}
		
		value = parameters.getArgument("batch-candidates");
		if(value != null) {
			batchCandidates = new Integer (value).intValue();
		}
		
		filename = parameters.getArgument("file");
		
		
		//
		// INITIALISE OBJECTS FOR ALGORITHM
		//
		marsInfo = csv.getInfo(0, 0, filename, limit);
		projectedClusteringWithTrannyTest =
				new ProjectedClusteringWithTrannyTest(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
		
		//System.exit(0);
		
		//
		// RUN ALGORITHM
		//
		if(marsInfo.getEntities().size() > 0) {
			
			long startTime = System.nanoTime();
			boolean successfulRun = projectedClusteringWithTrannyTest.run();
			// Nano second is one billionth of a second. Note, 10^9 = 1000000000.
			// Some rounding occurs with conversion from long to float, but we assume this is ok
			double elapsedTime = (System.nanoTime() - startTime) / 1000000000f;
			log.info("Execution time (seconds): " + elapsedTime);
			
			if(successfulRun == false) {
				log.warn("P3C returned false");
			}
			
			List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> maximalKSignatures =
					projectedClusteringWithTrannyTest.getSubspaceClustersAndTIDs();
			
			/*
			log.info("Writing formatted xlsx file");
			List<String[]> csvRecords = CSVUtils.csvToList("data\\" + filename + ".csv");
			MSExcelFormatter formatter = new MSExcelFormatter();
			// formatSubspaceCluster() adds xlsx extension 
			formatter.formatSubspaceClusterInXlsx(csvRecords, maximalKSignatures, "data\\" + filename);
			*/
			
			//log.info("Number of subspace clusters: " + clusterCores.size());
			
			
			log.info("Writing text file of subspace clusters ...");
			try {
				PrintWriter out = new PrintWriter("data" + File.separator + filename + ".txt");
				out.println(maximalKSignatures.toString());
				out.close();
			} catch (IOException e) {
				log.error("Error writing txt file for subspace clusters", e);
			}
			
			
			//log.info("Updating MarsInfoEntity;
			//p3cCrisp.updateSaturnInfoEntity();
			
			log.info("Computing stats");
			projectedClusteringWithTrannyTest.calculateStats();
			
			/*log.info("Computing performance measures");
			xPFourC.measurePeformance();*/
			
			/*
			new CSVExport().outputToCsv(new MarsMarsInfonfo.getSaturnInfoEntities()));
			log.warn("THE CSV HEADERS MUST BE CORRECTED");
			
			
			log.info("Serialising subspace clusters ...");
			List<TreeMap<String, TreeSet<String>>> subspaceClusters =
					Utils.formatSubspaceClustersForSaturnSerObject(p3cv.getSubspaceClustersAndTIDs());
			//		p3cv.formatSubspaceClustersForSaturn();
			
			if(subspaceClusters == null) {
				log.warn("Formatted subspaceClusters is null");
			}
			
			if(!Egress.writeSerialisedObject(subspaceClusters, "data\\" + folder + datasetName + ".ser")) {
				log.error("Failed to serialise subspaceClusters");
			}
			*/
			
			
			log.info("Writing subspace clusters to H2 database ...");
			
			MarsInfoEntity firstEntity = marsInfo.getEntities().get(0);
			MarsInfoEntity lastEntity = marsInfo.getEntities().get(marsInfo.getEntities().size() - 1);
			Timestamp start = new Timestamp(firstEntity.getStartTime().getTime());
			Timestamp end = new Timestamp(lastEntity.getStartTime().getTime());
			
			if(!projectedClusteringWithTrannyTest.saveClusterCoresToDatabase(projectedClusteringWithTrannyTest.getClass().getSimpleName(),
					filename, start, end)) {
				log.error("saveClusterCoresToDatabase() returned false");
			}
			
			
		} else {
			log.error("No instances in dataset");
		}
	}


}
