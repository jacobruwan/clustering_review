/**
 * 
 * @fileName	P3CSupport.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 * @copyright	(c) British Telecommunications plc, 2013, All Rights Reserved.
 * 
 */
package com.uob.mars.algorithms.clustering.hybrid.Support;

import java.util.ArrayList;
import java.util.List;

import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;


/**
 * TODO: Add comment describing what this class does.
 */
public abstract class P3CSupport implements P3CSupportInterface{
	
	
	/**
	 * Stores transaction IDs (TIDs) of records that support this object.
	 */
	//protected List<Integer> TIDs;
	
	
	/**
	 * Crisp support. Note same value as size of TIDs.
	 */
	protected int support;
	
	
	/**
	 * If true then lists of fuzzy supports are recorded.
	 * Very memory intensive, particularly for larger datasets.
	 */
	//TODO Move to fuzzysupport class
	protected boolean supportAsList;
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addSupport(/*Integer TID*/) {
		
		//if(TID >= 0){
			support++;
			return true;
		/*} else {
			return false;
		}*/
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSupport() {
		
		return support;
	}
	
	
	/*
	@Override
	public List<Integer> getSupportItems() {
		
		if(supportAsList){
			return TIDs;
		} else {
			return new ArrayList<Integer>();
		}
	}
	*/
	
	
	public String toString() {
		return new String("crisp=" + this.getSupport());
	}
}
