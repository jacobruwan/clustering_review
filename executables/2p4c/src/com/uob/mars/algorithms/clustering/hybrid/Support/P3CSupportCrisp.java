/**
 * 
 * @fileName	P3CSupportCrisp.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 * @copyright	(c) British Telecommunications plc, 2013, All Rights Reserved.
 * 
 */
package com.uob.mars.algorithms.clustering.hybrid.Support;


import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupport;


/**
 * Support that uses crisp numbers, i.e., Natural numbers or integer values.
 */
public class P3CSupportCrisp extends P3CSupport {
	
	
	/**
	 * Default constructor
	 */
	public P3CSupportCrisp(boolean supportAsList) {
		
		this.supportAsList = supportAsList;
		
		/*if(supportAsList) {
			TIDs = new ArrayList<Integer>();
		}*/
	}
	
	
}
