/**
 * 
 * @fileName	P3CSupportFuzzy.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 * @copyright	(c) British Telecommunications plc, 2013, All Rights Reserved.
 * 
 */
package com.uob.mars.algorithms.clustering.hybrid.Support;


import java.util.ArrayList;
import java.util.List;

import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupport;



/**
 * Support that uses fuzzy numbers, i.e., Real numbers or double values.
 */
public class P3CSupportFuzzy extends P3CSupport {
	
	
	/**
	 * Stores fuzzy support for each TID. Indexes match those in TIDs of P3CSupport.
	 */
	private List<Double> fuzzySupports;
	
	
	private double fuzzySupport;
	
	
	/**
	 * Default constructor
	 */
	public P3CSupportFuzzy(boolean supportAsList) {
		
		//this.supportAsList = supportAsList;
		
		if(supportAsList) {
			//TIDs = new ArrayList<Integer>();
			fuzzySupports = new ArrayList<Double>();
		}
		
		support = 0;
		fuzzySupport = 0.0;
	}
	
	
	/**
	 * Add a fuzzy support value
	 * 
	 * @param support
	 * @return
	 */
	public boolean addFuzzySupport(Double support) {
		
		fuzzySupport += support.doubleValue();
		
		if(supportAsList){
			return fuzzySupports.add(support);
		} else {
			return true;
		}
	}
	
	
	/**
	 * Get the fuzzy support for this object. This is a total value that uses
	 * sigma count.
	 * 
	 * @return
	 */
	public double getFuzzySupport() {
		
		return fuzzySupport;
	}
	
	
	public String toString() {
		return new String(super.toString() + " fuzzy=" + this.getFuzzySupport());
	}
}
