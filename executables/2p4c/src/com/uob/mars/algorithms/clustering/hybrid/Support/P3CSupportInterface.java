/**
 * 
 * @fileName	P3CSupportInterface.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 * @copyright	(c) British Telecommunications plc, 2013, All Rights Reserved.
 * 
 */
package com.uob.mars.algorithms.clustering.hybrid.Support;

import java.util.List;


/**
 * An interface for storing support counts. This has been designed to allow
 * for crisp and fuzzy supports, and supports containing lists of TIDs.
 */
public interface P3CSupportInterface {
	
	
	/**
	 * Increment crisp support, and, if supportAsList is set to true, append
	 * specified TID to end of internal list. 
	 * 
	 * @param TID transaction ID (TID) to add
	 * @return true upon successful append, false otherwise
	 */
	public boolean addSupport(/*Integer TID*/);
	
	
	/**
	 * Get the support 
	 * @return support value
	 */
	public int getSupport();
	
	
	/**
	 * Get the TIDs that form the support.
	 * @return
	 */
	//public List<Integer> getSupportItems();
	
	
}
