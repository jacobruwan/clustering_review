package com.uob.mars.algorithms.clustering.hybrid;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.uob.mars.data.DataUtils;
import com.uob.mars.data.MarsInfo;
import com.uob.mars.data.MarsInfoEntity;
import com.uob.mars.csv.CSV;
import com.uob.mars.db.SubspaceClustersEntry;
import com.uob.mars.maths.distributions.PoissonContinuous;
import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.maths.matrices.MatrixFuzzy;
import com.uob.mars.util.Parameters;
import com.uob.mars.algorithms.clustering.hybrid.ProjectedClusteringCommandLineOptions;
import com.uob.mars.algorithms.clustering.hybrid.P3C;
import com.uob.mars.algorithms.clustering.hybrid.TwoPFourC;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CComparators;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CIntervalFuzzy;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportFuzzy;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;
import com.uob.mars.algorithms.graphs.Graphs;


/**
 * 
 * @fileName	TwoPFourC.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

/**
 * Non-standard P3C implementation for categorical data that uses
 * possibilistic sets of intervals.
 * 
 * Reference:
 * Stephen G. Matthews and Trevor P. Martin (2014) Possibilistic Projected
 * Categorical Clustering via Cluster Cores. In: Proceedings of the 2014 IEEE
 * International Conference on Fuzzy Systems (FUZZ-IEEE 2014), Beijing, China.
 */
public class TwoPFourC extends P3C<P3CIntervalFuzzy> {
	
	
	private static final Logger log = Logger.getLogger(TwoPFourC.class);
	
	
	private String className = new String("TwoPFourC");
	
	
	/**
	 * 
	 * @param marsInfo
	 * @param poissonThreshold
	 * @param poissonThresholdIntGen
	 * @param batchCandidates
	 */
	public TwoPFourC (MarsInfo marsInfo, double poissonThreshold, double poissonThresholdIntGen, int batchCandidates) {
		
		super(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
		
		// Must call after super, otherwise arguments arguments will not have been initialised
		//arguments.put("poissonThreshold", Double.toString(poissonThreshold));
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public P3CIntervalFuzzy newInterval() {
		
		return new P3CIntervalFuzzy();
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public P3CIntervalFuzzy newInterval(String str) {
		
		P3CIntervalFuzzy bin = new P3CIntervalFuzzy();
		bin.put(str, new Double(1.0));
		
		return bin;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public P3CInterval<? extends Number> newInterval(List<String> keys) {
		
		P3CIntervalFuzzy bin = new P3CIntervalFuzzy();
		
		Iterator<String> iterKeys = keys.iterator();
		
		while(iterKeys.hasNext()) {
			
			bin.put(iterKeys.next(), new Double(1.0));
		}
		
		return bin;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public P3CSupportInterface newSupport(boolean recordSupportAsList) {
		
		return new P3CSupportFuzzy(recordSupportAsList);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public MatrixFuzzy createAdjacencyMatrix() {
		
		return new MatrixFuzzy(new Double(0.0));
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public Matrix<? extends Number> createAdjacencyMatrix(int size) {
		
		return new MatrixFuzzy(size, new Double(0.0));
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isConnected(Matrix<? extends Number> adjacencyMatrix, int row, int column) {
		
		// Return true if cell value is greater than 0.0, otherwise return false
		return adjacencyMatrix.compareToEntry(row,  column, new Double(0.0)) > 0 ? true : false;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public Number getMembership(TreeMap<Short, P3CInterval<? extends Number>> signature, MarsInfoEntity entity) {
		
		double dom = 1.0;
		
		Iterator<Short> attrKeys = signature.keySet().iterator();
		
		while(attrKeys.hasNext()) {
			
			Short key = attrKeys.next();
			
			String tag = entity.getTag(key);
			
			// Get the interval for this attribute
			P3CIntervalFuzzy interval = (P3CIntervalFuzzy)signature.get(key);
			
			// Get the degree of membership for this tag
			double membershipGrade = interval.get(tag);
			
			switch(0) {
			case 0: // minimum
				// Perform minimum for the intersection operator
				if(membershipGrade < dom) {
					dom = membershipGrade;
				}
				break;
			case 1: // product
				dom *= membershipGrade;
				break;
			default:
				log.error("Fuzzy intersection operator is undefined");
			}
		}
		
		return new Double(dom);
	}
	
	/**
	 * Uses fuzzy support
	 * 
	 * {@inheritDoc}
	 */
	public boolean incrementSupport(P3CSupportInterface support, Number membership/*, int recordNumber*/) {
		
		//double supportValue = ((P3CSupportFuzzy)support).getFuzzySupport();
		boolean success = true;
		/*
		// First condition: perform minimum for the intersection operator
		// Second condition: ensure this is not the first support
		if(membership.doubleValue() < supportValue || support.getSupport() != 0) {
			supportValue = membership.doubleValue();
		}
		
		// If crisp support is 0 then min(membership,0) means 0 is returned.
		// This happens only on the instance of support
		if(support.getSupport() == 0) {
			supportValue = membership.doubleValue();
		}
		*/
		// Add crisp support
		if(!support.addSupport(/*recordNumber*/)) {
			success = false;
		}
		
		// Add possibilistic support
		if(!((P3CSupportFuzzy)support).addFuzzySupport(new Double(membership.doubleValue()))) {
			success = false;
		}
		
		return success;
	}
	
	
	/**
	 * 
	 * Performs (fuzzy) sigma count.
	 * 
	 * {@inheritDoc}
	 */
	public boolean testKItemsCands(
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidates,
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> candidatesKMinus1Signatures,
			int k) {
		
		Iterator<TreeMap<Short, P3CInterval<? extends Number>>> iterCandidates;
		PoissonContinuous poissonContinuousDistribution = new PoissonContinuous();
		
		//////////////////////////////////////////////////////////////////////
		//1. Determine support for all signatures in candidates.
		//////////////////////////////////////////////////////////////////////
		/*Set<TreeMap<String, P3CInterval<? extends Number>>> candKeySet = candidates.keySet();
		
		// Stores dataset
		List<MarsInfoEntity> entities =
				marsInfo.getSaturnInfo().getEntities();
		
		
		TreeMap<String, P3CInterval<? extends Number>> signature;
		String[] tagAttributeValue;
		P3CInterval<? extends Number> interval;*/
		
		//countSupport(candidates, k);
		
		//////////////////////////////////////////////////////////////////////
		//2. Check downward closure property (i.e., prune).
		//////////////////////////////////////////////////////////////////////
		iterCandidates = candidates.keySet().iterator();
		
		log.info("Number of candidates before pruning = " + candidates.size());
		
		while(iterCandidates.hasNext()) {
			
			TreeMap<Short, P3CInterval<? extends Number>> candidateSignature =
					iterCandidates.next();
			
			Iterator<Short> signatureInterval =
					candidateSignature.keySet().iterator();
			
			// Number of times the set of both conditions are satisfied.
			int conditionsSatisfied = 0;
			
			// For any q-signature (a sub-signature Q of candidateSignature S in Definition 3.5)
			while(signatureInterval.hasNext()) {
				
				// intervalLabel is equivalent to S' in Definitions 3.2 and 3.5
				Short intervalLabel = signatureInterval.next();
				
				double supp =
						((P3CSupportFuzzy)candidates.get(candidateSignature)).getFuzzySupport();
				
				// Save some time: if candidateSignature S has crisp support of
				// 0 then condition 1 of Definition 3.5 will not hold, so don't
				// both wasting computation time
				if(supp > 0.0) {
					
					// A sub-signature. Equivalent to Q in Definition 3.5
					TreeMap<Short, P3CInterval<? extends Number>> candidateSubSignature =
							new TreeMap<Short, P3CInterval<? extends Number>>();
					
					// A one-signature, i.e., this is full-signature - sub-signature.
					// Equivalent to S' in Definition 3.5
					/*TreeMap<String, P3CInterval<? extends Number>> candidateOneSignature =
							new TreeMap<String, P3CInterval<? extends Number>>();*/
					
					Iterator<Short> subSignatureInterval =
							candidateSignature.keySet().iterator();
					
					while(subSignatureInterval.hasNext()) {
						
						Short intervalLabelSub = subSignatureInterval.next();
						
						//if(!intervalLabel.equalsIgnoreCase(intervalLabelSub)) {
						if(intervalLabel != intervalLabelSub) {
							
							candidateSubSignature.put(intervalLabelSub,
									candidateSignature.get(intervalLabelSub));
						}
					}
					
					P3CSupportFuzzy suppSubSig = (P3CSupportFuzzy)candidatesKMinus1Signatures.get(candidateSubSignature);
					
					// If candidateSubSignature matches
					if(suppSubSig != null) {
						
						P3CInterval<? extends Number> intervalValues =
								candidateSignature.get(intervalLabel);
						
						double width = 0.0;
						
						Iterator<String> iterIntervalValues =
								intervalValues.iterator();
						
						while(iterIntervalValues.hasNext()) {
							
							String key = iterIntervalValues.next();
							
							//sum used for fuzzy approach
							width += intervalValues.get(key).doubleValue();
							// other approaches are minimum, average, product?
						}
						
						/*
						"For numerical data sets, we can assume, without restricting
						the generality, that all attributes have normalized values
						... [0,1]" in second paragraph of Section 2. Hence, width
						for a numeric attribute is [0,1]. Although it is not stated
						in the paper, we assume that the width for categorical
						attributes is also normalised.
						*/
						width /= markedBinsAttributeCardinality.get(intervalLabel).doubleValue();
						
						double eSupp = (double)(suppSubSig.getFuzzySupport()) * width;
						
						// Definition 3.5, condition 1, part 1
						if(supp > eSupp){
							
							// Definition 3.5, condition 1, part 2
							if(poissonContinuousDistribution.test(supp, eSupp, this.poissonThreshold)) {
								
								conditionsSatisfied++;
							}
						}
					}
				}
			}
			
			if(conditionsSatisfied != candidateSignature.size()) {
				// Remove last candidate returned by iterator
				iterCandidates.remove();
			}
		}
		
		log.info("Number of candidates after pruning = " + candidates.size());
		
		return true;
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * Find all pairs, and construct a graph.
	 * 
	 * Find all connected components in a disconnected graph. This is the same
	 * as finding all disconnected subgraphs in a graph. Depth-first search is
	 * performed.
	 * 
	 * @param adjacencyMatrix All bins on both rows and columns
	 * @param binToIntMapping Maps the key to an integer index in the adjacencyMatrix
	 * @return
	 */
	@Deprecated
	public TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> generateCategoricalIntervalsOld(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping) {
		
		//Key is attribute label
		TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> allIntervals =
				new TreeMap<Short, TreeSet<P3CInterval<? extends Number>>>();
		
		TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes =
				new TreeMap<Short, Matrix<? extends Number>>();
		
		TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes =
				new TreeMap<Short, TreeMap<String, Integer>>();
		
		
		//double minValue = 1.0; // used for normalisation
		//double maxValue = 0.0;
		
		if(!generateGraphs(adjacencyMatrix, binToIntMapping, adjacencyMatrixAttributes, binToIntMappingAttributes)) {
			log.error("generateGraphs() failed");
		}
		
		//
		// CREATE COMPONENTS OF DISCONNECTED GRAPH
		//
		// Perform connected components labelling using depth-first search
		// http://stackoverflow.com/questions/14465297/connected-component-labelling
		// https://courses.cs.washington.edu/courses/cse576/02au/homework/hw3/ConnectComponent.java
		// http://www.cse.msu.edu/~stockman/Book/2002/Chapters/ch3.pdf
		
		TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributesLabels =
				new TreeMap<Short, Matrix<? extends Number>>();
		
		Iterator<Short> iterMatrices = adjacencyMatrixAttributes.keySet().iterator();
		
		TreeMap<Short, List<List<Integer>>> components =
				new TreeMap<Short, List<List<Integer>>>();
		
		// For every attribute's matrix
		while(iterMatrices.hasNext()) {
			
			Short key = iterMatrices.next();
			Matrix<? extends Number> matrix = adjacencyMatrixAttributes.get(key);
			
			Matrix<? extends Number> matrixLabels = this.createAdjacencyMatrix(matrix.numberOfRows());
			adjacencyMatrixAttributesLabels.put(key, matrixLabels);
			
			// Correct approach: DFS on adjacency matrix
			// http://math.stackexchange.com/questions/277045/easiest-way-to-determine-all-disconnected-sets-from-a-graph
			components.put(key, Graphs.findConnectedComponents(matrix));
			
			// Incorrect approach: Label connected components
			//Graphs.connectedComponentsLabelling(matrix, matrixLabels, true);
		}
		
		
		//
		// CREATE INTERVALS FROM COMPONENTS (OF DISCONNECTED GRAPH),
		// AND CREATE INTERVALS FROM THE LEFTOVER BINS
		//
		iterMatrices = adjacencyMatrixAttributes.keySet().iterator();
		
		// For every attribute's matrix
		while(iterMatrices.hasNext()) {
			
			Short key = iterMatrices.next();
			
			/*
			// Print graph to default output
			System.out.println("\n" + key);
			
			for(int i = 0; i < matrix.size(); i++) {
				
				System.out.print("\n" + matrix.get(i, 0));
				
				for(int j = 1; j < matrix.size(); j++) {
					
					System.out.print("," + matrix.get(i, j));
				}
			}
			
			
			// Print components to default output
			System.out.println();
			
			for(List<Integer> component : components.get(key)) {
				
				System.out.println(component);
			}
			*/
			
			//
			// Add bins that were not connected in the graph
			// (which creates intervals of length == 0, hence they do not
			// belong to an interval)
			//
			
			TreeMap<String, Integer> binToIntMappings =
					binToIntMappingAttributes.get(key);
			
			Iterator<String> iterBinLabels =
					binToIntMappings.keySet().iterator();
			
			TreeSet<P3CInterval<? extends Number>> intervalsForAttribute =
					new TreeSet<P3CInterval<? extends Number>>(P3CComparators.comparatorP3CInterval);
			
			//
			// Check for bins that do not belong to an interval,
			// create an interval of size 1.
			//
			while(iterBinLabels.hasNext()) {
				
				String binLabel = iterBinLabels.next();
				
				int binId = binToIntMappings.get(binLabel).intValue();
				
				// Now that we have binId, check if this was in a component
				// of a disconnected graph
				
				boolean alreadyInAnInterval = false;
				
				for(List<Integer> component : components.get(key)) {
					
					if(component.contains(new Integer(binId))) {
						alreadyInAnInterval = true;
					}
				}
				
				// If not in a component of a disconnected graph then
				// create a new interval containing just this bin
				if(!alreadyInAnInterval) {
					
					// Create a new interval
					P3CInterval<? extends Number> interval = this.newInterval();
					
					// Add with default membership, because the interval
					// contains just one value/bin
					interval.add(binLabel);
					intervalsForAttribute.add(interval);
				}
			}
			
			//
			// Add bins that were connected in the graph (which creates
			// intervals of length > 1). This is where 2P4C is different.
			//
			
			HashMap<String, Double> binColumnSums = new HashMap<String, Double>();
			HashMap<String, Integer> binColumnCounts = new HashMap<String, Integer>();
			
			//int size = binColumnSums.keySet().size();
			int numberOfBinsOutsideOfMatrix = binToIntMapping.size() -
					adjacencyMatrixAttributes.get(key).numberOfRows();
			
			double columnSum = 0.0;
			double maxColumnSum = columnSum;
			double minCellValue = 1.0;
			int columnCount = 0;
			
			// For each component in disconnected graph, i.e., fuzzy set
			for(List<Integer> component : components.get(key)) {
				
				// Stores the string keys. The keys are equivalent to those
				// in List<Integer> component
				List<String> componentKeys =
						new ArrayList<String>(component.size());
				
				P3CIntervalFuzzy interval = this.newInterval();
				
				iterBinLabels = binToIntMappings.keySet().iterator();
				
				// Find all the string keys from the integer keys
				// (We only have a one map from string => int, but we would
				// like to map int => string, which is not possibly in standard
				// Java collection types with a two-way map)
				while(iterBinLabels.hasNext()) {
					
					String binLabel = iterBinLabels.next();
					
					if(component.contains(binToIntMappings.get(binLabel).intValue())) {
						
						// Add the bins to a list. They will then be used to
						// calculate membership
						//componentKeys.add(key + ":" + binLabel);
						componentKeys.add(marsInfo.getAttributes().get(key)
								+ ":" + binLabel);
					}
				}
				
				
				Iterator<String> iterBinToIntMapping = binToIntMapping.keySet().iterator();
				
				binColumnSums = new HashMap<String, Double>();
				binColumnCounts = new HashMap<String, Integer>();
				
				//maxColumnSum = columnSum;
				
				// For each column, count the sum of values
				while(iterBinToIntMapping.hasNext()) {
					
					String keyMappingColumn = iterBinToIntMapping.next();
					int keyIDColumn = binToIntMapping.get(keyMappingColumn).intValue();
					
					columnSum = 0.0;
					columnCount = 0;
					
					// First, calculate sum and count
					for(String adjacencyMatrixKey : componentKeys) {
						
						Integer keyIDRowInteger = binToIntMapping.get(adjacencyMatrixKey);
						
						if(keyIDRowInteger != null) {
							
							int keyIDRow = keyIDRowInteger.intValue();
							
							double cellValue = adjacencyMatrix.get(keyIDRow, keyIDColumn).doubleValue();
							
							if(cellValue > 0.0) {
								
								columnSum += cellValue;
								columnCount++;
								
								if(cellValue < minCellValue) {
									minCellValue = cellValue;
								}
							}
						} else {
							log.error("null value found with key " + adjacencyMatrixKey);
						}
					}
					
					if(columnSum > 0.0) {
						binColumnSums.put(keyMappingColumn, new Double(columnSum));
						binColumnCounts.put(keyMappingColumn, new Integer(columnCount));
						
						// Record the maximum sum of a column from all components
						if(columnSum > maxColumnSum) {
							maxColumnSum = columnSum;
						}
					}
				}
				
				//maxValue = 0.0;
				
				// Calculate membership for each each bin of the interval
				for(String binKey : componentKeys) {
					
					int keyIDRow = binToIntMapping.get(binKey).intValue();
					
					Iterator<String> iterBinColumnSums = binColumnSums.keySet().iterator();
					
					double totalMembership = 0.0;
					
					while(iterBinColumnSums.hasNext()) {
						
						String binColumnSumsKey = iterBinColumnSums.next();
						
						int keyIDColumn = binToIntMapping.get(binColumnSumsKey).intValue();
						
						double cellValue = adjacencyMatrix.get(keyIDRow, keyIDColumn).doubleValue();
						
						if(cellValue > 0.0) {
							
							// Must be at least two bins that share a value from another attribute
							if(binColumnCounts.get(binColumnSumsKey).intValue() > 1) {
								
								double sum = binColumnSums.get(binColumnSumsKey).doubleValue();
								
								totalMembership += cellValue / sum;
							}
						}
					}
					
					// This gives possibilistic
					double value = totalMembership / (double)numberOfBinsOutsideOfMatrix;
					
					// This is fuzzy/probabilistic
					//double value = totalMembership;
					 
					interval.add(DataUtils.splitByFirstOccurrence(binKey, this.marsInfo.getSplitKeyword())[1], value);
					
					// Find the value to normalise by
					/*if(value > 0.0) {
						//if(value < minValue) {
						//	minValue = value;
						//}
						if(value > maxValue) {
							maxValue = value;
						}
					}*/
				}
				
				
				/*Iterator<String> iter = interval.iterator();
				
				while(iter.hasNext()) {
					
					String gradeKey = iter.next();
					double grade = (Double)interval.get(gradeKey).doubleValue();
					grade /= maxValue;
					
					if(grade < minValue) {
						minValue = grade;
					}
					
					interval.put(gradeKey, new Double(grade));
				}*/
				
				
				// for each adjacencyMatrixKey
				// if cell(adjacencyMatrixKey, binColumnSums) > 0.0
				// divide by binColumnSums
				
				// Add a complete interval
				intervalsForAttribute.add(interval);
			}
			
			
			boolean allAreSize1 = true;
			
			for(P3CInterval<? extends Number> interval : intervalsForAttribute) {
				
				if(interval.size() != 1) {
					
					allAreSize1 = false;
				}
			}
			
			// Reassign membership to every interval containing just one bin
			for(P3CInterval<? extends Number> interval : intervalsForAttribute) {
				
				if(interval.size() == 1) {
					
					if(numberOfBinsOutsideOfMatrix > 0) {
						
						double nominator = 1.0;
						
						if(!allAreSize1) {
							nominator = minCellValue / maxColumnSum;
						}
						
						//interval.put(interval.firstKey(), new Double((1.0 / binCardinality) / (double)size));
						interval.put(interval.firstKey(), new Double(nominator / (double)numberOfBinsOutsideOfMatrix));
						//interval.put(interval.firstKey(), new Double(1.0 / (double)size));
					} else {
						log.error("Size should always be > 0");
					}
				}
			}
			
			//binColumnSums.keySet().size();
			
			//System.out.println(intervalsForAttribute);
			
			allIntervals.put(key, intervalsForAttribute);
		}
		
		/*
		Iterator<Short> intervalsIter = allIntervals.keySet().iterator();
		
		// For every attribute's matrix
		while(intervalsIter.hasNext()) {
			
			String key = intervalsIter.next();
			TreeSet<P3CInterval<? extends Number>> intervals =
					allIntervals.get(key);
			
			boolean allAreSize1 = true;
			
			for(P3CInterval<? extends Number> interval : intervals) {
				
				if(interval.size() != 1) {
					
					allAreSize1 = false;
				}
			}
			
			// Reassign membership to every interval containing just one bin
			for(P3CInterval<? extends Number> interval : intervals) {
				
				if(interval.size() == 1) {
					
					
					// Normalise all values in the interval, using the largest we found.
					// This ensures we have: i) a fuzzy set, and ii) a normal fuzzy
					// set, i.e., at least one membership grade of 1.
					String intervalKey = interval.firstKey();
					double value = interval.get(intervalKey).doubleValue();
					value = minValue;
					interval.put(intervalKey, new Double(value));
					
					
					if(numberOfBinsOutsideOfMatrix > 0) {
						
						double nominator = 1.0;
						
						if(!allAreSize1) {
							nominator = minCellValue / maxColumnSum;
						}
						
						//interval.put(interval.firstKey(), new Double((1.0 / binCardinality) / (double)size));
						interval.put(interval.firstKey(), new Double(nominator / (double)numberOfBinsOutsideOfMatrix));
						//interval.put(interval.firstKey(), new Double(1.0 / (double)size));
					 
					} else {
						log.error("Size should always be > 0");
					}
				}
			}
		}
		*/
		
		return allIntervals;
	}
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * This method produces possibilistic sets (referred to as intervals in the 2P4C paper)
	 */
	@Override
	public void generateCategoricalIntervalFromGraphComponent(
			TreeSet<P3CInterval<? extends Number>> intervalsForAttribute,
			Matrix<? extends Number> adjMatAllBinBinPairs,
			Short key,
			TreeMap<Short, List<List<Integer>>> components,
			TreeMap<String, Integer> binToIntMapping,
			TreeMap<String, Integer> valueToIntMapping,
			TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes,
			TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes) {
		
		HashMap<String, Double> binColumnSums = new HashMap<String, Double>();
		HashMap<String, Integer> binColumnCounts = new HashMap<String, Integer>();
		
		int numberOfBinsOutsideOfMatrix = binToIntMapping.size() -
				adjacencyMatrixAttributes.get(key).numberOfRows();
		double columnSum = 0.0;
		double maxColumnSum = columnSum;
		double minCellValue = 1.0;
		int columnCount = 0;
		
		// For each component in disconnected graph, i.e., possibilistic set
		for(List<Integer> component : components.get(key)) {
			
			// Stores the string keys. The keys are equivalent to those
			// in List<Integer> component
			List<String> componentKeys =
					new ArrayList<String>(component.size());
			
			P3CIntervalFuzzy interval = this.newInterval();
			
			Iterator<String> iterBinLabels =
					valueToIntMapping.keySet().iterator();
			
			// Find all the string keys from the integer keys
			// (We only have a one map from string => int, but we would
			// like to map int => string, which is not possibly in standard
			// Java collection types with a two-way map)
			while(iterBinLabels.hasNext()) {
				
				String binLabel = iterBinLabels.next();
				
				if(component.contains(valueToIntMapping.get(binLabel).intValue())) {
					
					// Add the bins to a list. They will then be used to
					// calculate membership
					//componentKeys.add(key + ":" + binLabel);
					componentKeys.add(marsInfo.getAttributes().get(key)
							+ ":" + binLabel);
				}
			}
			
			
			Iterator<String> iterBinToIntMapping = binToIntMapping.keySet().iterator();
			
			binColumnSums = new HashMap<String, Double>();
			binColumnCounts = new HashMap<String, Integer>();
			
			//maxColumnSum = columnSum;
			
			// For each column, count the sum of values
			while(iterBinToIntMapping.hasNext()) {
				
				String keyMappingColumn = iterBinToIntMapping.next();
				int keyIDColumn = binToIntMapping.get(keyMappingColumn).intValue();
				
				columnSum = 0.0;
				columnCount = 0;
				
				// First, calculate sum and count
				for(String adjacencyMatrixKey : componentKeys) {
					
					Integer keyIDRowInteger = binToIntMapping.get(adjacencyMatrixKey);
					
					if(keyIDRowInteger != null) {
						
						int keyIDRow = keyIDRowInteger.intValue();
						
						double cellValue = adjMatAllBinBinPairs.get(keyIDRow, keyIDColumn).doubleValue();
						
						if(cellValue > 0.0) {
							
							columnSum += cellValue;
							columnCount++;
							
							if(cellValue < minCellValue) {
								minCellValue = cellValue;
							}
						}
					} else {
						log.error("null value found with key " + adjacencyMatrixKey);
					}
				}
				
				if(columnSum > 0.0) {
					binColumnSums.put(keyMappingColumn, new Double(columnSum));
					binColumnCounts.put(keyMappingColumn, new Integer(columnCount));
					
					// Record the maximum sum of a column from all components
					if(columnSum > maxColumnSum) {
						maxColumnSum = columnSum;
					}
				}
			}
			
			//maxValue = 0.0;
			
			// Calculate membership for each each bin of the interval
			for(String binKey : componentKeys) {
				
				int keyIDRow = binToIntMapping.get(binKey).intValue();
				
				Iterator<String> iterBinColumnSums = binColumnSums.keySet().iterator();
				
				double totalMembership = 0.0;
				
				while(iterBinColumnSums.hasNext()) {
					
					String binColumnSumsKey = iterBinColumnSums.next();
					
					int keyIDColumn = binToIntMapping.get(binColumnSumsKey).intValue();
					
					double cellValue = adjMatAllBinBinPairs.get(keyIDRow, keyIDColumn).doubleValue();
					
					if(cellValue > 0.0) {
						
						// Must be at least two bins that share a value from another attribute
						if(binColumnCounts.get(binColumnSumsKey).intValue() > 1) {
							
							double sum = binColumnSums.get(binColumnSumsKey).doubleValue();
							
							totalMembership += cellValue / sum;
						}
					}
				}
				
				// This gives possibilistic
				double value = totalMembership / (double)numberOfBinsOutsideOfMatrix;
				
				interval.add(DataUtils.splitByFirstOccurrence(binKey, this.marsInfo.getSplitKeyword())[1], value);
			}
			
			// Add a complete interval
			intervalsForAttribute.add(interval);
		}
		
		
		boolean allAreSize1 = true;
		
		for(P3CInterval<? extends Number> interval : intervalsForAttribute) {
			
			if(interval.size() != 1) {
				
				allAreSize1 = false;
			}
		}
		
		// Reassign membership to every interval containing just one bin
		for(P3CInterval<? extends Number> interval : intervalsForAttribute) {
			
			if(interval.size() == 1) {
				
				if(numberOfBinsOutsideOfMatrix > 0) {
					
					double nominator = 1.0;
					
					if(!allAreSize1) {
						nominator = minCellValue / maxColumnSum;
					}
					
					//interval.put(interval.firstKey(), new Double((1.0 / binCardinality) / (double)size));
					interval.put(interval.firstKey(), new Double(nominator / (double)numberOfBinsOutsideOfMatrix));
					//interval.put(interval.firstKey(), new Double(1.0 / (double)size));
				} else {
					log.error("Size should always be > 0");
				}
			}
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public void logExecutionStart() {
		
		log.info(className + ": started execution of algorithm");
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public void logExecutionEnd() {
		
		log.info(className + ": finished execution of algorithm");
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public void calculateStats() {
		
		Iterator<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> clusterCoresIterator =
				clusterCores.iterator();
		
		int k = 1;
		int pClusterCoresI = 0;
		Map<Integer, Integer> numberOfClustersPerK = new TreeMap<Integer, Integer>();
		Map<Integer, Double> averageFuzzySupportPerK = new TreeMap<Integer, Double>();
		
		while(clusterCoresIterator.hasNext()) {
			
			TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> pClusterCores =
					clusterCoresIterator.next();
			
			Iterator<TreeMap<Short, P3CInterval<? extends Number>>> pClusterCoresIterator =
					pClusterCores.navigableKeySet().iterator();
			
			int numberOfKClusters = 0;
			double averageSupportForK = 0.0d;
			
			while(pClusterCoresIterator.hasNext()) {
				
				// get the cluster
				TreeMap<Short, P3CInterval<? extends Number>> pClusterCore =
						pClusterCoresIterator.next();
				
				// Perform stats
				pClusterCoresI++;
				//kCluster++;				
				averageSupportForK += ((P3CSupportFuzzy)pClusterCores.get(pClusterCore)).getFuzzySupport();
				numberOfKClusters++;
			}
			
			if(numberOfKClusters != 0) {
				averageSupportForK /= (double)numberOfKClusters;
			}
			
			numberOfClustersPerK.put(new Integer(k), new Integer(numberOfKClusters));
			averageFuzzySupportPerK.put(new Integer(k), new Double(averageSupportForK));
			
			k++;
		}
		
		log.info("Number of clusters = " + pClusterCoresI);
		
		for(Integer key : numberOfClustersPerK.keySet()) {
			
			log.info(key + "-signature cluster" +
					": qty. = " + numberOfClustersPerK.get(key) +
					", avg. fuzzy support = " + averageFuzzySupportPerK.get(key));
		}
	}
	
	
	public String getSupport(P3CSupportInterface support) {
		
		P3CSupportFuzzy supportFuzzy = (P3CSupportFuzzy)support;
		
		String str = new String();
		str += "crisp support = " + supportFuzzy.getSupport();
		str += " fuzzy support = " + supportFuzzy.getFuzzySupport();
		return str;
	}
	
	
	/**
	 * This implementation adds crisp support and fuzzy support.
	 * 
	 * {@inheritDoc}
	 */
	public SubspaceClustersEntry createSubspaceClustersEntry(
			TreeMap<String, P3CInterval<? extends Number>> cluster,
			int clusterId,
			P3CSupportInterface support,
			int paramId) {
		
		P3CSupportFuzzy supportFuzzy = (P3CSupportFuzzy)support;
		
		SubspaceClustersEntry sce =
				SubspaceClustersEntry.create(cluster,
				clusterId,
				supportFuzzy.getSupport(),
				supportFuzzy.getFuzzySupport(),
				paramId);
		
		return sce;
	}
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * This is specific to 2P4C.
	 */
	@Override
	public Number newNumber(Number number) {
		
		return new Double(number.doubleValue()) /
				new Double(this.marsInfo.getEntities().size());
	}
	
	
	public static void main (String[] args) {
		
		String filename;
		double poissonThreshold = 1e-10;
		double poissonThresholdIntGen = poissonThreshold;
		int limit = 0;
		int batchCandidates = 0;
		Parameters parameters = new Parameters();
		ProjectedClusteringCommandLineOptions cmdLineOptions = new ProjectedClusteringCommandLineOptions();
		TwoPFourC twoPFourC;
		MarsInfo marsInfo;
		CSV csv = new CSV();
		String value;
		
		
		//
		// SETUP PARAMETERS AND GET COMMAND LINE ARGUMENTS
		//
		parameters.putParameterMin("poisson-threshold", new String("1e-100"));
		parameters.putParameterMax("poisson-threshold", new String("1e-1"));
		
		if(!cmdLineOptions.parseCommandLine(parameters, args)) {
			log.error("Not all required arguments were provided on the command line");
		}
		
		// From here onwards, it is safe to assume that all required arguments
		// were supplied at the command line.
		
		value = parameters.getArgument("limit");
		if(value != null) {
			limit = new Integer (value).intValue();
		}
		
		value = parameters.getArgument("poisson-threshold");
		if(value != null) {
			poissonThreshold = new Double (value).doubleValue();
		}
		
		value = parameters.getArgument("poisson-threshold-int-gen");
		if(value != null) {
			poissonThresholdIntGen = new Double (value).doubleValue();
		} else {
			poissonThresholdIntGen = poissonThreshold;
		}
		
		value = parameters.getArgument("batch-candidates");
		if(value != null) {
			batchCandidates = new Integer (value).intValue();
		}
		
		filename = parameters.getArgument("file");
		
		
		//
		// INITIALISE OBJECTS FOR ALGORITHM
		//
		marsInfo = csv.getInfo(0, 0, filename, limit);
		//marsInfo = new MarsInfo(marsInfo);
		twoPFourC = new TwoPFourC(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
		
		
		//System.exit(0);
		
		//
		// RUN ALGORITHM
		//
		if(marsInfo.getEntities().size() > 0) {
			
			long startTime = System.nanoTime();
			boolean successfulRun = twoPFourC.run();
			// Nano second is one billionth of a second. Note, 10^9 = 1000000000.
			// Some rounding occurs with conversion from long to float, but we assume this is ok
			double elapsedTime = (System.nanoTime() - startTime) / 1000000000f;
			log.info("Execution time (seconds): " + elapsedTime);
			
			if(successfulRun == false) {
				log.warn("P3C returned false");
			}
			
			List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> maximalKSignatures =
					twoPFourC.getSubspaceClustersAndTIDs();
			
			/*
			log.info("Writing formatted xlsx file");
			List<String[]> csvRecords = CSVUtils.csvToList("data\\" + filename + ".csv");
			MSExcelFormatter formatter = new MSExcelFormatter();
			// formatSubspaceCluster() adds xlsx extension 
			formatter.formatSubspaceClusterInXlsx(csvRecords, maximalKSignatures, "data\\" + filename);
			*/
			
			//log.info("Number of subspace clusters: " + clusterCores.size());
			
			
			log.info("Writing text file of subspace clusters ...");
			try {
				PrintWriter out = new PrintWriter("data" + File.separator + filename + ".txt");
				out.println(maximalKSignatures.toString());
				out.close();
			} catch (IOException e) {
				log.error("Error writing txt file for subspace clusters", e);
			}
			
			
			//log.info("Updating MarsInfoEntity");
			//p3cCrisp.updateSaturnInfoEntity();
			
			log.info("Computing stats");
			twoPFourC.calculateStats();
			
			log.info("Computing performance measures");
			twoPFourC.measurePeformance();
			
			/*
			new CSVExport().outputToCsv(new MarsInfo(marsInfo.getSaturnInfoEntities()));
			log.warn("THE CSV HEADERS MUST BE CORRECTED");
			
			
			log.info("Serialising subspace clusters ...");
			List<TreeMap<String, TreeSet<String>>> subspaceClusters =
					Utils.formatSubspaceClustersForSaturnSerObject(p3cv.getSubspaceClustersAndTIDs());
			//		p3cv.formatSubspaceClustersForSaturn();
			
			if(subspaceClusters == null) {
				log.warn("Formatted subspaceClusters is null");
			}
			
			if(!Egress.writeSerialisedObject(subspaceClusters, "data\\" + folder + datasetName + ".ser")) {
				log.error("Failed to serialise subspaceClusters");
			}
			*/
			
			
			log.info("Writing subspace clusters to H2 database ...");
			
			MarsInfoEntity firstEntity = marsInfo.getEntities().get(0);
			MarsInfoEntity lastEntity = marsInfo.getEntities().get(marsInfo.getEntities().size() - 1);
			Timestamp start = new Timestamp(firstEntity.getStartTime().getTime());
			Timestamp end = new Timestamp(lastEntity.getStartTime().getTime());
			
			if(!twoPFourC.saveClusterCoresToDatabase(twoPFourC.getClass().getSimpleName(),
					filename, start, end)) {
				log.error("saveClusterCoresToDatabase() returned false");
			}
			
			
		} else {
			log.error("No instances in dataset");
		}
	}
	
	
}
