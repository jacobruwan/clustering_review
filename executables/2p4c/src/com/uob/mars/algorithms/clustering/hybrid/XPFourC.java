package com.uob.mars.algorithms.clustering.hybrid;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;
import com.uob.mars.algorithms.graphs.Graphs;
import com.uob.mars.csv.CSV;
import com.uob.mars.data.MarsInfo;
import com.uob.mars.data.MarsInfoEntity;
import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.util.Parameters;
import com.uob.mars.algorithms.clustering.FuzzyKModes;
import com.uob.mars.algorithms.clustering.hybrid.ProjectedClusteringCommandLineOptions;
import com.uob.mars.algorithms.clustering.hybrid.TwoPFourC;
import com.uob.mars.algorithms.clustering.hybrid.XPFourC;



public class XPFourC extends TwoPFourC{
	
	
	private static final Logger log = Logger.getLogger(XPFourC.class);
	
	
	public XPFourC(MarsInfo marsInfo, double poissonThreshold,
			double poissonThresholdIntGen, int batchCandidates) {
		super(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * 
	 * Find all pairs, and construct a graph.
	 * 
	 * Find all connected components in a disconnected graph. This is the same
	 * as finding all disconnected subgraphs in a graph. Depth-first search is
	 * performed.
	 * 
	 * @param adjacencyMatrix All bins on both rows and columns
	 * @param binToIntMapping Maps the key to an integer index in the adjacencyMatrix
	 * @return
	 */
	@Override
	public TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> generateCategoricalIntervals(
			Matrix<? extends Number> adjacencyMatrix,
			TreeMap<String, Integer> binToIntMapping) {
		
		//Key is attribute label
		TreeMap<Short, TreeSet<P3CInterval<? extends Number>>> allIntervals =
				new TreeMap<Short, TreeSet<P3CInterval<? extends Number>>>();
		
		TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributes =
				new TreeMap<Short, Matrix<? extends Number>>();
		
		TreeMap<Short, TreeMap<String, Integer>> binToIntMappingAttributes =
				new TreeMap<Short, TreeMap<String, Integer>>();
		
		if(!generateGraphs(adjacencyMatrix, binToIntMapping, adjacencyMatrixAttributes, binToIntMappingAttributes)) {
			log.error("generateGraphs() failed");
		}
		
		//
		// CREATE COMPONENTS OF DISCONNECTED GRAPH
		//
		// Perform connected components labelling using depth-first search
		// http://stackoverflow.com/questions/14465297/connected-component-labelling
		// https://courses.cs.washington.edu/courses/cse576/02au/homework/hw3/ConnectComponent.java
		// http://www.cse.msu.edu/~stockman/Book/2002/Chapters/ch3.pdf
		
		TreeMap<Short, Matrix<? extends Number>> adjacencyMatrixAttributesLabels =
				new TreeMap<Short, Matrix<? extends Number>>();
		
		Iterator<Short> iterMatrices = adjacencyMatrixAttributes.keySet().iterator();
		
		TreeMap<Short, List<List<Integer>>> setOfComponents =
				new TreeMap<Short, List<List<Integer>>>();
		
		TreeMap<String, List<List<Double>>> partitionMatrices =
				new TreeMap<String, List<List<Double>>>();
		
		// For every attribute's matrix
		while(iterMatrices.hasNext()) {
			
			Short key = iterMatrices.next();
			Matrix<? extends Number> matrix = adjacencyMatrixAttributes.get(key);
			
			Matrix<? extends Number> matrixLabels = this.createAdjacencyMatrix(matrix.numberOfRows());
			adjacencyMatrixAttributesLabels.put(key, matrixLabels);
			
			// Correct approach: DFS on adjacency matrix
			// http://math.stackexchange.com/questions/277045/easiest-way-to-determine-all-disconnected-sets-from-a-graph
			setOfComponents.put(key, Graphs.findConnectedComponents(matrix));
			
			// Incorrect approach: Label connected components
			//Graphs.connectedComponentsLabelling(matrix, matrixLabels, true);
		}
		
		//
		// CLUSTER EACH ATTRIBUTE'S MATRIX
		//
		iterMatrices = adjacencyMatrixAttributes.keySet().iterator();
		
		// For every attribute's matrix
		while(iterMatrices.hasNext()) {
			
			Short keyAttribute = iterMatrices.next();
			Matrix<? extends Number> matrix =
					adjacencyMatrixAttributes.get(keyAttribute);
			
			//int k = components.get(keyAttribute).size(); // number of clusters
			List<List<Double>> partitionMatrix = new ArrayList<List<Double>>();
			
			//Iterator<String> iterComponent = components.keySet().iterator();
			
			List<List<Integer>> components = setOfComponents.get(keyAttribute);
			
			// Calculate the partition matrices
			// For each component (of which there are k)
			// (Each component represents a cluster)
			//while(iterComponent.hasNext()) {
			// Note: k == components.size()
			for(int clusterIndex = 0; clusterIndex < components.size(); clusterIndex++) {
				
				// Cluster centers
				List<Integer> prototype = components.get(clusterIndex);
				
				// Set initial capacity to all total number of other attributes' values.
				List<Double> partition = new ArrayList<Double>(adjacencyMatrixAttributes.get(keyAttribute).numberOfRows());
				
				for(int row = 0; row < matrix.numberOfRows(); row++) {
					
					int match = 0;
					
					Iterator<Integer> iterPrototype = prototype.iterator();
					
					while (iterPrototype.hasNext()) {
						
						int indexPrototype = iterPrototype.next().intValue();
						
						Double value = (Double)matrix.get(row, indexPrototype);
						
						if(value.equals(new Double(1.0))) {
							match++;
						}
					}
					
					partition.add(new Double(match));
				}
				
				partitionMatrix.add(partition);
			}
			
			
			//partitionMatrices.put(key, arg1)
			
			/*
			int k = components.get(key).size(); // number of clusters
			
			if(k > 1) {
				
				FuzzyKModes fuzzyKModes = new FuzzyKModes(marsInfo, k, 2.0);
				
				
				
				fuzzyKModes.setInitialPrototypes(prototypes);
				
				fuzzyKModes.run();
			}
			*/
		}
		
		
		
		return allIntervals;
	}
	
	
	public static void main (String[] args) {
		
		String filename;
		double poissonThreshold = 1e-10;
		double poissonThresholdIntGen = poissonThreshold;
		int limit = 0;
		int batchCandidates = 0;
		Parameters parameters = new Parameters();
		ProjectedClusteringCommandLineOptions cmdLineOptions = new ProjectedClusteringCommandLineOptions();
		XPFourC xPFourC;
		MarsInfo marsInfo;
		CSV csv = new CSV();
		String value;
		
		
		//
		// SETUP PARAMETERS AND GET COMMAND LINE ARGUMENTS
		//
		parameters.putParameterMin("poisson-threshold", new String("1e-100"));
		parameters.putParameterMax("poisson-threshold", new String("1e-1"));
		
		if(!cmdLineOptions.parseCommandLine(parameters, args)) {
			log.error("Not all required arguments were provided on the command line");
		}
		
		// From here onwards, it is safe to assume that all required arguments
		// were supplied at the command line.
		
		value = parameters.getArgument("limit");
		if(value != null) {
			limit = new Integer (value).intValue();
		}
		
		value = parameters.getArgument("poisson-threshold");
		if(value != null) {
			poissonThreshold = new Double (value).doubleValue();
		}
		
		value = parameters.getArgument("poisson-threshold-int-gen");
		if(value != null) {
			poissonThresholdIntGen = new Double (value).doubleValue();
		} else {
			poissonThresholdIntGen = poissonThreshold;
		}
		
		value = parameters.getArgument("batch-candidates");
		if(value != null) {
			batchCandidates = new Integer (value).intValue();
		}
		
		filename = parameters.getArgument("file");
		
		
		//
		// INITIALISE OBJECTS FOR ALGORITHM
		//
		marsInfo = csv.getInfo(0, 0, filename, limit);
		xPFourC = new XPFourC(marsInfo, poissonThreshold, poissonThresholdIntGen, batchCandidates);
		
		//System.exit(0);
		
		//
		// RUN ALGORITHM
		//
		if(marsInfo.getEntities().size() > 0) {
			
			long startTime = System.nanoTime();
			boolean successfulRun = xPFourC.run();
			// Nano second is one billionth of a second. Note, 10^9 = 1000000000.
			// Some rounding occurs with conversion from long to float, but we assume this is ok
			double elapsedTime = (System.nanoTime() - startTime) / 1000000000f;
			log.info("Execution time (seconds): " + elapsedTime);
			
			if(successfulRun == false) {
				log.warn("P3C returned false");
			}
			
			List<TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface>> maximalKSignatures =
					xPFourC.getSubspaceClustersAndTIDs();
			
			/*
			log.info("Writing formatted xlsx file");
			List<String[]> csvRecords = CSVUtils.csvToList("data\\" + filename + ".csv");
			MSExcelFormatter formatter = new MSExcelFormatter();
			// formatSubspaceCluster() adds xlsx extension 
			formatter.formatSubspaceClusterInXlsx(csvRecords, maximalKSignatures, "data\\" + filename);
			*/
			
			//log.info("Number of subspace clusters: " + clusterCores.size());
			
			
			log.info("Writing text file of subspace clusters ...");
			try {
				PrintWriter out = new PrintWriter("data" + File.separator + filename + ".txt");
				out.println(maximalKSignatures.toString());
				out.close();
			} catch (IOException e) {
				log.error("Error writing txt file for subspace clusters", e);
			}
			
			
			//log.info("Updating MarsInfoEntity;
			//p3cCrisp.updateSaturnInfoEntity();
			
			log.info("Computing stats");
			xPFourC.calculateStats();
			
			/*log.info("Computing performance measures");
			xPFourC.measurePeformance();*/
			
			/*
			new CSVExport().outputToCsv(new MarsMarsInfonfo.getSaturnInfoEntities()));
			log.warn("THE CSV HEADERS MUST BE CORRECTED");
			
			
			log.info("Serialising subspace clusters ...");
			List<TreeMap<String, TreeSet<String>>> subspaceClusters =
					Utils.formatSubspaceClustersForSaturnSerObject(p3cv.getSubspaceClustersAndTIDs());
			//		p3cv.formatSubspaceClustersForSaturn();
			
			if(subspaceClusters == null) {
				log.warn("Formatted subspaceClusters is null");
			}
			
			if(!Egress.writeSerialisedObject(subspaceClusters, "data\\" + folder + datasetName + ".ser")) {
				log.error("Failed to serialise subspaceClusters");
			}
			*/
			
			
			log.info("Writing subspace clusters to H2 database ...");
			
			MarsInfoEntity firstEntity = marsInfo.getEntities().get(0);
			MarsInfoEntity lastEntity = marsInfo.getEntities().get(marsInfo.getEntities().size() - 1);
			Timestamp start = new Timestamp(firstEntity.getStartTime().getTime());
			Timestamp end = new Timestamp(lastEntity.getStartTime().getTime());
			
			if(!xPFourC.saveClusterCoresToDatabase(xPFourC.getClass().getSimpleName(),
					filename, start, end)) {
				log.error("saveClusterCoresToDatabase() returned false");
			}
			
			
		} else {
			log.error("No instances in dataset");
		}
	}


}
