/**
 * 
 * @fileName	Graphs.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */
package com.uob.mars.algorithms.graphs;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.graphs.Graphs;

import com.uob.mars.maths.matrices.Matrix;


/**
 * TODO: Add comment describing what this class does.
 */
public class Graphs {
	
	
	private static final Logger log = Logger.getLogger(Graphs.class);
	
	
	private static int nextLabel = 1;
	
	
	/**
	 * Adapted from https://courses.cs.washington.edu/courses/cse576/02au/homework/hw3/ConnectComponent.java
	 * 
	 * @param input
	 * @param output
	 * @return
	 */
	public static boolean connectedComponentsLabelling(Matrix<? extends Number> input,
			Matrix<? extends Number> output, boolean onlyHalf) {
		
		if(input == null || output == null) {
			return false;
		}
		
		if(input.numberOfRows() == 0 || output.numberOfRows() == 0) {
			return false;
		}
		
		int[] parent = new int[10000];
		int[] labels = new int[10000];
		
		int nextRegion = 1;
		
		// FIRST PASS
		
		for(int i = 0; i < input.numberOfRows(); i++) {
			for(int j = onlyHalf ? i + 1 : 0; j < input.numberOfRows(); j++) {
				
				// If populated, i.e., not "background"/default value
				if(input.get(i, j) != input.getDefaultValue()) {
					
					int k = 0;
					boolean connected = false;
					
					// If connection to the left
					if((j > 0) && input.get(i, j - 1).equals(input.get(i, j))) {
						
						k = output.get(i, j - 1).intValue();
						connected = true;
					}
					
					// If connection to the top
					if((i > 0 && input.get(i - 1, j).equals(input.get(i, j))) &&
							(connected == false || input.get(i - 1, j).intValue() < k)) {
						
						k = output.get(i - 1, j).intValue();
						connected = true;
					}
					
					if (!connected) {
						k = nextRegion;
						nextRegion++;
					}
					
					output.setEntry(i, j, k);
					
					// if connected, but with different label, then do union
					if(j > 0 &&
							input.get(i, j - 1).equals(input.get(i, j)) &&
							output.get(i, j - 1).intValue() != k) {
						
						uf_union(k, output.get(i, j - 1).intValue(), parent);
					}
					
					if(i > 0 &&
							input.get(i - 1, j).equals(input.get(i, j)) &&
							output.get(i - 1, j).intValue() != k) {
						
						uf_union(k, output.get(i - 1, j).intValue(), parent);
					}
				}
			}
		}
		
		// SECOND PASS
		
		nextLabel = 1;		
		for(int i = 0; i < input.numberOfRows(); i++) {
			for(int j = onlyHalf ? i + 1 : 0; j < input.numberOfRows(); j++) {
				
				if(input.get(i, j) != input.getDefaultValue()) {
					
					output.setEntry(i, j, uf_find(output.get(i, j).intValue(), parent, labels));
					
				}
			}
		}
		
		nextLabel--;
		
		System.out.println(nextLabel + " regions");
		
		return true;
	}
	
	
	private static void uf_union(int x, int y, int[] parent)
	{
		while(parent[x] > 0) {
			x = parent[x];
		}
		
		while(parent[y] > 0) {
			y = parent[y];
		}
		
		if(x != y) {
			if(x < y) {
				parent[x] = y;
			}
			else {
				parent[y] = x;
			}
		}
	}
	
	
	public static int uf_find(int x, int[] parent, int[] label) {
		
		while(parent[x] > 0) {
			x = parent[x];
		}
		
		if(label[x] == 0) {
			label[x] = nextLabel++;
		}
		
		return label[x];
	}
	
	
	/**
	 * 
	 * http://math.stackexchange.com/questions/277045/easiest-way-to-determine-all-disconnected-sets-from-a-graph
	 * 
	 * @param input
	 * @return
	 */
	public static List<List<Integer>> findConnectedComponents(Matrix<? extends Number> input) {
		
		List<List<Integer>> components = new ArrayList<List<Integer>>();
		
		// create arraynumberOfRows of marked 
		
		for(int i = 0; i < input.numberOfRows(); i++) {
			
			for(int j = 0; j < input.numberOfRows(); j++) {
				
				// If value in matrix is set
				if(input.get(i, j) != input.getDefaultValue()) {
					
					boolean alreadyPresent = false;
					
					// Check if j has already been covered
					for(List<Integer> component : components) {
						
						if(component.contains(new Integer(j))) {
							alreadyPresent = true;
							break;
						}
					}
					
					// If not covered then add to new component containing j,
					// and perform DFS
					if(!alreadyPresent) {
						
						List<Integer> component = new ArrayList<Integer>();
						component.add(new Integer(j));
						components.add(component);
						
						dfsVisit(input, j, components);
					}
				}
			}
		}
		
		return components;
	}
	
	
	private static void dfsVisit(Matrix<? extends Number> input, int row, List<List<Integer>> components) {
		
		for(int j = 0; j < input.numberOfRows(); j++) {
			
			// If value in matrix is set
			if(input.get(row, j) != input.getDefaultValue()) {
				
				boolean alreadyPresent = false;
				
				// Check if j has already been covered
				for(List<Integer> component : components) {
					
					if(component.contains(new Integer(j))) {
						alreadyPresent = true;
						break;
					}
				}
				
				// If not covered then add to existing component containing j,
				// and perform DFS
				if(!alreadyPresent) {
					
					components.get(components.size() - 1).add(new Integer(j));
					
					dfsVisit(input, j, components);
				}
			}
		}
		
		return;
	}
	
	
}
