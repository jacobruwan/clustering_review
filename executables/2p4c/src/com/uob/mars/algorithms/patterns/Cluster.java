package com.uob.mars.algorithms.patterns;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.patterns.Cluster;
import com.uob.mars.algorithms.patterns.Pattern;

/**
 * A single cluster, or prototype, used in clustering algorithms
 */
public class Cluster implements Pattern {
	
	private static final Logger log = Logger.getLogger(Cluster.class);
	
	List<String> centres;
	
	
	// A constructor for list rather than array
	public Cluster(List<String> centres) {
		
		if (centres == null) {
			log.error("The cluster to be created was not defined");
		}
		
		/*if (this.centres.length != membership.size()) {
			log.error("The number of dimensions the cluster centre should have is " + this.centres.length
					+ ", but the number of dimensions of the cluster to be created was " + cluster.size());
		}*/
		
		this.centres = new ArrayList<String>(centres.size());

		for(int i = 0; i < centres.size(); i++) {
			this.centres.add(centres.get(i));
		}
		//setCluster(membership);
	}
	
	public Cluster(int numberOfDimensions) {

		centres = new ArrayList<String>(numberOfDimensions);
		
		for(int i = 0; i < numberOfDimensions; i++) {
			this.centres.add("XXX");
		}
	}
	
	
	// Return the cluster
	public List<String> getCentres() {
		return centres;
	}
	
	
	// Return just one dimension of the cluster
	public String getCentre(int dimension) {
		return centres.get(dimension);
	}
	
	public void setCluster(List<String> clusterCentreNew) {
		
		this.centres = new ArrayList<String>(clusterCentreNew.size());
		
		for(int i = 0; i < clusterCentreNew.size(); i++) {
		//for(int i = 0; i < this.centres.size(); i++) {
			
			this.centres.add(clusterCentreNew.get(i));
		}
	}
	
	public boolean setDimension(int dimension, String value) {
		
		if (dimension < 0 || dimension >= this.centres.size()) {
			log.error("The dimension of the cluster centre that was going to be changed was " + dimension
					+ ", but the number of dimensions of the cluster is " + this.centres.size());
			return false;
		}
		
		centres.set(dimension, value);
		return true;
	}
	
	public String toString() {
		return centres.toString();
	}
}
