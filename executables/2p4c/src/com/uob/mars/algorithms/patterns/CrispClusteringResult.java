package com.uob.mars.algorithms.patterns;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonProperty;

import com.uob.mars.algorithms.patterns.Cluster;
import com.uob.mars.algorithms.patterns.CrispClusteringResult;
import com.uob.mars.algorithms.patterns.CrispMembership;
import com.uob.mars.algorithms.patterns.Result;

/**
 * TODO: Add comment describing what this class does.
 */
public class CrispClusteringResult implements Result {
	
	private static final Logger log = Logger.getLogger(CrispClusteringResult.class);
	
	@JsonProperty
	private List<Cluster> clusters;
	
	//@JsonProperty // Result.getPatterns() is serialised to JSON so not required here
	private List<CrispMembership> membershipMatrix;
	
	// Stores the entropy of each variable/dimension
	private Map<String, Double> entropy;
	
	/**
	 * Attribute for clusters is declared and initialised with a capacity of numberClusters, which is empty.
	 */
	public CrispClusteringResult(int numberClusters, int numberRecords) {
		
		if(numberClusters > numberRecords) {
			log.error("numberClusters < numberRecords in CrispClusteringResult");
		}
		
		clusters = new ArrayList<Cluster>(numberClusters);
		// Responsibility of caller to add clusters. See addCluster().
		
		membershipMatrix = new ArrayList<CrispMembership>(numberClusters);
		
		for(int i = 0; i < numberClusters; i++) {
			membershipMatrix.add(new CrispMembership(numberRecords));
		}
		
		entropy = new TreeMap<String, Double>();
	}
	
	public CrispClusteringResult() {
		
		clusters = new ArrayList<Cluster>();
		// Responsibility of caller to add clusters. See addCluster().
		
		membershipMatrix = new ArrayList<CrispMembership>();
		// Responsibility of caller to add memberships. See addCluster().
	}
	
	@Override
	public List<CrispMembership> getPatterns() {
		return membershipMatrix;
	}
	
	@Override
	public CrispMembership getPattern(int id) {
		return membershipMatrix.get(id);
	}

	
	public boolean addCluster(Cluster cluster) {
		
		if(cluster == null) {
			return false;
		}
		
		clusters.add(cluster);
		return true;
	}
	
	public boolean setCluster(int id, Cluster cluster) {
		if(cluster == null) {
			log.error("Cluster was null");
			return false;
		}
		
		if(id < 0){
			log.error("id < 0");
			return false;
		}
		
		if(id >= clusters.size()){
			log.error("id is larger than size of list");
			return false;
		}
		
		clusters.set(id, new Cluster(cluster.getCentres()));
		return true;
	}
	
	public Cluster getCluster(int id) {
		return clusters.get(id);
	}
	
	public boolean addMembership(CrispMembership membership) {
		
		if(membership == null) {
			return false;
		}
		
		membershipMatrix.add(membership);
		return true;
	}
	
	public boolean setMembership(int clusterId, int recordId, int value) {
		membershipMatrix.get(clusterId).setMembership(recordId, value);
		return true;
	}
	
	public Map<String, Double> getEntropy () {
		return entropy;
	}
	
	public boolean setEntropy (Map<String, Double> entropy) {
		
		this.entropy = new TreeMap<String, Double>(entropy);
		
		return true;
	}
}