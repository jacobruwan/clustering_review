package com.uob.mars.algorithms.patterns;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.patterns.CrispMembership;
import com.uob.mars.algorithms.patterns.Membership;

/**
 * TODO: Add comment describing what this class does.
 */
public class CrispMembership implements Membership<Integer> {
	
	private static final Logger log = Logger.getLogger(CrispMembership.class);

	List<Integer> membership;
	
	public CrispMembership(int numberOfRecords) {
		
		if (numberOfRecords < 1) {
			log.error("Size of CrispMembership requested to be < 1");
		}
		
		this.membership = new ArrayList<Integer>(numberOfRecords);

		for(int i = 0; i < numberOfRecords; i++) {
			this.membership.add(new Integer(0));
		}
	}
	
	public CrispMembership(List<Integer> membership) {
		
		if (membership == null) {
			log.error("The cluster to be created was not defined");
		}
		
		this.membership = new ArrayList<Integer>(membership.size());

		for(int i = 0; i < membership.size(); i++) {
			this.membership.add(membership.get(i));
		}
	}
	
	@Override
	public List<Integer> getMemberships() {
		return membership;
	}
	
	public Integer getMembership(int recordIndex) {
		return membership.get(recordIndex);
	}
	
	public void setMembership(int recordIndex, int value) {
		membership.set(recordIndex, value);
	}
	
	public String toString() {
		return membership.toString();
	}
}
