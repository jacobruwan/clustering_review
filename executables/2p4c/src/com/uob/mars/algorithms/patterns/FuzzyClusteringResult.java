package com.uob.mars.algorithms.patterns;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.patterns.FuzzyMembership;
import com.uob.mars.algorithms.patterns.FuzzyClusteringResult;
import com.uob.mars.algorithms.patterns.Result;

/**
 * TODO: Add comment describing what this class does.
 */
public class FuzzyClusteringResult implements Result {
	
	private static final Logger log = Logger.getLogger(FuzzyClusteringResult.class);
	
	List<Cluster> clusters;
	
	List<FuzzyMembership> membershipMatrix;
	
	/**
	 * Attribute for clusters is declared and initialised with a capacity of numberClusters, which is empty.
	 */
	public FuzzyClusteringResult(int numberClusters, int numberRecords) {
		
		if(numberClusters > numberRecords) {
			log.error("numberClusters < numberRecords in CrispClusteringResult");
		}
		
		clusters = new ArrayList<Cluster>(numberClusters);
		// Responsibility of caller to add clusters. See addCluster().
		
		membershipMatrix = new ArrayList<FuzzyMembership>(numberClusters);
		
		for(int i = 0; i < numberClusters; i++) {
			membershipMatrix.add(new FuzzyMembership(numberRecords));
		}
	}
	
	public FuzzyClusteringResult() {
		
		clusters = new ArrayList<Cluster>();
		// Responsibility of caller to add clusters. See addCluster().
		
		membershipMatrix = new ArrayList<FuzzyMembership>();
		// Responsibility of caller to add memberships. See addCluster().
	}
	
	/*public List<CrispMembership> getClusterMembership() {
		return membershipMatrix;
	}*/
	
	@Override
	public List<FuzzyMembership> getPatterns() {
		return membershipMatrix;
	}
	
	@Override
	public FuzzyMembership getPattern(int id) {
		return membershipMatrix.get(id);
	}

	
	public boolean addCluster(Cluster cluster) {
		
		if(cluster == null) {
			return false;
		}
		
		clusters.add(cluster);
		return true;
	}
	
	public boolean setCluster(int id, Cluster cluster) {
		if(cluster == null) {
			log.error("Cluster was null");
			return false;
		}
		
		if(id < 0){
			log.error("id < 0");
			return false;
		}
		
		if(id >= clusters.size()){
			log.error("id is larger than size of list");
			return false;
		}
		
		clusters.set(id, new Cluster(cluster.getCentres()));
		return true;
	}
	
	public Cluster getCluster(int id) {
		return clusters.get(id);
	}
	
	public boolean addMembership(FuzzyMembership memberhsip) {
		
		if(memberhsip == null) {
			return false;
		}
		
		return membershipMatrix.add(memberhsip);
	}
	
	public boolean setMembership(int clusterId, int recordId, int value) {
		membershipMatrix.get(clusterId).setMembership(recordId, value);
		return true;
	}
}