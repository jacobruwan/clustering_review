package com.uob.mars.algorithms.patterns;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.patterns.FuzzyMembership;
import com.uob.mars.algorithms.patterns.Membership;

/**
 * TODO: Add comment describing what this class does.
 */
public class FuzzyMembership implements Membership<Double> {
	
	private static final Logger log = Logger.getLogger(FuzzyMembership.class);

	List<Double> membership;
	
	/**
	 * Assign random value to membership
	 * @param size
	 */
	public FuzzyMembership(int size) {
		
		if (size < 1) {
			log.error("Size of CrispMembership requested to be < 1");
		}
		
		this.membership = new ArrayList<Double>(size);

		for(int i = 0; i < size; i++) {
			//this.membership.add(new Double(0));
			this.membership.add(new Random().nextDouble());
		}
	}
	
	public FuzzyMembership(List<Double> membership) {
		
		if (membership == null) {
			log.error("The cluster to be created was not defined");
		}
		
		this.membership = new ArrayList<Double>(membership.size());

		for(int i = 0; i < membership.size(); i++) {
			this.membership.add(membership.get(i));
		}
	}

	public Double getMembership(int recordIndex) {
		return membership.get(recordIndex);
	}
	
	public void setMembership(int recordIndex, double value) {
		membership.set(recordIndex, value);
	}

	@Override
	public List<Double> getMemberships() {
		return membership;
	}
}
