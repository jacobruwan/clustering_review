package com.uob.mars.algorithms.patterns;

import java.util.List;

import com.uob.mars.algorithms.patterns.Pattern;

/**
 * TODO: Add comment describing what this class does.
 * Purpose of this interface is to limit the type to a Number. Otherwise, a non number could be used... where
 */
interface Membership<E extends Number> extends Pattern {
	
	public List<E> getMemberships();
	
	//public boolean setMemberships(List<E> memberhips);
}
