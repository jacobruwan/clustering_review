/**
 * (c) British Telecommunications plc, 2013, All Rights Reserved
 */
package com.uob.mars.algorithms.patterns;

/**
 * A pattern found by an algorithm (e.g., a rule, a sequence, a cluster ...).
 * Not be confused with pattern-recognition terminology.
 */
public interface Pattern {
}
