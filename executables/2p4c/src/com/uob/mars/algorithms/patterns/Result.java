package com.uob.mars.algorithms.patterns;

import java.util.List;

import com.uob.mars.algorithms.patterns.Pattern;

/**
 * TODO: Add comment describing what this class does.
 */
public interface Result {

	/*List<Pattern> results;
	
	public Result(List<Pattern> results) {
		this.results = results;
	}*/
	
	//@JsonIgnore // Only do this to remove the field
	public List<? extends Pattern> getPatterns();
	
	//public boolean setPattern(int id, Pattern pattern);
	
	public Pattern getPattern(int id);
}
