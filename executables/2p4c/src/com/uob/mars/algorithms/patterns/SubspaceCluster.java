package com.uob.mars.algorithms.patterns;

import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.uob.mars.algorithms.patterns.Pattern;
import com.uob.mars.algorithms.patterns.SubspaceCluster;

/**
 * TODO: Add comment describing what this class does.
 */
public class SubspaceCluster implements Pattern {
	
	private static final Logger log = Logger.getLogger(SubspaceCluster.class);
	
	//private List<TreeMap<TreeMap<String, TreeSet<String>>, List<Integer>>> maximalKSignatures = null;
	//private TreeMap<TreeMap<String, TreeSet<String>>, List<Double>> similarityMatrix = null;
	private TreeMap<TreeMap<String, TreeSet<String>>, List<Integer>> maximalKSignature = null;
	private List<Double> similarityMatrix = null;
	
	public SubspaceCluster(TreeMap<TreeMap<String, TreeSet<String>>, List<Integer>> maximalKSignature,
			List<Double> similarityMatrix) {
		
		this.maximalKSignature = maximalKSignature;
		this.similarityMatrix = similarityMatrix;
	}
	
	public TreeMap<TreeMap<String, TreeSet<String>>, List<Integer>> getMaximalKSignatures() {
		return maximalKSignature;
	}
	
	public List<Double> getMembership() {
		return similarityMatrix;
	}
}
