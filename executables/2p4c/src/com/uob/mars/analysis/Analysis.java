package com.uob.mars.analysis;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;


import com.uob.mars.algorithms.Algorithm;
import com.uob.mars.algorithms.patterns.Result;
import com.uob.mars.analysis.Analysis;
import com.uob.mars.csv.CSV;
import com.uob.mars.data.MarsInfo;


/**
 * TODO: Add comment describing what this class does.
 */
public class Analysis {
	
	private static final Logger log = Logger.getLogger(Analysis.class);

	// Can be changed to a collection at a later date?
	@JsonProperty
	private MarsInfo marsInfo;

	// Can be changed to a collection at a later date?
	private Algorithm algorithm;
	
	//TODO Result
	@JsonProperty
	private Result result;
	
	@JsonProperty
	boolean success;
	
	
	public boolean MarsInfo(MarsInfo marsInfo, Algorithm algorithm) {
		
		if(marsInfo == null || algorithm == null) {
			log.error("marsInfo or algorithm is null");
			return false;
		}
		
		this.marsInfo = marsInfo;
		this.algorithm = algorithm;
		
		success = algorithm.run();
		
		result = algorithm.getResult();
		
		return success;
	}
	
	
	public Result getResult() {
		return this.result;
	}
	
	
	public static void main(String[] args) {
		
		CSV csv = new CSV();
		MarsInfo marsInfo = csv.getInfo(0, 0, "csv\\Counterpane-sample.csv", 10);

		/*if (info.getEntities().size() > 0) {
			System.out.println(info.getEntities().get(0));
		}*/
		
		Analysis analysis = new Analysis();

		System.out.println("KMeans: start");

		//analysis.algorithm = new KMeans(marsInfo, 5);
		//analysis.run(marsInfo, new KMeans(marsInfo, 3));
		//System.out.println("KMeans: end");
		
		//List<Cluster> clusters = kMeans.getClusters();
		
		Result results = analysis.getResult();
		
		/*List<Cluster> clusters = results.getPatterns();
		
		for(Cluster cluster: clusters) {
			System.out.println(cluster.toString());
		}*/
	}
}
