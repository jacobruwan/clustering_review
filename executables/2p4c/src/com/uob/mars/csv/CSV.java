package com.uob.mars.csv;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import com.uob.mars.data.MarsInfo;
import com.uob.mars.data.MarsInfoEntity;
import com.uob.mars.csv.CSV;
import com.uob.mars.util.PrimitiveDataTypes;

public class CSV {

	private static final Logger log = Logger.getLogger(CSV.class);
	
	public static boolean commaSeparatedTags = false;
	
	public MarsInfo getInfo(int serviceId, int queryId, String filename, Integer limit) {
		
		MarsInfo marsInfo = new MarsInfo();
		
		log.debug("Reading in data from CSV file " + filename);
		
		try {
			CSVReader reader = new CSVReader(new FileReader("./data/" + filename));
			String[] marsHeader = reader.readNext();
			String[] dataHeader = reader.readNext();
			marsInfo = csvToMars(marsHeader, dataHeader, reader, limit, serviceId, queryId);
			
		} catch (FileNotFoundException e) {
			log.error("Could not find file: " + filename);
		} catch (IOException e) {
			e.printStackTrace();
		} 		
		if (marsInfo.getEntities().size() > 0) {
			log.debug("Mapped " + marsInfo.getEntities().size() + " entities from CSV file - example resultant entity: " + marsInfo.getEntities().get(0));
		}
		return marsInfo;		
	}
	
	public static MarsInfo csvToMars(String[] marsHeader, String[] dataHeader, CSVReader reader, Integer limit, int serviceId, int queryId) throws IOException {
		return csvToMars(marsHeader, dataHeader, reader, limit, serviceId, queryId, null, null);
	}
	
	public static MarsInfo csvToMars(String[] marsHeader, String[] dataHeader, CSVReader reader, int serviceId, int queryId) throws IOException {
		return csvToMars(marsHeader, dataHeader, reader, null, serviceId, queryId, null, null);
	}
	
	public static MarsInfo csvToMars(String[] marsHeader, String[] dataHeader, CSVReader reader, Integer limit, int serviceId, int queryId, Date fromDate, Date toDate) throws IOException {

		List<String[]> wholeFile = reader.readAll();
		List<String[]> input = new ArrayList<String[]>();
		if (limit == null || limit == 0 || wholeFile.size() <= limit) {
			log.debug("Reading in entire CSV file");
			input = wholeFile;
		}
		else {
			log.debug("Reading in last " + limit + " rows from CSV file");
			for (int i=(wholeFile.size()-limit); i<wholeFile.size(); i++) {
				input.add(wholeFile.get(i));
			}
		}
		return CSV.csvToMars(marsHeader, dataHeader, input, serviceId, queryId, fromDate, toDate);
	}
	
	
	public static MarsInfo csvToMars(String[] marsHeader, String[] dataHeader, List<String[]> input, int serviceId, int queryId, Date fromDate, Date toDate) {
		
		MarsInfo marsInfo = new MarsInfo();
		
		Map<String, List<Integer>> marsHeaderIndexes = new HashMap<String, List<Integer>>();
		
		for (int i=0; i<marsHeader.length; i++) {
			
			String col = marsHeader[i];
			
			if (!col.equals("")) {
				if (col.contains("|")) {
					// multiple field defined
					String[] colSplit = col.split("\\|");
					for (String colToken: colSplit) {
						if (!marsHeaderIndexes.containsKey(colToken)) {
							List<Integer> indexList = new ArrayList<Integer>();
							indexList.add(i);
							marsHeaderIndexes.put(colToken, indexList);
						}
						else {
							marsHeaderIndexes.get(colToken).add(i);
						}
					}
				}
				else {
					if (!marsHeaderIndexes.containsKey(col)) {
						List<Integer> indexList = new ArrayList<Integer>();
						indexList.add(i);
						marsHeaderIndexes.put(col, indexList);
					}
					else {
						marsHeaderIndexes.get(col).add(i);
					}
				}
			}
		}
		
		if(dataHeader.length > Short.MAX_VALUE) {
			log.error("Number of atttributes in dataset is greater than max value of Short data type");
		}
		
		for (short i = 0; i < (short)marsHeader.length; i++) {
			
			if(marsHeader[i].contains("tags")) {
				marsInfo.addAttribute(dataHeader[i]);
			}
		}
		
		for (String[] nextLine: input) {
			if (log.getLevel() == Level.TRACE) {
	    		String lineString = "";
	    		for (int i=0; i<nextLine.length; i++) {
		    		if (!lineString.equals("")) lineString += ",";
		    		lineString += nextLine[i];
		    	}
	    		log.trace("Reading in line: " + lineString + " from CSV");
	    	}
	    	MarsInfoEntity entity = new MarsInfoEntity();
	    	
	    	for (String key: marsHeaderIndexes.keySet()) {
	    		for (Integer index: marsHeaderIndexes.get(key)) {
	    			if (nextLine.length >= index+1) {
		    			if (!nextLine[index].equals("") && !nextLine[index].equals("NULL")) { // Check that cell has a non-empty entry
			    			/*if (key.equals("id")) {
			    				entity.setId(entity.getId() + " " + nextLine[index]);
			    			}
			    			if (key.equals("name")) {
			    				entity.setName(nextLine[index]);
			    			}
			    			if (key.equals("type")) {
			    				entity.setType(nextLine[index]);
			    			}
			    			if (key.equals("url")) {
			    				entity.setUrl(nextLine[index]);
			    			}*/
			    			if (key.startsWith("tags")) {
			    				String[] keySplit = key.split("\\%");
			    				if (keySplit.length == 2) {
			    					// Custom tag type
			    					if (commaSeparatedTags) {
			    						String[] tagSplit = nextLine[index].split(",");
			    						for (String tag: tagSplit) {
			    							//entity.addTag(keySplit[1] + ":" + tag);
			    							
			    							Integer indexInt = marsInfo.getAttributesIndex(keySplit[1]);
			    							Short indexShort = null;
			    							
			    							try {
			    								indexShort = PrimitiveDataTypes.safeIntegerToShort(indexInt);
			    							} catch(IllegalArgumentException e) {
			    								log.error(e);
			    							}
			    							
			    							entity.addTag(tag, indexShort);
			    						}
			    					}
			    					else {
			    						//entity.addTag(keySplit[1] + ":" + nextLine[index]);
			    						
			    						Integer indexInt = marsInfo.getAttributesIndex(keySplit[1]);
		    							Short indexShort = null;
		    							
		    							try {
		    								indexShort = PrimitiveDataTypes.safeIntegerToShort(indexInt);
		    							} catch(IllegalArgumentException e) {
		    								log.error(e);
		    							}
		    							
			    						entity.addTag(nextLine[index], indexShort);
			    					}
			    				}
			    				else {
			    					if (commaSeparatedTags) {
			    						String[] tagSplit = nextLine[index].split(",");
			    						for (String tag: tagSplit) {
			    							//entity.addTag(dataHeader[index] + ":" + tag);
			    							
			    							Integer indexInt = marsInfo.getAttributesIndex(dataHeader[index]);
			    							Short indexShort = null;
			    							
			    							try {
			    								indexShort = PrimitiveDataTypes.safeIntegerToShort(indexInt);
			    							} catch(IllegalArgumentException e) {
			    								log.error(e);
			    							}
			    							
			    							entity.addTag(tag, indexShort);
			    						}
			    					}
			    					else {
			    						//entity.addTag(dataHeader[index] + ":" + nextLine[index]);
			    						
			    						Integer indexInt = marsInfo.getAttributesIndex(dataHeader[index]);
		    							Short indexShort = null;
		    							
		    							try {
		    								indexShort = PrimitiveDataTypes.safeIntegerToShort(indexInt);
		    							} catch(IllegalArgumentException e) {
		    								log.error(e);
		    							}
		    							
			    						entity.addTag(nextLine[index], indexShort);
			    					}
			    				}
			    			}
			    			if (key.startsWith("categories")) {
			    				String[] keySplit = key.split("\\%"); // Extract custom date format from column header
			    				if (keySplit.length == 2) {
			    					String[] parentSplit = keySplit[1].split("\\.");
			    					boolean hasCompositeParent = true;
			    					Map<Integer,Integer> parentIndexes = new HashMap<Integer,Integer>();
			    					
			    					for (int p=0; p < parentSplit.length; p++) {
			    						for (int i=0; i < dataHeader.length; i++) {
				    						// For each potential parent node
				    						if (dataHeader[i].equals(parentSplit[p])) {
				    							parentIndexes.put(p, i);
				    						}
			    						}
			    						if (!parentIndexes.containsKey(p)) {
			    							hasCompositeParent = false;
			    						}
			    					}
			    					if (hasCompositeParent) {
			    						String compositeParent = dataHeader[parentIndexes.get(0)]; // Root category
			    						for (Integer parentIndex: parentIndexes.values()) {
			    							compositeParent += ("." + nextLine[parentIndex].replaceAll("\\.", "_")); 
			    						}
			    					
			    						String fullCategory = compositeParent + "." + nextLine[index].replaceAll("\\.", "_"); 
			    						
			    						// Clean up category in case missing fields exist
			    						// Remove all trailing .'s as well as any two or more .'s followed by any text
			    						fullCategory = fullCategory.replaceAll("\\.{2,}.*", "");
			    						
			    						entity.getCategories().add(fullCategory);
			    					} 
			    					else {
			    						// Just add custom root category
			    						entity.getCategories().add(keySplit[1] + nextLine[index].replaceAll("\\.", "_"));
			    					}
			    				}
			    				else {
			    					// Use data header as root category
			    					
			    					// Account for multiple categories mentioned in single cell but which are comma separated 
			    					// TODO account for above case in all cases
			    					/*if (nextLine[index].contains(",")) {
			    						String[] catSplit = nextLine[index].split(",");
			    						for (String cat: catSplit) {
			    							entity.addCategory(cat);
			    						}
			    					}
			    					else {*/
			    						entity.getCategories().add(dataHeader[index] + "." + nextLine[index].replaceAll("\\.", "_"));
			    					//}
			    				}
			    			}
			    			if (key.startsWith("start")) {
			    				String[] keySplit = key.split("\\%"); // Extract custom date format from column header
			    				if (keySplit[1].equals("epoch")) {
			    					if (nextLine[index].length() != 13) {
			    						entity.setStartTime(new Date(new Long(nextLine[index])*1000)); // ms adjustment
			    					}
			    					else {
			    						entity.setStartTime(new Date(new Long(nextLine[index]))); // milliseconds already
			    					}
			    				}
			    				else {
				    				SimpleDateFormat sdf = new SimpleDateFormat(keySplit[1]);
				    				try {
				    					entity.setStartTime(sdf.parse(nextLine[index]));
				    					//log.debug("Parsing date as: " + entity.getStartTime().toGMTString());
				    				} catch (ParseException e) {
				    					log.debug("Could not parse start date: " + nextLine[index]);
				    					//entity.setStartTime(Calendar.getInstance().getTime());
				    				}
		//		    				Calendar cal = Calendar.getInstance(); // Fake end date for range testing
		//		    				cal.setTime(sdf.parse(nextLine[index]));
		//		    				cal.add(Calendar.HOUR, 1);
		//		    				entity.setEndTime(cal.getTime());
			    				}
			    			}
			    			if (key.startsWith("end")) {
			    				String[] keySplit = key.split("\\%"); // Extract custom date format from column header
			    				if (keySplit[1].equals("epoch")) {
			    					if (nextLine[index].length() != 13) {
			    						entity.setStartTime(new Date(new Long(nextLine[index])*1000)); // ms adjustment
			    					}
			    					else {
			    						entity.setStartTime(new Date(new Long(nextLine[index]))); // milliseconds already
			    					}
			    				}
			    				else {
				    				/*SimpleDateFormat sdf = new SimpleDateFormat(keySplit[1]);
				    				try {
				    					entity.setEndTime(sdf.parse(nextLine[index]));
				    				} catch (ParseException e) {
				    					log.debug("Could not parse end date: " + nextLine[index]);
				    				}*/
			    				}
			    			}
			    			if (key.startsWith("acquisition")) {
			    				String[] keySplit = key.split("\\%"); // Extract custom date format from column header
			    				if (keySplit[1].equals("epoch")) {
			    					if (nextLine[index].length() != 13) {
			    						entity.setStartTime(new Date(new Long(nextLine[index])*1000)); // ms adjustment
			    					}
			    					else {
			    						entity.setStartTime(new Date(new Long(nextLine[index]))); // milliseconds already
			    					}
			    				}
			    				else {
				    				SimpleDateFormat sdf = new SimpleDateFormat(keySplit[1]);
				    				/*try {
				    					entity.setAcquisitionTime(sdf.parse(nextLine[index]));
				    				} catch (ParseException e) {
				    					log.debug("Could not parse acquisition date: " + nextLine[index]);
				    				}*/
			    				}
			    			}
			    			/*if (key.startsWith("data")) {
			    				String[] keySplit = key.split("\\%");
			    				if (keySplit.length == 2) {
			    					// Custom data prefix type type
			    					entity.setData(entity.getData() + keySplit[1] + ": " + nextLine[index] + " | "); // delimeter might not be appropriate for all CSV data
			    				}
			    				else {
			    					// With inferred prefix
			    					entity.setData(entity.getData() + dataHeader[index] + ": " + nextLine[index] + " | "); // delimeter might not be appropriate for all CSV data
			    					// Without prefix
			    					//entity.setData(entity.getData() + nextLine[index] + " | "); // delimeter might not be appropriate for all CSV data
			    				}
			    			}*/
			    			/*if (key.equals("source")) {
			    				// Check for IP validity, if fails check then search within text for a potential candidate
			    				if (IPFinder.isValidIP(nextLine[index])) {
			    					//entity.setSourceIp(nextLine[index]);
			    					entity.addTag("-Source IP:" + nextLine[index]);
			    				}
			    				else {
			    					for (String candidateIP: IPFinder.findUniqueIPAddresses(nextLine[index])) {
			    						//entity.setSourceIp(candidateIP);
			    						entity.addTag("-Source IP:" + candidateIP);
			    						break; // Only need one IP for source - take first 
			    					}
			    				}		    				
			    			}
			    			if (key.equals("destination")) {
			    				// Check for IP validity, if fails check then search within text for a potential candidate, if there are multiple then add all
			    				if (IPFinder.isValidIP(nextLine[index])) {
			    					//entity.getDestIps().add(nextLine[index]);
			    					entity.addTag("-Destination IP:" + nextLine[index]);
			    				}
			    				else {
			    					for (String candidateIP: IPFinder.findUniqueIPAddresses(nextLine[index])) {
			    						//entity.getDestIps().add(candidateIP);
			    						entity.addTag("-Destination IP:" + candidateIP);
			    					}
			    				}
			    			}*/
			    			/*try {
			    				// TODO add support for "%locName,locType" in order to support multiple locations with same type (e.g. for destinations)
			    				// ... need to check if String specified as 'locName' exists in dataHeader and use values from that column (as for categories)
			    				// ... otherwise, just use 'locName' verbatim
				    			if (key.startsWith("latitude")) {
				    				String[] keySplit = key.split("\\%"); // Extract location name from column header
				    				if (locs.containsKey(keySplit[1])) {
				    					locs.get(keySplit[1]).setLat(Double.parseDouble(nextLine[index])); // XXX lat/lon flipped
				    				}
				    				else {
				    					Location loc = new Location();
				    					loc.setName(keySplit[1]); // redundant, TODO add as above in comment
				    					loc.setType(keySplit[1]);
				    					loc.setLat(Double.parseDouble(nextLine[index])); // XXX lat/lon flipped
				    					locs.put(keySplit[1], loc);
				    				}
				    			}
				    			if (key.startsWith("longitude")) {
				    				String[] keySplit = key.split("\\%"); // Extract location name from column header
				    				if (locs.containsKey(keySplit[1])) {
				    					locs.get(keySplit[1]).setLon(Double.parseDouble(nextLine[index])); // XXX lat/lon flipped - fixed
				    				}
				    				else {
				    					Location loc = new Location();
				    					loc.setName(keySplit[1]); // redundant TODO, add as above in comment
				    					loc.setType(keySplit[1]);
				    					loc.setLon(Double.parseDouble(nextLine[index])); // XXX lat/lon flipped
				    					locs.put(keySplit[1], loc);
				    				}
				    			}
			    			} catch (NumberFormatException e) {
			    				log.debug("Could not read lat/lon from data source" + e);
			    			}*/
		    			}
	    			}
	    		}
	    	}
	    	
	    	// Remove excess array items to same memory
	    	((ArrayList<String>)entity.getTags()).trimToSize();
	    	((ArrayList<Short>)entity.getIndexes()).trimToSize();
	    	
			marsInfo.getEntities().add(entity);
	    }
		
		return marsInfo;
	}
	

	public static void main(String[] args) {
		CSV csv = new CSV();
		MarsInfo info = csv.getInfo(0, 0, args[0], 0);
		System.out.println("Parsed " + info.getEntities().size() + " entities");
//		if (info.getEntities().size() > 0) {
//			System.out.println(info.getEntities().get(0));
//		}
	}
	
}

