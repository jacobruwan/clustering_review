package com.uob.mars.data;


/**
 * 
 * @fileName	DataUtils.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */


public class DataUtils {
	
	
	/**
	 * Split tag by first occurrence of a keyword
	 * 
	 * @param tag
	 * @return String array of length 2.
	 */
	public static String[] splitByFirstOccurrence(String tag, String keyword) {
		
		String[] result = new String[2];
		int index = tag.indexOf(keyword);
		
		if(index != -1) {
			result[0] = tag.substring(0, index);
			result[1] = tag.substring(index + 1);
		}
		
		return result;
	}
}
