/**
 * (c) British Telecommunications plc, 2013, All Rights Reserved
 */
package com.uob.mars.data;

/**
 * A dataset can contain all numeric, all nominal, or mixed types.
 */
public enum DatasetType {
	EMPTY,
	QUANTITATIVE,
	NOMINAL,
	MIXED
}