package com.uob.mars.data;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * 
 * @fileName	MarsInfoEntity.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

public class MarsInfo implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	//private Map<String, Short> tagIndexes = new TreeMap<String, Short>();
	private ArrayList<String> attributes = new ArrayList<String>();
	private ArrayList<MarsInfoEntity> entities = new ArrayList<MarsInfoEntity>();
	//private Integer queryId = Integer.MAX_VALUE;
	//private MetaData metaData = new MetaData();
	
	/**
	 * Keyword for splitting tags.
	 */
	private String splitKeyword = new String();
	
	
	public MarsInfo () {
		setSplitKeyword(":");
	}
	
	
	public MarsInfo( ArrayList<MarsInfoEntity> entities) {
		this();
		this.entities = entities;
	}
	
	
	public ArrayList<MarsInfoEntity> getEntities() {
		return entities;
	}
	
	
	public void setEntities(ArrayList<MarsInfoEntity> entities) {
		this.entities = entities;
	}
	
	
	public ArrayList<String> getAttributes() {
		return attributes;
	}
	
	
	@Override
	public String toString() {
		String output = "";
		for (MarsInfoEntity entity: entities) {
			output += entity.toString() + "\n";
		}
		return output;
	}
	
	
	public void addAttribute (String key) {
		attributes.add(key);
	}
	
	
	public Integer getAttributesIndex (String attribute) {
		return attributes.indexOf(attribute);
	}


	public String getSplitKeyword() {
		return splitKeyword;
	}


	public void setSplitKeyword(String splitKeyword) {
		this.splitKeyword = splitKeyword;
	}
	
}