package com.uob.mars.data;

import java.io.Serializable;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;

/**
 * 
 * @fileName	MarsInfoEntity.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

public class MarsInfoEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	private Date startTime = Calendar.getInstance().getTime();
	private List<Short> indexes = new ArrayList<Short>();
	private List<String> tags = new ArrayList<String>();
	private List<String> categories = new ArrayList<String>();
	
	
	private static final Logger log = Logger.getLogger(MarsInfoEntity.class);
	
	
	public MarsInfoEntity() { }
	
	
	public MarsInfoEntity(Date date, List<String> tags) {
		
		this.startTime = date;
		this.tags = tags;
	}
	
	
	@Override
	public String toString() {
		return "Start time: " + ((startTime==null) ? "null" : startTime.toGMTString()) + "\n" 
			+ "tags: " + tags;
	}
	
	
	public List<String> getTags() {
		return tags;
	}
	
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	
	public String getTag(Short index) {
		if(index < 0) { return null; }
		return indexes.contains(index) ? tags.get(indexes.indexOf(index)) : null;
	}
	
	
	public List<Short> getIndexes() {
		return indexes;
	}
	
	
	public void setIndexes(List<Short> indexes) {
		this.indexes = indexes;
	}
	
	
	public Date getStartTime() {
		return startTime;
	}
	
	
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	
	public void addTag(String tag, Short index) {
		
		tags.add(tag);
		indexes.add(index);
	}
	
	
	public List<String> getCategories() {
		return categories;
	}
	
	
}
