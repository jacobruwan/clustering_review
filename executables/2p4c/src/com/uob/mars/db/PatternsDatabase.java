package com.uob.mars.db;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.h2.jaqu.Db;
import org.h2.jdbc.JdbcSQLException;
import org.h2.tools.Server;

import com.uob.mars.db.PatternsDatabase;

/**
 * Singleton pattern
 */
public class PatternsDatabase {
	
	
	private static final Logger log = Logger.getLogger(PatternsDatabase.class);
	
	private static Server tcpServer;
	//private Server webServer;
	private static Db db;
	private static HashMap<String, Db> dbList = new HashMap<String, Db>();
	private static PatternsDatabase instance;
	
	
	private PatternsDatabase(String databaseName) {
		
		// Start H2 server
		final String[] argsH2 = new String[] {
				"-baseDir", "./",
				"-tcpAllowOthers", // The options -xAllowOthers are potentially risky. For details, see Advanced Topics / Protection against Remote Access.
				"-webAllowOthers",
				"-tcpPort", "9093"}; // SATURN uses 9092?
				//"-webPort", "9096"}; // SATURN uses 9095?
		try {
			// Start server in a different Thread
			tcpServer = Server.createTcpServer(argsH2).start();
			//webServer = Server.createWebServer(argsH2).start();
			log.info("Started H2 Server on TCP port " + tcpServer.getPort());
			//log.info("Started H2 Server on TCP port " + tcpServer.getPort() + " Web server port " + webServer.getPort());
			// If database is already open then this throws...
			// java.lang.Error: org.h2.jdbc.JdbcSQLException: Database may be already in use: "Locked by another process". Possible solutions: close all other connection(s); use the server mode; SQL statement:
			// I do not know how to catch this
			db = Db.open("jdbc:h2:" + databaseName, "Saturn", "NECnet22");
		} catch (Exception e) {
			log.info("H2 Server already running");
			try {
				db = Db.open("jdbc:h2:tcp://localhost/" + databaseName, "Saturn", "NECnet22");
			} catch(Exception e2) {
				log.info("Unable to connect to H2 Server on localhost");
			}
		}
	}
	
	
	public static PatternsDatabase getInstance() {
		if (instance==null) instance = new PatternsDatabase("patterns");
		return instance;
	}
	
	
	public static PatternsDatabase getInstance(String databaseName) {
		if (instance==null) instance = new PatternsDatabase(databaseName);
		return instance;
	}
	
	
	public Db getDb() {
		return db;
	}
	
	
	public Db getDb(String name) {
		Db dbReturn = null;
		if (dbList.keySet().contains(name)) {
			dbReturn = dbList.get(name);
		}
		else {
			//dbReturn = Db.open("jdbc:h2:" + name, "Saturn", "NECnet22");
			dbReturn = Db.open("jdbc:h2:tcp://localhost/" + name, "Saturn", "NECnet22");
			dbList.put(name, dbReturn);
		}
		return dbReturn;
	}
	
	
	public static void stopInstance() {
		
		if(instance != null) {
			db.close();
			tcpServer.stop();
			//webServer.stop();
			instance = null;
			log.info("Stopped H2 Server");
		}
	}
	
	
	public static void main(String[] args) {
		PatternsDatabase kdb = PatternsDatabase.getInstance();
		kdb.stopInstance();
	}
}
