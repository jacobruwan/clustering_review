package com.uob.mars.db;

import static org.h2.jaqu.Define.primaryKey;
import static org.h2.jaqu.Define.tableName;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import org.h2.jaqu.Table;

import com.uob.mars.db.PatternsDatabase;
import com.uob.mars.db.SubspaceClustersEntry;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CComparators;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CIntervalCrisp;
import com.uob.mars.db.SubspaceClustersEntry;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CIntervalFuzzy;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportCrisp;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportFuzzy;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;

/**
 * TODO: Add comment describing what this class does.
 */
public class SubspaceClustersEntry implements Table, Serializable {
	
	
	private static final Logger log = Logger.getLogger(SubspaceClustersEntry.class);
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Example: "srcIP={192.168.0.1, 192.168.54.3},..."
	 * TODO Create a table bins (binId, variables)
	 */
	public String variables;
	
	
	/**
	 * Primary key
	 */
	public Integer id;
	
	
	/**
	 * Id of cluster in a set of results.
	 */
	public Integer clusterId;
	
	
	/**
	 * Foreign key for @see com.bt.mars.db.SubspaceClustersParamConfigsEntry
	 */
	public Integer paramId;
	
	
	/**
	 * 
	 */
	public Integer supportCrisp;
	
	
	/**
	 * 
	 */
	public Double supportFuzzy;
	
	
	@Override
	public void define() {
		tableName("SubspaceClusters");
		primaryKey(id);
	}
	
	
	/**
	 * 
	 * @param variables In JSON format
	 * @param clusterId
	 * @param paramId
	 * @param supportCrisp Crisp support of cluster
	 * @param supportFuzzy Fuzzy support of cluster. Note: this is set to 0.0
	 * when a crisp algorithm is used. Use the SubspaceClustersParamConfigsEntry
	 * table to identify whether a crisp or fuzzy algorithm is used.
	 * @return
	 */
	public static SubspaceClustersEntry create(TreeMap<String, P3CInterval<? extends Number>> variables,
			int clusterId,
			int supportCrisp,
			double supportFuzzy,
			int paramId) {
		
		SubspaceClustersEntry sce = new SubspaceClustersEntry();
		
		sce.id = SubspaceClustersEntry.getNextPrimaryKey();
		
		ObjectMapper mapper = new ObjectMapper();
		// Must set isGetterVisibility off otherwise this will produce
		// "empty":true/false in JSON
		mapper.setVisibilityChecker(mapper.getSerializationConfig().getDefaultVisibilityChecker()
				.withFieldVisibility(JsonAutoDetect.Visibility.ANY)
				.withGetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withSetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withCreatorVisibility(JsonAutoDetect.Visibility.NONE)
				.withIsGetterVisibility(JsonAutoDetect.Visibility.NONE));
		try {
			sce.variables = mapper.writeValueAsString(variables);
		} catch (JsonGenerationException e) {
			log.error(e);
		} catch (JsonMappingException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
		
		sce.clusterId = new Integer(clusterId);
		sce.paramId = new Integer(paramId);
		sce.supportCrisp = new Integer(supportCrisp);
		sce.supportFuzzy = new Double(supportFuzzy);
		PatternsDatabase.getInstance().getDb().insert(sce);
		
		//TODO Not returning result from a query, just the query (See QueryEntry.java), is this ok?
		return sce;
	}
	
	
	public static Integer getNextPrimaryKey() {
		SubspaceClustersEntry sc = new SubspaceClustersEntry();
		List<SubspaceClustersEntry> scResult = new ArrayList<SubspaceClustersEntry>();
		scResult = PatternsDatabase.getInstance().getDb().from(sc).select();
		
		if(scResult.size() > 0) {
			return new Integer(scResult.get(scResult.size() - 1).id.intValue() + 1);
		} else {
			return new Integer(1);
		}
	}
	
	
	/**
	 * 
	 * @param paramId
	 * @return 
	 */
	public static List<TreeMap<String, P3CInterval<? extends Number>>> getClustersByParamID(int paramId, boolean fuzzy) {
		
		List<TreeMap<String, P3CInterval<? extends Number>>> clusters =
				new ArrayList<TreeMap<String, P3CInterval<? extends Number>>>();
		
		SubspaceClustersEntry sce = new SubspaceClustersEntry();
		
		List<SubspaceClustersEntry> allRecords =
				PatternsDatabase.getInstance().getDb().from(sce)
				.where(sce.paramId).is(paramId).select();
		
		ObjectMapper mapper = new ObjectMapper();
		//TypeReference<TreeMap<String, P3CInterval<? extends Number>>> typeRef =
		//		new TypeReference<TreeMap<String, P3CIntervalCrisp>>() {};
		
		for(SubspaceClustersEntry record : allRecords) {
			
			TreeMap<String, P3CInterval<? extends Number>> clusterCore = null;
					//new TreeMap<String, P3CIntervalCrisp>();
			try {
				if(fuzzy) {
					clusterCore = mapper.readValue(record.variables,
							new TypeReference<TreeMap<String, P3CIntervalFuzzy>>(){});
				} else {
					clusterCore = mapper.readValue(record.variables,
							new TypeReference<TreeMap<String, P3CIntervalCrisp>>(){});
				}
			} catch (JsonParseException e) {
				log.error(e);
			} catch (JsonMappingException e) {
				log.error(e);
			} catch (IOException e) {
				log.error(e);
			}
			clusters.add(clusterCore);
		}
		
		return clusters;
	}
	
	
	/**
	 * Get clusters and support values by using the primary key of the parameter configuration.
	 * 
	 * @param paramId Primary key in subspaceclustersparamconfigs table in patterns.h2.db database file
	 * @param fuzzy
	 * @return
	 */
	
	
	/**
	 * See Experimentation.java instead.
	 * 
	 * Get clusters and support values by using the anme of the algorithm and dataset
	 * 
	 * @param algorithmId Algorithm name, such as "p3cvanilla"
	 * @param dataId Dataset name, such as "soybean.csv"
	 * @return
	 */
	/*
	@Deprecated
	public static TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> getClustersAndSupportByParamID(
			String algorithmId,
			String dataId) {
		
		TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> clusters =
				new TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface>(
						P3CComparators.comparatorTreeMapOfComparatorP3CIntervalWithStringNames);
		
		TreeMap<String, P3CInterval<? extends Number>> clusterCore = null;
		int supportCrisp = 0;
		double supportFuzzy = 0.0;
		P3CSupportInterface support = null;
		boolean supportAsList = false;
		int paramId = -1; // We need to find this usign the function arguments.
		boolean fuzzyAlgorithm = false;
		SubspaceClustersEntry sce = new SubspaceClustersEntry();
		SubspaceClustersParamConfigsEntry scpce = new SubspaceClustersParamConfigsEntry();
		
		paramId = scpce.getIDWhere(algorithmId, dataId);
		
		if(algorithmId.equals("twopfourc")) {
			fuzzyAlgorithm = true;
		}
		
		List<SubspaceClustersEntry> allRecords =
				PatternsDatabase.getInstance().getDb().from(sce)
				.where(sce.paramId).is(paramId).select();
		
		ObjectMapper mapper = new ObjectMapper();
		//TypeReference<TreeMap<String, P3CInterval<? extends Number>>> typeRef =
		//		new TypeReference<TreeMap<String, P3CIntervalCrisp>>() {};
		
		for(SubspaceClustersEntry record : allRecords) {
			
			try {
				if(fuzzyAlgorithm) {
					clusterCore = mapper.readValue(record.variables,
							new TypeReference<TreeMap<Short, P3CIntervalFuzzy>>(){});
				} else {
					clusterCore = mapper.readValue(record.variables,
							new TypeReference<TreeMap<Short, P3CIntervalCrisp>>(){});
				}
			} catch (JsonParseException e) {
				log.error(e);
			} catch (JsonMappingException e) {
				log.error(e);
			} catch (IOException e) {
				log.error(e);
			}
			
			supportCrisp = record.supportCrisp.intValue();
			supportFuzzy = record.supportFuzzy.doubleValue();
			
			if(fuzzyAlgorithm) {
				//initialise fuzzy support object
				P3CSupportFuzzy supportFuzzyTemp = new P3CSupportFuzzy(supportAsList);
				supportFuzzyTemp.addFuzzySupport(supportFuzzy);
				support = supportFuzzyTemp;
			} else {
				// initialise crisp support object
				support = new P3CSupportCrisp(supportAsList);
			}
			
			for(int i = 0; i < supportCrisp; i++) {
				support.addSupport();
			}
			
			clusters.put(clusterCore, support);
		}
		
		return clusters;
	}
	*/
	
	
	public static TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> getClustersAndSupportByParamID(
			int paramId,
			boolean fuzzy) {
		
		TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> clusters =
				new TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface>(
						P3CComparators.comparatorTreeMapOfComparatorP3CIntervalWithStringNames);
		
		TreeMap<String, P3CInterval<? extends Number>> clusterCore = null;
		int supportCrisp = 0;
		double supportFuzzy = 0.0;
		P3CSupportInterface support = null;
		boolean supportAsList = false;
		
		SubspaceClustersEntry sce = new SubspaceClustersEntry();
		
		List<SubspaceClustersEntry> allRecords =
				PatternsDatabase.getInstance().getDb().from(sce)
				.where(sce.paramId).is(paramId).select();
		
		ObjectMapper mapper = new ObjectMapper();
		//TypeReference<TreeMap<String, P3CInterval<? extends Number>>> typeRef =
		//		new TypeReference<TreeMap<String, P3CIntervalCrisp>>() {};
		
		for(SubspaceClustersEntry record : allRecords) {
			
			try {
				if(fuzzy) {
					clusterCore = mapper.readValue(record.variables,
							new TypeReference<TreeMap<String, P3CIntervalFuzzy>>(){});
				} else {
					clusterCore = mapper.readValue(record.variables,
							new TypeReference<TreeMap<String, P3CIntervalCrisp>>(){});
				}
			} catch (JsonParseException e) {
				log.error(e);
			} catch (JsonMappingException e) {
				log.error(e);
			} catch (IOException e) {
				log.error(e);
			}
			
			supportCrisp = record.supportCrisp.intValue();
			supportFuzzy = record.supportFuzzy.doubleValue();
			
			if(fuzzy) {
				//initialise fuzzy support object
				P3CSupportFuzzy supportFuzzyTemp = new P3CSupportFuzzy(supportAsList);
				supportFuzzyTemp.addFuzzySupport(supportFuzzy);
				support = supportFuzzyTemp;
			} else {
				// initialise crisp support object
				support = new P3CSupportCrisp(supportAsList);
			}
			
			for(int i = 0; i < supportCrisp; i++) {
				support.addSupport(/*new Integer(i)*/);
			}
			
			clusters.put(clusterCore, support);
		}
		
		return clusters;
	}

}
