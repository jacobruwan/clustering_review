package com.uob.mars.db;

import static org.h2.jaqu.Define.primaryKey;
import static org.h2.jaqu.Define.tableName;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.h2.jaqu.Db;
import org.h2.jaqu.Table;

import com.uob.mars.db.SubspaceClustersParamConfigsEntry;
import com.uob.mars.db.PatternsDatabase;

/**
 * 
 * @fileName	MarsInfoEntity.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

//@Entity
public class SubspaceClustersParamConfigsEntry implements Table, Serializable {
	
	
	private static final Logger log = Logger.getLogger(SubspaceClustersParamConfigsEntry.class);
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Primary key
	 */
	//@Id
	//@GeneratedValue//(strategy = GenerationType.IDENTITY)
	public Integer id;
	
	
	/**
	 * Simple class name (in lower case) of algorithms from @see com.bt.mars.algorithms
	 */
	public String algorithmId;
	
	
	/**
	 * Poisson threshold parameters used by subspace clustering algorithm.
	 * Example: "1.0E-50"
	 */
	public String poisson;
	
	
	/**
	 * Dataset name
	 * TODO create a table for dataset (id, datasetName)
	 */
	public String dataId;
	
	
	/**
	 * UTC time for start of dataset.
	 */
	public Timestamp start;
	
	
	/**
	 * UTC time for end of dataset.
	 */
	public Timestamp end;
	
	
	@Override
	public void define() {
		tableName("SubspaceClustersParamConfigs");
		primaryKey(id);
	}
	
	
	public SubspaceClustersParamConfigsEntry() {
		
		this.id = -1;
		this.algorithmId = "";
		this.poisson = "";
		this.dataId = "";
		this.start = new Timestamp(0L);
		this.end = new Timestamp(0L);
	}
	
	
	public static SubspaceClustersParamConfigsEntry create(String algorithmId,
			Double poisson,
			String dataId,
			Timestamp start,
			Timestamp end) {
		
		SubspaceClustersParamConfigsEntry scpc = new SubspaceClustersParamConfigsEntry();
		
		scpc.id = SubspaceClustersParamConfigsEntry.getNextPrimaryKey();
		//1;//Math.abs(start.hashCode() + end.hashCode());
		scpc.algorithmId = algorithmId.toLowerCase();
		scpc.poisson = poisson.toString();
		scpc.dataId = dataId;
		scpc.start = start;
		scpc.end = end;
		
		log.info("Creating parameter configuration entry in database for: " +
				scpc.algorithmId + ", " + scpc.poisson + ", " + scpc.dataId +
				", " + scpc.start + ", " + scpc.end);
		
		PatternsDatabase.getInstance().getDb().insert(scpc);
		
		//TODO Not returning result from a query, just the query (See QueryEntry.java), is this ok?
		
		return scpc;
	}
	
	
	public static SubspaceClustersParamConfigsEntry get(int queryId) {
		
		PatternsDatabase patterns = PatternsDatabase.getInstance();
		if(null == patterns) {
			log.error("null PatternsDatabase object");
		}
		
		Db db = patterns.getDb();
		if(null == db) {
			log.error("null Db object");
		}
		
		SubspaceClustersParamConfigsEntry scpc = new SubspaceClustersParamConfigsEntry();
		scpc = db.from(scpc).where(scpc.id).is(queryId).selectFirst();
		
		return scpc;
	}
	
	
	public static List<SubspaceClustersParamConfigsEntry> getAll() {
		
		SubspaceClustersParamConfigsEntry scpc = new SubspaceClustersParamConfigsEntry();
		
		PatternsDatabase patterns = PatternsDatabase.getInstance();
		if(null == patterns) {
			log.error("null PatternsDatabase object");
		}
		
		Db db = patterns.getDb();
		if(null == db) {
			log.error("null Db object");
		}
		
		List<SubspaceClustersParamConfigsEntry> scpcResults = db.from(scpc).select();
		
		return scpcResults;
	}
	
	
	public static int getIDWhere(String algorithmId,
			Double poisson,
			String dataId,
			Timestamp start,
			Timestamp end) {
		
		List<Integer> IDs = new ArrayList<Integer>();
		
		SubspaceClustersParamConfigsEntry scpc = new SubspaceClustersParamConfigsEntry();
		
		PatternsDatabase patterns = PatternsDatabase.getInstance();
		if(null == patterns) {
			log.error("null PatternsDatabase object");
		}
		
		Db db = patterns.getDb();
		if(null == db) {
			log.error("null Db object");
		}
		
		List<SubspaceClustersParamConfigsEntry> allRecords =
				//PatternsDatabase.getInstance().getDb().from(scpc).where(scpc.algorithmId).is(algorithmId)
				db.from(scpc).where(scpc.algorithmId).is(algorithmId)
				.and(scpc.poisson).is(poisson.toString())
				.and(scpc.dataId).is(dataId)
				.and(scpc.start).smallerEqual(start)
				.and(scpc.end).biggerEqual(end)
				.select();
		
		for(SubspaceClustersParamConfigsEntry record : allRecords) {
			
			if(!IDs.contains(record.id)) {
				IDs.add(record.id);
			}
		}
		
		//PatternsDatabase.stopInstance();
		
		if (IDs.size() == 1) {
			return IDs.get(0);
		} else {
			if(IDs.size() > 1) {
				log.warn("Multiple SubspaceClustersParamConfigsEntry were found");
			}
			return -1;
		}
	}
	
	
	public static int getIDWhere(String algorithmId,
			String dataId) {
		
		List<Integer> IDs = new ArrayList<Integer>();
		
		SubspaceClustersParamConfigsEntry scpc = new SubspaceClustersParamConfigsEntry();
		
		PatternsDatabase patterns = PatternsDatabase.getInstance();
		if(null == patterns) {
			log.error("null PatternsDatabase object");
		}
		
		Db db = patterns.getDb();
		if(null == db) {
			log.error("null Db object");
		}
		
		List<SubspaceClustersParamConfigsEntry> allRecords =
				//PatternsDatabase.getInstance().getDb().from(scpc).where(scpc.algorithmId).is(algorithmId)
				db.from(scpc).where(scpc.algorithmId).is(algorithmId)
				.and(scpc.dataId).is(dataId)
				.select();
		
		for(SubspaceClustersParamConfigsEntry record : allRecords) {
			
			if(!IDs.contains(record.id)) {
				IDs.add(record.id);
			}
		}
		
		//PatternsDatabase.stopInstance();
		
		if (IDs.size() == 1) {
			return IDs.get(0);
		} else {
			if(IDs.size() > 1) {
				log.warn("Multiple SubspaceClustersParamConfigsEntry were found");
			}
			return -1;
		}
	}
	
	
	public static Integer getNextPrimaryKey() {
		
		SubspaceClustersParamConfigsEntry scpc = new SubspaceClustersParamConfigsEntry();
		
		PatternsDatabase patterns = PatternsDatabase.getInstance();
		if(null == patterns) {
			log.error("null PatternsDatabase object");
		}
		
		Db db = patterns.getDb();
		if(null == db) {
			log.error("null Db object");
		}
		
		List<SubspaceClustersParamConfigsEntry> scpcResult = db.from(scpc).select();
		
		//PatternsDatabase.stopInstance();
		
		if(scpcResult.size() > 0) {
			return new Integer(scpcResult.get(scpcResult.size() - 1).id.intValue() + 1);
		} else {
			return new Integer(1);
		}
	}
	
	
	public static void main (String[] args) {
	}
}
