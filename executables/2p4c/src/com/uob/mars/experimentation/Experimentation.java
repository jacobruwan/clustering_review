/**
 * 
 * @fileName	Experimentation.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */
package com.uob.mars.experimentation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CComparators;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportFuzzy;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;
import com.uob.mars.db.PatternsDatabase;
import com.uob.mars.db.SubspaceClustersEntry;
import com.uob.mars.db.SubspaceClustersParamConfigsEntry;


/**
 * TODO: Add comment describing what this class does.
 */
public class Experimentation {
	
	
	private static final Logger log = Logger.getLogger(Experimentation.class);
	
	
	/*public static TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> getClusterResults(
			String algorithmId,
			Double poisson,
			String dataId,
			String start,
			String end) {
		
		DateTimeFormatter parser = ISODateTimeFormat.dateTimeNoMillis();
		
		DateTime startDT = new DateTime();
		DateTime endDT = new DateTime();
		
		try {
			startDT = parser.parseDateTime(start);
		} catch (IllegalArgumentException e) {
			log.error(e);
		}
		
		try {
			endDT = parser.parseDateTime(end);
		} catch (IllegalArgumentException e) {
			log.error(e);
		}
		
		Timestamp startT = new Timestamp(startDT.getMillis());
		Timestamp endT = new Timestamp(endDT.getMillis());
		
		int paramId = SubspaceClustersParamConfigsEntry.getIDWhere(algorithmId, poisson, dataId, startT, endT);
		
		return getClusterResults(paramId, algorithmId.contains("fuzzy"));
	}*/
	
	
	/*public static TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> getClusterResults(int paramId, boolean fuzzy) {
		
		TreeMap<TreeMap<Short, P3CInterval<? extends Number>>, P3CSupportInterface> clustersAndSupport =
				SubspaceClustersEntry.getClustersAndSupportByParamID(paramId, fuzzy);
		
		// Shutdown the database, which prevents a lock remaining present.
		// Causes an error when used here, so this was moved to main()
		//PatternsDatabase.getInstance().stopInstance();
		
		return clustersAndSupport;
	}*/
	
	
	public static TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> getClusterResults(String algorithmId, String dataId) {
		
		int paramId = SubspaceClustersParamConfigsEntry.getIDWhere(algorithmId, dataId);
		
		boolean fuzzyAlgorithm = false;
		
		if(algorithmId.equals("twopfourc") /* || algorithmId.equals("...") */) {
			fuzzyAlgorithm = true;
		}
		
		TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> clustersAndSupport =
				SubspaceClustersEntry.getClustersAndSupportByParamID(paramId, fuzzyAlgorithm);
		
		// Shutdown the database, which prevents a lock remaining present.
		// Causes an error when used here, so this was moved to main()
		//PatternsDatabase.getInstance().stopInstance();
		
		return clustersAndSupport;
	}
	
	
	/*static private class Tuple{
		public Tuple(){}
		public TreeMap<String, P3CInterval<? extends Number>> fuzzyCluster;
		public TreeMap<String, P3CInterval<? extends Number>> crispCluster;
		public P3CSupportInterface fuzzySupport;
		public P3CSupportInterface crispSupport;
		public int fuzzyOrCrispEmbedded; //-1 is crisp, 1 is fuzzy, 0 is ignored
		public int sizeOfNotOverlap;
	}*/
	
	
}
