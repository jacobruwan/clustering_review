package com.uob.mars.experimentation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.apache.log4j.Logger;


import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CComparators;
import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportFuzzy;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;
import com.uob.mars.db.PatternsDatabase;


public class FUZZIEEE2014analysis {
	
	
	private static final Logger log = Logger.getLogger(FUZZIEEE2014analysis.class);
	
	
	public static void main (String[] args) {
		
		TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> crispClusters = null;
		TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> fuzzyClusters = null;
		
		List<String> dataIds = new ArrayList<String>();
		
		String algorithmIdCrisp = new String("p3cvanilla");
		String algorithmIdFuzzy = new String("twopfourc");
		
		dataIds.add("breast-cancer-wisconsin.csv");
		dataIds.add("house-votes-84.csv");
		dataIds.add("lymphography.csv");
		dataIds.add("agaricus-lepiota.csv");
		dataIds.add("soybean.csv");
		dataIds.add("splice.csv");
		
		Iterator<String> dataIter = dataIds.iterator();
		
		while(dataIter.hasNext()) {
			
			System.out.println("================");
			
			String dataId = dataIter.next();
			
			crispClusters = Experimentation.getClusterResults(algorithmIdCrisp, dataId);
			if(crispClusters.isEmpty()) {
				log.error("No crisp clusters were found in " + dataId);
			}
			
			fuzzyClusters = Experimentation.getClusterResults(algorithmIdFuzzy, dataId);
			if(fuzzyClusters.isEmpty()) {
				log.error("No fuzzy clusters were found " + dataId);
			}
			
			List<Tuple> comparisons = new ArrayList<Tuple>(); 
			
			// Shutdown the database, which prevents a lock remaining present.
			PatternsDatabase.getInstance().stopInstance();
			
			//System.out.println(crispClusters);
			//System.out.println(fuzzyClusters);
			
			// Perform a pair-wise comparison of all combinations of results.
			// Similar/same functionality in P3C.removeNestedSignatures()
			
			Iterator<TreeMap<String, P3CInterval<? extends Number>>> crispIterator =
					crispClusters.keySet().iterator();
			Iterator<TreeMap<String, P3CInterval<? extends Number>>> fuzzyIterator = null;
			
			while(crispIterator.hasNext()) {
				
				TreeMap<String, P3CInterval<? extends Number>> crispCluster =
						crispIterator.next();
				
				fuzzyIterator = fuzzyClusters.keySet().iterator();
				
				while(fuzzyIterator.hasNext()) {
					
					TreeMap<String, P3CInterval<? extends Number>> fuzzyCluster =
							fuzzyIterator.next();
					
					Iterator<String> crispClusterVariables = crispCluster.keySet().iterator();
					//Iterator<Short> fuzzyClusterVariables = fuzzyCluster.keySet().iterator();
					//int numberOfMatches = 0;
					Tuple comparison = new Tuple();
					comparison.crispCluster = crispCluster;
					comparison.fuzzyCluster = fuzzyCluster;
					comparison.crispSupport = crispClusters.get(crispCluster);
					comparison.fuzzySupport = fuzzyClusters.get(fuzzyCluster);
					comparison.sizeOfNotOverlap = 0;
					
					
					/*while(crispClusterVariables.hasNext() && fuzzyClusterVariables.hasNext()) {
						
						String crispVariable = crispClusterVariables.next();
						String fuzzyVariable = fuzzyClusterVariables.next();
						
						if(crispVariable.equalsIgnoreCase(fuzzyVariable)) {
							
							//Check the value, such as 192.168.1.1
							int similarity =
									P3CComparators.comparatorP3CInterval.compare(
											crispCluster.get(crispVariable),
											fuzzyCluster.get(fuzzyVariable));
							
							// If they have exactly the same values
							if(similarity == 0) {
								comparison.sizeOfNotOverlap++;
							}
						}
					}*/
					
					while(crispClusterVariables.hasNext()) {
						
						String crispVariable = crispClusterVariables.next();
						
						if(fuzzyCluster.containsKey(crispVariable)) {
							
							//Check the value, such as 192.168.1.1
							int similarity =
									P3CComparators.comparatorP3CInterval.compare(
											crispCluster.get(crispVariable),
											fuzzyCluster.get(crispVariable));
							
							// If they have exactly the same values
							if(similarity == 0) {
								comparison.sizeOfNotOverlap++;
							}
						}
					}
					
					// If fuzzy embedded within crisp
					if(crispCluster.keySet().size() > fuzzyCluster.keySet().size()
							&& comparison.sizeOfNotOverlap > 0) {
						comparison.fuzzyOrCrispEmbedded = 1;
						comparison.sizeOfNotOverlap = fuzzyCluster.size() -
								comparison.sizeOfNotOverlap;
					}
					
					// If crisp embedded within fuzzy
					if(crispCluster.keySet().size() < fuzzyCluster.keySet().size()
							&& comparison.sizeOfNotOverlap > 0) {
						comparison.fuzzyOrCrispEmbedded = -1;
						comparison.sizeOfNotOverlap = crispCluster.size() -
								comparison.sizeOfNotOverlap;
					}
					
					comparisons.add(comparison);
				}
			}
			
			int numberOfFuzzyEmbeddedInCrisp = 0;
			int numberOfCrispEmbeddedInFuzzy = 0;
			
			for(Tuple comparison : comparisons) {
				
				/*String separator = "#"; // , already used in clusters
				P3CSupportFuzzy supportFuzzy = (P3CSupportFuzzy)comparison.fuzzySupport;
				
				System.out.print(comparison.fuzzyOrCrispEmbedded);
				System.out.print(separator);
				System.out.print(comparison.sizeOfNotOverlap);
				System.out.print(separator);
				System.out.print(comparison.crispCluster);
				System.out.print(separator);
				System.out.print(comparison.crispSupport.getSupport());
				System.out.print(separator);
				System.out.print(comparison.fuzzyCluster);
				System.out.print(separator);
				System.out.print(supportFuzzy.getSupport());
				System.out.print(separator);
				System.out.print(supportFuzzy.getFuzzySupport());
				System.out.println(separator);*/
				
				// fuzzy embedded in crisp
				if(comparison.fuzzyOrCrispEmbedded > 0 &&
						comparison.sizeOfNotOverlap == 0) {
					numberOfFuzzyEmbeddedInCrisp++;
				}
				
				// crisp embedded in fuzzy
				if(comparison.fuzzyOrCrispEmbedded < 0 &&
						comparison.sizeOfNotOverlap == 0) {
					numberOfCrispEmbeddedInFuzzy++;
				}
			}
			
			System.out.println("Dataset: " + dataId);
			
			System.out.println("\tnumberOfCrispEmbeddedInFuzzy = " +
					(crispClusters.isEmpty() || fuzzyClusters.isEmpty() ? "-" : numberOfCrispEmbeddedInFuzzy) +
					"\t(which means crisp clusters are more concise)");
			System.out.println("\tnumberOfFuzzyEmbeddedInCrisp = " +
					(crispClusters.isEmpty() || fuzzyClusters.isEmpty() ? "-" : numberOfFuzzyEmbeddedInCrisp) +
					"\t(which means fuzzy clusters are more concise)");
		}
		
		log.info("FUZZIEEE2014analysis complete.");
	}
	
	
}
