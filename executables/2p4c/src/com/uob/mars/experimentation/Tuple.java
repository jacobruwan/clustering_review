package com.uob.mars.experimentation;

import java.util.TreeMap;

import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;


public class Tuple{
	
	
	public Tuple(){}
	// Note, using String representation of attributes names (same as dataset and results database)
	// but different to Short representation used in the algorithm
	public TreeMap<String, P3CInterval<? extends Number>> fuzzyCluster;
	public TreeMap<String, P3CInterval<? extends Number>> crispCluster;
	public P3CSupportInterface fuzzySupport;
	public P3CSupportInterface crispSupport;
	public int fuzzyOrCrispEmbedded; //-1 is crisp, 1 is fuzzy, 0 is ignored
	public int sizeOfNotOverlap;
	
	
}