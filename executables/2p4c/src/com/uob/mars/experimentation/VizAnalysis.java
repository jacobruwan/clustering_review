package com.uob.mars.experimentation;

import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.uob.mars.db.PatternsDatabase;import com.uob.mars.algorithms.clustering.hybrid.Interval.P3CInterval;
import com.uob.mars.algorithms.clustering.hybrid.Support.P3CSupportInterface;


public class VizAnalysis {
	
	private static final Logger log = Logger.getLogger(VizAnalysis.class);
	
	public static void main (String[] args) {
		
		TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> crispClusters = null;
		TreeMap<TreeMap<String, P3CInterval<? extends Number>>, P3CSupportInterface> fuzzyClusters = null;
		
		/*
		1 - p3cCrisp, csv/darpa1999alerts-outside-03-30-1999.csv
		2 - twopfourc, csv/darpa1999alerts-outside-03-30-1999.csv
		3 - p3cCrisp, csv/darpa1999alerts-outside-03-30-1999-with-attack.csv
		4 - twopfourc, csv/darpa1999alerts-outside-03-30-1999-with-attack.csv
		*/
		
		crispClusters = Experimentation.getClusterResults(1, false);
		fuzzyClusters = Experimentation.getClusterResults(2, true);

		// Shutdown the database, which prevents a lock remaining present.
		PatternsDatabase.getInstance().stopInstance();
		
	}
	
	
}
