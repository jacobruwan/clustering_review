package com.uob.mars.maths;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;


import com.uob.mars.maths.FocusedCorrespondenceAnalysis;
import com.bt.saturn.data.SaturnInfo;
import com.bt.saturn.data.SaturnInfoEntity;
import com.bt.saturn.services.clusteringUtilities.Type;
import com.bt.saturn.services.csv.CSV;
import com.uob.mars.data.MarsInfoOldOld;


/**
 * Suitable for many variables with high cardinality
 * Taken from Rosario, G E, Rundensteiner, E A Brown, D C, Ward, M O, and Huang, (2004)
 * "Mapping nominal values to numbers for effective visualization" Information Visualisation 3(2), pp. 80-95.
 *
 * Also, see http://dimension.usherbrooke.ca/dimension/EricJBeh.pdf
 */
/*

Create 1xn table for one variable, and populate with counts


*/
public class FocusedCorrespondenceAnalysis {

	// Referred to as a Burt Table, the first two Strings represent rows and columns,
	// which are variable/attribute names, such as quality and colour.
	// Referred to as a count table, the second two Strings refer to rows and columns,
	// which are the values, such as good/bad/ok and red/blue/green.
	// The Integer is the count.
	Map<String, Map<String, Map<String, Map<String, Integer>>>> burt;

	// Maps
	Map<String, Type> tagTypes;

	List<SaturnInfoEntity> entities;

	/*
	 *
	 */
	public FocusedCorrespondenceAnalysis(List<SaturnInfoEntity> entities,
			Map<String, Type> tagTypes) throws Exception {

		if (entities == null || tagTypes == null) {
			throw new Exception("Null argument(s).");
		}

		if (entities.size() == 0 || tagTypes.size() == 0) {
			throw new Exception ("Empty containers in agurment(s).");
		}

		this.entities= entities;
		this.tagTypes = tagTypes;

		// Maps a variable name to a list of distinct values
		Map<String, Set<String>> distinctDataValues =
				new HashMap<String, Set<String>>();

		/*
		Initialise rows and columns for burt table
		*/
		// Choice of map: http://docs.oracle.com/javase/tutorial/collections/implementations/map.html
		burt = new HashMap<String, Map<String, Map<String, Map<String, Integer>>>> ();


		for (Map.Entry<String, Type> entry : tagTypes.entrySet()) {
		    String tag = entry.getKey();
		    Type type = entry.getValue();

		    if (type == Type.NOMINAL || type == Type.IPADDRESS) {
		    	//burt.put(tag, new HashMap<String, Map<String, Map<String, Integer>>>());
		    	distinctDataValues.put(tag, new HashSet<String>());
		    }
		}

		// Using the Set of variables (tags), now initialise the rows and columns of the Burt Table
		//for (String tag : variables) {
		for (Entry<String, Set<String>> entry : distinctDataValues.entrySet()) {

			String tag = entry.getKey();
		    Set<String> set = entry.getValue();

			// Rows of Burt Table
			burt.put(tag, new HashMap<String, Map<String, Map<String, Integer>>>());

			for (Entry<String, Set<String>> entryInner : distinctDataValues.entrySet()) {

				String tagInner = entryInner.getKey();

				// Columns of Burt Table
				burt.get(tag).put(tagInner, new HashMap<String, Map<String, Integer>>());
			}
		}

		/*
		Declare tags/attributes in the HashMap before we use
		*/
		/*for (Entry<String, Set<String>> entry : distinctDataValues.entrySet()) {
			entry.setValue(new HashSet<String>());
		}*/

		/*
		Initialise rows and columns for count table, which is inside the burt table
		*/

		for (int entitiesIndex = 0; entitiesIndex < entities.size(); entitiesIndex++) {

			List<String> tags = entities.get(entitiesIndex).getTags();

			for (int tagIndex = 0; tagIndex < tags.size(); tagIndex++) {

				String[] splitTag = tags.get(tagIndex).split(":");
				String attributeName = splitTag[0];
				String attributeValue = splitTag[1];

				if(tagTypes.get(attributeName) == Type.NOMINAL ||
						tagTypes.get(attributeName) == Type.IPADDRESS) {

					// Adding to the Set will ensure all values are unique
					distinctDataValues.get(attributeName).add(attributeValue);
				}
			}
		}

		/*for (Entry<String, Set<String>> entry : distinctDataValues.entrySet()) {

			String tag = entry.getKey();
		    Set<String> set = entry.getValue();
		    //distinctDataValues.get(tag).

		    System.out.println(tag);
		    for (String str : set) {
		    	System.out.println("\t" + str);
		    }
		}*/

		for (Entry<String, Map<String, Map<String, Map<String, Integer>>>> entryVariableRow : burt.entrySet()) {

			String tagVariableRow = entryVariableRow.getKey();
		    Map<String, Map<String, Map<String, Integer>>> mapVariableRow = entryVariableRow.getValue();

		    for (Entry<String, Map<String, Map<String, Integer>>> entryVariableColumn : mapVariableRow.entrySet()) {

		    	String tagVariableColumn = entryVariableColumn.getKey();
			    Map<String, Map<String, Integer>> mapVariableColumn = entryVariableColumn.getValue();


				for (Entry<String, Set<String>> entryRow : distinctDataValues.entrySet()) {

					String tagValueRow = entryRow.getKey();
				    Set<String> setValue = entryRow.getValue();

				    for(String valueRow: setValue){

						// Rows of count table
					    burt.get(tagVariableRow).get(tagVariableColumn).put(valueRow, new HashMap<String, Integer>());

						//for (Entry<String, Set<String>> entryColumn : distinctDataValues.entrySet()) {
					    for(String valueColumn: setValue){

							//String tagValueColumn = entryColumn.getKey();

							// Columns of count table
							//burt.get(tag).put(tagInner, new HashMap<String, Map<String, Integer>>());
							burt.get(tagVariableRow).get(tagVariableColumn).get(valueRow).put(valueColumn, 0);
						}
				    }
				}
		    }
		}
	}

	public boolean run () {

		for (int entriesIndex = 0; entriesIndex < entities.size(); entriesIndex++) {

			List<String> tags = entities.get(entriesIndex).getTags();

			for (int tagsIndexA = 0; tagsIndexA < (tags.size() - 1); tagsIndexA++) {

				String[] splitTagA = tags.get(tagsIndexA).split(":");
				String attributeNameA = splitTagA[0];
				String attributeValueA = splitTagA[1];

				for (int tagsIndexB = tagsIndexA + 1; tagsIndexB < tags.size(); tagsIndexB++) {

					String[] splitTagB = tags.get(tagsIndexB).split(":");
					String attributeNameB = splitTagB[0];
					String attributeValueB = splitTagB[1];

					int count = 1 + burt.get(attributeNameA).get(attributeNameB).get(attributeValueA).get(attributeValueB);
					burt.get(attributeNameA).get(attributeNameB).get(attributeValueA).put(attributeValueB, count);
				}
			}
		}

		System.out.println("dddd");

		return true;
	}

	public String toString() {
		String str = new String();

		str += "Keys for rows in Burt table:" + System.getProperty("line.separator");

		for (Map.Entry<String, Map<String, Map<String, Map<String, Integer>>>> entry : burt.entrySet()) {
		    String tag = entry.getKey();
		    Map<String, Map<String, Map<String, Integer>>> type = entry.getValue();

		    // Rows of Burt Table
		    str += tag + ":";

		    for (Map.Entry<String, Map<String, Map<String, Integer>>> entryInner : burt.get(tag).entrySet()) {
			    String tagInner = entryInner.getKey();
			    Map<String, Map<String, Integer>> typeInner = entryInner.getValue();

				// Columns of Burt Table
				str += " " + tagInner + ",";
			}

		    str += System.getProperty("line.separator");
		}

		return str;
	}

	// Yes, we want Burt!
	public Map<String, Map<String, Map<String, Map<String, Integer>>>> getBurt () {
		return burt;
	}

	public static void main (String[] args) {

		CSV csv = new CSV();
		SaturnInfo saturnInfo = csv.getInfo(0, 0, "csv\\Counterpane-sample-500-records.csv", 0);MarsInfoOldnfoOld marsInfo MarsInfoOldrsInfoOld(saturnInfo);
		FocusedCorrespondenceAnalysis mcs;

		try {
			mcs = new FocusedCorrespondenceAnalysis(
					saturnInfo.getEntities(),
					marsInfo.getTagTypes());

			mcs.run();

			System.out.println(mcs.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}



	}
}
