/**
 * 
 * @fileName	Sets.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 * @copyright	(c) British Telecommunications plc, 2013, All Rights Reserved.
 * 
 */
package com.uob.mars.maths;

import java.util.ArrayList;


public class Sets {
	
	
	/**
	 * Recursive function for calculating the power set.
	 * 
	 * Adapted from http://rosettacode.org/wiki/Power_set#Recursion
	 * 
	 * @param set
	 * @param n
	 * @param powerset
	 * @return
	 */
	public static ArrayList<ArrayList<String>> powerSetRecursvive(ArrayList<String> set,
			int n,
			ArrayList<ArrayList<String>> powerset)
	{
		if(n < 0) {
			return null;
		}
		
		if(n==0){
			if(powerset == null) {
				powerset = new ArrayList<ArrayList<String>>();
			}
			powerset.add(new ArrayList<String>());
			return powerset;
		}
		
		powerset = powerSetRecursvive(set, n-1, powerset);
		ArrayList<ArrayList<String>> newSet = new ArrayList<ArrayList<String>>();
		
		for(int i = 0; i < powerset.size(); i++) {
			
			ArrayList<String> temp = new ArrayList<String>();
			
			if(powerset.get(i).isEmpty()) {
				
				temp.add(set.get(n-1));
				newSet.add(temp);
			} else {
				
				temp.addAll(powerset.get(i));
				temp.add(set.get(n-1));
				newSet.add(temp);
			}
		}
		
		powerset.addAll(newSet);
		return powerset;
	}
}
