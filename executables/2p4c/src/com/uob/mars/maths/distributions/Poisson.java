/**
 * 
 * @fileName	Poisson.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */
package com.uob.mars.maths.distributions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

import org.apache.log4j.Logger;

import com.uob.mars.maths.distributions.PoissonInterface;



/**
 * A Poisson distribution, and associated functions.
 */
public class Poisson implements PoissonInterface <Integer>{
	
	
	private static final Logger log = Logger.getLogger(Poisson.class);
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * Statistical test for defining a cluster core and testing intervals.
	 * For cluster core computation, this is similar to the support-confidence
	 * framework in Apriori.
	 * 
	 * With regard to execution speed, the use of BigDecimal and/or BigInteger
	 * is very important. For advice on using BigDecimal, see:
	 * http://www.stichlberger.com/software/java-bigdecimal-gotchas/
	 * 
	 * @param observed Observed support
	 * @param expected Expected support
	 * @return true if passed, false otherwise
	 */
	@Override
	public boolean test(Integer observed, double expected, double poissonThreshold) {
		
		// And if the observed support is significantly larger
		// than the expected support
		// If Poisson(Supp({A} U {B}), ESupp({A} U {B} | {A})) < Poisson threshold
		
		if(observed.intValue() > 0) {
			
			// Limit precision to 100 because min Poisson threshold used in P3C is 1E-100
			MathContext mc = new MathContext(10, RoundingMode.HALF_UP);
			BigDecimal probability = BigDecimal.valueOf(Math.exp(-expected));
			BigDecimal power = BigDecimal.valueOf(expected);
			int suppCounter = observed.intValue();
			BigInteger suppFactorial = BigInteger.valueOf(observed.intValue());
			final BigDecimal eSuppBig = BigDecimal.valueOf(expected);
			
			--suppCounter;
			
			while(suppCounter > 0) {
				
				suppFactorial = suppFactorial.multiply(BigInteger.valueOf(suppCounter));
				--suppCounter;
			}
			
			for(int i = 0; i < (observed.intValue() - 1); i++) {
				power = power.multiply(eSuppBig, mc);
			}
			
			/*System.out.print(
					"------" +
					"\n\tobserved = " + observed.intValue() +
					"\n\texpected = " + expected +
					"\n\tfactorial = " + suppFactorial +
					"\n\tpower = " + power +
					"\n\texp(-expected) = " + probability);*/
			
			probability = probability.multiply(power, mc);
			
			/*System.out.print("\n\tnominator = " + probability);*/
			
			try {
				probability = probability.divide(new BigDecimal(suppFactorial), mc);
			} catch (ArithmeticException e) {
				log.error(e);
			}
			
			/*System.out.println("\n\tprobability = " + probability +
					"\n\tP = " + poissonThreshold);*/
			
			if(-1 == probability.compareTo(BigDecimal.valueOf(poissonThreshold))) {return true;}
		}
		
		return false;
	}
}
