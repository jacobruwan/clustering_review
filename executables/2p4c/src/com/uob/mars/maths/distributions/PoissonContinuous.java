/**
 * 
 * @fileName	PoissonContinuous.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */
package com.uob.mars.maths.distributions;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import org.apache.commons.math3.special.Gamma;
import org.apache.log4j.Logger;

import com.uob.mars.maths.distributions.PoissonContinuous;


/**
 * A continuous extension to the Poisson distribution, and associated functions
 */
public class PoissonContinuous implements PoissonInterface <Double> {
	
	
	private static final Logger log = Logger.getLogger(PoissonContinuous.class);
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * Statistical test for defining a cluster core and testing intervals.
	 * For cluster core computation, this is similar to the support-confidence
	 * framework in Apriori.
	 * 
	 * With regard to execution speed, the use of BigDecimal and/or BigInteger
	 * is very important. For advice on using BigDecimal, see:
	 * http://www.stichlberger.com/software/java-bigdecimal-gotchas/
	 * 
	 * @param observed Observed support
	 * @param expected Expected support
	 * @return true if passed (probability is less than Poisson threshold),
	 * false otherwise (probability is greater than or equal to the Poisson threshold)
	 */
	@Override
	public boolean test(Double observed, double expected, double poissonThreshold) {
		
		// And if the observed support is significantly larger
		// than the expected support
		// If Poisson(Supp({A} U {B}), ESupp({A} U {B} | {A})) < Poisson threshold
		
		// Use this line with PoissonContinuousTest.test()
		//System.out.print("supp = " + supp + " eSupp = " + eSupp + " is ");
		
		if(observed.doubleValue() > 0.0 && expected > 0.0) {
			
			// Limit precision to 100 because min Poisson threshold used in P3C is 1E-100
			MathContext mc = new MathContext(100, RoundingMode.HALF_UP);
			BigDecimal probability = BigDecimal.valueOf(Math.exp(-expected));
			BigDecimal power = BigDecimal.valueOf(expected);
			BigDecimal suppFactorial; // = new BigDecimal(gamma);
			final BigDecimal eSuppBig = BigDecimal.valueOf(expected);
			double gamma = Gamma.gamma(observed.doubleValue());
			double gamma2 = Gamma.gamma(observed.doubleValue());
			
			if(Double.isInfinite(gamma)) {
				gamma = Double.MIN_VALUE;
				log.warn("gamma is infinite so setting to minimum value of type Double");
			}
			
			if(Double.isNaN(gamma)) {
				gamma = Double.MIN_VALUE;
				log.warn("gamma is NaN set to minimum Double value");
			}
			
			suppFactorial = BigDecimal.valueOf(gamma);
			
			for(int i = 0; i < (observed.intValue() - 1); i++) {
				power = power.multiply(eSuppBig, mc);
			}
			
			
			
			probability = probability.multiply(power, mc);
			
			/*System.out.print("\n\tnominator = " + probability);*/
			
			try {
				if(Double.isInfinite(gamma2) || Double.isNaN(gamma2)) {
					
					System.out.print(
					"------" +
					"\n\tobserved = " + observed.doubleValue() +
					"\n\texpected = " + expected +
					"\n\tfactorial = " + suppFactorial +
					"\n\tpower = " + power +
					"\n\tgamma2 = " + gamma2);
					
					log.warn("observed = " + observed.doubleValue() + " expected " + expected);
					log.warn("probability = " + probability + " gamma = " + gamma);
				}
				
				probability = probability.divide(suppFactorial, mc);
				
				if(Double.isInfinite(gamma2) || Double.isNaN(gamma2)) {
					log.warn("probability / gamma = " + probability);
				}
				
			} catch (ArithmeticException e) {
				log.error(e);
			}
			
			/*System.out.println("\n\tprobability = " + probability +
					"\n\tP = " + poissonThreshold);*/
			
			// If probability is less than threshold
			if(-1 == probability.compareTo(BigDecimal.valueOf(poissonThreshold))) {return true;}
		}
		
		return false;
	}
	
	
}
