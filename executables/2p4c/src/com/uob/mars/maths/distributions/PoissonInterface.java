/**
 * 
 * @fileName	PoissonInterfaceace.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */
package com.uob.mars.maths.distributions;

/**
 * Interface for Poisson Distributions
 */
public interface PoissonInterface <T> {
	
	
	/**
	 * 
	 * @param observed Type T is a Number, typically either Integer of Double
	 * @param expected
	 * @param poissonThreshold
	 * @return
	 */
	public boolean test(T observed, double expected, double poissonThreshold);
	
	
}
