package com.uob.mars.maths.matrices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.maths.matrices.MatrixInterface;

/**
 * Fundamental difference to open source libraries is this class uses
 * ArrayLists so the size can be dynamic.
 * 
 * Only produces square matrices, and the class has been designed for
 * adjacency matrices.
 */
public abstract class Matrix <T> implements MatrixInterface <T>, Cloneable {
	
	
	private static final Logger log = Logger.getLogger(Matrix.class);
	
	
	protected ArrayList<ArrayList<T>> matrix;
	
	
	protected T defaultValue;
	
	
	public Matrix () {
		matrix = new ArrayList<ArrayList<T>>();
	}
	
	
	public Matrix (Matrix<T> matrix) {
		
		this.matrix = matrix.matrix;
		this.defaultValue = matrix.getDefaultValue();
	}
	
	
	/**
	 * 
	 * @param row
	 * @param column
	 * @return
	 */
	public T get(int row, int column) {
		return matrix.get(row).get(column);
	}
	
	
	/**
	 * Increase the number of rows and columns by 1. Only works for square
	 * matrices, hence the reason for private declaration of Matrix (int rows,
	 * int columns).
	 * @return false if any add() returned false, true otherwise
	 */
	public boolean growByOne() {
		
		boolean returnValue = true;
		
		ArrayList<T> list =
				new ArrayList<T>(this.matrix.size());
		
		for(int i = 0; i < this.matrix.size(); i++) {
			
			if(!list.add(this.defaultValue)) {
				returnValue = false;
			}
		}
		
		if(!this.matrix.add(list)) {
			returnValue = false;
		}
		
		// Now add an element to end of every row
		for(int i = 0; i < this.matrix.size(); i++) {
			
			if(!this.matrix.get(i).add(this.defaultValue)) {
				returnValue = false;
			}
		}
		
		return returnValue;
	}
	
	
	/**
	 * 
	 */
	public void trimToSize() {
		
		this.matrix.trimToSize();
	}
	
	
	/**
	 * Assumes matrix is square
	 * @return number of dimensions
	 */
	public int numberOfRows() {
		return matrix.size();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public int numberOfColumns() {
		return matrix.size() == 0 ? 0 : matrix.get(0).size();
	}
	
	
	public Matrix<T> copy() throws CloneNotSupportedException {
		
		Matrix<T> matrixCopy = (Matrix<T>)super.clone();
		
		
		
		return (Matrix<T>)super.clone();
	}
	
	
	/*public Object deepCopy() {
		
		Matrix<T> matrix = null;
		
		try {
			matrix = (Matrix<T>)super.clone();
		} catch (CloneNotSupportedException e) {
			log.error(e);
		}
		
		matrix.matrix.clear();
		
		for(int i = 0; i < this.matrix.size(); i++) {
			
			ArrayList
			
			matrix.m
		}
		
		return matrix;
	}*/
	
	
	/**
	 * Get the default value of type T
	 * @return
	 */
	public T getDefaultValue() {
		return this.defaultValue;
	}
	
	
	/**
	 * Print the adjacency matrix.
	 * 
	 * Example for integer type:
	 * dimensionA 000010000
	 * dimensionB 000011000
	 * dimensionC 100010010
	 * dimensionD 010010000
	 * 
	 * @param adjacencyMatrix Adjacency matrix of adjacencies between marked bins
	 * @param binToIntMapping Maps a bin name to an integer index, so it can be
	 * used in adjacencyMatrix.
	 */
	public void printAdjacencyMatrix(
			TreeMap<String, Integer> binToIntMapping) {
		
		Iterator<String> iterBinLabelsRows =
				binToIntMapping.keySet().iterator();
		
		// Print adjacency matrix
		while(iterBinLabelsRows.hasNext()) {
			
			Iterator<String> iterBinLabelsColumns =
					binToIntMapping.keySet().iterator();
			String rowLabel = iterBinLabelsRows.next();
			
			int labelSpaceLimit = 80;
			int labelSpaceFillerLength = labelSpaceLimit - rowLabel.length();
			
			if(labelSpaceFillerLength <= 0) {
				log.warn("Pretty printing might not be so pretty...");
				labelSpaceFillerLength = 1;
			}
			
			char[] spaceFiller = new char[labelSpaceFillerLength];
			Arrays.fill(spaceFiller, ' ');
			System.out.print((labelSpaceFillerLength > 0 ? new String(spaceFiller) : "") + rowLabel + " ");
			//System.out.print(rowLabel + (labelSpaceFillerLength > 0 ? new String(spaceFiller) : ""));
			
			while(iterBinLabelsColumns.hasNext()) {
				
				int i = binToIntMapping.get(iterBinLabelsColumns.next());
				int j = binToIntMapping.get(rowLabel);
				
				System.out.print(this.matrix.get(i).get(j) + ",");
				//TODO Identify generic type is Boolean and print 1/0 accordingly
				//System.out.print(this.matrix.get(i).get(j) ? 1 : 0);
			}
			System.out.println();
		}
		System.out.println();
	}
	
	
	/**
	 * Convert the adjacency matrix to a String.
	 * 
	 * Example:
	 *  ,A,B,C,D,E,F,G,H,I
	 * A,0,0,0,0,1,0,0,0,0
	 * B,0,0,0,0,1,1,0,0,0
	 * C,1,0,0,0,1,0,0,1,0
	 * D,0,1,0,0,1,0,0,0,0
	 * 
	 * (where A, B, ...I are dimension names)
	 * 
	 * @param adjacencyMatrix Adjacency matrix of adjacencies between marked bins
	 * @param binToIntMapping Maps a bin name to an integer index, so it can be
	 * used in adjacencyMatrix.
	 * @param writeRowDimensionName True will add the row name to every line,
	 * False will remove the first column in the above example.
	 */
	public String toString(
			TreeMap<String, Integer> binToIntMapping,
			boolean writeRowDimensionName) {
		
		String csv = new String();
		
		Iterator<String> iterBinLabelsRows =
				binToIntMapping.keySet().iterator();
		
		boolean firstColumn = true;
		
		// Print header
		while(iterBinLabelsRows.hasNext()) {
			
			String dimensionName = iterBinLabelsRows.next();
			
			if(firstColumn) {
				if (writeRowDimensionName) {
					
				} else {
					csv += dimensionName;
				}
			} else {
				csv += "," + dimensionName;
			}
			
			if(firstColumn) {firstColumn = false;}
		}
		
		// new line between header field and first data record
		csv += System.getProperty("line.separator");
		
		// Reset the iterator
		iterBinLabelsRows = binToIntMapping.keySet().iterator();
		
		// Print adjacency matrix
		while(iterBinLabelsRows.hasNext()) {
			
			Iterator<String> iterBinLabelsColumns =
					binToIntMapping.keySet().iterator();
			String rowLabel = iterBinLabelsRows.next();
			
			firstColumn = true;
			
			while(iterBinLabelsColumns.hasNext()) {
				
				int i = binToIntMapping.get(iterBinLabelsColumns.next());
				int j = binToIntMapping.get(rowLabel);
				
				if(firstColumn) {
					if (writeRowDimensionName) {
						csv += rowLabel;
					} else {
						csv += this.matrix.get(i).get(j);
						//csv += (this.matrix.get(i).get(j) ? 1 : 0);
					}
				} else {
					csv += "," + this.matrix.get(i).get(j);
					//csv += "," + (this.matrix.get(i).get(j) ? 1 : 0);
				}
				
				if(firstColumn) {firstColumn = false;}
			}
			csv += System.getProperty("line.separator");
		}
		csv += System.getProperty("line.separator");
		
		return csv;
	}
	
	
	public String toString() {
		
		String csv = new String();
		
		for(int i = 0; i < matrix.size(); i++) {
			csv += matrix.get(i).get(0);
			for(int j = 1; j < matrix.get(i).size(); j++) {
				csv += "," + matrix.get(i).get(j);
			}
			csv += System.getProperty("line.separator");
		}
		
		return csv;
	}
	
}
