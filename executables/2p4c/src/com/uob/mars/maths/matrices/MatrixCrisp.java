package com.uob.mars.maths.matrices;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.maths.matrices.MatrixCrisp;

/**
 * 
 * @fileName	MatrixCrisp.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */


public class MatrixCrisp extends Matrix <Integer> {
	
	
	private static final Logger log = Logger.getLogger(MatrixCrisp.class);
	
	
	public MatrixCrisp (Object defaultValue) {
		super();
		this.defaultValue = (Integer)defaultValue;
	}
	
	
	/**
	 * 
	 */
	public MatrixCrisp (int size, Object defaultValue) {
		this(size, size, defaultValue);
	}
	
	
	/**
	 * Declared as private, so only square matrices are created.
	 * 
	 * @param rows
	 * @param columns
	 * @param defaultValue
	 */
	private MatrixCrisp (int rows, int columns, Object defaultValue) {
		
		this.matrix = new ArrayList<ArrayList<Integer>>(rows);
		this.defaultValue = (Integer)defaultValue;
		
		for(int rowI = 0; rowI < rows; rowI++) {
			
			ArrayList<Integer> row = new ArrayList<Integer>(columns);
			
			for(int columnI = 0; columnI < columns; columnI++) {
				row.add(this.defaultValue);
			}
			
			this.matrix.add(row);
		}
	}
	
	
	public MatrixCrisp(MatrixCrisp matrix) {
		
		this.matrix = new ArrayList<ArrayList<Integer>>(matrix.numberOfRows());
		this.defaultValue = matrix.defaultValue;
		
		for(ArrayList<Integer> row: this.matrix) {
			
			row = new ArrayList<Integer>(matrix.numberOfColumns());
			
			for(int i = 0; i < matrix.numberOfColumns(); i++) {
				
				row.add(this.defaultValue);
			}
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer setEntry(int row, int column, Object element) {
		
		ArrayList<Integer> tempRow = matrix.get(row);
		return tempRow.set(column, (Integer)element);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareToEntry(int row, int column, Object anotherValue) {
		return this.matrix.get(row).get(column).compareTo((Integer)anotherValue);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer newCellValue(Number number) {
		return new Integer(number.intValue());
	}
	
	
	@Override
	public void incrementCellValueBy(Number number) {
		
	}
	
	
}
