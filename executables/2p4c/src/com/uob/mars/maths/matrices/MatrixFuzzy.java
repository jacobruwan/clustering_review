package com.uob.mars.maths.matrices;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.uob.mars.maths.matrices.Matrix;
import com.uob.mars.maths.matrices.MatrixFuzzy;

/**
 * 
 * @fileName	MatrixFuzzy.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */

/**
 * TODO: Add comment describing what this class does.
 */
public class MatrixFuzzy extends Matrix <Double> {
	
	
	private static final Logger log = Logger.getLogger(MatrixFuzzy.class);
	
	
	public MatrixFuzzy (Object defaultValue) {
		super();
		this.defaultValue = (Double)defaultValue;
	}
	
	
	/**
	 * 
	 */
	public MatrixFuzzy (int size, Object defaultValue) {
		this(size, size, defaultValue);
	}
	
	
	/**
	 * Declared as private, so only square matrices are created.
	 * 
	 * @param rows
	 * @param columns
	 * @param defaultValue
	 */
	private MatrixFuzzy (int rows, int columns, Object defaultValue) {
		
		this.matrix = new ArrayList<ArrayList<Double>>(rows);
		this.defaultValue = (Double)defaultValue;
		
		for(int rowI = 0; rowI < rows; rowI++) {
			
			ArrayList<Double> row = new ArrayList<Double>(columns);
			
			for(int columnI = 0; columnI < columns; columnI++) {
				row.add(this.defaultValue);
			}
			
			this.matrix.add(row);
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double setEntry(int row, int column, Object element) {
		
		ArrayList<Double> tempRow = matrix.get(row);
		return tempRow.set(column, (Double)element);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareToEntry(int row, int column, Object anotherValue) {
		return this.matrix.get(row).get(column).compareTo((Double)anotherValue);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double newCellValue(Number number) {
		return new Double(number.doubleValue());
	}
	
	
	@Override
	public void incrementCellValueBy(Number number) {
		
	}
	
	
}
