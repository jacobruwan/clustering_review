package com.uob.mars.maths.matrices;

/**
 * 
 * @fileName	MatrixInterface.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 * @copyright	(c) British Telecommunications plc, 2013, All Rights Reserved.
 * 
 */

/**
 * TODO: Add comment describing what this class does.
 */
public interface MatrixInterface <T> {
	
	
	/**
	 * Set the value of a cell to the given Object parameter.
	 * 
	 * @param row Index of row to be set
	 * @param column Index of column to be set
	 * @param element The element to be set, which will be cast to T
	 * @return The set element
	 */
	public T setEntry(int row, int column, Object element);
	
	
	/**
	 * Perform Java SDK's compareTo() on a matrix entry
	 * 
	 * @param row
	 * @param column
	 * @param anotherValue The value to be compared with
	 * @return a negative integer, zero, or a positive integer as this call
	 * value (sepcified by row and column) is less than, equal to, or greater
	 * than the specified object (anotherValue).
	 */
	public int compareToEntry(int row, int column, Object anotherValue);
	
	
	/**
	 * Used to create a value in the adjacency matrix.
	 * The type of the value is determined by the implementation.
	 * 
	 * @param number A number for the cell value. (Implementation uses either Double or Integer)
	 * @return
	 */
	public T newCellValue(Number number);
	
	
	/**
	 * 
	 * 
	 * @param number
	 */
	public void incrementCellValueBy(Number number);
	
	
}
