/**
 * (c) British Telecommunications plc, 2013, All Rights Reserved
 */
package com.uob.mars.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Utility functions for supporting reading/writing of CSV files
 */
public class CSVUtils {
	/**
	 * Read CSV file into memory
	 * @param filename Filename of CSV file 
	 * @return Contents of CSV file
	 */
	public static List<String[]> csvToList(String filename) {
		
		List<String[]> wholeFile = null;
		CSVReader reader = null;
		
		try {
			reader = new CSVReader(new FileReader(filename));
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		
		try {
			wholeFile = reader.readAll();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		return wholeFile;
	}
}
