package com.uob.mars.util;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.uob.mars.util.CollectionsUtils;

/**
 * TODO: Add comment describing what this class does.
 */
public class CollectionsUtils {
	
	
	private static final Logger log = Logger.getLogger(CollectionsUtils.class);
	
	
	/**
	 * Very important: assumes both lists are ordered.
	 * @param listA
	 * @param listB
	 * @return
	 */
	public static int intersectionCount(List<Integer> listA, List<Integer> listB) {
		
		int count = 0;
		
		if(listA != null && listB != null) {
			
			if(!listA.isEmpty() && !listB.isEmpty()) {
			
				Iterator<Integer> iterListA = listA.iterator();
				Iterator<Integer> iterListB = listB.iterator();
				
				int valueListB = 0;
				
				if(iterListB.hasNext()) {
					valueListB = iterListB.next().intValue();
				}
				
				while(iterListA.hasNext()) {
					
					int valueListA = iterListA.next().intValue();
					
					if(valueListA > valueListB) {
						
						while(iterListB.hasNext()) {
							valueListB = iterListB.next().intValue();
							
							if(valueListA < valueListB) {
								break;
							}
							
							if(valueListA == valueListB) {
								count++;
								break;
							}
						}
						
					} else {
						if(valueListA < valueListB) {
							//progress to next in iterListA
						}
						else { //same
							count++;
						}
					}
				}
			}
		}
		
		return count;
	}
}
