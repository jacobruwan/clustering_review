/**
 * 
 * @fileName	CommandLineOptionsInterface.java
 * @author 		Stephen G Matthews
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 */
package com.uob.mars.util;

import com.uob.mars.util.Parameters;


/**
 * Implementations of this interface can be used by any application in MARS.
 * The idea is to ensure we have consistent method calls for parsing command
 * line options.
 */
public interface CommandLineOptionsInterface {
	
	
	/**
	 * Reads the String array of command line arguments and parses into parameters
	 * @param parameters Contains parameters min andmax limits, and the
	 * arguments supplied by command line
	 * @param args Command line arguments
	 * @return
	 */
	public boolean parseCommandLine(Parameters parameters, String[] args);
	
	
}
