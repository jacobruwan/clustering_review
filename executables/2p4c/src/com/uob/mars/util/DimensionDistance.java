package com.uob.mars.util;

import org.apache.log4j.Logger;

import com.uob.mars.util.DimensionDistance;
import com.uob.mars.util.Type;

public class DimensionDistance {
	
	private static final Logger log = Logger.getLogger(DimensionDistance.class);
	
	/*
	 *  The distance between two values.
	 */
	protected static Double dimensionDistance(String[] tagASplit, String[] tagBSplit, Type tagType) {
		
		Double distance = 1.0;
		String tagAValue = "";
		String tagBValue = "";
		
		// Assume that tags always have a description, but might not have a value,
		// also assume that sTag and tTag is of form {tagDesc} or {tagDesc, tagVal}
		if (tagASplit.length == 1) {
			tagAValue = "";	
		} else {
			tagAValue = tagASplit[1];
		}
		
		if (tagBSplit.length == 1) {
			tagBValue = "";	
		} else {
			tagBValue = tagBSplit[1];
		}
		
		switch(tagType) {
			case IPADDRESS:
				distance = ipDistance(tagAValue, tagBValue);
				break;
			case QUANTITATIVE:
				distance = numericDistance(tagAValue, tagBValue);
				break;
			case NOMINAL:
				// TODO Semantic word case
				//distance = nominalDistance(tagAValue, tagBValue);
				break;
			case EMPTY:
				// If the whole dimension is empty then the distance between any two tags is 0.
				distance = 0.0;
			default:
				log.error("case not covered for tagType");
		}
		
		// If no case is identified then return the standard indicator function (i.e. either 0 or 1),
		// the case where they are identical is already taken care of so they must be different, hence return 1.
		return distance;
	}
	
	
	/*
	 * Returns 0 if identical, 1 if completely different. It goes through left to right on the IP addresses matching up sections,
	 * the more sections that match up the closer to 0 the distance is.
	 * e.g. ipDistance(11.22.33,11.22.44) = 1/4 and ipDistance(11.33.44,11.22.44) = 1/2
	 */
	private static Double ipDistance (String valueA, String valueB) {
		if (valueA.equals(valueB)) { return 0.0; }
		
		String[] valueASplit = valueA.split("\\.");
		String[] valueBSplit = valueB.split("\\.");
		
		int min = Math.min(valueBSplit.length, valueASplit.length);
		Double dist = 1.0;
		
		for (int i = 0 ; i < min ; i++) {
			if (valueASplit[i].equals(valueBSplit[i])) {
				dist -= Math.pow(0.5, i+1);
			} else {
				return dist;
			}
		}
		
		return dist;
	}
	
	
	/*
	 * The data should already be rescaled using new=(old-min)/(max-min).
	 * This places all values between 0 and 1, hence the distance will be between 0 and 1.
	 */
	private static double numericDistance (String valueA, String valueB) {
		
		Double numA = Double.parseDouble(valueA);
		Double numB = Double.parseDouble(valueB);
		
		if (numA > numB) {
			return (numA - numB);
		}
		return (numB - numA);
	}
	
	private static double nominalDistance (String valueA, String valueB) {
		
		return valueA.equals(valueB) ? 1.0 : 0.0;
	}

}
