package com.uob.mars.util;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.uob.mars.util.DimensionDistance;
import com.uob.mars.util.Distance;
import com.uob.mars.util.Type;

public class Distance {
	
	private static final Logger log = Logger.getLogger(Distance.class);
	
	/**
	 *  The Euclidean distance between two sets of tags.
	 */
	public static Double euclideanDistance(List<String> tagsA,	List<String> tagsB, Map<String, Type> tagTypes) {
		
		// If both null or both empty then they are the same and so distance is 0, if only one of them are in this case then distance 1.
		if (tagsA == null && tagsB == null) {
			return 0.0;
		}
		if (tagsA == null || tagsB == null) {
			return 1.0;
		}
		if (tagsA.size() == 0 && tagsB.size() == 0) {
			return 0.0;
		}
		if (tagsA.size() == 0 || tagsB.size() == 0) {
			return 1.0;
		}
		
		Double sum = 0.0;
		
		for (int indexA = 0 ; indexA < tagsA.size() ; indexA++) {
			
			String[] tagASplit = tagsA.get(indexA).split(":");
			
			for (int indexB = 0 ; indexB < tagsB.size() ; indexB++) {
				
				String[] tagBSplit = tagsB.get(indexB).split(":");
				
				// If the tag descriptions match then send to dimension distance
				if (tagASplit[0].equals(tagBSplit[0])) {
					Double dist = DimensionDistance.dimensionDistance(tagASplit, tagBSplit, tagTypes.get(tagASplit[0]));
					sum += dist*dist;
				}
				
			}
			
		}
		
		Double maxLength = (double)Math.max(tagsA.size(), tagsB.size());
		return ( Math.sqrt(sum / maxLength) );
	}
	
	//TODO Is this required?
	private static Double basicChecks(List<String> nodeA, List<String> nodeB) {
		
		// If both are null or are both empty then they are the same and so distance is 0
		// If only one of them are in one of these cases then the is distance 1.
		if (nodeA == null && nodeB == null) {
			return new Double(0);
		}
		if (nodeB == null || nodeB == null) {		
			return new Double(1);
		}
		if (nodeB.size() == 0 && nodeB.size() == 0) {
			return new Double(0);
		}
		if (nodeB.size() == 0 || nodeB.size() == 0) {
			return new Double(1);
		}
		return null;
	}
}
