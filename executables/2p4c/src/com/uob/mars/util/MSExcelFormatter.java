package com.uob.mars.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.uob.mars.util.MSExcelFormatter;

import au.com.bytecode.opencsv.CSVReader;


/**
 * Formatting options for MS Excel file
 */
public class MSExcelFormatter {
	
	private static final Logger log = Logger.getLogger(MSExcelFormatter.class);
	
	public MSExcelFormatter() {
		
	}
	
	/**
	 * Highlights subspace clusters in an spreadsheet
	 * @param csvRecords The dataset
	 * @param maximalKSignatures The patterns from P3C
	 * @param filename Filename of file to be read/created. File extensions not
	 * required, because ".cvs" and ".xlsx" will be appended by this function.
	 * @return
	 */
	public boolean formatSubspaceClusterInXlsx(List<String[]> csvRecords,
			List<TreeMap<TreeMap<String,TreeSet<String>>, List<Integer>>> maximalKSignatures,
			String filename) {
		
		InputStream inp = null;
		
		try {
			inp = new FileInputStream(filename + ".csv");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(csvRecords == null) {
			log.warn("csvRecords is null");
			return false;
		}
		
		if(csvRecords.isEmpty()) {
			log.warn("csvRecords is empty");
			return false;
		}
		
		//List<String[]> csvRecords = csvToList(filename);
		
		if(csvRecords.size() == 0) {
			log.warn("CSV contained no header or records");
		}
		if(csvRecords.size() < 3) {
			log.warn("CSV contains only two rows");
		}
		
		String[] saturnHeader = csvRecords.get(0);
		String[] dataHeader = csvRecords.get(1);
		
		/*
		Workbook wb = null;
		
		try {
			wb = WorkbookFactory.create(inp);
		} catch (InvalidFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		Workbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("Sheet1");
		
		
		IndexedColors[] colours = IndexedColors.values();
		int colourIndex = 0;
		
		for(int recordIndex = 0; recordIndex < csvRecords.size(); recordIndex++) {
			
			Row row = sheet.createRow((short)recordIndex);
			
			String[] csvRecord = csvRecords.get(recordIndex);
			
			colourIndex = 0;
			
			for(int columnIndex = 0; columnIndex < csvRecord.length; columnIndex++) {
				
				Cell cell = row.createCell(columnIndex);
				cell.setCellValue(csvRecord[columnIndex]);
			}
			
			
			
			
			if(recordIndex >= 2) {
				
				for(TreeMap<TreeMap<String,TreeSet<String>>, List<Integer>> signature: maximalKSignatures) {
					
					Iterator<TreeMap<String,TreeSet<String>>> sigIter = 
							signature.keySet().iterator();
					
					
					while(sigIter.hasNext()) {
						
						TreeMap<String,TreeSet<String>> signatureKey = sigIter.next();
						
						//int match = 0;
						List<Integer> matches = new ArrayList<Integer>();
						
						//If current variable in CSV data matches a variable in the signature
						//if(signatureKey.keySet().contains(dataHeader[columnIndex])) {
						
						Iterator<String> signatureVariables = signatureKey.keySet().iterator();
						
						while(signatureVariables.hasNext()) {
							
							String signatureVariable = signatureVariables.next();
							
							int columnIndex = 0; // Index of variable in dataHeader
							
							
							for(int i = 0; i < dataHeader.length; i++) {
								
								if(dataHeader[i].equalsIgnoreCase(signatureVariable)) {
									columnIndex = i;
									break;
								}
							}
							
							// If current variables' value is in the interval of a signature's variable
							if(signatureKey.get(dataHeader[columnIndex]).contains(row.getCell(columnIndex).getStringCellValue())) {
								
								List<Integer> IDs = signature.get(signatureKey);
								
								if(IDs.contains(new Integer(recordIndex - 2))) {
									
									/*style.setFillBackgroundColor(colours[colourIndex].index);
									style.setFillPattern(CellStyle.FINE_DOTS);
									cell.setCellStyle(style);*/
									//match++;
									matches.add(columnIndex);
								}
							}
						}
						
						
						
						// If all variables in signature match the data
						if(matches.size() == signatureKey.size()) {
							
							for(Integer matchesIndex : matches) {
								
								CellStyle style = wb.createCellStyle();
								style.setFillBackgroundColor(colours[colourIndex].index);
								style.setFillPattern(CellStyle.FINE_DOTS);
								row.getCell(matchesIndex).setCellStyle(style);
							}
						}
						
						
						//TODO Increase the number format options for patterns
						// Cycle through the colours
						colourIndex = (colourIndex + 1) == colours.length ? 0 : (colourIndex + 1);
						//colourIndex += 2;
						//System.out.println("");
					}
					
					//TODO Increase the number format options for patterns
					// Cycle through the colours
					//colourIndex = (colourIndex + 1) == colours.length ? 0 : (colourIndex + 1);
					//System.out.println("");
				}
			}
		}
		
		/*
		Sheet sheet = wb.getSheetAt(0);
		
		int numberOfRows = sheet.getLastRowNum();
		
		// 
		Row row = sheet.getRow(0);
		
		//
		int numberOfColumns = (int)row.getLastCellNum();
		
		CellStyle style = wb.createCellStyle();
		
		for(int rowNumber = 0; rowNumber < numberOfRows; rowNumber++) {
			
			row = sheet.getRow(rowNumber);
			
			int lastColumn = Math.max(row.getLastCellNum(), numberOfColumns);
			
			for (int cellNumber = 0; cellNumber < lastColumn; cellNumber++) {
				
				Cell cell = row.getCell(cellNumber, Row.RETURN_BLANK_AS_NULL);
				
				if (cell == null) {
					// The spreadsheet is empty in this cell
				} else {
					
					String cellValue = null;
					
					// Do something useful with the cell's contents
					switch (cell.getCellType()) {
					
						case Cell.CELL_TYPE_STRING:
							//System.out.println(cell.getRichStringCellValue().getString());
							cellValue = cell.getRichStringCellValue().getString();
							break;
							
						case Cell.CELL_TYPE_NUMERIC:
							if (DateUtil.isCellDateFormatted(cell)) {
								System.out.println(cell.getDateCellValue());
								cellValue = cell.getDateCellValue().toString();
							} else {
								System.out.println(cell.getNumericCellValue());
								cellValue = String.valueOf(cell.getNumericCellValue());
							}
							break;
							
						case Cell.CELL_TYPE_BOOLEAN:
							System.out.println(cell.getBooleanCellValue());
							cellValue = String.valueOf(cell.getBooleanCellValue());
							break;
							
						case Cell.CELL_TYPE_FORMULA:
							System.out.println(cell.getCellFormula());
							cellValue = cell.getCellFormula().toString();
							break;
							
						default:
							System.out.println();
					}
					
					style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
					style.setFillPattern(CellStyle.FINE_DOTS);
					cell.setCellValue("X");
					cell.setCellStyle(style);
					
				}
			}
		}
		*/
		// Write the output to a file
		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(filename + ".xlsx");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			wb.write(fileOut);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param args
	 * @see com.bt.mars.test.util.MSExcelFormatterTest
	 */
	/*public static void main(String[] args) {
		
		String filename = "./data/csv/darpa1999alerts-inside-weeks1-5-snapshot.csv";
		
		List<TreeMap<TreeMap<String,TreeSet<String>>,List<Integer>>> maximalKSignatures = null;
		
		List<String[]> csvRecords = CSVUtils.csvToList(filename);
		
		MSExcelFormatter formatter = new MSExcelFormatter();
		
		formatter.formatSubspaceCluster(csvRecords,
				maximalKSignatures,
				filename);
		
		return;
	}*/
}
