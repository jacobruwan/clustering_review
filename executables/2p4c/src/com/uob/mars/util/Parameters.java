/**
 * 
 * @fileName	Parameters.java
 * @author 		Stephen G Matthews (802280860)
 * @orgUnit 	University of Bristol (Collaborating with Security Futures Practice, BT Research & Innovation)
 * 
 * @copyright	(c) British Telecommunications plc, 2013, All Rights Reserved.
 * 
 */
package com.uob.mars.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all arguments supplied by the user and all min and max values for
 * each parameter.
 */
public class Parameters {
	
	
	/**
	 * User-specified values
	 */
	private Map<String, String> arguments;
	
	
	/**
	 * 
	 */
	private Map<String, String> parametersMin;
	
	
	/**
	 * 
	 */
	private Map<String, String> parametersMax;
	
	
	/**
	 * 
	 */
	public Parameters() {
		
		arguments = new HashMap<String, String>();
		parametersMin = new HashMap<String, String>();
		parametersMax = new HashMap<String, String>();
	}
	
	
	/**
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public String putArgument(String key, String value) {
		return arguments.put(key, value);
	}
	
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getArgument(String key) {
		return arguments.get(key);
	}
	
	
	/**
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public String putParameterMin(String key, String value) {
		return parametersMin.put(key, value);
	}
	
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getParameterMin(String key) {
		return parametersMin.get(key);
	}
	
	
	/**
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public String putParameterMax(String key, String value) {
		return parametersMax.put(key, value);
	}
	
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getParameterMax(String key) {
		return parametersMax.get(key);
	}
}
