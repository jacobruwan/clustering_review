package com.uob.mars.util;

public class PrimitiveDataTypes {

	public static Short safeIntegerToShort(Integer value) {
		
		
		if (value < Short.MIN_VALUE || value > Short.MAX_VALUE) {
			throw new IllegalArgumentException
			(value + "Cannot be cast to Short without changing its value.");
		}
		
		return new Short(new Integer(value).toString());
	}
}
