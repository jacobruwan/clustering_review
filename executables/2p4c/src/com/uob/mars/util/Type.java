/**
 * (c) British Telecommunications plc, 2013, All Rights Reserved
 */
package com.uob.mars.util;

/**
 * Description of type of data.
 * 
 * TODO Add: TEXT, DATE, TIME etc.
 */
public enum Type {
	
	/**
	 * - categorical data such as port numbers, colours etc - no ordering
	 * - mode average is used
	 */
	NOMINAL,
	
	/**
	 * - data that has a natural level of ordering, not necessarily numerical, such as {small, medium, large}.
	 * - median average is used
	 */
	ORDINAL,
	
	/**
	 * - basically numerical data - size, weight, temperature, number of something.
	 * - mean average is used
	 */
	QUANTITATIVE,
	
	/**
	 * - Specialised data type working similar to Nominal but taking into account the structure of IP addresses.
	 * - Special mode based average is used.
	 */
	IPADDRESS,
	
	/**
	 * - This doesn't have an average used on it, it's just a marker for when a tag doesn't have a value.
	 */
	EMPTY
};