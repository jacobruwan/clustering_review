Clusters can be broadly defined as multiple groups of objects, where objects within a cluster are "similar", and objects in different clusters are "dissimilar". The main concepts of clustering algorithms relevant to this research are the separation of clusters, the similarity measures and attribute types and the curse of dimensionality. See \citet{Han2011}, \citet{Xu2015} and \citet{Pandove2018} for more indepth reviews on advancements in the broader clustering topic.

\subsubsection{Separation of Clusters}
The first distinction used to describe clustering algorithms is their approach to separating clusters. \textbf{Partitioning} methods provide a single level of separation by initialising a clustering allocation, then iteratively relocating objects based on an optimisation objective \citep{Han2011}. The method coverges when the objective function reaches optimality (local or global). \textbf{Heirarchichal} methods produce multiple levels of separation, where clusters are nested within each other \citep{Han2011}. This separation can be further defined as 'hard' clustering, which assumes that each observation only belongs to a single cluster, and soft/fuzzy clustering, which gives each observation a probability of belonging to each cluster \citep{Kriegel2009}.

\subsubsection{Similarity Measures and Attribute Types}
The second distinction between algorithms is their approach to defining "similarity". \citet{Han2011} provides a detailed textbook overview of this topic, of which their main points are summarised below. 

For purely numeric output, \textbf{distance-based} measures such a Euclidean distance are commonly used. These measures tend to identify spherical, convex shaped clusters. For more arbitrary shaped clusters, the notion of 'density' can be used. \textbf{Density-based} methods use a 'neighbourhood of objects approach', where a cluster contains objects that have a 'density' above a threshold. Algorithms such as DBSCAN defined this as the number of objects within a given radius around a single object. \textbf{Grid-based} methods such as STRING utilise this "density" notion by partitioning the dimension space into a finite number of cells (intervals within attributes) within a grid structure, where clusters are formed with adjacent cells that are considered dense (contain a high number of objects).

Clustering mixed-attribute data requires a different approach to the numeric measures above. Most categorical attributes are nominal and have no inherent order, hence 'distance-based' measures are not appropriate \citet{Huang1998}. \citet{Boriah2008} provides an extensive review of purely categorical similarity measures, where the majority are based on the frequency and discrete probability distribution of attribute values. \citet{Jia2017} provides a summary of related work in mixed-attribute similarity measurements, where the main methods are to (1) discretize the numeric attributes and apply a categorical clustering or (2) encode the nominal attributes and apply a numerical clustering. The measures used for this research are discussed individually for the $K$-Protytpes and P3C algorithms introduced further.

\subsubsection{Curse of Dimensionality}
The third distinction discussed is the dimension space used for clustering and the associated issues. Traditional approaches suffer from the curse of dimensionality. \citet{Kriegel2009} provides an indepth summary of this problem. The foremost issue is that not all attributes are relevant for certain groups of data, and that these irrelevant attributes can degrade the output since the algorithm will attempt to utilise this noise. Algorithms that utilise the entire dimension space are unable to capture the nature of this interaction, as they attempt to describe each cluster with all attributes. Other issues are that proximity, distance or neighbourhood approaches become meaningless with increasing dimensionality, where all objects are essentially equidistant \citep{Agrawal1998}.

Two general methods exist for mitigating the curse of dimensionality if adopting classical approaches: transformation and subspace methods. Transformation methods seek to achieve lower dimensionality by constructing arbitrary subspaces, through methods such as correlation matrices \citep{Struski2015}. However, these methods lack interpretability since variables in the new feature space are not easily traced back to the original variable \citet{Parsons2004}. On the other hand, subspace methods refer to algorithms that archieve lower dimensionality by identifying a subset of the original attributes relevant to each cluster \citep{Han2011}.

\subsubsection{k-Prototypes}
For our application, the $K$-Prorotypes algorithm by \citet{Huang1998} is useful because it can cluster mixed-attriute data. The algorithm combined the k-Means similarity measure for numeric data, \textit{Euclidean Distance}, and k-Modes similarity measure for categorical data, \textit{Simple Matching}. The algorithm requires the number of clusters $k$ as an input, and uses an optimisation algorithm to allocate observations to clusters. The algorithms cost function is as follows:

$$P(W,Q) = \text{Cost Function} = \sum_{l=1}^{k}\sum_{i=1}^{n} w_{i,l}\cdot d(X,Y)$$

Where $w_{i,l}$ is the partition matrix and $d(X,Y)$ is the dissimilarity measure. The algorithm begins by initialising $Q_0$ (commonly randomly) with $k$ cluster centres, then iteratively updating both the partition matrix $W$ and $Q$ until the algorithm converges to a local optimum \citep{Huang1998}.

These values are calculated as follows. Note, the algorithm doesn't automatically scale the numeric attributes for the Euclidean Distance, however it is a recommended data pre-processing step \citep{Huang1998}.

$$d(X,Y) = \text{ Dissimilarity Measure } = \text{ Euclidean Distance + Simple Matching } = \sum_{j=1}^{p}{(x_j - y_j)^2} + \gamma\sum_{j = p+1}^{m}{\delta(x_j, y_j)}$$
\[ w_{i,l} =  \begin{cases} 
                   1 & d(X_i,Q_l) <= d(X_i,Q_t)\text{ for }1 \leq t \leq k \\
                   0 &\text{ for }t \neq l
               \end{cases}
\]

Where:
$$k = \text{ The input number of clusters}$$
$$p = \text{ The number of numeric attributes}$$
$$m - p = \text{ The number of categorical attributes}$$
\[ \delta(x_j, y_j) =  \begin{cases} 
                         1 & x_j \neq y_j \\
                         0 & x_j = y_j
                       \end{cases}
\]


