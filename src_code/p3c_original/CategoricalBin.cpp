#include "CategoricalBin.h"

/************ 
Constructor(s) and destructor
************/
//default constructor
CategoricalBin::CategoricalBin(){
	this->sCategoricalItem = NULL;
}

//constructor
CategoricalBin::CategoricalBin(int no_total_objects, int dim_id, char* sItem, int iNoChars)
:Bin(no_total_objects, dim_id)
{

	this->sCategoricalItem = new char[iNoChars + 1];
	assert(this->sCategoricalItem != NULL);

#ifdef WIN32
	int bFail = 0;
#endif

#ifdef WIN32
	bFail = strcpy_s(this->sCategoricalItem, iNoChars+1, sItem);
	assert(bFail == 0);
#else
	strcpy(this->sCategoricalItem, sItem);
#endif
	
}

//destructor
CategoricalBin::~CategoricalBin(){
	
	//cout << "Dtor CategoricalBin" << endl;
	delete []this->sCategoricalItem;
	this->sCategoricalItem = NULL;

	if (this->vObjects != NULL){
		delete []this->vObjects;
		this->vObjects = NULL;
	}
}

/************
Functions for manipulating private members of the class
************/
char* CategoricalBin::GetCategoricalItem() const{
	
	return this->sCategoricalItem;
}


/************
Displays a numerical bin for debugging purposes
************/
void CategoricalBin::Display(ostream& os){

	int iSupport = this->GetActualSupport();
	os << "Support = " << iSupport << endl;
	os << "Label = " << this->sCategoricalItem << endl;
	if (this->bDense == 1)
		os << "Dense" << endl;
	os << endl;
	
	//vector<int> vObjectsIDs = GetOneBits(this->vObjects, this->iNoCellsObjects, this->iNoTotalObjects);
	//for(int i=0;i<iSupport;i++)
	//	os << vObjectsIDs[i] << " ";
	//os << endl;

}

