#ifndef CATEGORICAL_HISTOGRAM
#define CATEGORICAL_HISTOGRAM

#include "Histogram.h"
#include "Utile.h"

#include <map>
#include <string>
using namespace std;

class CategoricalHistogram: public Histogram{

	map<string,int,less<string> > mapCategoriesIdentifiers; //each category is given an identifier

	void Display(ostream& os);

public:

	CategoricalHistogram();
	CategoricalHistogram(int no_bins, int dim_id, map<string,int,less<string> > &m);
	~CategoricalHistogram();
	
	map<string,int,less<string> >& GetMapCategoriesIdentifiers();
};

#endif

