#include "StringMatrix.h"

/************ 
Constructor(s) and destructor
************/

//emty constructor
StringMatrix::StringMatrix(){
    
    this->m_nCols = 0;
    this->m_nRows = 0;
    this->m_mat = NULL;
}

//constructor
StringMatrix::StringMatrix(int noRows, int noCols){
    this->m_nRows = noRows;
    this->m_nCols = noCols;
    this->m_mat = new char**[this->m_nRows];
    assert(this->m_mat != NULL);
    for(int i=0;i<this->m_nRows;i++){
        this->m_mat[i] = new char*[this->m_nCols];
        assert(this->m_mat[i] != NULL);
        for(int j=0;j<this->m_nCols;j++){
            this->m_mat[i][j] = NULL;
        }
    }
        
}

//copy constructor
StringMatrix::StringMatrix(const StringMatrix &m){
    /*
	if ( this->m_mat != NULL ){
        for (int i=0; i<this->m_nRows; i++){
            if (this->m_mat[i] != NULL){    
                for(int j=0;j<this->m_nCols;j++)
                    if (this->m_mat[i][j] != NULL){
                        delete []m_mat[i][j];
						m_mat[i][j] = NULL;	
                    }
                delete []m_mat[i];
				m_mat[i] = NULL;
            }
        }
		delete []m_mat;
		m_mat = NULL;
	}
	*/

#ifdef WIN32
	int bFail = 0;
#endif	
	this->m_nRows = m.m_nRows;
	this->m_nCols = m.m_nCols;
	this->m_mat = new char**[this->m_nRows];
    assert(this->m_mat != NULL);
    for(int i=0;i<this->m_nRows;i++){
        this->m_mat[i] = new char*[this->m_nCols];
        assert(this->m_mat[i] != NULL);
        for(int j=0;j<this->m_nCols;j++){
			this->m_mat[i][j] = new char[strlen(m.m_mat[i][j]) + 1];
			assert(this->m_mat[i][j] != NULL);
#ifdef WIN32
			bFail = strcpy_s(this->m_mat[i][j], strlen(m.m_mat[i][j]), m.m_mat[i][j]);
			assert(bFail == 0);
#else
			strcpy(this->m_mat[i][j], m.m_mat[i][j]);
#endif
        }
    }	
    
}


//destructor
StringMatrix::~StringMatrix(){

    if ( this->m_mat != NULL ){
        for (int i=0; i<this->m_nRows; i++){
            if (this->m_mat[i] != NULL){    
                for(int j=0;j<this->m_nCols;j++)
                    if (this->m_mat[i][j] != NULL){
                        delete []m_mat[i][j];
						m_mat[i][j] = NULL;
                    }
                delete []m_mat[i];
				m_mat[i] = NULL;
            }
        }
		delete []m_mat;
		m_mat = NULL;
	}

}

/************ 
Functions for manipulating private members of the class
************/

int StringMatrix::GetNoCols() const {

    return this->m_nCols;
}

int StringMatrix::GetNoRows() const {

    return this->m_nRows;
}

char* StringMatrix::GetElemAt(int i, int j){
    
    assert( (i >= 0) && (i < this->m_nRows) && (j >= 0) && (j < this->m_nCols) );
    return this->m_mat[i][j];
}

void StringMatrix::SetElemAt(int i, int j, char* val){
    
    assert( (i >= 0) && (i < this->m_nRows) && (j >= 0) && (j < this->m_nCols) );

	if (this->m_mat[i][j] != NULL){
		delete []this->m_mat[i][j];
		this->m_mat[i][j] = NULL;
	}
	this->m_mat[i][j] = new char[strlen(val)+1];
	assert(this->m_mat[i][j] != NULL);

#ifdef WIN32
	int bFail = 0;
#endif
#ifdef WIN32
	bFail = strcpy_s(this->m_mat[i][j], strlen(val)+1, val);
	assert(bFail == 0);
#else
	strcpy(this->m_mat[i][j], val);
#endif

}

/************
Overloading the operator =
************/
StringMatrix& StringMatrix::operator =(const StringMatrix &m){

	if ( this->m_mat != NULL ){
        for (int i=0; i<this->m_nRows; i++){
            if (this->m_mat[i] != NULL){    
                for(int j=0;j<this->m_nCols;j++)
                    if (this->m_mat[i][j] != NULL){
                        delete []m_mat[i][j];
						m_mat[i][j] = NULL;	
                    }
                delete []m_mat[i];
				m_mat[i] = NULL;
            }
        }
		delete []m_mat;
		m_mat = NULL;
	}
		
#ifdef WIN32
	int bFail = 0;
#endif
	this->m_nRows = m.m_nRows;
	this->m_nCols = m.m_nCols;
	this->m_mat = new char**[this->m_nRows];
    assert(this->m_mat != NULL);
    for(int i=0;i<this->m_nRows;i++){
        this->m_mat[i] = new char*[this->m_nCols];
        assert(this->m_mat[i] != NULL);
        for(int j=0;j<this->m_nCols;j++){
			this->m_mat[i][j] = new char[strlen(m.m_mat[i][j]) + 1];
			assert(this->m_mat[i][j] != NULL);
#ifdef WIN32
			bFail = strcpy_s(this->m_mat[i][j], strlen(m.m_mat[i][j]), m.m_mat[i][j]);
			assert(bFail == 0);
#else
			strcpy(this->m_mat[i][j], m.m_mat[i][j]);
#endif
        }
    }

	return *this;
}

/************
The format of the data file is item\spaceitem\space ....item\spacelabel\n
When reading the data matrix, the whole matrix is read, including the column with labels
Later in the computation, the last column will be ignored
************/
int StringMatrix::ReadDataFile(char* sInputFile){

    if (DetermineDataDimensionality(sInputFile) == FAILURE)
        return FAILURE;
    
    //initialize the matrix
    this->m_mat = new char**[this->m_nRows];
    assert(this->m_mat!=NULL);
    for(int i=0;i<this->m_nRows;i++){
        this->m_mat[i] = new char*[this->m_nCols];
        assert(this->m_mat[i]!=NULL);
        for(int j=0;j<this->m_nCols;j++){
            this->m_mat[i][j] = NULL;
        }
    }
        

	ifstream InputFile;
	InputFile.open(sInputFile, ifstream::in);
	if (!InputFile.is_open()){
		cerr << "StringMatrix: Cannot open the file " << sInputFile << endl;
		return FAILURE;
	}

    char *sLine = new char[LINE_LENGTH];
    assert(sLine != NULL);
            
    int currentRow = -1, currentCol = -1;
    char *ptrBegin, *ptrEnd;
	char *CarryOn, *stub;
	int lenCarryOn, lenStub;
    int index;
    int bNewLine = 1;
#ifdef WIN32
	int bFail = 0;
#endif
	while(!InputFile.eof()){
        
		InputFile.get(sLine,LINE_LENGTH);
		        
		if (InputFile.eof())
            continue;
		if (strstr(sLine,"Cluster")!=NULL){
            char cNextChar;
			InputFile.get(cNextChar);
			continue;
		}
		if (strstr(sLine,"SIGNATURE")!=NULL){
            char cNextChar;
			InputFile.get(cNextChar);
			continue;
		}
        
				
		if (bNewLine){ //i.e., we are at the beginning of a new line
			
			currentRow++;
			currentCol = -1;
			//cerr << "Line " << currentRow << endl;
			
			//eliminate the leading blanks
			index = 0;
			while(sLine[index] == ' ')
				index++;
			
			ptrBegin = sLine + index;
			ptrEnd = strchr(ptrBegin,' '); 
			
		}
		else{ //i.e., we are not at the beginning of a new line
		
			ptrBegin = sLine;
			ptrEnd = strchr(ptrBegin,' ');
			
			if (ptrEnd != NULL){ // if I have found a space
				
				//form the stub
				lenStub = ptrEnd - ptrBegin;
				stub = new char[lenStub + 1];
				assert(stub != NULL);
					
#ifdef WIN32
				bFail = strncpy_s(stub, lenStub + 1, ptrBegin, lenStub);
				assert(bFail == 0);
#else
				strncpy(stub,ptrBegin,lenStub);
#endif
				stub[lenStub] = '\0';	
				
				//add stub to CarryOn and create an item
				int lenItem = lenCarryOn + lenStub;
				char *item = new char[lenItem + 1];
				assert(item != NULL);
					
#ifdef WIN32
				bFail = strcpy_s(item, lenItem + 1, CarryOn);
				assert(bFail == 0);
				strcat_s(item, lenItem + 1, stub);
				assert(bFail == 0);
#else
				strcpy(item, CarryOn);
				strcat(item, stub);
#endif
				item[lenItem] = '\0';
				currentCol++;
				assert((currentRow>=0) && (currentRow<this->m_nRows) && (currentCol>=0) && (currentCol<this->m_nCols));
				this->SetElemAt(currentRow,currentCol,item);

				delete []CarryOn;
				CarryOn = NULL;
				lenCarryOn = 0;
				delete []stub;
				stub= NULL;
				lenStub = 0;
			
				ptrBegin = ptrEnd + 1;
				ptrEnd = strchr(ptrBegin,' ');

				//JUNE 26, 2008
				delete []item;
				item = NULL;
			
			}
			else{ // I do not find a space
				char cNextChar;
				cNextChar = InputFile.peek();
				if (cNextChar == '\n'){ //But I do find the end of line
					
					InputFile.get(cNextChar);

					//form the stub
					lenStub = sLine + strlen(sLine) - ptrBegin;
					stub = new char[lenStub + 1];
					assert(stub != NULL);
						
#ifdef WIN32
				    bFail = strncpy_s(stub, lenStub + 1, ptrBegin, lenStub);		
					assert(bFail == 0);
#else	
					strncpy(stub, ptrBegin, lenStub);
#endif
					stub[lenStub] = '\0';	
					
					//add stub to CarryOn and create an item
					int lenItem = lenCarryOn + lenStub;
					char *item = new char[lenItem + 1];
					assert(item != NULL);
						
#ifdef WIN32
					bFail = strcpy_s(item, lenItem + 1, CarryOn);
					assert(bFail == 0);
					strcat_s(item, lenItem + 1, stub);
					assert(bFail == 0);
#else
					strcpy(item, CarryOn);
					strcat(item, stub);
#endif
					item[lenItem] = '\0';
					currentCol++;
					assert((currentRow>=0) && (currentRow<this->m_nRows) && (currentCol>=0) && (currentCol<this->m_nCols));
					this->SetElemAt(currentRow,currentCol,item);
					
					delete []CarryOn;
					CarryOn = NULL;
					lenCarryOn = 0;
					delete []stub;
					stub= NULL;
					lenStub = 0;
										
					//JUNE 26, 2008
					delete []item;
					item = NULL;

					bNewLine = 1;
					continue;
				}
				else{ // I do not find a space, and I do find the end of line
	
					//form the stub
					lenStub = LINE_LENGTH - 1;
					stub = new char[lenStub + 1];
					assert(stub != NULL);
						
#ifdef WIN32
				    bFail = strncpy_s(stub, lenStub + 1, ptrBegin, lenStub);		
					assert(bFail == 0);
#else	
					strncpy(stub, ptrBegin, lenStub);
#endif
					stub[lenStub] = '\0';
					
					//add stub to CarryOn and create a TempItem
					int lenTemp = lenCarryOn + lenStub;
					char *TempItem = new char[lenTemp + 1];
					assert(TempItem != NULL);
						
#ifdef WIN32
					bFail = strcpy_s(TempItem, lenTemp + 1, CarryOn);
					assert(bFail == 0);
					strcat_s(TempItem, lenTemp + 1, stub);
					assert(bFail == 0);
#else
					strcpy(TempItem,CarryOn);
					strcat(TempItem,stub);
#endif
					TempItem[lenTemp] = '\0';
					
					//replace CarryOn by TempItem
					delete []CarryOn;
					CarryOn = NULL;
					CarryOn = new char[lenTemp + 1];
					assert(CarryOn != NULL);
#ifdef WIN32
					bFail = strcpy_s(CarryOn, lenTemp + 1, TempItem);
					assert(bFail == 0);
#else
					strcpy(CarryOn, TempItem);
#endif
					CarryOn[lenTemp] = '\0';
					lenCarryOn = lenTemp;

					delete []TempItem;
					TempItem = NULL;
					delete []stub;
					stub = NULL;
					continue;
				}
			}
			
							
		}//else bNewLine
		
        
		while(ptrEnd){
			int lenItem = ptrEnd - ptrBegin;
			char* item = new char[lenItem+1];
			assert(item != NULL);
				
#ifdef WIN32
			bFail = strncpy_s(item, lenItem + 1, ptrBegin, lenItem);
			assert(bFail == 0);
#else
			strncpy(item, ptrBegin, lenItem);
#endif
			item[lenItem] = '\0';
			currentCol++;
			assert((currentRow>=0) && (currentRow<this->m_nRows) && (currentCol>=0) && (currentCol<this->m_nCols));
			this->SetElemAt(currentRow,currentCol,item);
			
			ptrBegin = ptrEnd + 1;
			ptrEnd = strchr(ptrBegin,' ');

			//JUNE 26, 2008
			delete []item;
			item = NULL;
		}
		
		char cNextChar;
		cNextChar = InputFile.peek();
		if (cNextChar == '\n'){ //i.e., we have found the end of the line
			//cerr << "End line" << endl;
			InputFile.get(cNextChar);
			
			int lenItem = sLine + strlen(sLine) - ptrBegin;
			char* item = new char[lenItem+1];
			assert(item != NULL);
				
#ifdef WIN32
			bFail = strncpy_s(item, lenItem + 1, ptrBegin, lenItem);
			assert(bFail == 0);
#else
			strncpy(item, ptrBegin, lenItem);
#endif
			item[lenItem] = '\0';
			currentCol++;
			assert((currentRow>=0) && (currentRow<this->m_nRows) && (currentCol>=0) && (currentCol<this->m_nCols));
			this->SetElemAt(currentRow,currentCol,item);
			
			bNewLine = 1;

			//JUNE 26, 2008
			delete []item;
			item = NULL;
		}
		else{
			//cerr << "Carry on" << endl;
			lenCarryOn = sLine + LINE_LENGTH - 1 - ptrBegin;
			CarryOn = new char[lenCarryOn + 1];
			assert(CarryOn != NULL);
				
#ifdef WIN32
			bFail = strncpy_s(CarryOn, lenCarryOn + 1, ptrBegin, lenCarryOn);
			assert(bFail == 0);
#else
			strncpy(CarryOn, ptrBegin, lenCarryOn);
#endif
			CarryOn[lenCarryOn] = '\0';
			
			bNewLine = 0;
		}
		
		
    }//end while file
	
    delete []sLine;
	sLine = NULL;

	InputFile.close();
	
    //this->Display();
	
    return SUCCESS;
	
}

/************ 
This function determines how many rows and columns are present in the input file
************/
int StringMatrix::DetermineDataDimensionality(char* sInputFile){
    	
   	ifstream InputFile;
	InputFile.open(sInputFile, ifstream::in);
	if (!InputFile.is_open()){
		cerr << "StringMatrix: Cannot open the file " << sInputFile << endl;
		return FAILURE;
	}

    char *sLine = new char[LINE_LENGTH];
    assert(sLine != NULL);
    
    char *ptrBegin, *ptrEnd;
    int index;
	int bNewLine = 1;
    while(!InputFile.eof()){
        
		InputFile.get(sLine,LINE_LENGTH);
		
		if (InputFile.eof())
            continue;
		if (strstr(sLine,"Cluster")!=NULL){
           char cNextChar;
			InputFile.get(cNextChar);
			continue;
		}
		if (strstr(sLine,"SIGNATURE")!=NULL){
			char cNextChar;
			InputFile.get(cNextChar);           
			continue;
		}
		
		if (this->m_nRows == 0){ // i.e., we are on the first row
		
			index = 0;
			if(bNewLine){
				//eliminate the leading blanks
				while(sLine[index] == ' ')
					index++;
			}
			
			ptrBegin = sLine + index;
			ptrEnd = strchr(ptrBegin,' ');

			while(ptrEnd){
				this->m_nCols++;
				ptrBegin = ptrEnd + 1;
				ptrEnd = strchr(ptrBegin,' ');
			}
			
			char cNextChar;
			cNextChar = InputFile.peek();
			if (cNextChar == '\n'){ //i.e., we have found the end of line
				InputFile.get(cNextChar);
				this->m_nCols++;
				this->m_nRows++;
				bNewLine = 1;
				continue;
			}
			else{
				bNewLine = 0;
			}
		}

		if (this->m_nRows == 0)
			continue;
      
		char cNextChar;
		cNextChar = InputFile.peek();
		if (cNextChar == '\n'){ //i.e., we have found the end of line
			InputFile.get(cNextChar);
			bNewLine = 1;
			this->m_nRows++;
		}
		else{
			bNewLine = 0;
		}
		
		

    }//end while file

	delete []sLine;
	sLine = NULL;

	cout << "Data matrix has " << this->m_nRows << " rows and " << this->m_nCols << " columns" << endl;

	InputFile.close();

    return SUCCESS;
}




/************ 
Display the string matrix using stdout. Only for testing purposes
************/
void StringMatrix::Display(ostream& os){

    for(int i=0;i<this->m_nRows;i++){
        for(int j=0;j<this->m_nCols;j++){
            os << this->m_mat[i][j] << " ";
        }
        os << endl;
    }
}


