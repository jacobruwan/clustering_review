#ifndef CATEGORICAL_BIN
#define CATEGORICAL_BIN

#include "Bin.h"

class CategoricalBin: public Bin{

	char* sCategoricalItem;

public:

	CategoricalBin();
	CategoricalBin(int no_total_objects, int dim_id, char* sItem, int iNoChars);
	~CategoricalBin();

	char* GetCategoricalItem() const;

	void Display(ostream &os);
};

#endif
