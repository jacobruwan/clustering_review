#ifndef INTERVAL_H
#define INTERVAL_H

#include <vector>
#include <fstream>
using namespace std;

#include "Utile.h"

class Interval{

	int iDimID;
	vector<int> vIDBins;

public:
	Interval();
	Interval(int dim_id);
	~Interval();

	int GetDimID() const;
	int GetNoBins();
	vector<int> GetIDBins() const;	

	void AddBin(int iIDBin);
	void RemoveBin(int iIDBin);
		
};

#endif
