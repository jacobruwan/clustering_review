#include "OneDHistograms.h"

/************
Constructor(s) and destructor
************/
//default constructor
OneDHistograms::OneDHistograms(){
	
	this->DataMatrix = NULL;
	this->vAllHistograms = NULL;
	this->iNoHistograms = 0;
}

//constructor
OneDHistograms::OneDHistograms(StringMatrix *SM){
	this->DataMatrix = SM;
	this->iNoHistograms = this->DataMatrix->GetNoCols() - 1;
	this->vAllHistograms = NULL;
}

//destructor
OneDHistograms::~OneDHistograms(){
	
	//cout << "Dtor OneDHistograms" << endl;
	if (this->vAllHistograms != NULL){
		for(int i=0;i<this->iNoHistograms;i++){
			delete this->vAllHistograms[i];
			this->vAllHistograms[i] = NULL;
		}
		delete []this->vAllHistograms;
		this->vAllHistograms = NULL;
	}
}

/************
Functions for manipulating private members of the class
************/
Histogram** OneDHistograms::GetAllHistograms(){
	
	return this->vAllHistograms;
}
