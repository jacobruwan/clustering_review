#include "Round.h"

#include <math.h>

/************ 
Constructor(s) and destructor
************/
//default constructor
Round::Round(){
	
	this->MembershipMatrix = NULL;
}

//destructor
Round::~Round(){

	int iLengthVector = (int)this->vIntervals.size();
	for(int i=0; i<iLengthVector;i++){
		delete this->vIntervals[i];
		this->vIntervals[i] = NULL;
	}
	this->vIntervals.clear();

	iLengthVector = (int)this->vCC.size();
	for(int i=0; i<iLengthVector;i++){
		delete this->vCC[i];
		this->vCC[i] = NULL;
	}
	this->vCC.clear();

	delete this->MembershipMatrix;
	this->MembershipMatrix = NULL;

	iLengthVector = (int)this->vAllRelevantDimensions.size();
	for(int i=0; i<iLengthVector;i++){
		this->vAllRelevantDimensions[i].clear();
	}
	this->vAllRelevantDimensions.clear();

}

/************
Functions for manipulating private members of the class
************/
MatrixGeneric<unsigned int>* Round::GetMembershipMatrix(){

	return this->MembershipMatrix;
}

vector<vector<int> >& Round::GetAllRelevantDimensions(){

	return this->vAllRelevantDimensions;
}

/************ 
Perform one clustering round
************/
int Round::DoRound(ClusteringRounds* obj_CR){

	cout << "Marking dense bins ..." << endl; 
	this->MarkDenseBins(obj_CR);
	//obj_CR->obj_CategoricalOneDHistograms->Display(cout);

	cout << "Computing intervals ..." << endl;
	this->ComputeIntervals(obj_CR);
	int iLengthVector = (int)this->vIntervals.size();
	cout << "Number Intervals = " << iLengthVector << endl;
	//this->DisplayIntervals(cout, obj_CR);

	cout << "Generating cluster cores ..." << endl;
	if ( this->GenerateClusterCores(obj_CR) == FAILURE)
		return FAILURE;
	this->DisplayClusterCores(cout, obj_CR);

	if (this->vCC.empty())
		return FAILURE;
	
	cout << "Computing the membership matrix and relevant attributes ..." << endl;
	this->ComputeRelevantAttributes(obj_CR);
	this->ComputeMembership(obj_CR);
		
	return SUCCESS;
}


/************ 
For each attribute, the dense bins are marked using the Chi-square test
************/
void Round::MarkDenseBins(ClusteringRounds* obj_CR){

	int iNoTotalAttributes = obj_CR->DataMatrix->GetNoCols() - 1;
	Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);	

	int j, index;
		
	for(j=0;j<iNoTotalAttributes;j++){ //for each attribute
		
		int iNoBins = vAllHistograms[j]->GetNoBins();

		//collect the sizes (i.e., supports) of all bins
		int* vBinSizes = new int[iNoBins];
		for(index=0;index<iNoBins;index++){
			Bin* obj_Bin = vAllHistograms[j]->GetBin(index);
			assert(obj_Bin != NULL);
			vBinSizes[index] = obj_Bin->GetActualSupport();
		}

		int iNoDegreesFreedom = iNoBins - 2; // "-2", instead of "-1", because I compute the mean based on the data
        double dMeanBins = 0;
        double dChisquareStat = 0;
        int iNoActiveBins = iNoBins;
        int bSatisfyCondition = 1;

		while( bSatisfyCondition && (iNoActiveBins > 2) ){
            
			for(index=0; index<iNoBins; index++){
                if (vBinSizes[index] != -1){
                    dMeanBins += vBinSizes[index];
                }
            }
            dMeanBins /= iNoActiveBins; 
        
            for(index=0; index<iNoBins; index++){
                if (vBinSizes[index] != -1){
                    if (vBinSizes[index] < 5)
                        dChisquareStat += pow(vBinSizes[index] - dMeanBins - 0.5, 2) / dMeanBins; //Yates correction
                    else
                        dChisquareStat += pow(vBinSizes[index] - dMeanBins, 2) / dMeanBins;
                }
            }
            
            int alpha = 4; //alpha = 0.001 //tight bins
            //int alpha = 3;  // alpha = 0.01 //larger bins
            if (dChisquareStat <= obj_CR->CriticalValues->GetElemAt(iNoDegreesFreedom, alpha)){
                bSatisfyCondition = 0;   
            }
            else{
                int iMaxElem = -1;
                int iMaxElemIndex = -1;
                for(index=0; index<iNoBins; index++){
                    if (iMaxElem < vBinSizes[index]){
                        iMaxElem = vBinSizes[index];
                        iMaxElemIndex = index;
                    }
                }
                vBinSizes[iMaxElemIndex] = -1;
                
				Bin* obj_Bin = vAllHistograms[j]->GetBin(iMaxElemIndex);
				assert(obj_Bin != NULL);
				obj_Bin->SetBinDense(1);
				
                iNoDegreesFreedom--;
                iNoActiveBins--;
                dMeanBins = 0;
                dChisquareStat = 0;
            }
        
            //bSatisfyCondition = 0; // to force marking just one bin: the most dense bin; useful for real data

        }//end while


		if ((bSatisfyCondition == 1) && (iNoActiveBins == 2)){
			//I have marked all bins, and I have stopped when I am left with two unmarked bins
			for(index=0; index<iNoBins; index++){
				if (vBinSizes[index] != -1){
                    dMeanBins += vBinSizes[index];
                }
			}
			dMeanBins /= iNoActiveBins;

			for(index=0; index<iNoBins; index++){
				if (vBinSizes[index] != -1){ //one of the two unmarked bins
					//if( (vBinSizes[index] > dMeanBins) && (Poisson(vBinSizes[index], dMeanBins) < THRESHOLD_MARK) ){
						if 	(vBinSizes[index] > dMeanBins){
						//mark the bin whose size is larger than the size of the other bin
						//Poisson_threshold may be too tough (1.0e-20)
						//I could use instead THRESHOLD_MARK = 0.01
						Bin* obj_Bin = vAllHistograms[j]->GetBin(index);
						assert(obj_Bin != NULL);
						obj_Bin->SetBinDense(1);
					}
				}
			}
		}

		delete []vBinSizes;
		vBinSizes = NULL;
	}//end for j

}//end function


/************ 
For categorical attributes
************/
void Round::ComputeIntervals(ClusteringRounds *obj_CR){

	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;
	int iNoTotalAttributes = obj_CR->DataMatrix->GetNoCols() - 1;
	Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
		
	int i, j, index;

    //we keep pairs of the form (iDimID, marked_iBinID) in vSingle Projections
    vector<struct DimIDBinID*> vSingleProjections; 
    for(j=0; j<iNoTotalAttributes; j++){// for each attribute
        int iNoBins = vAllHistograms[j]->GetNoBins();
        for(index=0; index<iNoBins; index++){ //for each bin
			Bin* obj_Bin = vAllHistograms[j]->GetBin(index);
			assert(obj_Bin != NULL);
			if (obj_Bin->IsBinDense() == 1){
                struct DimIDBinID* s = new struct DimIDBinID;
				s->iDimID = j;
				s->iBinID = index;
                vSingleProjections.push_back(s);
            }
        }
    }

	//for each element in vSingle Projections, I need to compute its "neighbors",
    //which are also elements from vSingle Projections. I store the neighbors as 
    //IDs in the vector vSingle Projections
    vector<vector<int> > vNeighbors;
	int iLengthVector = (int)vSingleProjections.size();
    vNeighbors.reserve(iLengthVector);
    for(index=0; index <iLengthVector; index++){
        vector<int> vTemp;
        vNeighbors.push_back(vTemp);
    }

	iLengthVector = (int)vSingleProjections.size();
	for(i=0; i<iLengthVector; i++){
		
		int iDim_i = vSingleProjections[i]->iDimID;
		int iBin_i = vSingleProjections[i]->iBinID;
		Bin* objBin_i = vAllHistograms[iDim_i]->GetBin(iBin_i);
		assert(objBin_i != NULL);
		
		for(j=i+1; j<iLengthVector; j++){
			
			int iDim_j = vSingleProjections[j]->iDimID;
			int iBin_j = vSingleProjections[j]->iBinID;
			Bin* objBin_j = vAllHistograms[iDim_j]->GetBin(iBin_j);
			assert(objBin_j != NULL);
			
			if (iDim_i != iDim_j){ // only if they are on different dimensions
				
				int iNoObj_i = GetNumberOneBits(objBin_i->GetObjects(), iNoCellsObjects, iNoTotalObjects);
				int iNoObj_j = GetNumberOneBits(objBin_j->GetObjects(), iNoCellsObjects, iNoTotalObjects);

				unsigned int *vIntersection = new unsigned int[iNoCellsObjects];
				assert(vIntersection != NULL);
				for(index=0; index<iNoCellsObjects; index++)
					vIntersection[index] = ~0;

				IntersectionArrayBits(vIntersection, vIntersection, objBin_i->GetObjects(), iNoCellsObjects);
				IntersectionArrayBits(vIntersection, vIntersection, objBin_j->GetObjects(), iNoCellsObjects);

				int iNoObjIntersection = GetNumberOneBits(vIntersection, iNoCellsObjects, iNoTotalObjects);

				//the older version based on the Poisson distribution				
				// if the objects in bin i would be uniformly distributed in the dimension of bin_j
				//double dExpectedNoObj_i = (double)iNoObj_i / (double)vAllHistograms[iDim_j]->GetNoBins();
				// if the objects in bin j would be uniformly distributed in the dimension of bin_i
				//double dExpectedNoObj_j = (double)iNoObj_j / (double)vAllHistograms[iDim_i]->GetNoBins();
				//int cond1 = (iNoObjIntersection > dExpectedNoObj_i) && (Poisson(iNoObjIntersection, dExpectedNoObj_i) < obj_CR->dPoissonThreshold);
				//int cond2 = (iNoObjIntersection > dExpectedNoObj_j) && (Poisson(iNoObjIntersection, dExpectedNoObj_j) < obj_CR->dPoissonThreshold);

				//March 2008: new version with Binomial distribution and area under the curve
				int iCriticalValue_i = GetCriticalValueBinomialRight(iNoObj_i, 1.0 / (double)vAllHistograms[iDim_j]->GetNoBins(), obj_CR->dPoissonThreshold);
				int iCriticalValue_j = GetCriticalValueBinomialRight(iNoObj_j, 1.0 / (double)vAllHistograms[iDim_i]->GetNoBins(), obj_CR->dPoissonThreshold);
				int cond1 = (iNoObjIntersection >= iCriticalValue_i);
				int cond2 = (iNoObjIntersection >= iCriticalValue_j);

				if (cond1 && cond2){
					//j is a neighbor of i and i is a neighbor of j
					vNeighbors[i].push_back(j);
					vNeighbors[j].push_back(i);
				}

				delete []vIntersection;
				vIntersection = NULL;

			}//end if
		}//end for j
	}//end for i

	//Based on the neighbors, I compute similarities between pairs of attribute values on the same attribute
    //The similarity between two attribute values on the same attribute is 1,
	//if the number of common neighbors is larger than 0; and 0, otherwise
    //Based on the similarities, we determine intervals on the attribute
    int index1, index2;
    for(j=0; j<iNoTotalAttributes; j++){ //for each attribute in the data set
        
        //find out the posistions in vSingleProjections where we have marked bin on attribute j
        vector<int> vPositions;
		iLengthVector = (int)vSingleProjections.size();
        for(index=0; index<iLengthVector; index++){
			if (vSingleProjections[index]->iDimID == j)
                vPositions.push_back(index);
		} 

		int iNoElem = (int)vPositions.size();
		if (iNoElem > 0){ //j is an attribute with marked bins
					
            MatrixGeneric<int> *SimMatrix_j = new MatrixGeneric<int> (iNoElem,iNoElem);
			assert(SimMatrix_j != NULL);
            for(index=0; index<iNoElem; index++)
                SimMatrix_j->SetElemAt(index,index,1);

			for(index1=0; index1<iNoElem; index1++){
				
				for(index2=index1 + 1; index2<iNoElem; index2++){
					
					int iPos1 = vPositions[index1]; //iPos1 is the same position in vSingleProjections as in vNeighbors
					int iPos2 = vPositions[index2];
					int iDim_1 = vSingleProjections[iPos1]->iDimID;
					int iDim_2 = vSingleProjections[iPos2]->iDimID;
					assert(iDim_1 == iDim_2);
					int iNoCommonNeighbors = ComputeNoCommonElem(vNeighbors[iPos1],vNeighbors[iPos2]);
					
					if (iNoCommonNeighbors > 0){
						SimMatrix_j->SetElemAt(index1,index2,1);
						SimMatrix_j->SetElemAt(index2,index1,1);
					}
				}
			}
			//cout << *SimMatrix_j;

			//compute all maximal cliques of an undirected graph given by the adjacency matrix SimMatrix_j using the BK algorithm
			vector<vector<int> > vMaximalCliques;
			//BKAlgorithm(vMaximalCliques, SimMatrix_j);
			//compute connected components of an undirected graph given by the adjacency matrix SimMatrix_j
			ConnectedComponentsAlgorithm(vMaximalCliques, SimMatrix_j);		
			//DisplayVector(cout, vMaximalCliques);

			//create the intervals
			iLengthVector = (int)vMaximalCliques.size();
			for(index=0; index<iLengthVector; index++){
				
				Interval* obj_Interval = new Interval(j);
				assert(obj_Interval != NULL);

				for(index1 = 0; index1<(int)vMaximalCliques[index].size(); index1++){
					int iIDBin = vSingleProjections[vPositions[vMaximalCliques[index][index1]]]->iBinID;
					obj_Interval->AddBin(iIDBin);
				}
				
				this->vIntervals.push_back(obj_Interval);
			}

			delete SimMatrix_j;
			SimMatrix_j = NULL;

			iLengthVector = (int)vMaximalCliques.size();
			for(index=0; index<iLengthVector; index++)
				vMaximalCliques[index].clear();
			vMaximalCliques.clear();
		
		}//end if (vPositions.size() > 0)	

		vPositions.clear();
	
	}//end for j


	//cleaning the vectors
	iLengthVector = (int)vSingleProjections.size();
	for(index=0; index <iLengthVector; index++){
		delete vSingleProjections[index];
		vNeighbors[index].clear();
	}
	vSingleProjections.clear();
	vNeighbors.clear();

	
}

/************ 
Given the adjacency matrix of a graph, it computes all its maximal cliques using the Bron-Kerbosch algorithm
***********/
void Round::BKAlgorithm(vector<vector<int> >& vMaximalCliques, MatrixGeneric<int>* AdjacencyMatrix){

	vector<int> clique; //current (growing or shrinking) clique
    vector<int> cand; //points that are eligible to be added to the current clique
    vector<int> processed; //points that at an earlier stage already served as extension for current clique

	//initially, 'clique' is void, 'processed' is void, 'cand' contains all points
    int n = AdjacencyMatrix->GetCols();
	for(int i=0; i<n; i++)
		cand.push_back(i);

		
	Expand(clique, cand, processed, vMaximalCliques, AdjacencyMatrix); 
}

/************ 
This function generates all extensions of 'clique' with points from 'cand' that do not contain any point in 'processed'
Equivalently, all extensions of 'clique' with points from 'processed' have been already generated
************/
void Round::Expand(vector<int>& clique, vector<int> &cand, vector<int> &processed, vector<vector<int> >& vMaximalCliques, MatrixGeneric<int>* AdjacencyMatrix){

	int n = AdjacencyMatrix->GetCols();
	int i,j;

	if ((cand.size() == 0) && (processed.size() == 0) ){
		//first condition says that there are no points that can be used to extend current 'clique'
		//this condition is necessary but not sufficient
		//if processed.size()!=0, it means that all extensions of 'clique' with the points in 'processed' have been already generated,
		//thus, 'clique' is not maximal
		
		vMaximalCliques.push_back(clique);
		return;
	}

	while(cand.size() != 0){
		
		//the points in 'cand' found at positions 'eligible_positions_cand' can be selected for extension
		vector<int> eligible_positions_cand;

		//possible candidates are vertices in the 'cand' that are not connected to the last vertex added to 'processed'

		if (processed.size() > 0){

			int last_vertex_added_to_processed = processed[processed.size() - 1];
			int bFound = 0;
			for(i=0;i<(int)cand.size();i++){
				if (AdjacencyMatrix->GetElemAt(cand[i],last_vertex_added_to_processed) == 0) {
					bFound = 1;
					eligible_positions_cand.push_back(i);
				} 
			}
			if (bFound == 0){
				//the last vertex added to 'processed' is connected to all vertices in 'cand'
				//thus, there is no point in continuing, because I won't get to a maximal clique
				return;
			}
		}
		else{
			for(i=0;i<(int)cand.size();i++)
				eligible_positions_cand.push_back(i);
		}

		//for each of the eligible_candidates, we need to determine their connectivity to the other members of 'cand'
		vector<int> connectivity;
		connectivity.reserve(eligible_positions_cand.size());
		for(j=0; j<(int)eligible_positions_cand.size(); j++){
			int eligible_cand = cand[eligible_positions_cand[j]];
			int connections = 0;
			for(i=0;i<(int)cand.size();i++){
				if (AdjacencyMatrix->GetElemAt(eligible_cand,cand[i]) == 1)
					connections++;
			}
			connectivity.push_back(connections);
		}
		
		//find out the candidate with the largest connectivity
		int pos_max = -1;
		int max_elem = -1;
		for(j=0;j<(int)connectivity.size();j++){
			if (connectivity[j] >  max_elem){
				max_elem =  connectivity[j];
				pos_max = j;
			}
		}
		assert(pos_max != -1);

		int pos_cand = eligible_positions_cand[pos_max];
         
		int v = cand[pos_cand];

		//add the candidate to clique
        clique.push_back(v);
        
        //determine the neighbors of v
        vector<int> neighbors;
        for(i=0;i<n;i++){
            if ((v != i) && (AdjacencyMatrix->GetElemAt(v,i) == 1))
                neighbors.push_back(i);
        }

        //create new_cand and new_not
        vector<int> new_cand;
        vector<int> new_processed;
       
        //new_cand is 'cand' \intersect neighbors of v
        ComputeIntersectionVectors(new_cand,cand,neighbors);
        ComputeIntersectionVectors(new_processed,processed,neighbors);

		//recursive call
		Expand(clique, new_cand, new_processed, vMaximalCliques, AdjacencyMatrix);

		//clean the vectors
        neighbors.clear();
        new_cand.clear();
        new_processed.clear();

        //remove 'v' from 'clique'
        clique.erase(clique.end()-1);

        //remove this candidate from 'cand'
        cand.erase(cand.begin() + pos_cand);

        //add 'v' to 'not'
        processed.push_back(v);


	}//end while(cand.size() != 0)
}

/************ 
Given the adjacency matrix of a graph, it computes all its connected components
***********/
void Round::ConnectedComponentsAlgorithm(vector<vector<int> >& v, MatrixGeneric<int>* AdjacencyMatrix){

	int i;
	
	int iNoElem = AdjacencyMatrix->GetRows();
	vector<int> Visited;
	Visited.reserve(iNoElem);
	for(i=0; i<iNoElem; i++)
		Visited.push_back(0); //initially, no element is visited
	
	int iIndexFirstUnvisited = 0; // we start with the first element

	while(iIndexFirstUnvisited != -1){
	
		vector<int> vCurrent; //keeps the indexes in the current connected component
        
		vCurrent.push_back(iIndexFirstUnvisited);
        Visited[iIndexFirstUnvisited] = 1;
        		
        vector<int> ToBeVisited;
        //ToBeVisited.push_back(iIndexFirstUnvisited);
        for(i=0; i<iNoElem; i++){
            if ((iIndexFirstUnvisited != i) && (AdjacencyMatrix->GetElemAt(iIndexFirstUnvisited,i) == 1)){
                ToBeVisited.push_back(i);
            }
        }

		while(!ToBeVisited.empty()){
			
			int elem = ToBeVisited[0];
			if (Visited[elem] == 0){
				
				vCurrent.push_back(elem);
				Visited[elem] = 1;

				for(i = 0; i<iNoElem; i++){
					if ((elem != i) && (AdjacencyMatrix->GetElemAt(elem,i) == 1)){
						//add "i" to the queue if it's not already in ToBeVisited
						int bExist = (Visited[i]==1) || ExistElem(i,ToBeVisited);
						if (!bExist)
							ToBeVisited.push_back(i);
					}
				}
			}
			
			ToBeVisited.erase(ToBeVisited.begin());

		} //end second while

		//now in vCurrent I have the connected component
		//For synthetic data, I won't put it if it consists of a single bin
		//if (vCurrent.size() > 1)
		//	v.push_back(vCurrent);
		v.push_back(vCurrent);

		vCurrent.clear();

		iIndexFirstUnvisited = -1;
        for(i=0; i<iNoElem; i++){
            if(Visited[i] == 0){
                iIndexFirstUnvisited = i;
                break;
            }
        }


	}//end first while

	Visited.clear();
}

/************ 
Compute the number of elements common to two vectors
************/
int Round::ComputeNoCommonElem(vector<int> &v1, vector<int> &v2){
    int res = 0;
    int iLenght1 = (int)v1.size();
	int iLenght2 = (int)v2.size();
	if (iLenght1 < iLenght2){
        for(int i=0; i<iLenght1; i++){
            int bExist = ExistElem(v1[i],v2);
            if (bExist)
                res++;
        }
    }
    else{
        for(int i=0; i<iLenght2; i++){
            int bExist = ExistElem(v2[i],v1);
            if (bExist)
                res++;
        }
    }

    return res;
}

/************ 
Compute the intersection between vectors "a" and "b" and store it in "inter"
Need to maintain the order in 'a'
************/
void Round::ComputeIntersectionVectors(vector<int> &inter, vector<int> &a, vector<int> &b){
    
    int iLength = (int)a.size();
	for(int i=0; i<iLength; i++){
		int bExist = ExistElem(a[i],b);
		if (bExist)
			inter.push_back(a[i]);
	}
    
}

/************ 
Computes the membership of the interval 'iIntervalID' ans stores it in 'vMembership' using bits
************/
void Round::GetMembershipInterval(unsigned int* vMembership, int iIntervalID, ClusteringRounds *obj_CR){

	int iLengthVector = (int)this->vIntervals.size();
	assert( (iIntervalID >= 0) && (iIntervalID < iLengthVector) );

	Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;
		
	Interval* obj_Interval = this->vIntervals[iIntervalID];
	assert(obj_Interval != NULL);
		
	int iDimID = obj_Interval->GetDimID();
	vector<int> vIDBins = obj_Interval->GetIDBins();
	int iNoBins = (int)vIDBins.size();
		
	for(int index=0;index<iNoBins;index++){
		Bin *obj_Bin = vAllHistograms[iDimID]->GetBin(vIDBins[index]);
		assert(obj_Bin != NULL);
		UnionArrayBits(vMembership, vMembership, obj_Bin->GetObjects(), iNoCellsObjects);
	}

}

/************ 
Computes the actual support of the interval 'iIntervalID'
************/
int Round::GetActualSupportInterval(int iIntervalID, ClusteringRounds* obj_CR){

	int iAS = 0;

	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;
		
	unsigned int* vMembership = new unsigned int[iNoCellsObjects];
	assert(vMembership != NULL);
	for(int i=0;i<iNoCellsObjects;i++)
		vMembership[i] = 0;
	
	this->GetMembershipInterval(vMembership, iIntervalID, obj_CR);

	iAS = GetNumberOneBits(vMembership, iNoCellsObjects, iNoTotalObjects);

	delete []vMembership;
	vMembership = NULL;

	return iAS;
}


/************ 
Generate cluster cores in an Apriori-like style
************/ 
int Round::GenerateClusterCores(ClusteringRounds* obj_CR){

	int i,j,index,index2;

	int iNoIntervals = (int)this->vIntervals.size();
    if (iNoIntervals == 0){
        return FAILURE;
    }

    Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	
	int k = 1; //k is the current level of generation

	int iNoTotalAttributes = obj_CR->DataMatrix->GetNoCols() - 1;
	vector<ClusterCore*> level_k; //nodes at level k
	vector<ClusterCore*> candidates_next_level; //candidates at level (k+1)    

	time_t timeBegin;
	time_t timeEnd;

	//for level 1
	time(&timeBegin);
	for(i=0;i<iNoIntervals;i++){

		//create a combination consisting of only one interval
		vector<int> vTemp;
		vTemp.push_back(i);
		ClusterCore* cand_cc = new ClusterCore(vTemp);    
		int iAS = this->GetActualSupportIntersectionIntervals(vTemp, obj_CR);
		cand_cc->SetActualSupport(iAS);
		
		level_k.push_back(cand_cc);

		vTemp.clear();
	}
	time(&timeEnd);

	cout << "Level " << k << "- " << level_k.size() << " combinations - Elapsed time: " << timeEnd-timeBegin << endl;
	//this->DisplayLevel(cout, level_k);
	
	//for following levels   
	int iSizeLevelk = (int)level_k.size();
	while(iSizeLevelk !=0){

		time(&timeBegin);

		k++;

		if (k == iNoTotalAttributes + 1){ //if I generated already d levels
			//check the ones on the last level
			for(i=0; i<iSizeLevelk; i++){
				int bMaximal = SatisfiesMaximality(level_k[i], obj_CR);
				if (bMaximal){
					ClusterCore* CopyCC = new ClusterCore(*level_k[i]); 
					this->vCC.push_back(CopyCC);
				}
			}
			
			//delete the objects on level_k
			for(i=0; i<iSizeLevelk; i++){
				delete level_k[i];
				level_k[i] = NULL;
			}
			level_k.clear();
			iSizeLevelk = 0;
			continue;
		}

		
		//I keep a vector with bits; number of bits = number entries on this level
		//bit will be 1 if that entry is "suspected" to be maximal

		int iNoCellLevelk = iSizeLevelk / (8*sizeof(unsigned int)) + 1;
		unsigned int* vBitLevelk = new unsigned int[iNoCellLevelk];
		assert(vBitLevelk != NULL);
		//at the begining, everybody is suspect
		for(i=0; i<iNoCellLevelk; i++)
			vBitLevelk[i] = ~0;

		for(i=0; i<iSizeLevelk; i++){

			ClusterCore* cand_i = level_k[i];
			int* elems_i = cand_i->GetElems();


			for(j=i+1; j<iSizeLevelk; j++){

				ClusterCore* cand_j = level_k[j];
				int* elems_j = cand_j->GetElems();

				int bValidForCombination = 1;
				for(index=0; index<k-2 && bValidForCombination; index++){
					if (elems_i[index] != elems_j[index])
						bValidForCombination = 0;
				}
				//this is dead code; the condition is all the time false
				if (elems_i[k-2] >= elems_j[k-2])
					bValidForCombination = 0;

				int dimID1 = this->vIntervals[elems_i[k-2]]->GetDimID();
				int dimID2 = this->vIntervals[elems_j[k-2]]->GetDimID();
				if (dimID1 == dimID2)
					bValidForCombination = 0;

				if (!bValidForCombination)
					continue;


				//in v, I keep the new combination
				vector<int> v;
				v.reserve(k);
				for(index=0; index<k-2; index++)
					v.push_back(elems_i[index]);
				v.push_back(elems_i[k-2]);
				v.push_back(elems_j[k-2]);

				//create a new combination
				ClusterCore* cand_cc = new ClusterCore(v);
				int iAS = this->GetActualSupportIntersectionIntervals(v, obj_CR);
				cand_cc->SetActualSupport(iAS);


				//check if all subsets of length (k-1) of v are in level_k
				//if a subset of length (k-1) does exist in level_k, then check the Poisson condition
				int bAll = 1;
				for(index=k-1; index>=0 && bAll; index--){

					//generate a subset of length (k-1)
					//because I start with index := k-1; first I will generate cand_i and cand_j
					int* elems_temp = new int[k-1];
					int elem_temp_index = 0;
					for(index2=0; index2<k; index2++){
						if (index2 != index)
							elems_temp[elem_temp_index++] = v[index2];
					}

					//check if the generated subset exists in level_k
					int bExist = 0;
					int posExist = -1; //if the subset exists, then it exists at position posExist
					for(index2=0; index2<iSizeLevelk && !bExist; index2++){
						assert(level_k[index2]->GetNoElems() == k-1);
						int* elems_index2 = level_k[index2]->GetElems();
						bExist = AreVectorsEqual(elems_temp,k-1,elems_index2,k-1);
						if (bExist)
							posExist = index2;
					}
					delete []elems_temp;
					elems_temp = NULL;

					if (!bExist){ 
						//if a subset of length (k-1) does not exist on level_k, then cand_cc won't be generated
						//so there is no need to verify other subsets of length (k-1)
						bAll = 0;
					}
					else{
						assert(posExist != -1);
						//a subset of length (k-1) exists, so let's verify the Poisson cond

						int bSatisfyPoisson = 0;

						int iSupportParent = level_k[posExist]->GetActualSupport();
						int iSupportTarget = cand_cc->GetActualSupport();

						int dimID_interval = this->vIntervals[v[index]]->GetDimID();
						vector<int> vIDBins = this->vIntervals[v[index]]->GetIDBins();
						int iNoTotalBins = vAllHistograms[dimID_interval]->GetNoBins();
						double dWidthInterval = (double)vIDBins.size() / (double) iNoTotalBins;
						
						//this is the older version based on the Poisson distribution
						//double dDependentExpectedSupport = (double)iSupportParent * dWidthInterval;
						//if ((iSupportTarget > dDependentExpectedSupport) && (Poisson(iSupportTarget, dDependentExpectedSupport) < obj_CR->dPoissonThreshold)){
						//	bSatisfyPoisson = 1;
						//}

						//March 2008: the newer version based on the Binomial distribution
						int iCriticalValueRight = GetCriticalValueBinomialRight(iSupportParent, dWidthInterval, obj_CR->dPoissonThreshold);
						if (iSupportTarget >= iCriticalValueRight){
							bSatisfyPoisson = 1;
						}


						if (!bSatisfyPoisson){
							// if the Poisson cond fails for this parent, then there is no point to check other parents
							bAll = 0; 
						}
						else{
							//if Poisson holds, the this parent cannot be suspect anymore
							UnsetBit(vBitLevelk, posExist, iNoCellLevelk, iSizeLevelk);
						}


					}//end else
				
				}//end for index

				if (!bAll){
					delete cand_cc;
				}
				else{
					candidates_next_level.push_back(cand_cc);
				}

				v.clear();

			}//end for j


		}//end for i

		//in bits_level, where I see an entry 1, I have a suspect that might be maximal
		vector<int> suspect_pos = GetOneBits(vBitLevelk, iNoCellLevelk, iSizeLevelk);

		int iNoSuspects = (int)suspect_pos.size();
		if (k>2){//i.e., ignore cluster cores on level 1
			for(i=0;i<iNoSuspects;i++){

				int bMaximal = SatisfiesMaximality(level_k[suspect_pos[i]], obj_CR);
				if (bMaximal){
					ClusterCore* CopyCC = new ClusterCore(*level_k[suspect_pos[i]]); 
					this->vCC.push_back(CopyCC);
					
				}
			}   
		}

		delete []vBitLevelk;
		vBitLevelk = NULL;

		for(i=0; i< iSizeLevelk; i++){
			delete level_k[i];
			level_k[i] = NULL;
		}
		level_k.clear();
		
		iSizeLevelk = (int)candidates_next_level.size();
		for(i=0; i<iSizeLevelk; i++){
			ClusterCore* CopyCC = new ClusterCore(*candidates_next_level[i]);
			level_k.push_back(CopyCC);
		}
		
		for(i=0; i<iSizeLevelk; i++){
			delete candidates_next_level[i];
			candidates_next_level[i] = NULL;
		};
		candidates_next_level.clear();

		time(&timeEnd);

		cout << "Level " << k << "- " << level_k.size() << " combinations - Elapsed time: " << timeEnd-timeBegin << endl;
		//this->DisplayLevel(cout, level_k);
			
	}//end while

	return SUCCESS;
}

/************ 
Checks  if 'cand_cc' is maximal; if I've found one possible extension, then it is not maximal
************/
int Round::SatisfiesMaximality(ClusterCore* cand_cc, ClusteringRounds *obj_CR){

    int bMaximal = 1;

    int i,j;
    
	int iNoIntervals = (int)this->vIntervals.size();
    Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	
	int no_elems_cc = cand_cc->GetNoElems();
    int* elems_cc = cand_cc->GetElems();

    for(j=0;j<iNoIntervals && bMaximal;j++){
        //determine whether j is one of the intervals in cand_cc
        int bExist = ExistElem(j, elems_cc, no_elems_cc);
        if (bExist)
            continue;

        //determine whether the dimension of j is one of the dimensions of the elements in cand_cc
        int bSameDim = 0;
        int dim_j = this->vIntervals[j]->GetDimID();
        for(i=0;i<no_elems_cc && !bSameDim;i++){
            int dim_i = this->vIntervals[elems_cc[i]]->GetDimID();
            if (dim_j == dim_i)
                bSameDim = 1;
        }

        if (bSameDim)
            continue;

        //verify the condition for extensability
        vector<int> temp;
        for(i=0;i<no_elems_cc;i++)
            temp.push_back(elems_cc[i]);
        temp.push_back(j);

        ClusterCore *child = new ClusterCore(temp);
		int iChildAS = this->GetActualSupportIntersectionIntervals(temp, obj_CR); 
		child->SetActualSupport(iChildAS);
        
		int dimID_interval = this->vIntervals[j]->GetDimID();
		vector<int> vIDBins = this->vIntervals[j]->GetIDBins();
		int iNoTotalBins = vAllHistograms[dimID_interval]->GetNoBins();
		double dWidthInterval = (double)vIDBins.size() / (double) iNoTotalBins;
		
		//this is the older version based on the Poisson distribution
		//double dDependentExpectedSupport = cand_cc->GetActualSupport() * dWidthInterval;
        //if ((child->GetActualSupport() > dDependentExpectedSupport) && (Poisson(child->GetActualSupport(),dDependentExpectedSupport) < obj_CR->dPoissonThreshold)){
        //   bMaximal = 0;
		//}

		//March 2008: the newer version based on the Binomial distribution
		int iCriticalValueRight = GetCriticalValueBinomialRight(cand_cc->GetActualSupport(), dWidthInterval, obj_CR->dPoissonThreshold);
		if (child->GetActualSupport() >= iCriticalValueRight){
			bMaximal = 0;
		}


        delete child;
		child = NULL;
        temp.clear();

    
    }//end for j
    
    return bMaximal;

}//end function

/************ 
Computes the actual support of the intersection of the intervals in vector 'v'
************/
int Round::GetActualSupportIntersectionIntervals(vector<int> &v, ClusteringRounds *obj_CR){

	int iAS = 0;

	int i;
	int iNoIntervals = (int)v.size();
	if (iNoIntervals == 0)
		return iAS;

	Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;

	unsigned int* vIntersection = new unsigned int[iNoCellsObjects];
	assert(vIntersection != NULL);
	for(i=0;i<iNoCellsObjects;i++)
		vIntersection[i] = ~0;

	for(i=0; i<iNoIntervals;i++){ //for each interval in 'v'
		
		int iIntervalID = v[i];

		unsigned int* vMembership = new unsigned int[iNoCellsObjects];
		assert(vMembership != NULL);
		for(int i=0;i<iNoCellsObjects;i++)
			vMembership[i] = 0;

		this->GetMembershipInterval(vMembership, iIntervalID, obj_CR);

		IntersectionArrayBits(vIntersection, vIntersection, vMembership, iNoCellsObjects);

		delete []vMembership;
		vMembership = NULL;

	}
	
	iAS = GetNumberOneBits(vIntersection, iNoCellsObjects, iNoTotalObjects);

	delete []vIntersection;
	vIntersection = NULL;

	return iAS;
}

/************ 
Computes the revelant attributes for the found cluster cores
************/
void Round::ComputeRelevantAttributes(ClusteringRounds *obj_CR){

	int iNoCC = (int)this->vCC.size();

	for(int j=0; j<iNoCC; j++){//for each cluster core
		
		ClusterCore* current = this->vCC[j];
		int current_no_elems = current->GetNoElems();
		int* current_elems = current->GetElems();

		//a cluster core is a cross-product of intervals
		vector<int> vTemp;
		vTemp.reserve(current_no_elems);
		for(int index=0; index<current_no_elems; index++){
			
			Interval *obj_Interval = this->vIntervals[current_elems[index]];
			assert(obj_Interval != NULL);
			
			int iDimID = obj_Interval->GetDimID(); //the dimension of the interval
			
			vTemp.push_back(iDimID);
		}
		
		this->vAllRelevantDimensions.push_back(vTemp);
	}

}


/************ 
Computes the membership matrix given a set of CCs
************/	
void Round::ComputeMembership(ClusteringRounds *obj_CR){
	
	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;
	int iNoCC = (int)this->vCC.size();

	Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);

	this->MembershipMatrix = new MatrixGeneric<unsigned int>(iNoTotalObjects, iNoCC);
	assert(this->MembershipMatrix != NULL);	

	int i,j,index;

	//each CC is represented in a space given by all pairs (attribute_id, bin_id) 
	//that appear in the definition of any CC

	//detect the pairs (attribute_id, bin_id) that appear in the definition of any CC
	vector<struct DimIDBinID *> vAttributeBinPairs;
	for(j=0; j<iNoCC; j++){//for each cluster core
	
		ClusterCore* current = this->vCC[j];
		int current_no_elems = current->GetNoElems();
		int* current_elems = current->GetElems();
		
		//a cluster core is a cross-product of intervals
		for(index=0; index<current_no_elems; index++){
				
			Interval *obj_Interval = this->vIntervals[current_elems[index]];
			assert(obj_Interval != NULL);
			
			int iDimID = obj_Interval->GetDimID(); //the dimension of the interval
			
			vector<int> vIDBins = obj_Interval->GetIDBins();
			int iNoBins = (int)vIDBins.size();

			for(i=0; i<iNoBins; i++){ //for each bin in the interval
				
				struct DimIDBinID *temp = new struct DimIDBinID;
				temp->iDimID = iDimID;
				temp->iBinID = vIDBins[i];
				vAttributeBinPairs.push_back(temp);
			}
			
			vIDBins.clear();
		}
	}
	
	int iNoAttributeBinPairs = (int)vAttributeBinPairs.size();

	//create for each CC a representation in this space
	MatrixGeneric<double> *CCRepresentation = new MatrixGeneric<double>(iNoCC, iNoAttributeBinPairs);
	assert(CCRepresentation != NULL);

	for(j=0; j<iNoCC; j++){//for each cluster core
		
		//find out the objects that belong to cluster core j
		unsigned int* vObjectsCC = new unsigned int[iNoCellsObjects];
		assert(vObjectsCC != NULL);
		for(i=0;i<iNoCellsObjects;i++)
			vObjectsCC[i] = ~0;

		ClusterCore* current = this->vCC[j];
		int current_no_elems = current->GetNoElems();
		int* current_elems = current->GetElems();	
		
		//a cluster core is a cross-product of intervals
		//vObjectsCC is the intersection of its intervals
		for(index=0; index<current_no_elems; index++){
				
			unsigned int* vMembership = new unsigned int[iNoCellsObjects];
			assert(vMembership != NULL);
			for(i=0;i<iNoCellsObjects;i++)
				vMembership[i] = 0;

			this->GetMembershipInterval(vMembership, current_elems[index], obj_CR);
			IntersectionArrayBits(vObjectsCC, vObjectsCC, vMembership, iNoCellsObjects);

			delete []vMembership;
			vMembership = NULL;
	
		}//end for index
		
		for(index=0; index<iNoAttributeBinPairs; index++){ // for each (attribute_id, bin_id) pair
		
			//find out the objects that belong to bin 'bin_id' on attribute 'attribute_id'	
			int iDimID = vAttributeBinPairs[index]->iDimID;
			int iBinID = vAttributeBinPairs[index]->iBinID;

			Bin *obj_Bin = vAllHistograms[iDimID]->GetBin(iBinID);
			assert(obj_Bin != NULL);

			//determine the number of points in the CC that also belong to bin 'bin_id' on attribute 'attribute_id'
			unsigned int* vIntersection = new unsigned int[iNoCellsObjects];
			assert(vIntersection != NULL);
			for(i=0;i<iNoCellsObjects;i++)
				vIntersection[i] = ~0;

			IntersectionArrayBits(vIntersection, vIntersection, vObjectsCC, iNoCellsObjects);
			IntersectionArrayBits(vIntersection, vIntersection, obj_Bin->GetObjects(), iNoCellsObjects);

			int iSupportIntersection= GetNumberOneBits(vIntersection, iNoCellsObjects, iNoTotalObjects);
			int iSupportCC = GetNumberOneBits(vObjectsCC, iNoCellsObjects, iNoTotalObjects);
			int iSupportBin = GetNumberOneBits(obj_Bin->GetObjects(), iNoCellsObjects, iNoTotalObjects);

			double dVal = ((double)iSupportIntersection / (double) iSupportCC) * ((double)iNoTotalObjects / (double)iSupportBin);
			CCRepresentation->SetElemAt(j,index, dVal);	

			delete []vIntersection;
			vIntersection = NULL;

			//determine the number of data points that also belong to bin 'bin_id' on attribute 'attribute_id'
		}
				
		delete []vObjectsCC;
		vObjectsCC = NULL;
	}

	//cout << *CCRepresentation;

	// each data point is represented in the same space
	// an entry is 1 if the data point belongs to bin 'bin_id' on attribute 'attribute_id'
	// else the entry is 0
	MatrixGeneric<double> *SimMatrix = new MatrixGeneric<double>(iNoTotalObjects, iNoCC);
	assert(SimMatrix != NULL);

	for(i=0; i<iNoTotalObjects; i++){ //for each data object
		for(j=0; j<iNoCC; j++){//for each cluster core
			
			double dDotProduct = 0;
			for(index=0; index<iNoAttributeBinPairs; index++){ // for each (attribute_id, bin_id) pair
				
				int iDimID = vAttributeBinPairs[index]->iDimID;
				int iBinID = vAttributeBinPairs[index]->iBinID;

				Bin *obj_Bin = vAllHistograms[iDimID]->GetBin(iBinID);
				assert(obj_Bin != NULL);

				int bExist = GetBit(obj_Bin->GetObjects(), i, iNoCellsObjects, iNoTotalObjects);
				if (bExist == 1)
					dDotProduct += CCRepresentation->GetElemAt(j,index);
			}

			SimMatrix->SetElemAt(i,j,dDotProduct);
		}
	}

	//normalize SimMatrix on the rows with the sum of a row
	/*
	for(i=0; i<iNoTotalObjects; i++){ //for each data object
		double dSumRow = 0;
		for(j=0; j<iNoCC; j++){//for each cluster core
			dSumRow += SimMatrix->GetElemAt(i,j);
		}
		if (dSumRow != 0){
			for(j=0; j<iNoCC; j++){
				double dOldValue = SimMatrix->GetElemAt(i,j);
				//double dNewValue = dOldValue/ dSumRow;
				SimMatrix->SetElemAt(i,j, dOldValue/ dSumRow);
			}
		}
		else{
			for(j=0; j<iNoCC; j++)
				SimMatrix->SetElemAt(i,j, 0);
		}	
	}
	*/

	//normalize SimMatrix on the rows with min-max normalization
	for(i=0; i<iNoTotalObjects; i++){ //for each data object
		double dMinRow = RAND_MAX;
		double dMaxRow = -1;
		for(j=0; j<iNoCC; j++){//for each cluster core
			if (SimMatrix->GetElemAt(i,j) < dMinRow){
				dMinRow = SimMatrix->GetElemAt(i,j);
			}
			if (SimMatrix->GetElemAt(i,j) > dMaxRow){
				dMaxRow = SimMatrix->GetElemAt(i,j);
			}
		}

		if ((dMaxRow - dMinRow) != 0){
			for(j=0; j<iNoCC; j++){
				double dOldValue = SimMatrix->GetElemAt(i,j);
				double dNewValue = (dOldValue - dMinRow)/ (dMaxRow - dMinRow);
				SimMatrix->SetElemAt(i,j, dNewValue);
			}
		}
		else{
			if (dMaxRow == 0){
				for(j=0; j<iNoCC; j++)
					SimMatrix->SetElemAt(i,j, 0);
			}
			else{
				for(j=0; j<iNoCC; j++)
					SimMatrix->SetElemAt(i,j, 1);
			}
		}
	}

	//produce the membership matrix
	if (obj_CR->bDisjoint == 1){
		//dijoint clusters are desired
		for(i=0; i<iNoTotalObjects; i++){ //for each data object
			double dMaxVal = -1;
			int iPos = -1;
			for(j=0; j<iNoCC; j++){//for each cluster core
				if (SimMatrix->GetElemAt(i,j) > dMaxVal){
					dMaxVal = SimMatrix->GetElemAt(i,j);
					iPos = j;
				}
			}
			assert(iPos != -1);
			if (dMaxVal != 0)
				this->MembershipMatrix->SetElemAt(i,iPos,1);
		}
	}

	if (obj_CR->bDisjoint == 0){
		//overlaping clusters are desired
		//double dThreshold = (double)1.0 / (double)iNoCC;
		double dThreshold = 0.75;
		for(i=0; i<iNoTotalObjects; i++){ //for each data object
			for(j=0; j<iNoCC; j++){//for each cluster core
				if (SimMatrix->GetElemAt(i,j) > dThreshold)
					this->MembershipMatrix->SetElemAt(i,j,1);
			}
		}
	}

	//clean up
	delete SimMatrix;
	SimMatrix = NULL;
	
	delete CCRepresentation;
	CCRepresentation = NULL;

	for(i=0; i<iNoAttributeBinPairs; i++){
		delete vAttributeBinPairs[i];
		vAttributeBinPairs[i] = NULL;
	}
	vAttributeBinPairs.clear();
}


//////////////////////////////////////////////	DISPLAY FUNCTIONS //////////////////////////////////////////////

/************ 
Display function
************/
void Round::DisplayIntervals(ostream &os, ClusteringRounds* obj_CR){
	
	Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	
	int iLengthVector = (int)this->vIntervals.size();
	for(int i=0;i<iLengthVector;i++){
		
		Interval* obj_Interval = this->vIntervals[i];
		assert(obj_Interval != NULL);
		
		int iDimID = obj_Interval->GetDimID();
		vector<int> vIDBins = obj_Interval->GetIDBins();
		int iNoBins = (int)vIDBins.size();
				
		os << "Attribute " << iDimID + 1 << ":" << endl;
		/*
		os << "Labels: ";
		for(int index=0; index<iNoBins;index++){
			Bin *obj_Bin = vAllHistograms[iDimID]->GetBin(vIDBins[index]);
			assert(obj_Bin != NULL);
			os << static_cast<CategoricalBin*>(obj_Bin)->GetCategoricalItem() << " ";
		} 
		*/
		os << "IDs: ";
		for(int index=0; index<iNoBins;index++){
			os << vIDBins[index] << " ";
		}
		os << endl;
		os << "Support = " << this->GetActualSupportInterval(i, obj_CR) << endl;
		os << endl;
	}
}

/************ 
Display function
************/
void Round::DisplayVector(ostream& os, vector<vector<int> >& v){

	int iLength = (int)v.size();
	for(int i=0; i< iLength; i++){
		for(int j=0; j<(int)v[i].size(); j++)
			os << v[i][j] << " ";
		os << endl;
	}
}

/************ 
Display function
************/
void Round::DisplayClusterCores(ostream &os, ClusteringRounds *obj_CR){

	os << "CLUSTER CORES:" << endl;
	int i, index;
	int iNoCC = (int)this->vCC.size();
	for(i=0; i<iNoCC; i++){
        
        ClusterCore *current = this->vCC[i];
        int current_no_elems = current->GetNoElems();
        int* current_elems = current->GetElems();

        os << "(";
        /*
		for(index=0; index<current_no_elems; index++){
            if (index != current_no_elems - 1)
				os << current_elems[index] + 1 << " ";
			else
				os << current_elems[index] + 1;
        }
        os << ") - (";
		*/
        for(index=0;index<current_no_elems;index++){
            if (index != current_no_elems - 1)
				os << this->vIntervals[current_elems[index]]->GetDimID() + 1 << " ";
			else
				os << this->vIntervals[current_elems[index]]->GetDimID() + 1;
        }
        os << ")" << endl;
    }

}

/************ 
Display function
************/
void Round::DisplayLevel(ostream& os, vector<ClusterCore*> &v){

	int iLengthVector = (int)v.size();
	for(int i=0; i<iLengthVector; i++){
		
		ClusterCore *current = v[i];
        int current_no_elems = current->GetNoElems();
        int* current_elems = current->GetElems();
		
		os << "(";
		for(int index=0; index<current_no_elems; index++){
            if (index != current_no_elems - 1)
				os << current_elems[index] + 1 << " ";
			else
				os << current_elems[index] + 1;
        }
		os << ") - AS = " << v[i]->GetActualSupport() << endl;

	}
}

////////////////////////////////////		OLD CODE	//////////////////////////////////


/************ 
Computes the membership matrix in an iterative way
************/
/*
void Round::ComputeIterativelyMembershipMatrix(ClusteringRounds *obj_CR){

	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCC = (int)this->vCC.size();
	this->MembershipMatrix = new MatrixGeneric<unsigned int>(iNoTotalObjects, iNoCC);
	assert(this->MembershipMatrix != NULL);	

	int bContinue = 1;
	int iNoIter = 0;

	while(bContinue){
	
		iNoIter++;
		cout << "Iteration " << iNoIter << endl;
		DisplayIntervals(cout, obj_CR);

		//compute the membership given the current set of CCs
		ComputeMembership(obj_CR);

		//refine the current set of CC
		bContinue = 0;
		RefineCC(obj_CR, bContinue);

		cout << endl;
				
	}//end while
	cout << "Refinement converges in " << iNoIter << " iterations" << endl;

}
*/

/************ 
Computes the membership matrix given a set of CCs
************/	
/*
void Round::ComputeMembership(ClusteringRounds *obj_CR){
	
	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;
	int iNoCC = (int)this->vCC.size();

	int i,j,index;

	//cancel the previous membership info
	for(i=0; i<iNoTotalObjects; i++){
		for(j=0; j<iNoCC; j++){
			this->MembershipMatrix->SetElemAt(i,j,0);
		}
	}

	for(j=0; j<iNoCC; j++){//for each final cluster core
	
		//find out the objects that belong to cluster core j
		unsigned int* vObjectsCC = new unsigned int[iNoCellsObjects];
		assert(vObjectsCC != NULL);
		for(i=0;i<iNoCellsObjects;i++)
			vObjectsCC[i] = ~0;

		ClusterCore* current = this->vCC[j];
		int current_no_elems = current->GetNoElems();
		int* current_elems = current->GetElems();	
		
		//a cluster core is a cross-product of intervals
		//vObjectsCC is the intersection of its intervals
		for(index=0; index<current_no_elems; index++){
				
			unsigned int* vMembership = new unsigned int[iNoCellsObjects];
			assert(vMembership != NULL);
			for(i=0;i<iNoCellsObjects;i++)
				vMembership[i] = 0;

			this->GetMembershipInterval(vMembership, current_elems[index], obj_CR);
			IntersectionArrayBits(vObjectsCC, vObjectsCC, vMembership, iNoCellsObjects);

			delete []vMembership;
			vMembership = NULL;
	
		}//end for index
		
		// set-up simply the membership matrix
		vector<int> vTemp = GetOneBits(vObjectsCC, iNoCellsObjects, iNoTotalObjects);
		int iLenghtVector = (int)vTemp.size();
		for(index=0; index<iLenghtVector;index++)
			this->MembershipMatrix->SetElemAt(vTemp[index], j, 1);
		vTemp.clear();
		
		delete []vObjectsCC;
		vObjectsCC = NULL;
	}
}
*/

/************ 
Refine the current set of CC
************/
/*
void Round::RefineCC(ClusteringRounds* obj_CR, int &bContinue){

	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;
	int iNoCC = (int)this->vCC.size();

	Histogram** vAllHistograms = obj_CR->obj_CategoricalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);

	vector<Interval*> vNewIntervals;

	int i,j,index,index2;

	for(j=0; j<iNoCC; j++){//for each final cluster core
	
		//cout << "Cluster core " << (j+1) << ":" << endl;
		
		//determine the objects that belong to this CC
		unsigned int* vObjectsCC = new unsigned int[iNoCellsObjects];
		assert(vObjectsCC != NULL);
		for(i=0;i<iNoCellsObjects;i++)
			vObjectsCC[i] = 0;
		
		for(i=0; i<iNoTotalObjects; i++){
			if (this->MembershipMatrix->GetElemAt(i,j) == 1)
				SetBit(vObjectsCC, i, iNoCellsObjects, iNoTotalObjects);
		}
		int iNoObjCC = GetNumberOneBits(vObjectsCC, iNoCellsObjects, iNoTotalObjects); // number of objects in this CC

		ClusterCore* current = this->vCC[j];
		int current_no_elems = current->GetNoElems();
		int* current_elems = current->GetElems();

		vector<int> vNewElemsCC; // the IDs of the new intervals in CC
		
		//a CC is a cross-product of intervals
		for(index=0; index<current_no_elems; index++){ //for each interval in CC j
		
			Interval *obj_Interval = this->vIntervals[current_elems[index]];
			assert(obj_Interval != NULL);

			int iDimID = obj_Interval->GetDimID(); //the dimension of the interval
			vector<int> vIDBins = obj_Interval->GetIDBins(); //the bins of this interval
			int iNoBinsInterval = obj_Interval->GetNoBins(); //the number of bins in the interval
			int iNoTotalBins = vAllHistograms[iDimID]->GetNoBins(); //the total number of bins on this attribute
			//cout << "Attribute " << iDimID + 1 << endl;

			//I will have to create a new interval; it may be the old one, or it may be a different one
			Interval* obj_NewInterval = new Interval(iDimID);

			//take into account all bins that exist on this attribute
			for(index2=0; index2<iNoTotalBins; index2++){
				CategoricalBin* obj_Bin =  static_cast<CategoricalBin*>(vAllHistograms[iDimID]->GetBin(index2));
				assert(obj_Bin != NULL);

				unsigned int* vObjectsBin = obj_Bin->GetObjects();

				unsigned int* vIntersection = new unsigned int[iNoCellsObjects];
				assert(vIntersection != NULL);
				for(i=0; i<iNoCellsObjects; i++)
					vIntersection[i] = ~0;	

				IntersectionArrayBits(vIntersection, vIntersection, vObjectsCC, iNoCellsObjects);
				IntersectionArrayBits(vIntersection, vIntersection, vObjectsBin, iNoCellsObjects);

				int iNoObjIntersection = GetNumberOneBits(vIntersection, iNoCellsObjects, iNoTotalObjects);
				//expected number of CC points in a bin if the Interval will truely represent a cluster signature
				double dExpectedBin = (double)iNoObjCC / (double)iNoBinsInterval; //expected number of CC points in a bin if the Interval will truely represent a cluster signature
				//cout << "Label " << obj_Bin->GetCategoricalItem() << " : " << "Actual = " << iNoObjIntersection << " Expected = " << dExpectedBin << endl;

				int bExclude = (iNoObjIntersection < dExpectedBin) && (Poisson(iNoObjIntersection, dExpectedBin) < obj_CR->dPoissonThreshold);
				
				if (!bExclude){
					//bin with ID 'vIDBins[index2]' should not be part of the obj_NewInterval
					obj_NewInterval->AddBin(index2);
				}

				delete []vIntersection;
				vIntersection = NULL;

			}//end for index2

			vector<int> vIDBinsNewInterval = obj_NewInterval->GetIDBins();
			if (AreVectorsEqual(vIDBins, vIDBinsNewInterval) == 0)
				bContinue = 1;

			int iNoBinsNewInterval = obj_NewInterval->GetNoBins();
			if (iNoBinsNewInterval != 0){ // might have kicked out all bins, and obtained an empty interval
				vNewIntervals.push_back(obj_NewInterval);	
				int iContor = (int)vNewIntervals.size();
				vNewElemsCC.push_back(iContor - 1);
			}
		
		}//end for index

		//reconstruct the CC
		delete this->vCC[j];
		this->vCC[j] = NULL;
		if (!vNewElemsCC.empty())
			this->vCC[j] = new ClusterCore(vNewElemsCC);

		vNewElemsCC.clear();

		delete []vObjectsCC;
		vObjectsCC = NULL;

	}//end for j

	//update the current set of intervals
	int iLengthVector = (int)this->vIntervals.size();
	for(i=0; i<iLengthVector;i++){
		delete this->vIntervals[i];
		this->vIntervals[i] = NULL;
	}
	this->vIntervals.clear();

	iLengthVector = (int)vNewIntervals.size();
	for(i=0; i<iLengthVector;i++){
		this->vIntervals.push_back(vNewIntervals[i]);
	}

	//clean up vNewIntervals
	//for(i=0; i<iLengthVector;i++){
	//	delete vNewIntervals[i];
	//	vNewIntervals[i] = NULL;
	//}
	//vNewIntervals.clear();
}
*/


