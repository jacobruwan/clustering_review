#ifndef ROUND_H
#define ROUND_H

#include "ClusteringRounds.h"
#include "Interval.h"
#include "Utile.h"
#include "MatrixGeneric.h"
#include "ClusterCore.h"

struct DimIDBinID{
    int iDimID;
    int iBinID;
};

class ClusteringRounds;

class Round{

	vector<Interval*> vIntervals;

	void MarkDenseBins(ClusteringRounds* obj_CR);

	vector<ClusterCore*> vCC;

	MatrixGeneric<unsigned int> *MembershipMatrix;
	vector<vector<int> > vAllRelevantDimensions;
	
	void ComputeIntervals(ClusteringRounds* obj_CR);
	int ComputeNoCommonElem(vector<int> &v1, vector<int> &v2);
	void ComputeIntersectionVectors(vector<int> &inter, vector<int> &a, vector<int> &b);
	void BKAlgorithm(vector<vector<int> >& vMaximalCliques, MatrixGeneric<int>* AdjacencyMatrix);
	void Expand(vector<int>& clique, vector<int> &cand, vector<int> &processed, vector<vector<int> >& vMaximalCliques, MatrixGeneric<int>* AdjacencyMatrix);
	void GetMembershipInterval(unsigned int* vMembership, int iIntervalID, ClusteringRounds *obj_CR);
	int GetActualSupportInterval(int iIntervalID, ClusteringRounds* obj_CR);
	void ConnectedComponentsAlgorithm(vector<vector<int> >& v, MatrixGeneric<int>* AdjacencyMatrix);
	void DisplayIntervals(ostream &os, ClusteringRounds* obj_CR);
	void DisplayVector(ostream& os, vector<vector<int> >& v);
	void DisplayLevel(ostream& os, vector<ClusterCore*> &v);
	
	int GenerateClusterCores(ClusteringRounds* obj_CR);
	int GetActualSupportIntersectionIntervals(vector<int> &v, ClusteringRounds* obj_CR);
	int SatisfiesMaximality(ClusterCore* cand_cc, ClusteringRounds *obj_CR);
	void DisplayClusterCores(ostream &os, ClusteringRounds* obj_CR);

	void ComputeRelevantAttributes(ClusteringRounds *obj_CR);
	void ComputeMembership(ClusteringRounds* obj_CR);
	
public:
	Round();
	~Round();
	
	MatrixGeneric<unsigned int> * GetMembershipMatrix();
	vector<vector<int> >& GetAllRelevantDimensions();


	int DoRound(ClusteringRounds* obj_CR); //class ClusteringRounds is a friend of class Round;
										   //thus, class Round has access to private/protected members 
										   //of Clustering Rounds

};

#endif

