#include "string.h"
#include "ClusteringRounds.h"


void usage(char* sNameProgram);

/************ 
MAIN function
************/
int main(int argc, char* argv[]){
   
    if (argc!=5){
        usage(argv[0]);
        return FAILURE;
    }

    ClusteringRounds *obj_CR = new ClusteringRounds(argv[1],argv[2],atof(argv[3]),atoi(argv[4]));



    if (obj_CR->Run() == FAILURE){
      delete obj_CR;
	  return FAILURE;
    }

    delete obj_CR;

    return SUCCESS;
    
}

/************ 
Usage function
************/
void usage(char* sNameProgram){

    cerr << "Syntax for this program is: " << endl;
    cerr << "\tname_program name_input_file name_output_file Poisson_threshold 1|2" << endl;
    cerr << "where:" << endl;
    cerr << "\tname_program is " << sNameProgram << endl;
    cerr << "\tname_input_file is the name of the file containing the raw data" << endl;;
    cerr << "\tname_output_file is the name of the file containing the clustered data" << endl;
	cerr << "\tPoisson_threshold is the value of the parameter Poisson_threshold" << endl;
	cerr << "\t1, if we want RPCC to compute disjoint clusters; 2, if we want RPCC to compute overlapping clusters" << endl;

}


