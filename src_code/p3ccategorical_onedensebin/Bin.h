#ifndef BIN_H
#define BIN_H

#include "Utile.h"

#include <assert.h>

#include <iostream>
using namespace std;

class Bin{

protected:
	
	int iNoTotalObjects; //stores the total number of objects in the database
	int iNoCellsObjects; //stores the number of unsigned int necessary to represent the total number of objects on bits

	int iDimID;

	int bDense;

	unsigned int* vObjects; //for each object in the database, keep a bit equal to 1, if the object belongs
                            //to this bin, and equal to 0, otherwise

public:

	Bin();
	Bin(int no_total_objects, int dim_id);
    virtual ~Bin();

	int GetDimID() const;
	
	void SetBinDense(int flag);
    int IsBinDense () const;

	unsigned int* GetObjects();
	
	int IsMemberObject(int object_id);
	
	void AddObject(int object_id);
	void RemoveObject(int object_id);
	
	int GetActualSupport();	

	virtual void Display(ostream& os);

};

#endif

