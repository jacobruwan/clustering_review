#include "CategoricalHistogram.h"

/************
Constructor(s) and destructor
************/
//default constructor
CategoricalHistogram::CategoricalHistogram(){

}

//constructor
CategoricalHistogram::CategoricalHistogram(int no_bins, int dim_id, map<string,int,less<string> > &m)
:Histogram(no_bins, dim_id)
{
	this->mapCategoriesIdentifiers.clear();
	assert(this->mapCategoriesIdentifiers.empty());

	map<string,int,less<string> >::iterator it_map;
	for(it_map = m.begin(); it_map != m.end(); it_map++){
		this->mapCategoriesIdentifiers[it_map->first] = it_map->second;
	}

	//this->Display(cout);
}

//destructor
CategoricalHistogram::~CategoricalHistogram(){

	//cout << "Dtor CategoricalHistogram" << endl;
	this->mapCategoriesIdentifiers.clear();

	if (this->vMembers != NULL){
		for(int i=0;i<this->iNoBins;i++){
			delete this->vMembers[i];
			this->vMembers[i] = NULL;
		}
		delete []this->vMembers;
		this->vMembers = NULL;
	}
}

/************
Functions for manipulating private members of the class
************/
map<string,int,less<string> >& CategoricalHistogram::GetMapCategoriesIdentifiers(){
	
	return this->mapCategoriesIdentifiers;
}

/************
Displays this->mapCategoriesIdentifiers for debugging purposes
************/
void CategoricalHistogram::Display(ostream &os){
		
	map<string,int,less<string> >::iterator it_map;

	os << "Label" << "\t" << "Identifier:" << endl;
	for(it_map = this->mapCategoriesIdentifiers.begin(); it_map != this->mapCategoriesIdentifiers.end(); it_map++)
		os << it_map->first << "\t" << it_map->second << endl;

}
