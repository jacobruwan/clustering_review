#ifndef CATEGORICAL_ONED_HISTOGRAMS
#define	CATEGORICAL_ONED_HISTOGRAMS

#include "OneDHistograms.h"
#include "CategoricalBin.h"
#include "CategoricalHistogram.h"
#include "StringMatrix.h"
#include "Utile.h"

class CategoricalOneDHistograms: public OneDHistograms{

	
public:
	CategoricalOneDHistograms();
	CategoricalOneDHistograms(StringMatrix* SM);
	~CategoricalOneDHistograms();

	void BuildAllHistograms();	

	void Display(ostream& os);
};

#endif
