#ifndef CLUSTERINGROUNDS_H
#define CLUSTERINGROUNDS_H

#include "Utile.h"
#include "StringMatrix.h"
#include "CategoricalOneDHistograms.h"
#include "Round.h"
#include "MatrixGeneric.h"

#include <iostream>
#include<fstream>
using namespace std;

#include <time.h>

class ClusteringRounds {
       
    char* sInputFile;
    char* sOutputFile;
	double dPoissonThreshold;
	int bDisjoint;
	    
	StringMatrix *DataMatrix;
	MatrixGeneric<double>* CriticalValues;

	int bClusteringRounds;
	int iNoIter;

	CategoricalOneDHistograms* obj_CategoricalOneDHistograms;

	int ReadDataMatrix();
	int WriteOutputForEM(MatrixGeneric<unsigned int>* M, vector<vector<int> > &R);
	int WriteOutputSimple(MatrixGeneric<unsigned int>* M, vector<vector<int> > &R);	

	int ReadCriticalValuesMatrix();

	void BuildAllHistograms();

public:
    ClusteringRounds();
	ClusteringRounds(char* sInputFile, char* sOutputFile, double dPoissonThreshold, double iDisjoint);
    ~ClusteringRounds();

    int Run();

	friend class Round;
};

#endif




