#ifndef ONED_HISTOGRAMS
#define	ONED_HISTOGRAMS

#include "Histogram.h"
#include "StringMatrix.h"

class OneDHistograms{
	
protected:
	StringMatrix* DataMatrix;
	Histogram** vAllHistograms;
	int iNoHistograms;
    
public:
	OneDHistograms();
	OneDHistograms(StringMatrix* SM);
	virtual ~OneDHistograms();

	Histogram** GetAllHistograms();
};

#endif
