#include "CategoricalOneDHistograms.h"

/************
Constructor(s) and destructor
************/
//default constructor
CategoricalOneDHistograms::CategoricalOneDHistograms(){
	
}

//constructor
CategoricalOneDHistograms::CategoricalOneDHistograms(StringMatrix *SM)
:OneDHistograms(SM)
{
	
}

//destructor
CategoricalOneDHistograms::~CategoricalOneDHistograms(){
	
	//cout << "Dtor CategoricalOneDHistograms" << endl;
	if (this->vAllHistograms != NULL){
		for(int i=0;i<this->iNoHistograms;i++){
			delete this->vAllHistograms[i];
			this->vAllHistograms[i] = NULL;
		}
		delete []this->vAllHistograms;
		this->vAllHistograms = NULL;
	}
}

/************
Builds a histogram for each attribute and stores it into vAllHistograms
************/
void CategoricalOneDHistograms::BuildAllHistograms(){

	int iNoTotalObjects = this->DataMatrix->GetNoRows();
	int iNoTotalAttributes = this->DataMatrix->GetNoCols() - 1;

	int i,j,index;

	//initialize the histograms
	this->vAllHistograms = new Histogram*[iNoTotalAttributes];
	assert(this->vAllHistograms != NULL);

	map<string,int,less<string> >::iterator it_map;
	for(j=0;j<iNoTotalAttributes;j++){
		
		//determine the categories on attribute j
		map<string,int,less<string> > m;
		int indexCategory = 0;

		for(i=0;i<iNoTotalObjects;i++){
			
			string sValue = this->DataMatrix->GetElemAt(i,j);
			if (strcmp(sValue.c_str(),"?") != 0){ //missing values are ignored
				it_map = m.find(sValue);
				if (it_map == m.end()){
					m[sValue] = indexCategory;
					indexCategory++;
				}
			}

		}//end for i

		int iNoBins = (int)m.size();
		this->vAllHistograms[j] = new CategoricalHistogram(iNoBins, j, m);
		assert(this->vAllHistograms[j] != NULL);

		it_map = m.begin();
		for(index=0;index<iNoBins;index++){
			
			string sItem = it_map->first;
			int iPos = it_map->second;

			CategoricalBin* obj_Bin = new CategoricalBin(iNoTotalObjects, j, (char*)sItem.c_str(), (int)sItem.size());
			assert(obj_Bin != NULL);

			this->vAllHistograms[j]->AddBin(obj_Bin, iPos);

			it_map++;
		}
		
		m.clear();
		assert(m.empty());
		 
	}//end for j

	//populate the histograms
	for(j=0;j<iNoTotalAttributes;j++){ //for each attribute
			
		map<string,int,less<string> > m = static_cast<CategoricalHistogram*>(this->vAllHistograms[j])->GetMapCategoriesIdentifiers();

		for(i=0;i<iNoTotalObjects;i++){ //for each object
			
			string sItem = this->DataMatrix->GetElemAt(i,j);
			if (strcmp(sItem.c_str(), "?") != 0){
				it_map = m.find(sItem);
				assert(it_map != m.end());

				CategoricalBin *obj_Bin = static_cast<CategoricalBin*>(this->vAllHistograms[j]->GetBin(it_map->second));
				assert(obj_Bin != NULL);

				obj_Bin->AddObject(i);
			}

		}//end for i
	}//end for j
}


/************
Displays all histograms for debugging purposes
************/
void CategoricalOneDHistograms::Display(ostream& os){

	int iNoTotalAttributes = this->DataMatrix->GetNoCols() - 1;

	for(int j=0;j<iNoTotalAttributes;j++){
		this->vAllHistograms[j]->Display(os);
	}
}


