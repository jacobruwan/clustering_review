#include "ClusteringRounds.h"


/************ 
Constructor(s) and destructor
************/
//default constructor
ClusteringRounds::ClusteringRounds(){
	
	this->sInputFile = NULL;
	this->sOutputFile = NULL;
	this->obj_CategoricalOneDHistograms = NULL;
	this->CriticalValues = NULL;
	this->dPoissonThreshold = 0;
	this->bDisjoint = 0;
	
}

//constructor
ClusteringRounds::ClusteringRounds(char* sInputFile, char* sOutputFile, double dPoissonThreshold, double iDisjoint){
    
#ifdef WIN32
	int bFail = 0;
#endif

	this->sInputFile = new char[strlen(sInputFile)+1];
	assert(this->sInputFile != NULL);

#ifdef WIN32
	bFail = strcpy_s(this->sInputFile, strlen(sInputFile)+1, sInputFile);
	assert(bFail == 0);
#else
	strcpy(this->sInputFile, sInputFile);
#endif
    
	this->sOutputFile = new char[strlen(sOutputFile)+1];
	assert(this->sOutputFile != NULL);

#ifdef WIN32
	bFail = strcpy_s(this->sOutputFile, strlen(sOutputFile)+1, sOutputFile);
	assert(bFail == 0);
#else
    strcpy(this->sOutputFile,sOutputFile);
#endif
	
	this->dPoissonThreshold = dPoissonThreshold;

	this->DataMatrix = new StringMatrix();
	assert(this->DataMatrix != NULL);

	this->iNoIter = 0;
	this->bClusteringRounds = 1;
	if (iDisjoint == 1)
		this->bDisjoint = 1;
	else
		this->bDisjoint = 0;


	this->obj_CategoricalOneDHistograms = NULL;
}

//destructor
ClusteringRounds::~ClusteringRounds(){
    
    //cout << "Dtor ClusteringRounds" << endl;
	delete []this->sInputFile;
	this->sInputFile = NULL;
    delete []this->sOutputFile;
	this->sOutputFile = NULL;	

	delete this->DataMatrix;
	this->DataMatrix = NULL;

	delete this->CriticalValues;
	this->CriticalValues = NULL;

	delete this->obj_CategoricalOneDHistograms;
	this->obj_CategoricalOneDHistograms = NULL;
}


/************ 
Runs clustering rounds until a certain condition is satisfied 
************/

int ClusteringRounds::Run(){
    

    //read the data matrix 
    if (this->ReadDataMatrix() == FAILURE)
        return FAILURE;

	this->BuildAllHistograms();
	
	this->ReadCriticalValuesMatrix();

	time_t timeBegin;
	time_t timeEnd;
	
	time(&timeBegin);
	while(this->bClusteringRounds){
	
		this->iNoIter++;
		
		cout << "****************************" << endl;
		cout << "Round " << this->iNoIter << endl << endl;

		Round* obj_Round = new Round();
		assert(obj_Round != NULL);
		if (obj_Round->DoRound(this) == FAILURE){
			return FAILURE;
		}

		this->WriteOutputSimple(obj_Round->GetMembershipMatrix(), obj_Round->GetAllRelevantDimensions());
				
		delete obj_Round;

		this->bClusteringRounds = 0;
		
		cout << "****************************" << endl;
		
	}
	time(&timeEnd);
	cout << "\tElapsed time = " << timeEnd - timeBegin << endl << endl;

	return SUCCESS;
}

/************ 
Read the data matrix 
************/
int ClusteringRounds::ReadDataMatrix(){
	
	if (this->DataMatrix->ReadDataFile(this->sInputFile) == FAILURE){
        cout << "ClusteringRounds: Fail to read the data matrix" << endl;
		return FAILURE;
	}

	cout << "Data matrix read" << endl << endl;

	return SUCCESS;
}

/************ 
Write the output that will be fed into EM
************/
int ClusteringRounds::WriteOutputForEM(MatrixGeneric<unsigned int>* M, vector<vector<int> > &R){

	ofstream OutputFile;
	OutputFile.open(this->sOutputFile, ios::out);
    if (!OutputFile.is_open()){
        cerr << "ClusteringRounds: Cannot open the file " << this->sOutputFile << endl;
		return FAILURE;
    };
    
	OutputFile << *M;	

	OutputFile.close();

	char* tmpPtr = strstr(this->sOutputFile,"_PartialInitialization.txt");
    if (tmpPtr == NULL){
        cerr << "ClusteringRounds: WriteOutputForEM: The output file does not contain _PartialInitialization.txt" << endl;
        return FAILURE;
    }
 
#ifdef WIN32
	int bFail = 0;
#endif

	char* stub_name = new char[tmpPtr - this->sOutputFile + 1];
#ifdef WIN32
	bFail = strncpy_s(stub_name, tmpPtr - this->sOutputFile + 1, sOutputFile, tmpPtr - this->sOutputFile);
	assert(bFail == 0);
#else
	strncpy(stub_name, sOutputFile,tmpPtr - this->sOutputFile);
#endif
    stub_name[tmpPtr-sOutputFile] = '\0';

    string final_name = stub_name;
    final_name += "_RelevantDimensions.txt";
    
   	OutputFile.open(final_name.c_str(), ios::out);
    if (!OutputFile.is_open()){
        cerr << "ClusteringRounds: Cannot open the file " << final_name.c_str() << endl;
		delete []stub_name;
		stub_name = NULL;
		return FAILURE;
    };
	
	int iNoCC = (int)R.size();
	int iNoTotalAttributes = this->DataMatrix->GetNoCols() - 1;
	for(int i=0; i<iNoCC; i++){
        for(int j=0; j<iNoTotalAttributes; j++){
            int bExist = ExistElem(j,R[i]);
            if (bExist)
                OutputFile << 1 << " ";
            else
                OutputFile << 0 << " ";
        }
        OutputFile << endl;
    }
    
    OutputFile.close();
    
	delete []stub_name;
	stub_name = NULL;
	    
	return SUCCESS;

}

/************ 
Write the output that will be directly evaluated
************/
int ClusteringRounds::WriteOutputSimple(MatrixGeneric<unsigned int>* M, vector<vector<int> > &R){

	char* tmpPtr = strstr(this->sOutputFile,".txt");
    if (tmpPtr == NULL){
        cerr << "ClusteringRounds: WriteOutputSime: The output file does not contain .txt" << endl;
        return FAILURE;
    }

#ifdef WIN32
	int bFail = 0;
#endif

	char* stub_name = new char[tmpPtr - this->sOutputFile + 1];
#ifdef WIN32
	bFail = strncpy_s(stub_name, tmpPtr - this->sOutputFile + 1, sOutputFile, tmpPtr - this->sOutputFile);
	assert(bFail == 0);
#else
	strncpy(stub_name, sOutputFile,tmpPtr - this->sOutputFile);
#endif
    stub_name[tmpPtr-sOutputFile] = '\0';

    string final_name = stub_name;
	if (this->bDisjoint == 1)
		final_name += "_hard.txt";
	else
		final_name += "_soft.txt";

	ofstream OutputFile;
	OutputFile.open(final_name.c_str(), ios::out);
    if (!OutputFile.is_open()){
        cerr << "ClusteringRounds: Cannot open the file " << final_name.c_str() << endl;
		return FAILURE;
    };

	int i,j;

	//write out the relevant dimensions
	int iNoCC = (int)R.size();
	int iNoTotalObjects = this->DataMatrix->GetNoRows();
	int iNoTotalAttributes = this->DataMatrix->GetNoCols() - 1;
	for(i=0; i<iNoCC; i++){
        OutputFile << "ClusterResult" << (i+1) << " ";
		for(j=0; j<iNoTotalAttributes; j++){
            int bExist = ExistElem(j,R[i]);
            if (bExist)
                OutputFile << 1 << " ";
            else
                OutputFile << 0 << " ";
        }
        OutputFile << endl;
    }

	//write out the clustering result
	OutputFile << "LABELING" << endl;
	for(i=0; i<iNoTotalObjects; i++){
		vector<int> vClusterLabels;
		for(j=0; j<iNoCC; j++){
			if (M->GetElemAt(i,j) == 1)
				vClusterLabels.push_back(j);    
		}
		int iNoLabels = (int)vClusterLabels.size();		
		if (iNoLabels == 0){
			OutputFile << -1 << endl;
		}
		else{
			if (this->bDisjoint == 1){
				OutputFile << vClusterLabels[0] + 1 << endl; // it belongs to the first one
			}
			else{
				for(int index=0; index<iNoLabels; index++){
					if (index != iNoLabels - 1)
						OutputFile << vClusterLabels[index] + 1 << ",";
					else
						OutputFile << vClusterLabels[index] + 1 << endl;
				}//end for index
			}
		}
	}    


	OutputFile.close();

	return SUCCESS;
}


/************ 
Read the matrix with Critical Values for the chi-aquare test
************/
int ClusteringRounds::ReadCriticalValuesMatrix(){

	this->CriticalValues = new MatrixGeneric<double>(100,5);
	assert(this->CriticalValues != NULL);

	ifstream InputFile;
	InputFile.open("UpperCriticalValuesChisquare.txt", ifstream::in);
	if (!InputFile.is_open()){
		cerr << "ClusteringRounds: Cannot open the file UpperCriticalValuesChisquare.txt" << endl;
		return FAILURE;
	}

	InputFile >> *this->CriticalValues;

	InputFile.close();

	return SUCCESS;
}

/************ 
Builds a histogram for each attribute
************/
void ClusteringRounds::BuildAllHistograms(){
	
	cout << "Building all histograms ... " << endl << endl;

	this->obj_CategoricalOneDHistograms = new CategoricalOneDHistograms(this->DataMatrix);
	assert(this->obj_CategoricalOneDHistograms != NULL);

	this->obj_CategoricalOneDHistograms->BuildAllHistograms();

	//this->obj_CategoricalOneDHistograms->Display(cout);	
}
