#include "Histogram.h"

/************
Constructor(s) and destructor
************/
//default constructor
Histogram::Histogram(){

	this->iDimID = -1;
	this->iNoBins = 0;
	this->vMembers = NULL;
}

//constructor
Histogram::Histogram(int no_bins, int dim_id){
	
	this->iDimID = dim_id;
	this->iNoBins = no_bins;
	this->vMembers = new Bin*[this->iNoBins];
	assert(this->vMembers != NULL);
	for(int i=0;i<this->iNoBins;i++)
		this->vMembers[i] = NULL;
}

//destructor
Histogram::~Histogram(){
	
	//cout << "Dtor Histogram" << endl;
	if (this->vMembers != NULL){
		for(int i=0;i<this->iNoBins;i++){
			delete this->vMembers[i];
			this->vMembers[i] = NULL;
		}
		delete []this->vMembers;
		this->vMembers = NULL;
	}
}

/************
Functions for manipulating private members of the class
************/
int Histogram::GetDimID() const{

	return this->iDimID;
}

int Histogram::GetNoBins() const{
	return this->iNoBins;
}


/************
Adds the bin 'obj_bin' to this->vMembers at position 'pos'
************/
void Histogram::AddBin(Bin* obj_bin, int pos){

	assert( (pos >=0) && (pos < this->iNoBins));
	if (this->vMembers[pos] != NULL){
		delete this->vMembers[pos];
		this->vMembers[pos] = NULL;
	}
	this->vMembers[pos] = obj_bin;
}

Bin* Histogram::GetBin(int pos){

	assert( (pos >=0) && (pos < this->iNoBins));
	return this->vMembers[pos];
}

/************
Displays the histogram for debugging purposes
************/
void Histogram::Display(std::ostream &os){
	
	os << "Attribute " << this->iDimID + 1 << ":" << endl;
	for(int i=0;i<this->iNoBins;i++){
		cout << "Bin " << (i+1) << ":" << endl;
		Bin* obj_Bin = this->vMembers[i];
		//if ( (obj_Bin != NULL) && (obj_Bin->IsBinDense() == 1) )
		if (obj_Bin != NULL)
			obj_Bin->Display(os);
	}
}

