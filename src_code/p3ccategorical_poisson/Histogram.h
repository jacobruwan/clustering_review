#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include "Bin.h"

class Histogram{

protected:
	
	int iDimID;

	Bin** vMembers; //the histogram is a vector of pointers of type Bin
	int iNoBins; //the number of bins in the histogram

	
public:
	Histogram();
	Histogram(int no_bins, int dim_id);
	virtual ~Histogram();

	int GetDimID() const;

	int GetNoBins() const;
	

	void AddBin(Bin* obj_bin, int pos);
	Bin* GetBin(int pos);

	void Display(ostream& os);
	
};


#endif

