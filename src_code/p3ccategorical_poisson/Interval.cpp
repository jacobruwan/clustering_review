#include "Interval.h"

#include <assert.h>

/************
Constructor(s) and destructor
************/
//default constructor
Interval::Interval(){
	
	this->iDimID = -1;
}

//constructor
Interval::Interval(int dim_id){
	
	this->iDimID = dim_id;
}

//destructor
Interval::~Interval(){
	
	this->vIDBins.clear();
}

/************
Functions for manipulating private members of the class
************/
int Interval::GetDimID() const{

	return this->iDimID;
}

vector<int> Interval::GetIDBins() const{

	return this->vIDBins;
}

int Interval::GetNoBins(){

	return (int)this->vIDBins.size();
}
/************
Adds the bin 'iIDBin' to 'this->vIDBins'
************/
void Interval::AddBin(int iIDBin){
	
	int bExist = ExistElem(iIDBin, this->vIDBins);
	if (!bExist)
		this->vIDBins.push_back(iIDBin);
}

/************
Removes the bin 'iIDBin' from 'this->vIDBins'
************/
void Interval::RemoveBin(int iIDBin){
	
	int iNoBins = (int)this->vIDBins.size();
	int iPos = -1;
	for(int i=0; i<iNoBins; i++){
		if (this->vIDBins[i] == iIDBin)
			iPos = i;
	}
	assert(iPos != -1);
	this->vIDBins.erase(this->vIDBins.begin() + iPos);
}
