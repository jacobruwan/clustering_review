P3C for Categorical Data
----------------------

P3C - Input Arguments
----------------------
1. file_name:
	The name of the file that stores the data. 
	Each line in the input file has the following format:
	value \space value \space ... \space value \space label \n
	The last column contains labels. If no labels are known, give each object the same label (e.g., 1).
	If there are known outliers, they receive the largest label. 
	See "SampleData.txt" for an example.
	"SampleData.txt" is a synthetic data set with 10000 objects and 100 attributes. There are 5 clusters, 
	and 500 outliers. Attribute 101 contains the labels. Cluster points are labeled from 1 to 5, and the outliers
	receive label 6.
2. output_file_name:
	The name of the output file that will contain the clustering result.
3. Poisson threshold:
	The value for the Poisson threshold (e.g., 1.0E-20)
4. Parameter HS: HS = 1 means disjoint clusters (and outliers)
		     HS = 2 means overlapping clusters (and outliers)



P3C - Output Format
---------------------------------
4. File with the clustering result (e.g., SampleData_P3C.txt)
	Format of the output file:
	ClusterResult1 1 \space 0 \space .. \space 1 \n
	...
	ClusterResult5 0 \space 1 \space .. \space 0 \n
	LABELING
	4,5
	-1
	2
	...
	The first part of the result file contains the relevant attributes for each found cluster.
	The "LABELING" part of the result file contains the label computed for each data point.
	Outliers are labeled with -1. 
	If overlapping clusters were computed, a data point may belong to more than one cluster.			


How to run P3C
--------------
1. make clean
2. make
3. 
disjoint clusters + outliers
./P3CCategorical SampleData.txt SampleData_P3C.txt 1.0E-20 1
The result is stored in SampleData_P3C_hard.txt

overlapping clusters + outliers
./P3CCategorical SampleData.txt SampleData_P3C.txt 1.0E-20 2
The result is stored in SampleData_P3C_soft.txt


	
	
	

