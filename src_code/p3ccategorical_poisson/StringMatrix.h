#ifndef STRING_MATRIX
#define STRING_MATRIX

#include "Utile.h"

#include <assert.h>
#include <string.h>

#include <iostream>
#include <fstream>
using namespace std;

//the maximum number of chars read by fgets from the input file
#define LINE_LENGTH 2000000

class StringMatrix{

    int m_nCols;
	int m_nRows;
	char*** m_mat;

	int DetermineDataDimensionality(char* sInputFile);

public:
     StringMatrix();
     StringMatrix(int noRows, int noCols);
     StringMatrix(const StringMatrix &m);
    ~StringMatrix();

    int GetNoCols() const;
    int GetNoRows() const;

    char* GetElemAt(int i, int j);
    void SetElemAt(int i, int j, char* val);

	StringMatrix& operator= (const StringMatrix &m);

    int ReadDataFile(char* sInputFile);
   
    void Display(ostream& os);

};

#endif


