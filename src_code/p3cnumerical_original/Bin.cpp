#include "Bin.h"

/************
Constructor(s) and destructor
************/
//default constructor
Bin::Bin(){
	
	this->iNoTotalObjects = -1;
	this->iNoCellsObjects = -1;
	this->iDimID = -1;
	this->bDense = 0;
	this->vObjects = NULL;
}

//constructor
Bin::Bin(int no_total_objects, int dim_id){
	
	this->iNoTotalObjects = no_total_objects;
	this->iNoCellsObjects = this->iNoTotalObjects / (8*sizeof(unsigned int)) + 1;

	this->iDimID = dim_id;
		
	this->vObjects = new unsigned int[this->iNoCellsObjects];
	assert(this->vObjects != NULL);
    for(int i=0; i<this->iNoCellsObjects; i++)
        this->vObjects[i] = 0;

}

//destructor
Bin::~Bin(){
	
	if (this->vObjects != NULL){
		delete []this->vObjects;
		this->vObjects = NULL;
	}

}


/************
Functions for manipulating private members of the class
************/
int Bin::GetDimID() const{
    
	return this->iDimID;
}

void Bin::SetBinDense(int flag){
    this->bDense = flag;
}

int Bin::IsBinDense() const{
    return this->bDense;
}

unsigned int* Bin::GetObjects(){

	return this->vObjects;
}

/************
Returns 1 if the object 'object_id' is a member of the bin
************/
int Bin::IsMemberObject(int object_id){

	int bIsMember = 0;

	bIsMember = GetBit(this->vObjects, object_id, this->iNoCellsObjects, this->iNoTotalObjects);

	return bIsMember;

}

/************
Adds the object 'object_id' to the bin, if this object is not already there
************/
void Bin::AddObject(int object_id){
	
	assert( (object_id >= 0 ) && (object_id < this->iNoTotalObjects) );
	SetBit(this->vObjects,object_id,this->iNoCellsObjects,this->iNoTotalObjects);
}

/************
Removes the object 'object_id' from the bin, if this object is not already removed
************/
void Bin::RemoveObject(int object_id){

	assert( (object_id >= 0 ) && (object_id < this->iNoTotalObjects) );
	UnsetBit(this->vObjects,object_id,this->iNoCellsObjects,this->iNoTotalObjects);
}

/************
It computes the number of objects in this bin
************/
int Bin::GetActualSupport(){
	
	int iActualSupport = GetNumberOneBits(this->vObjects,this->iNoCellsObjects,this->iNoTotalObjects);
	return iActualSupport;
}


/************
Displays a numerical bin for debugging purposes
************/
void Bin::Display(ostream &os){

}


