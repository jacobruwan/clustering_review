#include "ClusteringRounds.h"


/************ 
Constructor(s) and destructor
************/
//default constructor
ClusteringRounds::ClusteringRounds(){
	
	this->sInputFile = NULL;
	this->sOutputFile = NULL;
	this->dPoissonThreshold = 0;
	this->obj_NumericalOneDHistograms = NULL;
	this->CriticalValues = NULL;
}

//constructor
ClusteringRounds::ClusteringRounds(char* sInputFile, char* sOutputFile, double dPoissonThreshold){
    
#ifdef WIN32
	int bFail = 0;
#endif

	this->sInputFile = new char[strlen(sInputFile)+1];
	assert(this->sInputFile != NULL);

#ifdef WIN32
	bFail = strcpy_s(this->sInputFile, strlen(sInputFile)+1, sInputFile);
	assert(bFail == 0);
#else
	strcpy(this->sInputFile, sInputFile);
#endif
    
	this->sOutputFile = new char[strlen(sOutputFile)+1];
	assert(this->sOutputFile != NULL);

#ifdef WIN32
	bFail = strcpy_s(this->sOutputFile, strlen(sOutputFile)+1, sOutputFile);
	assert(bFail == 0);
#else
    strcpy(this->sOutputFile,sOutputFile);
#endif
	
	this->dPoissonThreshold = dPoissonThreshold;

	this->DataMatrix = new StringMatrix();
	assert(this->DataMatrix != NULL);

	this->iNoIter = 0;
	this->bClusteringRounds = 1;

	this->obj_NumericalOneDHistograms = NULL;
}

//destructor
ClusteringRounds::~ClusteringRounds(){
    
    delete []this->sInputFile;
	this->sInputFile = NULL;
    delete []this->sOutputFile;
	this->sOutputFile = NULL;	

	delete this->DataMatrix;
	this->DataMatrix = NULL;

	delete this->CriticalValues;
	this->CriticalValues = NULL;

	delete this->obj_NumericalOneDHistograms;
	this->obj_NumericalOneDHistograms = NULL;
}


/************ 
Runs clustering rounds until a certain condition is satisfied 
************/

int ClusteringRounds::Run(){
    

    //read the data matrix 
    if (this->ReadDataMatrix() == FAILURE)
        return FAILURE;

	this->BuildAllHistograms();
	
	this->ReadCriticalValuesMatrix();
	
	time_t timeBegin;
	time_t timeEnd;
	
	time(&timeBegin);
	while(this->bClusteringRounds){
	
		this->iNoIter++;
		
		cout << "****************************" << endl;
		cout << "Round " << this->iNoIter << endl << endl;

		Round* obj_Round = new Round();
		assert(obj_Round != NULL);
		if (obj_Round->DoRound(this) == FAILURE){
			return FAILURE;
		}

		this->WriteOutput(obj_Round->GetMembershipMatrix(), obj_Round->GetAllRelevantDimensions());

		delete obj_Round;

		this->bClusteringRounds = 0;
		
		cout << "****************************" << endl;
		
	}
	time(&timeEnd);
	cout << "\tElapsed time = " << timeEnd - timeBegin << endl << endl;

	return SUCCESS;
}

/************ 
Read the data matrix 
************/
int ClusteringRounds::ReadDataMatrix(){
	
	if (this->DataMatrix->ReadDataFile(this->sInputFile) == FAILURE){
        cout << "ClusteringRounds: Fail to read the data matrix" << endl;
		return FAILURE;
	}

	cout << "Data matrix read" << endl << endl;

	return SUCCESS;
}

/************ 
Write the output
************/
int ClusteringRounds::WriteOutput(MatrixGeneric<unsigned int>* M, vector<vector<int> > &R){

	ofstream OutputFile;
	OutputFile.open(this->sOutputFile, ios::out);
    if (!OutputFile.is_open()){
        cerr << "ClusteringRounds: Cannot open the file " << this->sOutputFile << endl;
		return FAILURE;
    };
    
	OutputFile << *M;	

	OutputFile.close();

	char* tmpPtr = strstr(this->sOutputFile,"_PartialInitialization.txt");
    if (tmpPtr == NULL){
        cerr << "ClusteringRounds: WritePartialOutput: The output file does not contain _PartialInitialization.txt" << endl;
        return FAILURE;
    }
 
#ifdef WIN32
	int bFail = 0;
#endif

	char* stub_name = new char[tmpPtr - this->sOutputFile + 1];
#ifdef WIN32
	bFail = strncpy_s(stub_name, tmpPtr - this->sOutputFile + 1, sOutputFile, tmpPtr - this->sOutputFile);
	assert(bFail == 0);
#else
	strncpy(stub_name, sOutputFile,tmpPtr - this->sOutputFile);
#endif
    stub_name[tmpPtr-sOutputFile] = '\0';

    string final_name = stub_name;
    final_name += "_RelevantDimensions.txt";
    
   	OutputFile.open(final_name.c_str(), ios::out);
    if (!OutputFile.is_open()){
        cerr << "ClusteringRounds: Cannot open the file " << final_name.c_str() << endl;
		delete []stub_name;
		stub_name = NULL;
		return FAILURE;
    };
	
	int iNoCC = (int)R.size();
	int iNoTotalAttributes = this->DataMatrix->GetNoCols() - 1;
	for(int i=0; i<iNoCC; i++){
        for(int j=0; j<iNoTotalAttributes; j++){
            int bExist = ExistElem(j,R[i]);
            if (bExist)
                OutputFile << 1 << " ";
            else
                OutputFile << 0 << " ";
        }
        OutputFile << endl;
    }
    
    OutputFile.close();
    
	delete []stub_name;
	stub_name = NULL;
	    
	return SUCCESS;
}

/************ 
Read the matrix with Critical Values for the chi-square test
************/
int ClusteringRounds::ReadCriticalValuesMatrix(){

	this->CriticalValues = new MatrixGeneric<double>(100,5);
	assert(this->CriticalValues != NULL);

	ifstream InputFile;
	InputFile.open("UpperCriticalValuesChisquare.txt", ifstream::in);
	if (!InputFile.is_open()){
		cerr << "ClusteringRounds: Cannot open the file UpperCriticalValuesChisquare.txt" << endl;
		return FAILURE;
	}

	InputFile >> *this->CriticalValues;

	InputFile.close();

	return SUCCESS;
}

/************ 
Builds a histogram for each attribute
************/
void ClusteringRounds::BuildAllHistograms(){
	
	cout << "Building all histograms ... " << endl << endl;

	this->obj_NumericalOneDHistograms = new NumericalOneDHistograms(this->DataMatrix);
	assert(this->obj_NumericalOneDHistograms != NULL);

	this->obj_NumericalOneDHistograms->BuildAllHistograms();

	//this->obj_NumericalOneDHistograms->Display(cout);	

}
