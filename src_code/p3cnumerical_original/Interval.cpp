#include "Interval.h"

/************
Constructor(s) and destructor
************/
//default constructor
Interval::Interval(){
	
	this->iDimID = -1;
}

//constructor
Interval::Interval(int dim_id){
	
	this->iDimID = dim_id;
}

//destructor
Interval::~Interval(){
	
	this->vIDBins.clear();
}

/************
Functions for manipulating private members of the class
************/
int Interval::GetDimID() const{

	return this->iDimID;
}

vector<int> Interval::GetIDBins() const{

	return this->vIDBins;
}

/************
Adds the bin 'iIDBin' to 'this->vIDBins'
************/
void Interval::AddBin(int iIDBin){
	
	int bExist = ExistElem(iIDBin, this->vIDBins);
	if (!bExist)
		this->vIDBins.push_back(iIDBin);
}

