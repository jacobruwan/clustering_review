#ifndef MATRIXGENERIC_H
#define MATRIXGENERIC_H

# ifdef WIN32
#   define LTGT
# else
#  ifdef CXX_NO_FRIEND_TMPL_DECL
#    define LTGT
#  else
#    define LTGT <>
#  endif
# endif


#include <iostream>
#include <fstream>

#include <string>
#include <vector>
using namespace std;


#ifndef LINE_LENGTH_SHORT
#define LINE_LENGTH_SHORT 10000
#endif


template <class DataType>
class MatrixGeneric {
	int m_nCols;
	int m_nRows;
	DataType ** m_mat;

public:
	MatrixGeneric();
	MatrixGeneric(int noRows, int noCols);	
	MatrixGeneric(const MatrixGeneric &);
	~MatrixGeneric();
	
	void Rebuild(int newLines, int newCols);

	int GetCols();
	int GetRows();
	void SetCols(int j);
	void SetRows(int i);

	void SetElemAt(int i, int j, DataType val);
	DataType GetElemAt(int i, int j);

	DataType GetMinElemOnCol(int j);
	DataType GetMaxElemOnCol(int j);
	
/*#ifdef WIN32
	template<typename DataType> friend ostream& operator << LTGT (ostream& os, MatrixGeneric<DataType>& m);
	template<typename DataType> friend istream& operator >> LTGT (istream& is, MatrixGeneric<DataType>& m);
#else
	friend ostream& operator << LTGT (ostream& os, MatrixGeneric<DataType>& m);
	friend istream& operator >> LTGT (istream& is, MatrixGeneric<DataType>& m);
#endif*/
};

#ifdef WIN32
template <typename DataType>
ostream& operator<<(ostream& os, MatrixGeneric<DataType>& m) 
#else
template <class DataType>
ostream& operator<<(ostream& os, MatrixGeneric<DataType>& m)
#endif
{
        
        for (int r=0;r<m.GetRows();r++) {
            for (int c=0;c<m.GetCols();c++) {
                os << m.GetElemAt(r,c) << " ";
                
            }
            os <<endl;
        }
        os << endl;
		return os;
}

#ifdef WIN32
template <typename DataType>
istream& operator>>(istream& is, MatrixGeneric<DataType>& m) 
#else
template <class DataType>
istream& operator>>(istream& is, MatrixGeneric<DataType>& m)
#endif
{

	char sline[LINE_LENGTH_SHORT];
	is.getline(sline,LINE_LENGTH_SHORT);
	
	char *stemp = strchr(sline,' ');
	int noCols = 0;	
	while (stemp){ 
		noCols++; 
		stemp = strchr(stemp+1,' ');
	}
	is.seekg(0);

	DataType element;
	int currentLine=0;
	vector<DataType>* pVector = new vector<DataType>[noCols];
	while (!is.eof()){
		for(int i=0; i<noCols; i++){
			is >> element;
			if (is.eof()) break;
			pVector[i].push_back(element);
		}
		if (is.eof()) break;
		currentLine++;
	}
	int noLines = currentLine;
	m.Rebuild(noLines, noCols);
	for(int i=0; i<noLines; i++)
		for(int j=0; j<noCols; j++)
			m.SetElemAt(i,j,pVector[j][i]);

	delete []pVector;
	
	return is;
}


//the empty constructor
template <class DataType> 
MatrixGeneric<DataType>::MatrixGeneric(){
	m_nRows = 0;
	m_nCols = 0;
	m_mat = NULL;
}

//the constructor
template <class DataType> 
MatrixGeneric<DataType>::MatrixGeneric(int noRows, int noCols) {
	m_nRows = noRows;
	m_nCols = noCols;
	m_mat = new DataType* [noRows];
	for (int i=0; i<noRows; i++) {
		m_mat[i] = new DataType [noCols];
		for (int j=0; j<noCols; j++)
			m_mat[i][j] = 0;
	}
}

//copy constructor
template <class DataType> 
MatrixGeneric<DataType>::MatrixGeneric(const MatrixGeneric<DataType> &m){
	this->m_nRows = m.m_nRows;
	this->m_nCols = m.m_nCols;
	this->m_mat = new DataType* [this->m_nRows];
	for (int i=0;i<this->m_nRows;i++){
		this->m_mat[i] = new DataType [this->m_nCols];
		for (int j=0;j<this->m_nCols;j++)
			this->m_mat[i][j] = m.m_mat[i][j];
	}

}

//destructor
template <class DataType> 
MatrixGeneric<DataType>::~MatrixGeneric(){
	if ( m_mat != NULL ){
		for (int i=0; i<m_nRows; i++)
			delete m_mat[i];
		delete m_mat;
	}
}

//build a new matrix
template <class DataType> 
void MatrixGeneric<DataType>::Rebuild(int newLines, int newCols){
	if ( m_mat != NULL ){
		for (int i=0; i<m_nRows; i++)
			delete m_mat[i];
			delete m_mat;
	}
	this->m_nRows = newLines;
	this->m_nCols = newCols;
	this->m_mat = new DataType* [this->m_nRows];
	for (int i=0;i<this->m_nRows;i++)
		this->m_mat[i] = new DataType [this->m_nCols];
	
}


//return the number of Rows
template <class DataType> 
int MatrixGeneric<DataType>::GetRows(){
	return m_nRows;
}

//return the number of Cols
template <class DataType>
int MatrixGeneric<DataType>::GetCols(){
	return m_nCols;
}

//set the number of Rows
template <class DataType> 
void MatrixGeneric<DataType>::SetRows(int i) {
	m_nRows = i;
}

//set the number of Cols
template <class DataType> 
void MatrixGeneric<DataType>::SetCols(int j) {
	m_nCols = j;
}

template <class DataType> 
DataType MatrixGeneric<DataType>::GetMinElemOnCol(int j) {
	DataType min_elem = RAND_MAX;
	for (int i=0;i<m_nRows;i++){
		if (m_mat[i][j]<min_elem){
			min_elem = m_mat[i][j];
		}
	}
	return min_elem;

}

template <class DataType> 
DataType MatrixGeneric<DataType>::GetMaxElemOnCol(int j) {
	DataType max_elem = -RAND_MAX;
	for (int i=0;i<m_nRows;i++){
		if (m_mat[i][j]>max_elem){
			max_elem = m_mat[i][j];
		}
	}
	return max_elem;

}


//gives the element at the position (i, j) the value val
template <class DataType> 
void MatrixGeneric<DataType>::SetElemAt(int i, int j, DataType val){
	if ((i<m_nRows) && (j<m_nCols) && (i>=0) && (j>=0))
		m_mat[i][j] = val;
	else
		cout << "MatrixGeneric: Index out of bounds" << endl;
}

//gives the element in the position (i,j)
template <class DataType> 
DataType MatrixGeneric<DataType>::GetElemAt(int i, int j){
	if ((i<m_nRows) && (j<m_nCols) && (i>=0) && (j>=0))
		return m_mat[i][j];
	else {
		cout << "MatrixGeneric: Index out of bounds" << endl;
		return -100;
	}
}


#endif
