#ifndef NUMERICAL_BIN
#define NUMERICAL_BIN

#include "Bin.h"

class NumericalBin: public Bin{
	
	double dStartPoint;
	double dEndPoint;

public:

	NumericalBin();
	NumericalBin(int no_total_objects, int dim_id, double start_point, double end_point);
	~NumericalBin();

	double  GetStartPoint() const;
	double  GetEndPoint() const;

	void Display(ostream &os);
};

#endif
