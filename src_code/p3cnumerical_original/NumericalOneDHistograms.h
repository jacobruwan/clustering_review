#ifndef NUMERICAL_ONED_HISTOGRAMS
#define	NUMERICAL_ONED_HISTOGRAMS

#include "OneDHistograms.h"
#include "NumericalBin.h"
#include "StringMatrix.h"
#include "Utile.h"

class NumericalOneDHistograms: public OneDHistograms{

	double* tmins; // stores the min on each column, for normalization purposes
    double* tmaxs; // stores the max on each column, for normalization purposes 
	
	void NormalizeData();
	
public:
	NumericalOneDHistograms();
	NumericalOneDHistograms(StringMatrix* SM);
	~NumericalOneDHistograms();

	double* GetMinOnAttributes();
	double* GetMaxOnAttributes();

	void BuildAllHistograms();	

	void Display(ostream& os);
};

#endif
