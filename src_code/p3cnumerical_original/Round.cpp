#include "Round.h"

#include <math.h>

/************ 
Constructor(s) and destructor
************/
//default constructor
Round::Round(){
	
	this->MembershipMatrix = NULL;
}

//destructor
Round::~Round(){
	
	int iLengthVector = (int)this->vIntervals.size();
	for(int i=0; i<iLengthVector;i++){
		delete this->vIntervals[i];
		this->vIntervals[i] = NULL;
	}
	this->vIntervals.clear();

	iLengthVector = (int)this->vCC.size();
	for(int i=0; i<iLengthVector;i++){
		delete this->vCC[i];
		this->vCC[i] = NULL;
	}
	this->vCC.clear();

	delete this->MembershipMatrix;
	this->MembershipMatrix = NULL;

	iLengthVector = (int)this->vAllRelevantDimensions.size();
	for(int i=0; i<iLengthVector;i++){
		this->vAllRelevantDimensions[i].clear();
	}
	this->vAllRelevantDimensions.clear();
}

/************
Functions for manipulating private members of the class
************/
MatrixGeneric<unsigned int>* Round::GetMembershipMatrix(){

	return this->MembershipMatrix;
}

vector<vector<int> >& Round::GetAllRelevantDimensions(){

	return this->vAllRelevantDimensions;
}

/************ 
Perform one clustering round
************/
int Round::DoRound(ClusteringRounds* obj_CR){

	cout << "Marking dense bins ..." << endl; 
	this->MarkDenseBins(obj_CR);

	cout << "Computing intervals ..." << endl;
	this->ComputeIntervals(obj_CR);
	int iLengthVector = (int)this->vIntervals.size();
	cout << "Number Intervals = " << iLengthVector << endl;
	//this->DisplayIntervals(cout, obj_CR);

	cout << "Generating cluster cores ..." << endl;
	if ( this->GenerateClusterCores(obj_CR) == FAILURE)
		return FAILURE;
	this->DisplayClusterCores(cout, obj_CR);

	cout << "Computing the membership matrix and relevant attributes ..." << endl;
	this->ComputeMembershipMatrix(obj_CR);
	

	return SUCCESS;
}


/************ 
For each attribute, the dense bins are marked using the Chi-square test
************/
void Round::MarkDenseBins(ClusteringRounds* obj_CR){

	int iNoTotalAttributes = obj_CR->DataMatrix->GetNoCols() - 1;
	Histogram** vAllHistograms = obj_CR->obj_NumericalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);	

	int j, index;
		
	for(j=0;j<iNoTotalAttributes;j++){ //for each attribute
		
		int iNoBins = vAllHistograms[j]->GetNoBins();

		//collect the sizes (i.e., supports) of all bins
		int* vBinSizes = new int[iNoBins];
		for(index=0;index<iNoBins;index++){
			Bin* obj_Bin = vAllHistograms[j]->GetBin(index);
			assert(obj_Bin != NULL);
			vBinSizes[index] = obj_Bin->GetActualSupport();
		}

		int iNoDegreesFreedom = iNoBins - 2; // "-2", instead of "-1", because I compute the mean based on the data
        double dMeanBins = 0;
        double dChisquareStat = 0;
        int iNoActiveBins = iNoBins;
        int bSatisfyCondition = 1;

		while( bSatisfyCondition && (iNoActiveBins > 2) ){
            
			for(index=0; index<iNoBins; index++){
                if (vBinSizes[index] != -1){
                    dMeanBins += vBinSizes[index];
                }
            }
            dMeanBins /= iNoActiveBins; 
        
            for(index=0; index<iNoBins; index++){
                if (vBinSizes[index] != -1){
                    if (vBinSizes[index] < 5)
                        dChisquareStat += pow(vBinSizes[index] - dMeanBins - 0.5, 2) / dMeanBins; //Yates correction
                    else
                        dChisquareStat += pow(vBinSizes[index] - dMeanBins, 2) / dMeanBins;
                }
            }
            
            int alpha = 4; //alpha = 0.001 //tight bins
            //int alpha = 3;  // alpha = 0.01 //larger bins
            if (dChisquareStat <= obj_CR->CriticalValues->GetElemAt(iNoDegreesFreedom, alpha)){
                bSatisfyCondition = 0;   
            }
            else{
                int iMaxElem = -1;
                int iMaxElemIndex = -1;
                for(index=0; index<iNoBins; index++){
                    if (iMaxElem < vBinSizes[index]){
                        iMaxElem = vBinSizes[index];
                        iMaxElemIndex = index;
                    }
                }
                vBinSizes[iMaxElemIndex] = -1;
                
				Bin* obj_Bin = vAllHistograms[j]->GetBin(iMaxElemIndex);
				assert(obj_Bin != NULL);
				obj_Bin->SetBinDense(1);
				
                iNoDegreesFreedom--;
                iNoActiveBins--;
                dMeanBins = 0;
                dChisquareStat = 0;
            }
        
            //bSatisfyCondition = 0; // to force marking just one bin: the most dense bin; useful for real data

        }//end while


		if ((bSatisfyCondition == 1) && (iNoActiveBins == 2)){
			//I have marked all bins, and I have stopped when I am left with two unmarked bins
			for(index=0; index<iNoBins; index++){
				if (vBinSizes[index] != -1){
                    dMeanBins += vBinSizes[index];
                }
			}
			dMeanBins /= iNoActiveBins;

			for(index=0; index<iNoBins; index++){
				if (vBinSizes[index] != -1){ //one of the two unmarked bins
					//if( (vBinSizes[index] > dMeanBins) && (Poisson(vBinSizes[index], dMeanBins) < obj_CR->dPoissonThreshold) ){
					if 	(vBinSizes[index] > dMeanBins){
						//mark the bin whose size is larger than the size of the other bin
						//Poisson_threshold may be too tough (1.0e-10)
						//I could use instead THRESHOLD_MARK = 0.01
						Bin* obj_Bin = vAllHistograms[j]->GetBin(index);
						assert(obj_Bin != NULL);
						obj_Bin->SetBinDense(1);
					}
				}
			}
		}

		delete []vBinSizes;
		vBinSizes = NULL;
	}//end for j

}//end function

/************ 
For numeric attributes, adjacent marked bins are merged into intervals
************/
void Round::ComputeIntervals(ClusteringRounds *obj_CR){

	int iNoTotalAttributes = obj_CR->DataMatrix->GetNoCols() - 1;
	Histogram** vAllHistograms = obj_CR->obj_NumericalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);	

	int j, index, index2;
		
	for(j=0;j<iNoTotalAttributes;j++){ //for each attribute
		
		int iNoBins = vAllHistograms[j]->GetNoBins();
		int iFirstBin = -1;
        int iLastBin  = -1;

		for(index=0;index<iNoBins;index++){//for each bin
			Bin* obj_Bin = vAllHistograms[j]->GetBin(index);
			assert(obj_Bin != NULL);
			if (obj_Bin->IsBinDense() == 1){
				if (iFirstBin == -1){
					iFirstBin = index;
				}
			}
			else{
				if (iFirstBin != -1){
					iLastBin = index;
					
					//Create a new interval with the bins from iFirstBin to iLastBin
					Interval* obj_Interval = new Interval(j);
					assert(obj_Interval != NULL);
					for(index2 = iFirstBin; index2 < iLastBin; index2++){
						obj_Interval->AddBin(index2);
					}
					this->vIntervals.push_back(obj_Interval);
					
					iFirstBin = -1;
				}
			}
		}// end for index

		if (iFirstBin != -1){
			iLastBin = iNoBins;

			//Create a new interval with the bins from iFirstBin to iLastBin
			Interval* obj_Interval = new Interval(j);
			assert(obj_Interval != NULL);
			for(index2 = iFirstBin; index2 < iLastBin; index2++){
				obj_Interval->AddBin(index2);
			}
			this->vIntervals.push_back(obj_Interval);
		}

	}//end for j

}


/************ 
Computes the membership of the interval 'iIntervalID' ans stores it in 'vMembership' using bits
************/
void Round::GetMembershipInterval(unsigned int* vMembership, int iIntervalID, ClusteringRounds *obj_CR){

	int iLengthVector = (int)this->vIntervals.size();
	assert( (iIntervalID >= 0) && (iIntervalID < iLengthVector) );

	Histogram** vAllHistograms = obj_CR->obj_NumericalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;
		
	Interval* obj_Interval = this->vIntervals[iIntervalID];
	assert(obj_Interval != NULL);
		
	int iDimID = obj_Interval->GetDimID();
	vector<int> vIDBins = obj_Interval->GetIDBins();
	int iNoBins = (int)vIDBins.size();
		
	for(int index=0;index<iNoBins;index++){
		Bin *obj_Bin = vAllHistograms[iDimID]->GetBin(vIDBins[index]);
		assert(obj_Bin != NULL);
		UnionArrayBits(vMembership, vMembership, obj_Bin->GetObjects(), iNoCellsObjects);
	}

}

/************ 
Computes the actual support of the interval 'iIntervalID'
************/
int Round::GetActualSupportInterval(int iIntervalID, ClusteringRounds* obj_CR){

	int iAS = 0;

	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;
		
	unsigned int* vMembership = new unsigned int[iNoCellsObjects];
	assert(vMembership != NULL);
	for(int i=0;i<iNoCellsObjects;i++)
		vMembership[i] = 0;
	
	this->GetMembershipInterval(vMembership, iIntervalID, obj_CR);

	iAS = GetNumberOneBits(vMembership, iNoCellsObjects, iNoTotalObjects);

	delete []vMembership;
	vMembership = NULL;

	return iAS;
}

/************ 
Generate cluster cores in an Apriori-like style
************/ 
int Round::GenerateClusterCores(ClusteringRounds* obj_CR){

	int i,j,index,index2;

	int iNoIntervals = (int)this->vIntervals.size();
    if (iNoIntervals == 0){
        return FAILURE;
    }

    Histogram** vAllHistograms = obj_CR->obj_NumericalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	
	int k = 1; //k is the current level of generation
	int iNoTotalAttributes = obj_CR->DataMatrix->GetNoCols() - 1;

	vector<ClusterCore*> level_k; //nodes at level k
	vector<ClusterCore*> candidates_next_level; //candidates at level (k+1)    

	time_t timeBegin;
	time_t timeEnd;

	//for level 1
	time(&timeBegin);
	for(i=0;i<iNoIntervals;i++){

		//create a combination consisting of only one interval
		vector<int> vTemp;
		vTemp.push_back(i);
		ClusterCore* cand_cc = new ClusterCore(vTemp);    
		int iAS = this->GetActualSupportIntersectionIntervals(vTemp, obj_CR);
		cand_cc->SetActualSupport(iAS);
		
		level_k.push_back(cand_cc);

		vTemp.clear();
	}
	time(&timeEnd);

	cout << "Level " << k << "- " << level_k.size() << " combinations - Elapsed time: " << timeEnd-timeBegin << endl;
	
	//for following levels   
	int iSizeLevelk = (int)level_k.size();
	while(iSizeLevelk !=0){

		time(&timeBegin);

		k++;
		
		if (k == iNoTotalAttributes + 1){ //if I generated already d levels
			//check the ones on the last level
			for(i=0; i<iSizeLevelk; i++){
				int bMaximal = SatisfiesMaximality(level_k[i], obj_CR);
				if (bMaximal){
					ClusterCore* CopyCC = new ClusterCore(*level_k[i]); 
					this->vCC.push_back(CopyCC);
				}
			}
			
			//delete the objects on level_k
			for(i=0; i<iSizeLevelk; i++){
				delete level_k[i];
				level_k[i] = NULL;
			}
			level_k.clear();
			iSizeLevelk = 0;
			continue;
		}


		//I keep a vector with bits; number of bits = number entries on this level
		//bit will be 1 if that entry is "suspected" to be maximal

		int iNoCellLevelk = iSizeLevelk / (8*sizeof(unsigned int)) + 1;
		unsigned int* vBitLevelk = new unsigned int[iNoCellLevelk];
		assert(vBitLevelk != NULL);
		//at the begining, everybody is suspect
		for(i=0; i<iNoCellLevelk; i++)
			vBitLevelk[i] = ~0;

		for(i=0; i<iSizeLevelk; i++){

			ClusterCore* cand_i = level_k[i];
			int* elems_i = cand_i->GetElems();


			for(j=i+1; j<iSizeLevelk; j++){

				ClusterCore* cand_j = level_k[j];
				int* elems_j = cand_j->GetElems();

				int bValidForCombination = 1;
				for(index=0; index<k-2 && bValidForCombination; index++){
					if (elems_i[index] != elems_j[index])
						bValidForCombination = 0;
				}
				//this is dead code; the condition is all the time false
				if (elems_i[k-2] >= elems_j[k-2])
					bValidForCombination = 0;

				int dimID1 = this->vIntervals[elems_i[k-2]]->GetDimID();
				int dimID2 = this->vIntervals[elems_j[k-2]]->GetDimID();
				if (dimID1 == dimID2)
					bValidForCombination = 0;

				if (!bValidForCombination)
					continue;


				//in v, I keep the new combination
				vector<int> v;
				v.reserve(k);
				for(index=0; index<k-2; index++)
					v.push_back(elems_i[index]);
				v.push_back(elems_i[k-2]);
				v.push_back(elems_j[k-2]);

				//create a new combination
				ClusterCore* cand_cc = new ClusterCore(v);
				int iAS = this->GetActualSupportIntersectionIntervals(v, obj_CR);
				cand_cc->SetActualSupport(iAS);


				//check if all subsets of length (k-1) of v are in level_k
				//if a subset of length (k-1) does exist in level_k, then check the Poisson condition
				int bAll = 1;
				for(index=k-1; index>=0 && bAll; index--){

					//generate a subset of length (k-1)
					//because I start with index := k-1; first I will generate cand_i and cand_j
					int* elems_temp = new int[k-1];
					int elem_temp_index = 0;
					for(index2=0; index2<k; index2++){
						if (index2 != index)
							elems_temp[elem_temp_index++] = v[index2];
					}

					//check if the generated subset exists in level_k
					int bExist = 0;
					int posExist = -1; //if the subset exists, then it exists at position posExist
					for(index2=0; index2<iSizeLevelk && !bExist; index2++){
						assert(level_k[index2]->GetNoElems() == k-1);
						int* elems_index2 = level_k[index2]->GetElems();
						bExist = AreVectorsEqual(elems_temp,k-1,elems_index2,k-1);
						if (bExist)
							posExist = index2;
					}
					delete []elems_temp;
					elems_temp = NULL;

					if (!bExist){ 
						//if a subset of length (k-1) does not exist on level_k, then cand_cc won't be generated
						//so there is no need to verify other subsets of length (k-1)
						bAll = 0;
					}
					else{
						assert(posExist != -1);
						//a subset of length (k-1) exists, so let's verify the Poisson cond

						int bSatisfyPoisson = 0;

						int iSupportParent = level_k[posExist]->GetActualSupport();
						int iSupportTarget = cand_cc->GetActualSupport();

						double dWidthInterval = 0;

						int dimID_interval = this->vIntervals[v[index]]->GetDimID();
						vector<int> vIDBins = this->vIntervals[v[index]]->GetIDBins();
						int iNoBins = (int)vIDBins.size();

						Bin* obj_FirstBin = vAllHistograms[dimID_interval]->GetBin(vIDBins[0]);
						assert(obj_FirstBin != NULL);
						double dRangeMin = static_cast<NumericalBin*>(obj_FirstBin)->GetStartPoint();
						
						Bin* obj_LastBin = vAllHistograms[dimID_interval]->GetBin(vIDBins[iNoBins-1]);
						assert(obj_LastBin != NULL);
						double dRangeMax = static_cast<NumericalBin*>(obj_LastBin)->GetEndPoint();
						
						dWidthInterval = dRangeMax - dRangeMin;

									
						//this is the older version based on the Poisson distribution
						//double dDependentExpectedSupport = (double)iSupportParent * dWidthInterval;
						//if ((iSupportTarget > dDependentExpectedSupport) && (Poisson(iSupportTarget, dDependentExpectedSupport) < obj_CR->dPoissonThreshold)){
						//	bSatisfyPoisson = 1;
						//}

						//March 2008: the newer version based on the Binomial distribution
						int iCriticalValueRight = GetCriticalValueBinomialRight(iSupportParent, dWidthInterval, obj_CR->dPoissonThreshold);
						if (iSupportTarget >= iCriticalValueRight){
							bSatisfyPoisson = 1;
						}


						if (!bSatisfyPoisson){
							// if the Poisson cond fails for this parent, then there is no point to check other parents
							bAll = 0; 
						}
						else{
							//if Poisson holds, the this parent cannot be suspect anymore
							//Gabi - 13 Oct 2007
							UnsetBit(vBitLevelk, posExist, iNoCellLevelk, iSizeLevelk);
						}


					}//end else
				
				}//end for index

				if (!bAll){
					delete cand_cc;
					cand_cc = NULL;
				}
				else{
					/*
					//Gabi - 13 Oct 2007
					//cand_cc satisfies the propery with all its parents; so its parents cannot be maximal 
					for(index = k-1; index >= 0; index--){

						//generate a subset of length (k-1)
						//because I start with index := k-1; first I will generate cand_i and cand_j
						int* elems_temp = new int[k-1];
						assert(elems_temp != NULL);
						int elem_temp_index = 0;
						for(index2 = 0; index2 < k; index2++){
							if (index2 != index)
								elems_temp[elem_temp_index++] = v[index2];
						}

						//find the position where the subset exists in level_k				
						int bExist = 0;
						int posExist = -1; 
						for(index2 = 0; index2 < iSizeLevelk && !bExist; index2++){
							assert(level_k[index2]->GetNoElems() == k-1);
							int* elems_index2 = level_k[index2]->GetElems();
							bExist = AreVectorsEqual(elems_temp, k-1, elems_index2, k-1);
							if (bExist)
								posExist = index2;
						}
						delete []elems_temp;
						elems_temp = NULL;

						assert(posExist != -1);
						UnsetBit(vBitLevelk, posExist, iNoCellLevelk, iSizeLevelk);

					}
					*/
					
					candidates_next_level.push_back(cand_cc);
				}

				v.clear();

			}//end for j


		}//end for i

		//in bits_level, where I see an entry 1, I have a suspect that might be maximal
		vector<int> suspect_pos = GetOneBits(vBitLevelk, iNoCellLevelk, iSizeLevelk);

		int iNoSuspects = (int)suspect_pos.size();
		if (k > 2){//i.e., ignore cluster cores on level 1
			for(i = 0; i < iNoSuspects; i++){
				
				//Gabi - 13 Oct 2007
				int bMaximal = SatisfiesMaximality(level_k[suspect_pos[i]], obj_CR);
				if (bMaximal){
					ClusterCore* CopyCC = new ClusterCore(*level_k[suspect_pos[i]]); 
					this->vCC.push_back(CopyCC);
					
				}
			}   
		}

		delete []vBitLevelk;
		vBitLevelk = NULL;

		suspect_pos.clear();

		for(i=0; i< iSizeLevelk; i++){
			delete level_k[i];
			level_k[i] = NULL;
		}
		level_k.clear();
		
		iSizeLevelk = (int)candidates_next_level.size();
		for(i=0; i<iSizeLevelk; i++){
			ClusterCore* CopyCC = new ClusterCore(*candidates_next_level[i]);
			level_k.push_back(CopyCC);
		}
		
		for(i=0; i<iSizeLevelk; i++){
			delete candidates_next_level[i];
			candidates_next_level[i] = NULL;
		};
		candidates_next_level.clear();

		time(&timeEnd);

		cout << "Level " << k << "- " << level_k.size() << " combinations - Elapsed time: " << timeEnd-timeBegin << endl;
		//if (k==9)
		//	this->DisplayLevel(cout,level_k);
			
	}//end while

	return SUCCESS;
}

/************ 
Checks  if 'cand_cc' is maximal; if I've found one possible extension, then it is not maximal
************/
int Round::SatisfiesMaximality(ClusterCore* cand_cc, ClusteringRounds *obj_CR){

    int bMaximal = 1;

    int i,j;
    
	int iNoIntervals = (int)this->vIntervals.size();
    Histogram** vAllHistograms = obj_CR->obj_NumericalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	
	int no_elems_cc = cand_cc->GetNoElems();
    int* elems_cc = cand_cc->GetElems();

    for(j=0;j<iNoIntervals && bMaximal;j++){
        //determine whether j is one of the intervals in cand_cc
        int bExist = ExistElem(j, elems_cc, no_elems_cc);
        if (bExist)
            continue;

        //determine whether the dimension of j is one of the dimensions of the elements in cand_cc
        int bSameDim = 0;
        int dim_j = this->vIntervals[j]->GetDimID();
        for(i=0;i<no_elems_cc && !bSameDim;i++){
            int dim_i = this->vIntervals[elems_cc[i]]->GetDimID();
            if (dim_j == dim_i)
                bSameDim = 1;
        }

        if (bSameDim)
            continue;

        //verify the condition for extensability
        vector<int> temp;
        for(i=0;i<no_elems_cc;i++)
            temp.push_back(elems_cc[i]);
        temp.push_back(j);

        ClusterCore *child = new ClusterCore(temp);
		int iChildAS = this->GetActualSupportIntersectionIntervals(temp, obj_CR); 
		child->SetActualSupport(iChildAS);
        
		vector<int> vIDBins = this->vIntervals[j]->GetIDBins();
		int iNoBins = (int)vIDBins.size();

		Bin* obj_FirstBin = vAllHistograms[dim_j]->GetBin(vIDBins[0]);
		assert(obj_FirstBin != NULL);
		double dRangeMin = static_cast<NumericalBin*>(obj_FirstBin)->GetStartPoint();

		Bin* obj_LastBin = vAllHistograms[dim_j]->GetBin(vIDBins[iNoBins-1]);
		assert(obj_LastBin != NULL);
		double dRangeMax = static_cast<NumericalBin*>(obj_LastBin)->GetEndPoint();

		double dWidthInterval = dRangeMax - dRangeMin;

		//this is the older version based on the Poisson distribution
		//double dDependentExpectedSupport = cand_cc->GetActualSupport() * dWidthInterval;
		//if ((child->GetActualSupport() > dDependentExpectedSupport) && (Poisson(child->GetActualSupport(),dDependentExpectedSupport) < obj_CR->dPoissonThreshold)){
        //    bMaximal = 0;
        //}

		//March 2008: the newer version based on the Binomial distribution
		int iCriticalValueRight = GetCriticalValueBinomialRight(cand_cc->GetActualSupport(), dWidthInterval, obj_CR->dPoissonThreshold);
		if (child->GetActualSupport() >= iCriticalValueRight){
			bMaximal = 0;
		}

        delete child;
		child = NULL;
        temp.clear();

    
    }//end for j
    
    return bMaximal;

}//end function


/************ 
Computes the actual support of the intersection of the intervals in vector 'v'
************/
int Round::GetActualSupportIntersectionIntervals(vector<int> &v, ClusteringRounds *obj_CR){

	int iAS = 0;

	int i;
	int iNoIntervals = (int)v.size();
	if (iNoIntervals == 0)
		return iAS;

	Histogram** vAllHistograms = obj_CR->obj_NumericalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;

	unsigned int* vIntersection = new unsigned int[iNoCellsObjects];
	assert(vIntersection != NULL);
	for(i=0;i<iNoCellsObjects;i++)
		vIntersection[i] = ~0;

	for(i=0; i<iNoIntervals;i++){ //for each interval in 'v'
		
		int iIntervalID = v[i];

		unsigned int* vMembership = new unsigned int[iNoCellsObjects];
		assert(vMembership != NULL);
		for(int i=0;i<iNoCellsObjects;i++)
			vMembership[i] = 0;

		this->GetMembershipInterval(vMembership, iIntervalID, obj_CR);

		IntersectionArrayBits(vIntersection, vIntersection, vMembership, iNoCellsObjects);

		delete []vMembership;
		vMembership = NULL;

	}
	
	iAS = GetNumberOneBits(vIntersection, iNoCellsObjects, iNoTotalObjects);

	delete []vIntersection;
	vIntersection = NULL;

	return iAS;
}

/************ 
Computes the membership matrix and the union of all revelant dimensions for the found cluster cores
************/
void Round::ComputeMembershipMatrix(ClusteringRounds *obj_CR){

	int iNoTotalObjects = obj_CR->DataMatrix->GetNoRows();
	int iNoCellsObjects = iNoTotalObjects / (8*sizeof(unsigned int)) + 1;

	int iNoCC = (int)this->vCC.size();

	//Histogram** vAllHistograms = obj_CR->obj_NumericalOneDHistograms->GetAllHistograms();
	//assert(vAllHistograms != NULL);
	//double* tmins = obj_CR->obj_NumericalOneDHistograms->GetMinOnAttributes();
	//assert(tmins != NULL);
	//double* tmaxs = obj_CR->obj_NumericalOneDHistograms->GetMaxOnAttributes();
	//assert(tmaxs != NULL);
	
	int i,j,index;

	this->MembershipMatrix = new MatrixGeneric<unsigned int>(iNoTotalObjects, iNoCC);
	assert(this->MembershipMatrix != NULL);

	//compute membership of data objects to cluster cores signatures
	//an object belongs to a cluster core if it fully satisfies its signature
	
	for(j=0; j<iNoCC; j++){//for each final cluster core
		
		//find out the objects that belong to cluster core j
		unsigned int* vObjectsCC = new unsigned int[iNoCellsObjects];
		assert(vObjectsCC != NULL);
		for(i=0;i<iNoCellsObjects;i++)
			vObjectsCC[i] = ~0;

		ClusterCore* current = this->vCC[j];
		int current_no_elems = current->GetNoElems();
		int* current_elems = current->GetElems();	
		
		//a cluster core is a cross-product of intervals
		//vObjectsCC is the intersection of its intervals
		for(index=0; index<current_no_elems; index++){
				
			unsigned int* vMembership = new unsigned int[iNoCellsObjects];
			assert(vMembership != NULL);
			for(int i=0;i<iNoCellsObjects;i++)
				vMembership[i] = 0;

			this->GetMembershipInterval(vMembership, current_elems[index], obj_CR);
			IntersectionArrayBits(vObjectsCC, vObjectsCC, vMembership, iNoCellsObjects);

			delete []vMembership;
			vMembership = NULL;
	
		}//end for index

		vector<int> vTemp = GetOneBits(vObjectsCC, iNoCellsObjects, iNoTotalObjects);
		int iLenghtVector = (int)vTemp.size();
		for(index=0; index<iLenghtVector;index++)
			this->MembershipMatrix->SetElemAt(vTemp[index], j, 1);
		vTemp.clear();

		delete []vObjectsCC;
		vObjectsCC = NULL;

	}
	
	/* this code is slow
	for(i=0; i<iNoTotalObjects; i++){ //for each data object
		for(j=0; j<iNoCC; j++){//for each final cluster core
			
			ClusterCore* current = this->vCC[j];
			int current_no_elems = current->GetNoElems();
			int* current_elems = current->GetElems();

			//a cluster core is a cross-product of intervals
			int iNoIntervalsIncluded = 0;
			for(index=0; index<current_no_elems; index++){
				
				Interval *obj_Interval = this->vIntervals[current_elems[index]];
				assert(obj_Interval != NULL);
				
				int iDimID = obj_Interval->GetDimID(); //the dimension of the interval
				vector<int> vIDBins = obj_Interval->GetIDBins();
				int iNoBins = (int)vIDBins.size();

				Bin* obj_FirstBin = vAllHistograms[iDimID]->GetBin(vIDBins[0]);
				assert(obj_FirstBin != NULL);
				double dRangeMin = static_cast<NumericalBin*>(obj_FirstBin)->GetStartPoint();

				Bin* obj_LastBin = vAllHistograms[iDimID]->GetBin(vIDBins[iNoBins-1]);
				assert(obj_LastBin != NULL);
				double dRangeMax = static_cast<NumericalBin*>(obj_LastBin)->GetEndPoint();

				if (strcmp(obj_CR->DataMatrix->GetElemAt(i,iDimID),"?") != 0){ //missing values are excluded
					double dValue = atof(obj_CR->DataMatrix->GetElemAt(i,iDimID));
					double dNormalizedValue;
					if (tmaxs[iDimID] != tmins[iDimID])
						dNormalizedValue = (dValue - tmins[iDimID]) / (tmaxs[iDimID] - tmins[iDimID]);
					else
						dNormalizedValue = 0;
					assert((dNormalizedValue >= 0) && (dNormalizedValue <= 1));

					if ((dNormalizedValue >= dRangeMin) && (dNormalizedValue <= dRangeMax))
						iNoIntervalsIncluded++;
				}
			}

			if (iNoIntervalsIncluded == current_no_elems)
				this->MembershipMatrix->SetElemAt(i,j,1);

		} //end for j    
	}//end for i
	*/


	//at the same time, compute the union of relevant dimensions corresponding to final CC
	this->vAllRelevantDimensions.reserve(iNoCC);
	for(j=0; j<iNoCC; j++){//for each cluster core
		
		ClusterCore* current = this->vCC[j];
		int current_no_elems = current->GetNoElems();
		int* current_elems = current->GetElems();

		//a cluster core is a cross-product of intervals
		vector<int> vTemp;
		vTemp.reserve(current_no_elems);
		for(index=0; index<current_no_elems; index++){
			
			Interval *obj_Interval = this->vIntervals[current_elems[index]];
			assert(obj_Interval != NULL);
			
			int iDimID = obj_Interval->GetDimID(); //the dimension of the interval
			
			vTemp.push_back(iDimID);
		}
		
		this->vAllRelevantDimensions.push_back(vTemp);
	}


}

/************ 
Display function
************/
void Round::DisplayIntervals(ostream &os, ClusteringRounds* obj_CR){
	
	Histogram** vAllHistograms = obj_CR->obj_NumericalOneDHistograms->GetAllHistograms();
	assert(vAllHistograms != NULL);
	
	int iLengthVector = (int)this->vIntervals.size();
	for(int i=0;i<iLengthVector;i++){
		
		Interval* obj_Interval = this->vIntervals[i];
		assert(obj_Interval != NULL);
		
		int iDimID = obj_Interval->GetDimID();
		vector<int> vIDBins = obj_Interval->GetIDBins();
		int iNoBins = (int)vIDBins.size();
		
		Bin* obj_FirstBin = vAllHistograms[iDimID]->GetBin(vIDBins[0]);
		assert(obj_FirstBin != NULL);
		double dRangeMin = static_cast<NumericalBin*>(obj_FirstBin)->GetStartPoint();
		Bin* obj_LastBin = vAllHistograms[iDimID]->GetBin(vIDBins[iNoBins-1]);
		assert(obj_LastBin != NULL);
		double dRangeMax = static_cast<NumericalBin*>(obj_LastBin)->GetEndPoint();
		
		os << "Attribute " << iDimID + 1 << ":" << endl;
		os << "[" << dRangeMin << "," << dRangeMax << "]" << endl;
		os << "Support = " << this->GetActualSupportInterval(i, obj_CR) << endl;
		os << endl;
	}
}

/************ 
Display function
************/
void Round::DisplayClusterCores(ostream &os, ClusteringRounds *obj_CR){

	os << "CLUSTER CORES:" << endl;
	int i, index;
	int iNoCC = (int)this->vCC.size();
	for(i=0; i<iNoCC; i++){
        
        ClusterCore *current = this->vCC[i];
        int current_no_elems = current->GetNoElems();
        int* current_elems = current->GetElems();

        os << "(";
        /*
		for(index=0; index<current_no_elems; index++){
            if (index != current_no_elems - 1)
				os << current_elems[index] + 1 << " ";
			else
				os << current_elems[index] + 1;
        }
        os << ") - (";
		*/
        for(index=0;index<current_no_elems;index++){
            if (index != current_no_elems - 1)
				os << this->vIntervals[current_elems[index]]->GetDimID() + 1 << " ";
			else
				os << this->vIntervals[current_elems[index]]->GetDimID() + 1;
        }
        os << ")" << endl;
    }

}

/************ 
Display a level in the generation of cluster cores
************/
void Round::DisplayLevel(ostream& os, vector<ClusterCore*> &v){

	int iLengthVector = (int)v.size();
	for(int i=0; i<iLengthVector;i++){
		v[i]->Display(os);
		os << endl;
	}
}

