#include "ClusteringRounds.h"

/**
*	This program is an implementation of the P3C algorithm
*	for discovering projected clusters in numerical data
*
*	author Gabriela Moise
*   version 1.0 
*   
*   version 2.0
*   Poisson distribution replaced by Binomial distribution
*   Poisson threshold signifies the area under the curve 
**/

void usage(char* sNameProgram);

/************ 
MAIN function
************/
int main(int argc, char* argv[]){
   
    if (argc != 4){
        usage(argv[0]);
        return FAILURE;
    }

	//double res1 = Binomial(941,2393,0.3571428);
	//double res2 = Binomial2(941,2393,0.3571428);
	//return SUCCESS;

    ClusteringRounds *obj_CR = new ClusteringRounds(argv[1], argv[2], atof(argv[3]));


    if (obj_CR->Run() == FAILURE){
      delete obj_CR;
	  return FAILURE;
    }

    delete obj_CR;

    return SUCCESS;
    
}

/************ 
Usage function
************/
void usage(char* sNameProgram){

    cerr << "Syntax for this program is: " << endl;
    cerr << "\tname_program name_input_file name_output_file Poisson_threshold" << endl;
    cerr << "where:" << endl;
    cerr << "\tname_program is " << sNameProgram << endl;
    cerr << "\tname_input_file is the name of the file containing the raw data" << endl;;
    cerr << "\tname_output_file is the name of the file containing the clustered data" << endl;
	cerr << "\tPoisson_threshold is the value of the parameter Poisson_threshold" << endl;
}


