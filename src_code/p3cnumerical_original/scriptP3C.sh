#!/bin/bash

## compute cluster cores
./P3CNumerical SampleData.txt SampleData_PartialInitialization.txt 1.0E-5

## refinement

## hard partitioning
R --vanilla --slave --args "SampleData_PartialInitialization.txt" "SampleData_RelevantDimensions.txt" "SampleData.txt" "SampleData_P3C_hard.txt" 1 < Refinement.R

## soft partitioning 
#R --vanilla --slave --args "SampleData_PartialInitialization.txt" "SampleData_RelevantDimensions.txt" "SampleData.txt" "SampleData_P3C_soft.txt" 2 < Refinement.R


