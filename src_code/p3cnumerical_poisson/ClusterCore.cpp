#include "ClusterCore.h"

/************ 
Constructor(s) and destructor
************/
//default onstructor
ClusterCore::ClusterCore(){
	
	this->iNoElems = 0;
	this->vElems = NULL;
	this->iActualSupport = 0;
}
//constructor
ClusterCore::ClusterCore(vector<int> &v){
    
	this->iNoElems = (int)v.size();
    
	this->vElems = new int[this->iNoElems];
	assert(this->vElems != NULL);
    for(int i=0;i<this->iNoElems;i++)
		this->vElems[i] = v[i];
    
	this->iActualSupport = 0;
}

//constructor
ClusterCore::ClusterCore(int* v, int size_v){

	this->iNoElems = size_v;
    
	this->vElems = new int[size_v];
	assert(this->vElems != NULL);
    for(int i=0;i<this->iNoElems;i++)
		this->vElems[i] = v[i];
    
	this->iActualSupport = 0;	
}

//copy constructor
ClusterCore::ClusterCore(const ClusterCore &C){

	//if (this->vElems != NULL){
	//	delete []this->vElems;
	//	this->vElems = NULL;
	//}

	this->iNoElems = C.iNoElems;
	this->vElems = new int[this->iNoElems];
	assert(this->vElems != NULL);
	for(int i=0;i<this->iNoElems;i++)
		this->vElems[i] = C.vElems[i];
    
	this->iActualSupport = C.iActualSupport;
}

ClusterCore::~ClusterCore(){
	if (this->vElems != NULL)
        delete []this->vElems;
	this->vElems = NULL;
}

/************ 
Functions for manipulating private members of the class
************/
int ClusterCore::GetNoElems() const{
    
	return this->iNoElems;
}

int* ClusterCore::GetElems(){
    
	return this->vElems;
}

int ClusterCore::GetActualSupport() const{
    
	return this->iActualSupport;
}

void ClusterCore::SetActualSupport(int val){
    
	this->iActualSupport = val;
}

/************ 
Display function
************/
void ClusterCore::Display(ostream &os){
	
	if (this->iNoElems != 0){
		os << "(";
		for(int i=0; i<this->iNoElems; i++){
			if (i < this->iNoElems - 1)	
				os << this->vElems[i] + 1 << " ";
			else
				os << this->vElems[i] + 1;		
		}
		os << ")";
	}
}

