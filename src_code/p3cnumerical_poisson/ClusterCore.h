#ifndef CLUSTERCORE_H
#define CLUSTERCORE_H

#include <vector>
#include <ostream>
using namespace std;

#include <assert.h>

class ClusterCore{

    int* vElems;
    int iNoElems;
    int iActualSupport;

public:
    ClusterCore();
	ClusterCore(vector<int> &v);
	ClusterCore(int* v, int size_v);
	ClusterCore(const ClusterCore &C);
    ~ClusterCore();

    int GetNoElems() const;
    int* GetElems();
    int GetActualSupport() const;
    void SetActualSupport(int val);

	//ClusterCore& ClusterCore::operator =(const ClusterCore &C);

	void Display(ostream& os);
    
};

#endif


