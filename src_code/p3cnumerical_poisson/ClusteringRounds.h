#ifndef CLUSTERINGROUNDS_H
#define CLUSTERINGROUNDS_H

#include "Utile.h"
#include "StringMatrix.h"
#include "NumericalOneDHistograms.h"
#include "Round.h"
#include "MatrixGeneric.h"

#include <iostream>
#include <fstream>
using namespace std;

#include <time.h>

class ClusteringRounds {
       
    char* sInputFile;
    char* sOutputFile;
	double dPoissonThreshold;
    
	StringMatrix *DataMatrix;
	MatrixGeneric<double>* CriticalValues;

	int bClusteringRounds;
	int iNoIter;

	NumericalOneDHistograms* obj_NumericalOneDHistograms;

	int ReadDataMatrix();
	int WriteOutput(MatrixGeneric<unsigned int>* M, vector<vector<int> > &R);
	
	int ReadCriticalValuesMatrix();

	void BuildAllHistograms();

public:
    ClusteringRounds();
	ClusteringRounds(char* sInputFile, char* sOutputFile, double dPoissonThreshold);
    ~ClusteringRounds();

    int Run();

	friend class Round;
};

#endif




