#ifndef INTERVAL_H
#define INTERVAL_H

#include <vector>
#include <fstream>
using namespace std;

#include "Utile.h"

class Interval{

	int iDimID;
	vector<int> vIDBins;

public:
	Interval();
	Interval(int dim_id);
	~Interval();

	int GetDimID() const;
	vector<int> GetIDBins() const;	

	void AddBin(int iIDBin);
		
};

#endif
