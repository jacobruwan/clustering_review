#include "NumericalBin.h"

/************ 
Constructor(s) and destructor
************/
//default constructor
NumericalBin::NumericalBin(){
	this->dStartPoint = -1;
	this->dEndPoint = -1;	
}

//constructor
NumericalBin::NumericalBin(int no_total_objects, int dim_id, double start_point, double end_point)
:Bin(no_total_objects, dim_id)
{
	this->dStartPoint = start_point;
	this->dEndPoint = end_point;
}

//destructor
NumericalBin::~NumericalBin(){
	
	if (this->vObjects != NULL){
		delete []this->vObjects;
		this->vObjects = NULL;
	}
}



/************
Functions for manipulating private members of the class
************/
double NumericalBin::GetStartPoint() const{
	
	return this->dStartPoint;
}

double NumericalBin::GetEndPoint() const{
	
	return this->dEndPoint;
}

/************
Displays a numerical bin for debugging purposes
************/
void NumericalBin::Display(ostream &os){
	
	int iSupport = this->GetActualSupport();
	os << "Support = " << iSupport << endl;
	os << "[" << this->dStartPoint << "," << this->dEndPoint << "]" << endl;
	if (this->bDense == 1)
		os << "Dense" << endl;
	os << endl;
	
	//vector<int> vObjectsIDs = GetOneBits(this->vObjects, this->iNoCellsObjects, this->iNoTotalObjects);
	//for(int i=0;i<iSupport;i++)
	//	os << vObjectsIDs[i] << " ";
	//os << endl;
}



