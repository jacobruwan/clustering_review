#include "NumericalOneDHistograms.h"

#include <math.h>

/************
Constructor(s) and destructor
************/
//default constructor
NumericalOneDHistograms::NumericalOneDHistograms(){
	
	this->tmins = NULL;
	this->tmaxs = NULL;
}

//constructor
NumericalOneDHistograms::NumericalOneDHistograms(StringMatrix *SM)
:OneDHistograms(SM)
{
	this->tmins = NULL;
	this->tmaxs = NULL;
}

//destructor
NumericalOneDHistograms::~NumericalOneDHistograms(){
	
	delete []this->tmins;
	this->tmins = NULL;
	delete []this->tmaxs;
	this->tmaxs = NULL;

	if (this->vAllHistograms != NULL){
		for(int i=0;i<this->iNoHistograms;i++){
			delete this->vAllHistograms[i];
			this->vAllHistograms[i] = NULL;
		}
		delete []this->vAllHistograms;
		this->vAllHistograms = NULL;
	}
}

/************ 
Functions for manipulating private members of the class
************/
double* NumericalOneDHistograms::GetMinOnAttributes(){

	return this->tmins;
}

double* NumericalOneDHistograms::GetMaxOnAttributes(){

	return this->tmaxs;
}

/************
Determine min and max on each column
************/
void NumericalOneDHistograms::NormalizeData(){
	
	int iNoTotalObjects = this->DataMatrix->GetNoRows();
	int iNoTotalAttributes = this->DataMatrix->GetNoCols() - 1;
	
	int i, j;

	//initialize tmins 
	tmins = new double[iNoTotalAttributes];
	assert(tmins != NULL);
    for(j=0;j<iNoTotalAttributes;j++)
        this->tmins[j] = MAX_ATTR;

	//initialize tmaxs
    tmaxs = new double[iNoTotalAttributes];
	assert(tmaxs != NULL);
    for(j=0;j<iNoTotalAttributes;j++)
        this->tmaxs[j] = MIN_ATTR;

	//determine min, max on each column
	for(i=0;i<iNoTotalObjects;i++){ //for each data object
		for(j=0;j<iNoTotalAttributes;j++){//for each attribute
			if (strcmp(this->DataMatrix->GetElemAt(i,j),"?") != 0){
				double elem = atof(this->DataMatrix->GetElemAt(i,j));
				if (elem < tmins[j])
					this->tmins[j] = elem;
				if (elem > tmaxs[j])
					this->tmaxs[j] = elem;
			}
			
		}
    }

	
}

/************
Builds a histogram for each attribute and stores it into vAllHistograms
************/
void NumericalOneDHistograms::BuildAllHistograms(){

	this->NormalizeData();

	int iNoTotalObjects = this->DataMatrix->GetNoRows();
	int iNoTotalAttributes = this->DataMatrix->GetNoCols() - 1;

	//In this algorithm, the number of bins on each attribute is the same
	//and it is computed as a function of the iNoTotalObjects
	int iNoBins = (int)floor(1+log((double)iNoTotalObjects) / log(2.0));
	double dBinWidth = 1.0/(double)iNoBins;

	int i,j;

	//initialize the histograms
	this->vAllHistograms = new Histogram*[iNoTotalAttributes];
	assert(this->vAllHistograms != NULL);
	
	for(j=0;j<iNoTotalAttributes;j++){
		
		this->vAllHistograms[j] = new Histogram(iNoBins, j);
		assert(this->vAllHistograms[j] != NULL);
		
		for(i=0;i<iNoBins;i++){
			
			//assumes that data is normalized between 0 and 1
			double dStartPoint = i * dBinWidth;
			double dEndPoint = (i+1) * dBinWidth;
			NumericalBin* obj_Bin = new NumericalBin(iNoTotalObjects, j, dStartPoint, dEndPoint);
			assert(obj_Bin != NULL);

			this->vAllHistograms[j]->AddBin(obj_Bin, i);
		}
	}

	//populate the histograms
	for(i=0;i<iNoTotalObjects;i++){
		for(j=0;j<iNoTotalAttributes;j++){

			if (strcmp(this->DataMatrix->GetElemAt(i,j),"?") != 0){ // only if the value is not missing
				double dValue = atof(this->DataMatrix->GetElemAt(i,j));
				double dNormalizedValue;
				if (this->tmaxs[j] != this->tmins[j])
					dNormalizedValue = (dValue - this->tmins[j]) / (this->tmaxs[j] - this->tmins[j]);
				else
					dNormalizedValue = 0;
				assert((dNormalizedValue>=0) && (dNormalizedValue<=1));

				int iBinID = (int)floor(dNormalizedValue / dBinWidth );
				if (iBinID == iNoBins)
					iBinID = iNoBins - 1;

				Bin* obj_Bin = this->vAllHistograms[j]->GetBin(iBinID);
				assert(obj_Bin != NULL);
				obj_Bin->AddObject(i);
			}

		}
	}

}

/************
Displays all histograms for debugging purposes
************/
void NumericalOneDHistograms::Display(ostream& os){

	int iNoTotalAttributes = this->DataMatrix->GetNoCols() - 1;

	for(int j=0;j<iNoTotalAttributes;j++){
		this->vAllHistograms[j]->Display(os);
	}
}

