P3C for Numerical Data
----------------------

P3C - Input Arguments
----------------------
1. file_name:
	The name of the file that stores the data. 
	Each line in the input file has the following format:
	value \space value \space ... \space value \space label \n
	The last column contains labels. If no labels are known, give each object the same label (e.g., 1).
	If there are known outliers, they receive the largest label. 
	See "SampleData.txt" for an example.
	"SampleData.txt" is a synthetic data set with 1000 objects and 100 attributes. There are 5 clusters, 
	and 50 outliers. Attribute 101 contains the labels. Cluster points are labeled from 1 to 5, and the outliers
	receive label 6.
2. output_file_name:
	The name of the output file must be the name of the input file concatenated with "_PartialInitialization.txt".
	For example, if the input file is "SampleData.txt", the output file is "SampleData_PartialInitialization.txt"
3. Poisson threshold:
	The value for the Poisson threshold (e.g., 1.0E-5)

P3C - Output
------------
1. P3C computes several cluster cores.
	For instance, (1,2,3) signifies that this cluster core consists of 3 intervals, 
	one interval on each of the attributes 1, 2 and 3.
2. SampleData_PartialInitialization.txt
	This file stores a data matrix that describes the membership of cluster points to the computed cluster cores.
	This data matrix will have as many rows as data points, and as many columns as cluster cores.
	The entries are all either 0 and 1. Entry (i,l) equal to 1 signifies that data point i is in the support set 
	of cluster core l.
3. SampleData_RelevantDimensions.txt
	This file stores the relevant attributes for each cluster core.

P3C - Refinement of Cluster Cores
---------------------------------
1. It is performed using R (R Project, http://www.r-project.org/). Library "mclust" must be installed.
2. The R code is in Refinement.R
3. Input to the refinement:
	File with membership matrix of cluster points to cluster cores (e.g., SampleData_PartialInitialization.txt)
	File with relevant attributes for cluster cores (e.g., SampleData_RelevantDimensions.txt)
	Original data file (e.g., SampleData.txt)
	Parameter HS: HS = 1 means disjoint clusters (and outliers)
			  HS = 2 means overlapping clusters (and outliers)
4. Output to the refinement:
	File with the clustering result (e.g., SampleData_P3C.txt)
	Format of the output file:
	ClusterResult1 1 \space 0 \space .. \space 1 \n
	...
	ClusterResult5 0 \space 1 \space .. \space 0 \n
	LABELING
	4,5
	-1
	2
	...
	The first part of the result file contains the relevant attributes for each found cluster.
	The "LABELING" part of the result file contains the label computed for each data point.
	Outliers are labeled with -1. 
	If overlapping clusters were computed, a data point may belong to more than one cluster.			


How to run P3C
--------------
1. make clean
2. make
	This will produce the executable P3CNumerical.
3. scriptP3C.sh	
	
	

