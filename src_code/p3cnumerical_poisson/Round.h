#ifndef ROUND_H
#define ROUND_H

#include "ClusteringRounds.h"
#include "Interval.h"
#include "ClusterCore.h"
#include "MatrixGeneric.h"
#include "Utile.h"

class ClusteringRounds;

class Round{
	
	vector<Interval*> vIntervals; 

	vector<ClusterCore*> vCC;

	MatrixGeneric<unsigned int> *MembershipMatrix;
	vector<vector<int> > vAllRelevantDimensions;

	void MarkDenseBins(ClusteringRounds* obj_CR);

	void ComputeIntervals(ClusteringRounds* obj_CR);
	void GetMembershipInterval(unsigned int* vMembership, int iIntervalID, ClusteringRounds* obj_CR);
	int GetActualSupportInterval(int iIntervalID, ClusteringRounds* obj_CR);
	void DisplayIntervals(ostream &os, ClusteringRounds* obj_CR);

	int GenerateClusterCores(ClusteringRounds* obj_CR);
	int GetActualSupportIntersectionIntervals(vector<int> &v, ClusteringRounds* obj_CR);
	int SatisfiesMaximality(ClusterCore* cand_cc, ClusteringRounds *obj_CR);
	void DisplayClusterCores(ostream &os, ClusteringRounds* obj_CR);

	void ComputeMembershipMatrix(ClusteringRounds* obj_CR);

	void DisplayLevel(ostream& os, vector<ClusterCore*> &v);

public:
	Round();
	~Round();

	MatrixGeneric<unsigned int> * GetMembershipMatrix();
	vector<vector<int> >& GetAllRelevantDimensions();
	
	int DoRound(ClusteringRounds* obj_CR); //class ClusteringRounds is a friend of class Round;
										   //thus, class Round has access to private/protected members 
										   //of Clustering Rounds
	

};

#endif

